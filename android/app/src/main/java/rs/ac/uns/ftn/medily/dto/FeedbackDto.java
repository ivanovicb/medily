package rs.ac.uns.ftn.medily.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FeedbackDto {

    @SerializedName("username")
    @Expose
    private String username;

    @SerializedName("feedback")
    @Expose
    private String feedback;

    public FeedbackDto() {
    }

    public FeedbackDto(String username, String feedback) {
        this.username = username;
        this.feedback = feedback;
    }
}
