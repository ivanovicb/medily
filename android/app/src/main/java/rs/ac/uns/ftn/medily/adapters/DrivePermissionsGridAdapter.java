package rs.ac.uns.ftn.medily.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import org.apache.commons.io.FilenameUtils;

import java.util.List;

import rs.ac.uns.ftn.medily.R;
import rs.ac.uns.ftn.medily.model.File;
import rs.ac.uns.ftn.medily.model.FilePermission;
import rs.ac.uns.ftn.medily.model.Folder;
import rs.ac.uns.ftn.medily.model.FolderPermission;

public class DrivePermissionsGridAdapter extends BaseAdapter {

    private final Context mContext;
    private List<FolderPermission> folders;
    private List<FilePermission> files;
    private String[] photoFileExtensions;
    private String[] audioFileExtensions;
    private String[] videoFileExtensions;

    public DrivePermissionsGridAdapter(Context context, List<FolderPermission> folders, List<FilePermission> files) {
        this.mContext = context;
        this.files = files;
        this.folders = folders;

        photoFileExtensions = mContext.getResources().getStringArray(R.array.photo_file_extensions);
        audioFileExtensions = mContext.getResources().getStringArray(R.array.audio_file_extensions);
        videoFileExtensions = mContext.getResources().getStringArray(R.array.video_file_extensions);
    }

    @Override
    public int getCount() {
        return folders.size() + files.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater layoutInflater = LayoutInflater.from(mContext);
            convertView = layoutInflater.inflate(R.layout.drive_item, null);
        }
        convertView.setPadding(3, 3, 3, 3);
        TextView nameTextView = convertView.findViewById(R.id.text_view_drive_item);
        ImageView imageView = convertView.findViewById(R.id.image_view_drive_item);

        if(position < folders.size()){
            FolderPermission folder = folders.get(position);

            if (folder.isAllow()) {
                convertView.setBackground(ContextCompat.getDrawable(mContext, R.drawable.border_allowed));
                convertView.setTag(R.drawable.border_allowed);
            } else {
                convertView.setBackground(ContextCompat.getDrawable(mContext, R.drawable.border_forbidden));
                convertView.setTag(R.drawable.border_forbidden);
            }

            imageView.setImageResource(R.drawable.ic_folder);
            nameTextView.setText(folder.getName());
        }
        else {
            FilePermission file = files.get(position-folders.size());

            if (file.isAllow()) {
                convertView.setBackground(ContextCompat.getDrawable(mContext, R.drawable.border_allowed));
                convertView.setTag(R.drawable.border_allowed);
            } else {
                convertView.setBackground(ContextCompat.getDrawable(mContext, R.drawable.border_forbidden));
                convertView.setTag(R.drawable.border_forbidden);
            }

            String extension = FilenameUtils.getExtension(file.getName());

            if(isPhoto(extension)){
                imageView.setImageResource(R.drawable.ic_photo);
            } else if (isAudio(extension)) {
                imageView.setImageResource(R.drawable.ic_audio);
            } else if (isVideo(extension)) {
                imageView.setImageResource(R.drawable.ic_video);
            } else {
                imageView.setImageResource(R.drawable.ic_file);
            }

            nameTextView.setText(file.getName());
        }

        return convertView;
    }

    private boolean isPhoto(String extension) {
        for(String photoExtension: photoFileExtensions){
            if(photoExtension.toLowerCase().equals(extension.toLowerCase())){
                return true;
            }
        }
        return false;
    }

    private boolean isAudio(String extension) {
        for(String audioExtension: audioFileExtensions){
            if(audioExtension.toLowerCase().equals(extension.toLowerCase())){
                return true;
            }
        }
        return false;
    }

    private boolean isVideo(String extension) {
        for(String videoExtension: videoFileExtensions){
            if(videoExtension.toLowerCase().equals(extension.toLowerCase())){
                return true;
            }
        }
        return false;
    }
}
