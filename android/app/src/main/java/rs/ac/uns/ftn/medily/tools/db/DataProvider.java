package rs.ac.uns.ftn.medily.tools.db;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class DataProvider extends ContentProvider {
    private DatabaseHelper database;
    private static final String AUTHORITY = "rs.ac.uns.ftn.medily";
    private static final String SONG_PATH = "song";
    private static final String PLAYLIST_PATH = "playlist";
    private static final int SONG = 1;
    private static final int SONG_ID = 2;
    private static final int PLAYLIST = 3;
    private static final int PLAYLIST_ID = 4;
    private static final int PLAYLIST_NAME = 5;
    private static final UriMatcher sURIMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        sURIMatcher.addURI(AUTHORITY, SONG_PATH, SONG);
        sURIMatcher.addURI(AUTHORITY, SONG_PATH + "/#", SONG_ID);

        sURIMatcher.addURI(AUTHORITY, PLAYLIST_PATH, PLAYLIST);
        sURIMatcher.addURI(AUTHORITY, PLAYLIST_PATH + "/#", PLAYLIST_ID);
        sURIMatcher.addURI(AUTHORITY, PLAYLIST_PATH + "/name=", PLAYLIST_NAME);
    }

    @Override
    public boolean onCreate() {
        database = DbContext.getDB(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection,
                        @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        int uriType = sURIMatcher.match(uri);
        switch (uriType) {
            case SONG:
                queryBuilder.setTables(SongRepository.TABLE_SONGS);
                break;
            case SONG_ID:
                queryBuilder.appendWhere(SongRepository.COLUMN_ID + "="
                        + uri.getLastPathSegment());
            case PLAYLIST:
                queryBuilder.setTables(PlaylistRepository.TABLE_PLAYLISTS);
                break;
            case PLAYLIST_ID:
                queryBuilder.appendWhere(PlaylistRepository.COLUMN_ID + "="
                        + uri.getLastPathSegment());
            case PLAYLIST_NAME:
                queryBuilder.appendWhere(PlaylistRepository.COLUMN_NAME + "="
                        + uri.getLastPathSegment());

            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }

        SQLiteDatabase db = database.getWritableDatabase();
        Cursor cursor = queryBuilder.query(db, projection, selection,
                selectionArgs, null, null, sortOrder);

        if (getContext() != null) {
            cursor.setNotificationUri(getContext().getContentResolver(), uri);
        }

        return cursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        return null;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }
}
