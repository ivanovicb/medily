package rs.ac.uns.ftn.medily.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ChangePermissionDto implements Serializable {

    @SerializedName("username")
    @Expose
    private String username;

    @SerializedName("id")
    @Expose
    private Long id;

    @SerializedName("type")
    @Expose
    private String type;

    @SerializedName("allow")
    @Expose
    private boolean allow;

    public ChangePermissionDto() {
    }

    public ChangePermissionDto(String username, Long id, String type, boolean allow) {
        this.username = username;
        this.id = id;
        this.type = type;
        this.allow = allow;
    }
}
