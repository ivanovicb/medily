package rs.ac.uns.ftn.medily.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


import lombok.SneakyThrows;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rs.ac.uns.ftn.medily.R;
import rs.ac.uns.ftn.medily.dto.LoginDto;
import rs.ac.uns.ftn.medily.dto.LoginResultDto;
import rs.ac.uns.ftn.medily.service.ServiceUtils;
import rs.ac.uns.ftn.medily.tools.SharedPreferencesService;
import rs.ac.uns.ftn.medily.tools.db.DatabaseHelper;
import rs.ac.uns.ftn.medily.tools.db.DbContext;
import rs.ac.uns.ftn.medily.tools.db.DriveRepository;

public class LoginActivity extends AppCompatActivity {

    private EditText username;
    private EditText password;
    private Button login;
    private Button forgot;
    private Button register;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        this.setTitle("Sign in to Medily");
        init();
    }

    private void init() {
        username = findViewById(R.id.username);
        password = findViewById(R.id.password);
        login = findViewById(R.id.login_button);
        forgot = findViewById(R.id.forgot_button);
        register = findViewById(R.id.reg_button);
        addListeners();
    }

    private void addListeners() {
        login.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                //LoginActivity.this.startActivity(intent);
                /*DatabaseHelper dbHelper = new DatabaseHelper(getApplicationContext());
                SQLiteDatabase db = dbHelper.getWritableDatabase();
                dbHelper.onUpgrade(db, 1, 1);*/
              if (validInput()) {
                    Call<LoginResultDto> call = ServiceUtils.userService.login(new LoginDto(username.getText().toString(),
                            password.getText().toString()));
                    call.enqueue(new Callback<LoginResultDto>() {
                        @SneakyThrows
                        @Override
                        public void onResponse(Call<LoginResultDto> call, Response<LoginResultDto> response) {
                            if (response.code() == 200) {
                                SharedPreferencesService.add("user_id", response.body().getId());
                                SharedPreferencesService.add("token", response.body().getToken());
                                SharedPreferencesService.add("token", response.body().getToken());
                                SharedPreferencesService.add("name", response.body().getName());
                                SharedPreferencesService.add("username", response.body().getUsername());
                                SharedPreferencesService.add("family", response.body().getFamily());
                                DriveRepository.checkIfDriveForUserExists(getApplicationContext());
                                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                LoginActivity.this.startActivity(intent);
                                finish();
                            } else if (response.code() == 400) {
                                Toast.makeText(LoginActivity.this, response.errorBody().string(),
                                        Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(LoginActivity.this, "Invalid login!",
                                        Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<LoginResultDto> call, Throwable t) {
                            Toast.makeText(LoginActivity.this, t.getMessage(),
                                    Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        });
        register.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, RegistrationActivity.class);
                LoginActivity.this.startActivity(intent);
            }
        });
        forgot.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, ForgotPasswordActivity.class);
                LoginActivity.this.startActivity(intent);
            }
        });
    }

    private boolean validInput() {
        if (username.getText().toString().length() < 1) {
            Toast.makeText(LoginActivity.this, "Username not defined",
                    Toast.LENGTH_SHORT).show();
            return false;
        } else if (password.getText().toString().length() < 1) {
            Toast.makeText(LoginActivity.this, "Password not defined",
                    Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }
}
