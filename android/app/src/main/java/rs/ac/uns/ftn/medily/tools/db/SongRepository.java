package rs.ac.uns.ftn.medily.tools.db;

import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

import org.apache.commons.io.FilenameUtils;

import java.util.ArrayList;
import java.util.List;

import rs.ac.uns.ftn.medily.R;
import rs.ac.uns.ftn.medily.dto.SongInPlaylistDto;
import rs.ac.uns.ftn.medily.model.File;
import rs.ac.uns.ftn.medily.model.Playlist;
import rs.ac.uns.ftn.medily.model.Song;
import rs.ac.uns.ftn.medily.tools.SharedPreferencesService;
import rs.ac.uns.ftn.medily.tools.Util;

public class SongRepository {

    public static final String TABLE_SONGS = "songs";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_GLOBAL_ID = "global_id";
    public static final String COLUMN_ARTIST = "artist";
    public static final String COLUMN_GENRE = "genre";
    public static final String COLUMN_DURATION = "duration";
    public static final String COLUMN_DOWNLOADED_BY = "downloaded_by";
    public static final String COLUMN_FILE = "file";

    public static final String DB_CREATE = "create table "
            + TABLE_SONGS + "("
            + COLUMN_ID + " integer primary key autoincrement, "
            + COLUMN_GLOBAL_ID + " integer, "
            + COLUMN_FILE + " integer not null, "
            + COLUMN_ARTIST + " text, "
            + COLUMN_GENRE + " text, "
            + COLUMN_DURATION + " integer, "
            + COLUMN_DOWNLOADED_BY + " text, "
            + "FOREIGN KEY (" + COLUMN_FILE + ") REFERENCES " + FileRepository.TABLE_FILES + " (_id)"
            + ")";

    public static Cursor query(String selection, String[] selectionArgs, Context context) {
        String[] projection = {COLUMN_ID, COLUMN_GLOBAL_ID, COLUMN_ARTIST, COLUMN_GENRE, COLUMN_DURATION, COLUMN_DOWNLOADED_BY, COLUMN_FILE};
        SQLiteDatabase db = DbContext.getDB(context).getWritableDatabase();
        return db.query(TABLE_SONGS, projection, selection,
                selectionArgs, null, null, null);
    }

    public static List<Song> findSongs(String selection, String[] selectionArgs, Context context) {
        List<Song> result = new ArrayList<>();
        Cursor cursor = query(selection, selectionArgs, context);
        while (cursor.moveToNext()) {
            List<File> files = FileRepository.findFiles(FileRepository.COLUMN_ID + " = ?",
                    new String[]{cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_FILE))}, context);
            Song song = new Song(cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_ARTIST)),
                    cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_GENRE)),
                    cursor.getInt(cursor.getColumnIndexOrThrow(COLUMN_DURATION)),
                    cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_DOWNLOADED_BY)),
                    files.size() == 1 ? files.get(0) : new File(),
                    cursor.getLong(cursor.getColumnIndexOrThrow(COLUMN_ID)),
                    cursor.getLong(cursor.getColumnIndexOrThrow(COLUMN_GLOBAL_ID)));
            result.add(song);
        }
        cursor.close();
        return result;
    }

    public static Song findByFileId(Long id, Context context) {
        String selection = COLUMN_FILE + " = ?";
        List<Song> songs = findSongs(selection, new String[] { id.toString() }, context);
        return songs.size() == 1 ? songs.get(0) : null;
    }

    public static Song add(Song song, Context context) {
        SQLiteDatabase db = DbContext.getDB(context).getWritableDatabase();
        ContentValues entry = new ContentValues();
        entry.put(COLUMN_FILE, song.getFile().getId().toString());
        entry.put(COLUMN_GLOBAL_ID, song.getGlobalId());
        entry.put(COLUMN_ARTIST, song.getArtist());
        entry.put(COLUMN_GENRE, song.getGenre());
        entry.put(COLUMN_DURATION, song.getDuration());
        entry.put(COLUMN_DOWNLOADED_BY, song.getDownloadedBy());
        long result = db.insert(TABLE_SONGS, null, entry);
        song.setId(result);
        return song;
    }

    public static Song update(Song song, Context context, String selection, String[] selectionArgs) {
        SQLiteDatabase db = DbContext.getDB(context).getWritableDatabase();
        ContentValues entry = new ContentValues();
        entry.put(COLUMN_FILE, song.getFile().getId().toString());
        entry.put(COLUMN_GLOBAL_ID, song.getGlobalId());
        entry.put(COLUMN_ARTIST, song.getArtist());
        entry.put(COLUMN_GENRE, song.getGenre());
        entry.put(COLUMN_DURATION, song.getDuration());
        entry.put(COLUMN_DOWNLOADED_BY, song.getDownloadedBy());
        db.update(TABLE_SONGS, entry, selection, selectionArgs);
        return song;
    }

    public static int delete(Context context, String selection, String[] selectionArgs, Song song) {
        List<SongInPlaylistDto> playlists = PlaylistRepository.findPlaylistsOfSong(PlaylistRepository.COLUMN_SONG + " = ?",
                new String[] {song.getId().toString()}, context);
        for (SongInPlaylistDto songInPlaylistDto: playlists) {
            Playlist playlist = songInPlaylistDto.getPlaylist();
            PlaylistRepository.deleteSongsFromPlaylist(context,
                    PlaylistRepository.COLUMN_PLAYLIST + " = ? AND " + PlaylistRepository.COLUMN_SONG + " = ?",
                    new String[]{playlist.getId().toString(), song.getId().toString()});

            playlist.getSongs().remove(song);
            Util.removeSongToPlaylistInGlobalDb(playlist, song, context, songInPlaylistDto.getPosition());
            Util.updatePositionOfSongsAfterDelete(playlist.getId(), songInPlaylistDto.getPosition(), context);
        }

        SQLiteDatabase db = DbContext.getDB(context).getWritableDatabase();
        return db.delete(TABLE_SONGS, selection, selectionArgs);
    }

    public static List<Song> searchSongQuery(String query, Context context, String downloadedBy) {
        List<Song> songs = findSongs(COLUMN_ARTIST + " LIKE ? AND " + COLUMN_DOWNLOADED_BY + " = ?",
                new String[]{ "%" + query + "%", downloadedBy }, context);
        List<File> files = FileRepository.findFiles(FileRepository.COLUMN_NAME + " LIKE ?", new String[]{ "%" + query + "%" }, context);
        for (File file : files) {
            if (isAudio(FilenameUtils.getExtension(file.getName()), context)) {
                List<Song> found = findSongs(COLUMN_FILE + " = ? AND " + COLUMN_DOWNLOADED_BY + " = ?",
                        new String[]{ file.getId().toString(), downloadedBy }, context);
                if (found.size() == 1) {
                    boolean add = true;
                    for (Song s: songs) {
                        if (s.getId().equals(found.get(0).getId())) {
                            add = false;
                            break;
                        }
                    }
                    if (add) {
                        songs.add(found.get(0));
                    }
                }
            }
        }
        return songs;
    }

    private static boolean isAudio(String extension, Context context) {
        for (String audioExtension : context.getResources().getStringArray(R.array.audio_file_extensions)) {
            if (audioExtension.toLowerCase().equals(extension.toLowerCase())) {
                return true;
            }
        }
        return false;
    }

    public static List<Song> findUsersDownloadedSongs(String username, Context context) {
        String selection = COLUMN_DOWNLOADED_BY + " = ?";
        return findSongs(selection, new String[] { username }, context);
    }

    public static boolean doesPlaylistSongExist(Long songId, Long playlistId, Context context) {
        String query = String.format("SELECT * \n" +
                "FROM playlists_songs \n" +
                "INNER JOIN songs, playlists ON playlists_songs.song = songs._id AND playlists_songs.playlist = playlists._id \n" +
                "WHERE songs.global_id = %s AND playlists.downloaded_by = \"%s\" AND playlists.global_id = %s", songId.toString(), SharedPreferencesService.get("username"), playlistId.toString());
        SQLiteDatabase db = DbContext.getDB(context).getWritableDatabase();
        Cursor cursor =  db.rawQuery(query, null);
        boolean exists = cursor.getCount() > 0;
        cursor.close();
        return exists;
    }
}
