package rs.ac.uns.ftn.medily.tools;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class PlayPauseButtonNotificationListener extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent i = new Intent();
        i.putExtra("action", "play");
        i.setAction("medily.receiver");
        context.sendBroadcast(i);
        NotificationUtil.publishNotification(context);
    }
}
