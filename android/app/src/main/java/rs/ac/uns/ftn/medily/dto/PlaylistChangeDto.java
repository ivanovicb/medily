package rs.ac.uns.ftn.medily.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

import lombok.Data;

@Data
public class PlaylistChangeDto {

    @SerializedName("id")
    @Expose
    private Long id;

    @SerializedName("playlistId")
    @Expose
    private Long playlistId;

    @SerializedName("modified")
    @Expose
    private Date modified;

    @SerializedName("playlistChangeType")
    @Expose
    private PlaylistChangeType playlistChangeType;

    @SerializedName("songId")
    @Expose
    private Long songId;
}
