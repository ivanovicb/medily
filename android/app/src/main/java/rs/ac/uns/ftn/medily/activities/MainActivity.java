package rs.ac.uns.ftn.medily.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.squareup.seismic.ShakeDetector;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

import rs.ac.uns.ftn.medily.R;
import rs.ac.uns.ftn.medily.fragments.AccountFragment;
import rs.ac.uns.ftn.medily.fragments.DriveFragment;
import rs.ac.uns.ftn.medily.fragments.MusicFragment;
import rs.ac.uns.ftn.medily.sync.SyncManager;
import rs.ac.uns.ftn.medily.tools.AppListenerService;
import rs.ac.uns.ftn.medily.tools.FragmentTransition;
import rs.ac.uns.ftn.medily.tools.MusicPlayer;
import rs.ac.uns.ftn.medily.tools.NotificationUtil;
import rs.ac.uns.ftn.medily.tools.SharedPreferencesService;


public class MainActivity extends AppCompatActivity implements SensorEventListener, ShakeDetector.Listener{

    SensorManager sensorManager;
    private NavigationView navigationView;
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    private BottomNavigationView bottomNavigationView;
    public static Context context;
    private DriveFragment driveFragment;
    private MusicFragment musicFragment;
    private AccountFragment accountFragment;
    private SyncManager syncManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startService(new Intent(getBaseContext(), AppListenerService.class));
        setContentView(R.layout.activity_main);
        selected = 1;
        drawerLayout = (DrawerLayout) findViewById (R.id.drawer_layout);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.open, R.string.close);

        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        navigationView = (NavigationView) findViewById(R.id.nv);
        navigationView.setNavigationItemSelectedListener(navigationListener);
        navigationView.bringToFront();

        View header = navigationView.getHeaderView(0);
        TextView tx = (TextView) header.findViewById(R.id.text_view_logged_user);
        if (tx != null) {
            String user = "Hello " + SharedPreferencesService.get("name") + "!";
            tx.setText(user);
        }

        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        ShakeDetector sd = new ShakeDetector(this);
        sd.start(sensorManager);

        bottomNavigationView = findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(navListener);
        navigationListener.onNavigationItemSelected(navigationView.getMenu().getItem(1));
        context = this;

        driveFragment = DriveFragment.newInstance();
        accountFragment = AccountFragment.newInstance();

        FragmentTransition.to(getMusicFragment(), this, false, R.id.main_fragment_container);

        syncManager = new SyncManager(this);

        switch (getResources().getConfiguration().uiMode & Configuration.UI_MODE_NIGHT_MASK) {
            case Configuration.UI_MODE_NIGHT_YES:
                SharedPreferencesService.add("phone_theme", "dark");
                break;
            case Configuration.UI_MODE_NIGHT_NO:
                SharedPreferencesService.add("phone_theme", "light");
                break;
        }
    }

    private NavigationView.OnNavigationItemSelectedListener navigationListener = new NavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            int id = item.getItemId();
            switch(id)
            {
                case R.id.nav_profile:
                    bottomNavigationView.setSelectedItemId(R.id.action_nav_account);
                    break;
                case R.id.nav_songs:
                    selected = 1;
                    bottomNavigationView.setSelectedItemId(R.id.action_nav_music);
                    break;
                case R.id.nav_drive:
                    selected = 0;
                    bottomNavigationView.setSelectedItemId(R.id.action_nav_drive);
                    break;
                case R.id.nav_playlists:
                    selected = 0;
                    bottomNavigationView.setSelectedItemId(R.id.action_nav_music);
                    break;
                case R.id.nav_feedback:
                    context.startActivity(new Intent(context, FeedbackActivity.class));
                    break;
                case R.id.nav_settings:
                    context.startActivity(new Intent(context, SettingsActivity.class));
                    break;
                default:
                    return true;
            }

            navigationView.getMenu().findItem(id).setChecked(false);
            drawerLayout.closeDrawers();
            return true;
        }
    };

    private int selected;

    private BottomNavigationView.OnNavigationItemSelectedListener navListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                    Fragment selectedFragment = null;

                    switch (item.getItemId()){
                        case R.id.action_nav_account:
                            selectedFragment = accountFragment;
                            break;
                        case R.id.action_nav_music:
                            selectedFragment = getMusicFragment();
                            break;
                        case R.id.action_nav_drive:
                            selectedFragment = driveFragment;
                            break;
                    }

                    if (selectedFragment == null) {
                        return false;
                    }
                    selected = selectedFragment.getId();

                    getSupportFragmentManager().beginTransaction()
                            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                            .replace(R.id.main_fragment_container, selectedFragment)
                            .commit();

                    return true;
                }
            };

    @Override
    public void onBackPressed() {
        try {
            FragmentManager fragManager = this.getSupportFragmentManager();
            int count = this.getSupportFragmentManager().getBackStackEntryCount();
            Fragment frag = fragManager.getFragments().get(count > 0 ? count - 1 : count);
            if (frag instanceof MusicFragment) {
                if (((MusicFragment) frag).getSongsInPlaylistFragment() != null) {
                    ((MusicFragment) frag).hidePlaylist();
                    Objects.requireNonNull(this).setTitle("Your music box");
                    return;
                }
            }
        } catch (Exception e) {}
        super.onBackPressed();
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            float[] values = event.values;
            float x = values[0];
            float y = values[1];
            float z = values[2];

            if (Boolean.parseBoolean(SharedPreferencesService.get("sensors"))) {
                if (x < 0 && y < 0 && z < 0) {
                    // stop music if playing
                    MusicPlayer.pauseOnFlip(this);
                }
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    /*
     * add listeners for sensors
     * */
    @Override
    protected void onResume() {
        super.onResume();
        // register this class as a listener for the orientation and
        // accelerometer sensors
        sensorManager.registerListener(this,
                sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_NORMAL);
    }

    /*
     * remove listeners for sensors
     * */
    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(this);
    }

    @Override
    public void hearShake() {
        if (Boolean.parseBoolean(SharedPreferencesService.get("sensors"))) {
            MusicPlayer.nextSong(this);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(actionBarDrawerToggle.onOptionsItemSelected(item))
            return true;

        return super.onOptionsItemSelected(item);
       /* if (item.getItemId() == android.R.id.home) {
            if (!Util.isBackClicked(actionBarDrawerToggle)) {
                drawerLayout.openDrawer(GravityCompat.START);
            }
        }*/
    }

    @Override
    protected void onDestroy() {
        try {
            if (MusicFragment.myReceiver != null) {
                    unregisterReceiver(MusicFragment.myReceiver);
            }
        } catch (Exception e) {}
        try {
            NotificationUtil.clearPlayerNotificationView(getBaseContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }

    public AccountFragment getAccountFragment() {
        return accountFragment;
    }

    public MusicFragment getMusicFragment() {
        return MusicFragment.newInstance(actionBarDrawerToggle, selected);
    }

    public NavigationView.OnNavigationItemSelectedListener getNavigationListener() {
        return navigationListener;
    }

    public NavigationView getNavigationView() {
        return navigationView;
    }

    private List<Fragment> getChildFragments(Fragment fragment, List<Fragment> result) {
        List<Fragment> childFragments = fragment.getChildFragmentManager().getFragments();
        if (childFragments == null || childFragments.isEmpty()) {
            return Collections.emptyList();
        }
        result.addAll(childFragments);
        for (Fragment child: childFragments) {
            getChildFragments(child, result);
        }
        return childFragments;
    }

    public List<Fragment> getFragments() {
        return getSupportFragmentManager().getFragments();
    }
}
