package rs.ac.uns.ftn.medily.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;

import java.util.ArrayList;
import java.util.List;

import rs.ac.uns.ftn.medily.R;
import rs.ac.uns.ftn.medily.dto.ManageSongWithinPlaylistDto;
import rs.ac.uns.ftn.medily.model.Playlist;
import rs.ac.uns.ftn.medily.model.Song;
import rs.ac.uns.ftn.medily.tools.SharedPreferencesService;
import rs.ac.uns.ftn.medily.tools.Util;
import rs.ac.uns.ftn.medily.tools.db.PlaylistRepository;

public class ManageSongAdapter extends BaseAdapter {

    private List<Playlist> data;
    private static Context context;
    private Song clickedSong;
    private LayoutInflater layoutInflater;

    public ManageSongAdapter() {}

    public ManageSongAdapter(Song song, Context ctx, List<Playlist> playlists) {
        this.data = Util.filterPlaylists(playlists, SharedPreferencesService.get("username"));
        this.clickedSong = song;
        context = ctx;

        this.layoutInflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() { return data.size(); }

    @Override
    public Object getItem(int position) { return data.get(position); }

    @Override
    public long getItemId(int position) { return position; }

    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {
        View view = convertView;
        final Playlist playlist = data.get(position);

        if(view == null) {
            view = layoutInflater.inflate(R.layout.layout_playlist, null);
        }

        TextView playlistName = (TextView) view.findViewById(R.id.playlist_name);
        playlistName.setText(playlist.getName());

        final ImageButton imageButton = (ImageButton) view.findViewById(R.id.btn_manage_song);
        if (PlaylistRepository.findSongsInPlaylist(PlaylistRepository.COLUMN_SONG + " = ? AND " + PlaylistRepository.COLUMN_PLAYLIST + " = ?",
                new String[] { clickedSong.getId().toString(), playlist.getId().toString()}, view.getContext()).size() > 0) {
            imageButton.setImageDrawable(ContextCompat.getDrawable(view.getContext(), R.drawable.ic_minus));
        } else {
            imageButton.setImageDrawable(ContextCompat.getDrawable(view.getContext(), R.drawable.ic_plus_green));
        }

        imageButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int position = playlist.getSongs().indexOf(clickedSong) + 1;

                if (PlaylistRepository.findSongsInPlaylist(PlaylistRepository.COLUMN_SONG + " = ? AND " + PlaylistRepository.COLUMN_PLAYLIST + " = ?",
                        new String[] { clickedSong.getId().toString(), playlist.getId().toString()}, v.getContext()).size() > 0) {

                    PlaylistRepository.deleteSongsFromPlaylist(v.getContext(),
                            PlaylistRepository.COLUMN_PLAYLIST + " = ? AND " + PlaylistRepository.COLUMN_SONG + " = ?",
                            new String[] {playlist.getId().toString(), clickedSong.getId().toString()});

                    playlist.getSongs().remove(clickedSong);
                    Util.removeSongToPlaylistInGlobalDb(playlist, clickedSong, context, position);
                    Util.updatePositionOfSongsAfterDelete(playlist.getId(), position, context);
                    imageButton.setImageDrawable(ContextCompat.getDrawable(v.getContext(), R.drawable.ic_plus_green));
                } else {
                    PlaylistRepository.addSongsToPlaylist(playlist, new ArrayList<Song>() {{
                        add(clickedSong);
                    }}, v.getContext());
                    playlist.getSongs().add(clickedSong);
                    addSongToPlaylistInGlobalDb(playlist, clickedSong, position);
                    imageButton.setImageDrawable(ContextCompat.getDrawable(v.getContext(), R.drawable.ic_minus));
                }
            }
        });

        return view;
    }

    private void addSongToPlaylistInGlobalDb(Playlist playlist, Song clickedSong, int position) {
        Util.manageSongWithinPlaylistInGlobalDb(new ManageSongWithinPlaylistDto(playlist.getGlobalId(),
                clickedSong.getGlobalId(), "add", position), context);
    }
}
