package rs.ac.uns.ftn.medily.service;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rs.ac.uns.ftn.medily.dto.PlaylistChangeDto;

public interface SyncService {

    @GET("/sync")
    Call<List<PlaylistChangeDto>> getChanges(@Query("lastSync") String lastSync);
}
