package rs.ac.uns.ftn.medily.model;

public enum DriveType {
    LOCAL,
    REMOTE
}
