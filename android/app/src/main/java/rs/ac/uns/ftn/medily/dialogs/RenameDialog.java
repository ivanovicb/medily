package rs.ac.uns.ftn.medily.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;

import rs.ac.uns.ftn.medily.R;

public class RenameDialog extends Dialog implements View.OnClickListener {

    private int position;
    private View clickedView;
    private RenameDialog.RenameDialogListener renameDialogListener;

    public RenameDialog(@NonNull Context context, int position, RenameDialogListener renameDialogListener,
                        View view) {
        super(context);
        this.position = position;
        this.clickedView = view;
        this.renameDialogListener = renameDialogListener;
    }

    public RenameDialog(@NonNull Context context, int position, RenameDialogListener renameDialogListener) {
        super(context);
        this.position = position;
        this.renameDialogListener = renameDialogListener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.layout_rename);

        Button buttonCancel = findViewById(R.id.btn_drive_rename_cancel);
        buttonCancel.setOnClickListener(this);
        Button buttonOk = findViewById(R.id.btn_drive_rename_ok);
        buttonOk.setOnClickListener(this);

        EditText editText = findViewById(R.id.drive_rename_text);
        TextView oldValue = clickedView.findViewById(R.id.text_playlist_item);
        if (oldValue != null) {
            editText.setText(oldValue.getText());
            editText.setSelection(editText.getText().length());
        } else {
            oldValue = clickedView.findViewById(R.id.text_view_drive_item);
            if (oldValue != null) {
                editText.setText(oldValue.getText());
                editText.setSelection(editText.getText().length());
            }
        }
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btn_drive_rename_ok){
            rename();
        }
        dismiss();
    }

    private void rename() {
        EditText editText = findViewById(R.id.drive_rename_text);
        String newName = editText.getText().toString();
        renameDialogListener.rename(newName, position);
    }

    public interface RenameDialogListener {
        void rename(String name, int position);
    }
}
