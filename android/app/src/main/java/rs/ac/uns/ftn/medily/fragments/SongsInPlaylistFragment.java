package rs.ac.uns.ftn.medily.fragments;

import android.app.Activity;
import android.content.Context;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.woxthebox.draglistview.DragListView;
import com.woxthebox.draglistview.swipe.ListSwipeHelper;
import com.woxthebox.draglistview.swipe.ListSwipeItem;

import java.util.List;
import java.util.Objects;

import rs.ac.uns.ftn.medily.R;
import rs.ac.uns.ftn.medily.adapters.SongInPlaylistAdapter;
import rs.ac.uns.ftn.medily.dto.UpdateSongPositionDto;
import rs.ac.uns.ftn.medily.model.Playlist;
import rs.ac.uns.ftn.medily.model.Song;
import rs.ac.uns.ftn.medily.tools.MusicPlayer;
import rs.ac.uns.ftn.medily.tools.Util;
import rs.ac.uns.ftn.medily.tools.db.PlaylistRepository;

public class SongsInPlaylistFragment {

    private Playlist playlist;
    private SongInPlaylistAdapter adapter;
    private boolean stop;
    private Context context;

    public SongsInPlaylistFragment(Playlist showPlaylist, Context appContext) {
        playlist = showPlaylist;
        context = appContext;
    }

    private String stripName(String name) {
        if (name.length() > 15) {
            name = name.substring(0, 10) + "...";
        }

        return name;
    }

    public void onCreateView(View view) {
        stop = false;
        final DragListView listView = view.findViewById(R.id.song_in_playlist_list);
        listView.setDragListListener(new DragListView.DragListListener() {

            Song song = null;

            @Override
            public void onItemDragStarted(int position) {
                List items = listView.getAdapter().getItemList();
                song = (Song) items.get(position);
            }

            @Override
            public void onItemDragging(int itemPosition, float x, float y) {
            }

            @Override
            public void onItemDragEnded(int fromPosition, int toPosition) {
                if (fromPosition != toPosition && song != null) {
                    // update in sqlite based on local id and in mysql based on global id
                    PlaylistRepository.updateSongPositionInPlaylist(new UpdateSongPositionDto(song.getId(), playlist.getId(), fromPosition + 1, toPosition + 1) , context);
                    Util.updateSongPositionInPlaylist(new UpdateSongPositionDto(song.getGlobalId(), playlist.getGlobalId(), fromPosition + 1, toPosition + 1), context);
                }
            }
        });

        listView.setDragListCallback(new DragListView.DragListCallbackAdapter() {
            @Override
            public boolean canDragItemAtPosition(int dragPosition) {
                return !stop;
            }

            @Override
            public boolean canDropItemAtPosition(int dropPosition) {
                return !stop;
            }
        });

        listView.setSwipeListener(new ListSwipeHelper.OnSwipeListenerAdapter() {
            @Override
            public void onItemSwipeStarted(ListSwipeItem item) {
            }

            @Override
            public void onItemSwiping(ListSwipeItem item, float swipedDistanceX) {}

            @SuppressWarnings("unchecked")
            @Override
            public void onItemSwipeEnded(ListSwipeItem item, ListSwipeItem.SwipeDirection swipedDirection) {
                if (swipedDirection == ListSwipeItem.SwipeDirection.RIGHT) {
                    Song song = (Song) item.getTag();
                    int pos = listView.getAdapter().getPositionForItem(song);
                    PlaylistRepository.deleteSongsFromPlaylist(context,
                            PlaylistRepository.COLUMN_PLAYLIST + " = ? AND " + PlaylistRepository.COLUMN_SONG + " = ?",
                            new String[] {playlist.getId().toString(), song.getId().toString()});
                    playlist.getSongs().remove(song);
                    Util.removeSongToPlaylistInGlobalDb(playlist, song, context, pos + 1);
                    Util.updatePositionOfSongsAfterDelete(playlist.getId(), pos, context);
                    listView.getAdapter().removeItem(pos);
                }
            }
        });

        listView.setLayoutManager(new LinearLayoutManager(context));
        adapter = new SongInPlaylistAdapter(PlaylistRepository.findSongsInPlaylist(PlaylistRepository.COLUMN_PLAYLIST + " = ?",
                new String[] { playlist.getId().toString()}, context), R.layout.layout_song_in_playlist,
                R.id.item_layout, false, playlist);
        listView.setAdapter(adapter, true);
        listView.setCanDragHorizontally(false);
    }

    public void searchSongInPlaylist(String query) {
        adapter.getFilter().filter(query);
    }

    public void playAll(View view) {
        Activity activity = (Activity) context;
        if (playlist.getSongs().size() == 0) {
            Toast.makeText(activity, "Playlist cannot be played, it is empty",
                    Toast.LENGTH_LONG).show();
        } else {
            ImageButton button = (ImageButton) activity.findViewById(R.id.play_collapsed);
            button.setImageDrawable(ContextCompat.getDrawable(Objects.requireNonNull(context), R.drawable.ic_pause_collapsed));
            button.setVisibility(View.VISIBLE);
            button.setTag(R.drawable.ic_pause_collapsed);
            Util.syncViewsButtons("collapsed", button, activity, view);

            MusicPlayer.playAll((Activity) context, playlist.getId());
        }
    }

    public void shufflePlaylist() {
        MusicPlayer.shufflePlaylist(playlist.getId());
    }

    public void stopShuffle() {
        MusicPlayer.stopShufflePlaylist(playlist.getId());
    }

    public void setInitTitleSharePlaylist(MenuItem item) {
        if (item != null) {
            if (playlist.isSharePlaylist()) {
                item.setTitle(R.string.stop_share_text);
            } else {
                item.setTitle(R.string.switch_text);
            }
        }

        setPlaylistName();
    }

    private void setPlaylistName() {
        Objects.requireNonNull((Activity) context).setTitle(stripName(playlist.getName()));
    }

    public boolean isStop() {
        return stop;
    }

    public void setStop(boolean stop) {
        this.stop = stop;
    }

    public Playlist getPlaylist() {
        return playlist;
    }

    public void setPlaylist(Playlist playlist) {
        this.playlist = playlist;
    }
}
