package rs.ac.uns.ftn.medily.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.VideoView;

import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.RawResourceDataSource;
import com.google.android.exoplayer2.util.Util;

import java.io.File;
import java.util.Objects;

import rs.ac.uns.ftn.medily.R;
import rs.ac.uns.ftn.medily.tools.FileHandler;

public class VideoActivity extends AppCompatActivity {

    private SimpleExoPlayer player;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);
        String path = Objects.requireNonNull(getIntent().getExtras()).getString("video");
        showVideo(path);
    }

    private void showVideo(String path) {
        File file = new File(path);
        if (file.exists()) {
            try {
                PlayerView playerView = findViewById(R.id.video_show);
                playerView.setBackgroundColor(Color.BLACK);

                player = ExoPlayerFactory.newSimpleInstance(this);
                playerView.setPlayer(player);
                DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(this,
                        Util.getUserAgent(this, file.getName()));
                MediaSource firstSource = new ExtractorMediaSource.Factory(dataSourceFactory)
                        .createMediaSource(Uri.fromFile(file));
                player.prepare(firstSource);
                player.setPlayWhenReady(true);
            } catch (Exception e) {
                e.printStackTrace();
                showError();
            }
        } else {
            showError();
        }
    }

    private void showError() {
        findViewById(R.id.video_show).setVisibility(View.GONE);
        LinearLayout image = findViewById(R.id.error_show);
        image.setVisibility(View.VISIBLE);
        // image.setScaleType(ImageView.ScaleType.CENTER_CROP); // FOR FULLSCREEN UNCOMMENT
        this.setTitle("Error loading video");
    }

    @Override
    public void onBackPressed() {
        if (player != null) {
            player.setPlayWhenReady(false);
            player.stop();
            player.seekTo(0);
        }
        super.onBackPressed();
    }
}
