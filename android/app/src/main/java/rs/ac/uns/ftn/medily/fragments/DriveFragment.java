package rs.ac.uns.ftn.medily.fragments;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.material.tabs.TabLayout;

import java.util.Objects;

import rs.ac.uns.ftn.medily.R;
import rs.ac.uns.ftn.medily.adapters.DriveTabsPagerAdapter;


public class DriveFragment extends Fragment {

    public DriveFragment() { }

    public static DriveFragment newInstance(){
        return new DriveFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Objects.requireNonNull(getActivity()).setTitle(R.string.family_drive);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_drive, container, false);
        DriveTabsPagerAdapter tabsPagerAdapter = new DriveTabsPagerAdapter(this, getChildFragmentManager());

        ViewPager viewPager = view.findViewById(R.id.drive_view_pager);
        viewPager.setAdapter(tabsPagerAdapter);

        TabLayout tabs = view.findViewById(R.id.drive_tab_layout);
        tabs.setupWithViewPager(viewPager);

        return view;
    }

}
