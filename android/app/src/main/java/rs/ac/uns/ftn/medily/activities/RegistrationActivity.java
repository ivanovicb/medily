package rs.ac.uns.ftn.medily.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import lombok.SneakyThrows;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rs.ac.uns.ftn.medily.R;
import rs.ac.uns.ftn.medily.dto.RegistrationDto;
import rs.ac.uns.ftn.medily.service.ServiceUtils;

public class RegistrationActivity extends AppCompatActivity {

    private EditText family;
    private EditText name;
    private EditText email;
    private EditText username;
    private EditText password;
    private EditText retyped;
    private Button registration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        this.setTitle("Registration to Medily");
        init();
    }

    private void init() {
        family = findViewById(R.id.reg_family_name);
        name = findViewById(R.id.reg_name);
        email = findViewById(R.id.reg_email);
        username = findViewById(R.id.reg_username);
        password = findViewById(R.id.reg_password);
        retyped = findViewById(R.id.reg_retype_password);
        registration = findViewById(R.id.register_button);
        addListeners();
    }

    private void addListeners() {
        registration.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (validInput()) {
                    Call<ResponseBody> call = ServiceUtils.userService.registration(
                            new RegistrationDto(family.getText().toString(),
                                    email.getText().toString(),
                                    name.getText().toString(),
                                    username.getText().toString(),
                                    password.getText().toString()));
                    call.enqueue(new Callback<ResponseBody>() {
                        @SneakyThrows
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            if (response.code() == 200) {
                                Toast.makeText(RegistrationActivity.this, "Registration successful! "
                                        + "Confirmation link is sent to your email address so that you can "
                                        + "activate your account.", Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(RegistrationActivity.this, LoginActivity.class);
                                RegistrationActivity.this.startActivity(intent);
                            } else if (response.code() == 400) {
                                Toast.makeText(RegistrationActivity.this, response.errorBody().string(),
                                        Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(RegistrationActivity.this, "Invalid registration!",
                                        Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            Toast.makeText(RegistrationActivity.this, t.getMessage(),
                                    Toast.LENGTH_LONG).show();
                        }
                    });

                }
            }
        });
    }

    private boolean validInput() {
        if (family.getText().toString().length() < 1) {
            Toast.makeText(RegistrationActivity.this, "Family name not defined",
                    Toast.LENGTH_SHORT).show();
            return false;
        } else if (name.getText().toString().length() < 1) {
            Toast.makeText(RegistrationActivity.this, "Name not defined",
                    Toast.LENGTH_SHORT).show();
            return false;
        } else if (email.getText().toString().length() < 1) {
            Toast.makeText(RegistrationActivity.this, "Email not defined",
                    Toast.LENGTH_SHORT).show();
            return false;
        } else if (username.getText().toString().length() < 1) {
            Toast.makeText(RegistrationActivity.this, "Username not defined",
                    Toast.LENGTH_SHORT).show();
            return false;
        } else if (password.getText().toString().length() < 1) {
            Toast.makeText(RegistrationActivity.this, "Password not defined",
                    Toast.LENGTH_SHORT).show();
            return false;
        } else if (retyped.getText().toString().length() < 1) {
            Toast.makeText(RegistrationActivity.this, "Retyped password not defined",
                    Toast.LENGTH_SHORT).show();
            return false;
        } else if (!password.getText().toString().equals(retyped.getText().toString())) {
            Toast.makeText(RegistrationActivity.this, "Password not same retyped password",
                    Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }
}
