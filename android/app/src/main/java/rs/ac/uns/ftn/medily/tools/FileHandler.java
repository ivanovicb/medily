package rs.ac.uns.ftn.medily.tools;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.os.Build;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URLConnection;

public class FileHandler {

    public static void writeToFile(String data, Context context, String path, Activity activity) {
        if (Build.VERSION.SDK_INT >= 23) {
            int permissionCheck = ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE);
            if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
            }
        }
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput(path, Context.MODE_PRIVATE));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String checkFileType(String path) {
        if (checkIfImage(path)) {
            return "image";
        } else if (checkIfVideo(path)) {
            return "video";
        } else if (checkIfSong(path)) {
            return "music";
        } else {
            return null;
        }
    }

    private static boolean checkIfSong(String path) {
        return false;
    }

    private static boolean checkIfImage(String path) {
        String mimeType = URLConnection.guessContentTypeFromName(path);
        return mimeType != null && mimeType.startsWith("image");
    }

    private static boolean checkIfVideo(String path) {
        String mimeType = URLConnection.guessContentTypeFromName(path);
        return mimeType != null && mimeType.startsWith("video");
    }

    /*
    // DOWNLOAD FROM SERVER
    ServiceUtils.userService.getImage().enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        try {
                            File path = Environment.getExternalStorageDirectory();
                            File file = new File(path, "ftn.jpg");
                            FileOutputStream fileOutputStream = new FileOutputStream(file);
                            IOUtils.write(response.body().bytes(), fileOutputStream);
                        }
                        catch (Exception ex){
                            ex.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        System.out.println(t.getMessage());
                    }
                }); */

    // READ DOWNLOADED IMAGE
    /*
    File path = Environment.getExternalStorageDirectory();
                File file = new File(path, "ftn.jpg");
                Bitmap img = (Bitmap) FileHandler.readFromFile(getApplicationContext(), file.getPath());
                image.setImageBitmap(img);
    */

    // SHOW IN INTENT
    /*
    Intent intent = new Intent(LoginActivity.this, ImageActivity.class);
                intent.putExtra("image", "ff.jpg");
    LoginActivity.this.startActivity(intent);
    */

}
