package rs.ac.uns.ftn.medily.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;

import androidx.fragment.app.Fragment;

import java.util.Objects;
import java.util.Random;

import lombok.SneakyThrows;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rs.ac.uns.ftn.medily.R;
import rs.ac.uns.ftn.medily.dto.ProfileDto;
import rs.ac.uns.ftn.medily.service.ServiceUtils;
import rs.ac.uns.ftn.medily.tools.FragmentTransition;
import rs.ac.uns.ftn.medily.tools.LoadingRotation;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AccountFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AccountFragment extends Fragment {

    private String role;
    private ImageView image;

    public AccountFragment() { }

    public static AccountFragment newInstance() {
        return new AccountFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Objects.requireNonNull(getActivity()).setTitle("Profile");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_account, container, false);
        Handler handler = new Handler();
        init(view);
        handler.postDelayed(task, 100);
        return view;
    }

    private void init(View view) {
        image = view.findViewById(R.id.loading_image);
        image.startAnimation(LoadingRotation.rotation());
    }

    private Runnable task = new Runnable() {
        public void run() {
            Call<ProfileDto> call = ServiceUtils.userService.profile();
            call.enqueue(new Callback<ProfileDto>() {
                @SneakyThrows
                @Override
                public void onResponse(Call<ProfileDto> call, Response<ProfileDto> response) {
                    if (response.code() == 200) {
                        role = response.body().getRole();
                        showProfile(null);
                    } else if (response.code() == 400) {
                        showProfile(response.errorBody().string());
                    } else {
                        showProfile("Connection to server could not be established. Please try again later.");
                    }
                }

                @Override
                public void onFailure(Call<ProfileDto> call, Throwable t) {
                    showProfile("Connection to server could not be established. Please try again later.");
                }
            });
        }
    };

    private void showProfile(String message) {
        try {
            if (getActivity() != null) {
                if (role == null) {
                    FragmentTransition.to(ErrorFragment.newInstance(message),
                            Objects.requireNonNull(getActivity()), false,
                            R.id.main_fragment_container);
                } else {
                    FragmentTransition.to(ParentProfileFragment.newInstance(role),
                            Objects.requireNonNull(getActivity()), false,
                            R.id.main_fragment_container);
                }
            }
        } catch(Exception e){
            e.printStackTrace();
        }
    }

    private String getSaltString() {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 11) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        return salt.toString();
    }

    public String getRole() {
        return role;
    }
}
