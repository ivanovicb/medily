package rs.ac.uns.ftn.medily.tools;

import android.app.Activity;
import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.view.View;
import android.widget.SeekBar;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import rs.ac.uns.ftn.medily.model.Playlist;
import rs.ac.uns.ftn.medily.model.Song;
import rs.ac.uns.ftn.medily.tools.db.PlaylistRepository;

public class MusicPlayer {

    public static Song currentlyPlaying;
    public static List<Song> songsToPlay = new ArrayList<>();
    public static MediaPlayer mediaPlayer = new MediaPlayer();
    private static boolean wasPlaying = false;
    public static boolean started = false;
    public static boolean allPlaying = true;

    public static SeekBar seekBar;
    private static boolean shuffle = false;
    private static boolean loop = false;

    private static Context context;

    public MusicPlayer() {}

    public static void init(Song currentlyPlayingSong, Context ctx) {
        context = ctx;
        currentlyPlaying = currentlyPlayingSong;
        started = true;
    }

    public static void init(Context ctx) {
        context = ctx;
        currentlyPlaying = null;
        started = true;
        mediaPlayer = new MediaPlayer();
    }

    public static void play(Song clicked, final Activity activity) {
        if (clicked != null) {
            try {
                currentlyPlaying = clicked;
                Util.startAllTextAnimationsOnPlay(activity);

                if (!wasPlaying) {
                    if (mediaPlayer == null) {
                        mediaPlayer = new MediaPlayer();
                    }
                    seekBar.setProgress(0);
                    mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

                        @Override
                        public void onPrepared(MediaPlayer player) {
                            player.start();
                            setSeekBarStatus();
                            NotificationUtil.setPlayerNotificationView(activity);
                        }

                    });

                    mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            nextSong(activity);
                        }
                    });

                    initSong(clicked);
                }
                NotificationUtil.setPlayerNotificationView(activity);
                wasPlaying = false;
                SharedPreferencesService.add("song", clicked.getId().toString());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void initSong(Song clicked) {
        try {
            File file = new File(clicked.getFile().getPath());
            mediaPlayer.reset();
            mediaPlayer.setDataSource(file.getAbsolutePath());

            mediaPlayer.prepareAsync(); // prepare async to not block main thread
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.setVolume(0.5f, 0.5f);
            mediaPlayer.setLooping(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void playInList(Playlist playlist, Long songId, Activity activity) {
        if (playlist != null) {
            songsToPlay = playlist.getSongs();
            allPlaying = false;
        }

        Song currentSong = getCurrentlyPlaying(songId);
        play(currentSong, activity);
    }

    public static void playCurrent() {
        if (mediaPlayer != null && !mediaPlayer.isPlaying()) {
            mediaPlayer.start();
            NotificationUtil.setPlayerNotificationView(context);
            Util.startAllTextAnimationsOnPlay((Activity) context);
        }
    }

    private static Song getCurrentlyPlaying(Long songId) {
        Song currentSong = null;
        for (Song s: songsToPlay) {
            if (s.getFile().getId().equals(songId)) {
                currentSong = s;
                break;
            }
        }

        return currentSong;
    }

    private static Song getPreviousSong() {
        Song prevSong = null;
        for (int i = 0; i < songsToPlay.size(); i++) {
            if (songsToPlay.get(i).getFile().getName().equals(currentlyPlaying.getFile().getName())) {
                if (i == 0) {
                    // get last
                    prevSong = songsToPlay.get(songsToPlay.size() - 1);
                } else {
                    prevSong = songsToPlay.get(i - 1);
                }
                break;
            }
        }

        return prevSong;
    }

    private static Song getNextSong() {
        Song nextSong = null;
        for (int i = 0; i < songsToPlay.size(); i++) {
            if (songsToPlay.get(i).getFile().getName().equals(currentlyPlaying.getFile().getName())) {
                if (i == songsToPlay.size() - 1) {
                    nextSong = songsToPlay.get(0);
                } else {
                    nextSong = songsToPlay.get(i + 1);
                }

                break;
            }
        }

        return nextSong;
    }

    private static Song getShuffledNext() {
        Song nextSong = null;
        Random random = new Random();
        int nextIndex = random.nextInt(songsToPlay.size()); // gives random in range from 0 to n-1
        if (songsToPlay.get(nextIndex) != null) {
            nextSong = songsToPlay.get(nextIndex);
        }

        return nextSong;
    }

    public static void pause() {
        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            mediaPlayer.pause();
            NotificationUtil.setPlayerNotificationView(context);
            Util.stopAllTextAnimationsOnPause((Activity) context);
        }
    }

    public static void seek(int progress) {
        mediaPlayer.seekTo(progress);
    }

    public static void previousSong(Activity activity) {
        if (mediaPlayer != null) {
            seekBar.setProgress(0);

            Song prevSong = getPreviousSong();
            if (prevSong != null) {
                play(prevSong, activity);
                Util.syncCurrentlyPlayingSong(currentlyPlaying, activity);
            }
        }
    }

    public static void nextSong(Activity activity) {
        if (mediaPlayer != null) {
            seekBar.setProgress(0);

            if (!loop) {
                Song nextSong = null;
                if (shuffle) {
                    nextSong = getShuffledNext();
                } else {
                    nextSong = getNextSong();
                }

                if (nextSong != null) {
                    play(nextSong, activity);
                    Util.syncCurrentlyPlayingSong(currentlyPlaying, activity);
                } else {
                    stopPlaying(activity);
                }
            }

            play(currentlyPlaying, activity);
        }
    }

    public static void clearMediaPlayer() {
        seekBar.setProgress(0);
        mediaPlayer.stop();
        mediaPlayer.release();
        mediaPlayer = null;
    }

    private static void stopPlaying(Activity activity) {
        mediaPlayer.stop();
        seekBar.setProgress(0);
        Util.setInitCurrentDurationPlayer(activity);
    }

    public static void setSeekBarStatus() {

        new Thread(new Runnable() {

            @Override
            public void run() {

                int currentPosition = seekBar.getProgress();
                int total = 0;
                if (currentlyPlaying != null) {
                    total = mediaPlayer.getDuration();
                }
                seekBar.setMax(total);
                while (mediaPlayer != null && currentPosition < total) {
                    try {
                        Thread.sleep(1000);
                        if (mediaPlayer != null) {
                            currentPosition = mediaPlayer.getCurrentPosition();
                        }
                    } catch (InterruptedException e) {
                        return;
                    }
                    seekBar.setProgress(currentPosition);
                }
                Thread.currentThread().interrupt();
            }
        }).start();
    }

    public static void shuffle() {
        shuffle = true;
    }

    public static void stopShuffle() {
        shuffle = false;
    }

    /* play all from start */
    public static void playAll(Activity activity, Long playlistId) {
        Playlist playlist = PlaylistRepository.findById(playlistId, activity.getApplicationContext());
        if (playlist != null) {
            songsToPlay = playlist.getSongs();
            if (songsToPlay.size() > 0) {
                play(songsToPlay.get(0), activity);
                Util.syncCurrentlyPlayingSong(songsToPlay.get(0), activity);
            }
        }
    }

    public static void shufflePlaylist(Long id) {
        if (songsToPlay == null) {
            Playlist playlist = PlaylistRepository.findById(id, context);
            if (playlist != null && songsToPlay.size() == 0) {
                songsToPlay = playlist.getSongs();
            }
        }

        shuffle();
    }

    public static void stopShufflePlaylist(Long id) {
        if (songsToPlay == null) {
            Playlist playlist = PlaylistRepository.findById(id, context);
            if (playlist != null && songsToPlay.size() == 0) {
                songsToPlay = playlist.getSongs();
            }
        }

        shuffle = false;
    }

    public static void loop(boolean loopActive) {
        loop = loopActive;
    }

    public static void pauseOnFlip(Activity activity) {
        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            mediaPlayer.pause();
            Util.setPauseIcon(activity);
            NotificationUtil.setPlayerNotificationView(context);
            Util.stopAllTextAnimationsOnPause(activity);
        }
    }

    public static void setSongsToPlay(List<Song> songList) {
        songsToPlay = songList;
        allPlaying = true;
    }
}
