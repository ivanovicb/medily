package rs.ac.uns.ftn.medily.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.widget.ImageView;

import java.io.File;
import java.util.Objects;

import rs.ac.uns.ftn.medily.R;
import rs.ac.uns.ftn.medily.tools.FileHandler;

public class ImageActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);
        String image = Objects.requireNonNull(getIntent().getExtras()).getString("image");
        showImage(image);
    }

    private void showImage(String path) {
        ImageView imageView = findViewById(R.id.image_show);
        File file = new File(path);
        if (file.exists()) {
            imageView.setBackgroundColor(Color.BLACK);
            Bitmap img = BitmapFactory.decodeFile(file.getPath());
            if (img == null) {
                showError(imageView);
            }
            imageView.setImageBitmap(img);
            this.setTitle(file.getName());
        } else {
            showError(imageView);
        }
    }

    private void showError(ImageView image) {
        image.setBackgroundColor((Color.WHITE));
        // image.setScaleType(ImageView.ScaleType.CENTER_CROP);
        this.setTitle("Error loading image");
    }
}
