package rs.ac.uns.ftn.medily.tools.db;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import rs.ac.uns.ftn.medily.model.Drive;
import rs.ac.uns.ftn.medily.model.File;
import rs.ac.uns.ftn.medily.model.Folder;
import rs.ac.uns.ftn.medily.tools.SharedPreferencesService;
import rs.ac.uns.ftn.medily.tools.Util;

public class DriveRepository {
    public static final String TABLE_DRIVES = "drives";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_USERNAME = "name";

    public static final String DB_CREATE = "create table "
            + TABLE_DRIVES + "("
            + COLUMN_ID  + " integer primary key autoincrement, "
            + COLUMN_USERNAME + " text"
            + ")";

    public static final String TABLE_DRIVE_FILES = "drive_files";
    public static final String COLUMN_DRIVE = "drive";
    public static final String COLUMN_FILE = "file";

    public static final String DB_CREATE_DRIVE_FILES = "create table "
            + TABLE_DRIVE_FILES + "("
            + COLUMN_ID  + " integer primary key autoincrement, "
            + COLUMN_DRIVE + " integer not null, "
            + COLUMN_FILE + " integer not null, "
            + "FOREIGN KEY (" + COLUMN_DRIVE + ") REFERENCES " + TABLE_DRIVES + " (_id), "
            + "FOREIGN KEY (" + COLUMN_FILE + ") REFERENCES " + FileRepository.TABLE_FILES + " (_id)"
            + ")";

    public static final String TABLE_DRIVE_FOLDERS = "drive_folders";
    public static final String COLUMN_FOLDER = "folder";
    public static final String DB_CREATE_DRIVE_FOLDERS = "create table "
            + TABLE_DRIVE_FOLDERS + "("
            + COLUMN_ID  + " integer primary key autoincrement, "
            + COLUMN_DRIVE + " integer not null, "
            + COLUMN_FOLDER + " integer not null, "
            + "FOREIGN KEY (" + COLUMN_DRIVE + ") REFERENCES " + TABLE_DRIVES + " (_id), "
            + "FOREIGN KEY (" + COLUMN_FOLDER + ") REFERENCES " + FolderRepository.TABLE_FOLDERS + " (_id)"
            + ")";


    public static Cursor query(String selection, String[] selectionArgs, Context context) {
        String[] projection = {COLUMN_ID};
        SQLiteDatabase db = DbContext.getDB(context).getWritableDatabase();
        return db.query(TABLE_DRIVES, projection, selection,
                selectionArgs, null, null, null);
    }

    public static List<Drive> findDrives(String selection, String[] selectionArgs, Context context) {
        List<Drive> result = new ArrayList<>();
        Cursor cursor = query(selection, selectionArgs, context);
        while(cursor.moveToNext()) {
            Long id = cursor.getLong(cursor.getColumnIndexOrThrow(COLUMN_ID));
            Drive drive = new Drive(id,
                    findFilesInDrive(COLUMN_ID + " = ?", new String[] {id.toString()}, context),
                    findFoldersInDrive(COLUMN_ID + " = ?", new String[] {id.toString()}, context));
            result.add(drive);
        }
        cursor.close();
        return result;
    }

    public static void checkIfDriveForUserExists(Context context) {
        List<Drive> drives = findDrives(COLUMN_USERNAME + " = ?", new String[] {SharedPreferencesService.get("username")}, context);
        if (drives.size() == 0) {
            DriveRepository.add(new Drive(), context);
        }
    }

    public static Drive add(Drive drive, Context context) {
        SQLiteDatabase db = DbContext.getDB(context).getWritableDatabase();
        ContentValues entry = new ContentValues();
        entry.put(COLUMN_USERNAME, SharedPreferencesService.get("username"));
        long result = db.insert(TABLE_DRIVES, null, entry);
        drive.setId(result);
        return drive;
    }

    public static Drive addFilesToDrive(Drive drive, List<File> files, Context context) {
        SQLiteDatabase db = DbContext.getDB(context).getWritableDatabase();
        for (File file: files) {
            ContentValues entry = new ContentValues();
            entry.put(COLUMN_DRIVE, drive.getId().toString());
            entry.put(COLUMN_FILE, file.getId().toString());
            db.insert(TABLE_DRIVE_FILES, null, entry);
        }
        return drive;
    }

    public static Drive addFoldersToDrive(Drive drive, List<Folder> folders, Context context) {
        SQLiteDatabase db = DbContext.getDB(context).getWritableDatabase();
        for (Folder folder: folders) {
            ContentValues entry = new ContentValues();
            entry.put(COLUMN_DRIVE, drive.getId().toString());
            entry.put(COLUMN_FOLDER, folder.getId().toString());
            db.insert(TABLE_DRIVE_FOLDERS, null, entry);
        }
        return drive;
    }

    public static Cursor queryFilesInDrive(String selection, String[] selectionArgs, Context context) {
        String[] projection = {COLUMN_ID, COLUMN_DRIVE, COLUMN_FILE};
        SQLiteDatabase db = DbContext.getDB(context).getWritableDatabase();
        return db.query(TABLE_DRIVE_FILES, projection, selection,
                selectionArgs, null, null, null);
    }

    public static List<File> findFilesInDrive(String selection, String[] selectionArgs, Context context) {
        List<File> result = new ArrayList<>();
        Cursor cursor = queryFilesInDrive(selection, selectionArgs, context);
        while(cursor.moveToNext()) {
            Long id = cursor.getLong(cursor.getColumnIndexOrThrow(COLUMN_FILE));
            List<File> files = FileRepository.findFiles(FileRepository.COLUMN_ID + " = ?",
                    new String[] {id.toString()}, context);
            if (files.size() == 1) {
                result.add(files.get(0));
            }
        }
        cursor.close();
        return result;
    }

    public static Cursor queryFoldersInDrive(String selection, String[] selectionArgs, Context context) {
        String[] projection = {COLUMN_ID, COLUMN_DRIVE, COLUMN_FOLDER};
        SQLiteDatabase db = DbContext.getDB(context).getWritableDatabase();
        return db.query(TABLE_DRIVE_FOLDERS, projection, selection,
                selectionArgs, null, null, null);
    }

    public static List<Folder> findFoldersInDrive(String selection, String[] selectionArgs, Context context) {
        List<Folder> result = new ArrayList<>();
        Cursor cursor = queryFoldersInDrive(selection, selectionArgs, context);
        while (cursor.moveToNext()) {
            Long id = cursor.getLong(cursor.getColumnIndexOrThrow(COLUMN_FOLDER));
            List<Folder> folders = FolderRepository.findFolders(FolderRepository.COLUMN_ID + " = ?",
                    new String[]{id.toString()}, context);
            if (folders.size() == 1) {
                result.add(folders.get(0));
            }
        }
        cursor.close();
        return result;
    }
}
