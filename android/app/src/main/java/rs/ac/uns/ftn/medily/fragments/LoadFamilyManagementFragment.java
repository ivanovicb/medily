package rs.ac.uns.ftn.medily.fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.List;
import java.util.Objects;

import lombok.SneakyThrows;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rs.ac.uns.ftn.medily.R;
import rs.ac.uns.ftn.medily.dto.ProfileDto;
import rs.ac.uns.ftn.medily.service.ServiceUtils;
import rs.ac.uns.ftn.medily.tools.FragmentTransition;
import rs.ac.uns.ftn.medily.tools.LoadingRotation;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link LoadFamilyManagementFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LoadFamilyManagementFragment extends Fragment {

    private ImageView image;

    public LoadFamilyManagementFragment() { }

    public static LoadFamilyManagementFragment newInstance() {
        LoadFamilyManagementFragment fragment = new LoadFamilyManagementFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_load_family_management, container, false);
        init(view);
        return view;
    }

    private void init(View view) {
        image = view.findViewById(R.id.loading_image);
        image.startAnimation(LoadingRotation.rotation());
        loadFamily();
    }

    private void loadFamily() {
        Call<List<ProfileDto>> call = ServiceUtils.userService.getFamily();
        call.enqueue(new Callback<List<ProfileDto>>() {
            @SneakyThrows
            @Override
            public void onResponse(Call<List<ProfileDto>> call, Response<List<ProfileDto>> response) {
                if (getActivity() != null) {
                    if (response.code() == 200) {
                        FragmentTransition.to(ManageFamilyFragment.newInstance(response.body()),
                                Objects.requireNonNull(getActivity()), false,
                                R.id.main_fragment_container);
                    } else if (response.code() == 400) {
                        FragmentTransition.to(ErrorFragment.newInstance(response.errorBody().string()),
                                Objects.requireNonNull(getActivity()), false,
                                R.id.main_fragment_container);
                    } else {
                        FragmentTransition.to(ErrorFragment.newInstance(
                                "Connection to server could not be established. Please try again later."),
                                Objects.requireNonNull(getActivity()), false,
                                R.id.main_fragment_container);
                    }
                }
            }

            @Override
            public void onFailure(Call<List<ProfileDto>> call, Throwable t) {
                FragmentTransition.to(ErrorFragment.newInstance(
                        "Connection to server could not be established. Please try again later."),
                        Objects.requireNonNull(getActivity()), false,
                        R.id.main_fragment_container);
            }
        });

    }
}
