package rs.ac.uns.ftn.medily.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PlaylistDto {

    @SerializedName("id")
    @Expose
    private Long id;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("share")
    @Expose
    private boolean share;

    @SerializedName("songs")
    @Expose
    private List<SongDto> songs;

    @SerializedName("createdBy")
    @Expose
    private String createdBy;

    public PlaylistDto() {
    }

    public PlaylistDto(Long id, String name, boolean share, List<SongDto> songs,
                       String createdBy) {
        this.id = id;
        this.name = name;
        this.share = share;
        this.songs = songs;
        this.createdBy = createdBy;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isShare() {
        return share;
    }

    public void setShare(boolean share) {
        this.share = share;
    }

    public List<SongDto> getSongs() {
        return songs;
    }

    public void setSongs(List<SongDto> songs) {
        this.songs = songs;
    }

    public String getCreatedBy() { return createdBy; }

    public void setCreatedBy(String createdBy) { this.createdBy = createdBy; }
}
