package rs.ac.uns.ftn.medily.sync;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Queue;

import lombok.SneakyThrows;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rs.ac.uns.ftn.medily.dto.PlaylistChangeDto;
import rs.ac.uns.ftn.medily.dto.PlaylistDto;
import rs.ac.uns.ftn.medily.dto.SongDto;
import rs.ac.uns.ftn.medily.model.Playlist;
import rs.ac.uns.ftn.medily.service.ServiceUtils;
import rs.ac.uns.ftn.medily.tools.Downloader;
import rs.ac.uns.ftn.medily.tools.SharedPreferencesService;
import rs.ac.uns.ftn.medily.tools.Util;
import rs.ac.uns.ftn.medily.tools.db.PlaylistRepository;
import rs.ac.uns.ftn.medily.tools.db.SongRepository;


public class SyncService extends Service {

//	private static String RESULT_CODE = "RESULT_CODE";
	private Queue<PlaylistChangeDto> playlistChanges;
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		
//		Intent ints = new Intent("SYNC_DATA");
		int status = Util.getConnectivityStatus(getApplicationContext());
//		ints.putExtra(RESULT_CODE, status);
		if(status == Util.TYPE_WIFI || status == Util.TYPE_MOBILE){
            final PlaylistChangeDto playlistChangeDto = new PlaylistChangeDto();
            playlistChangeDto.setId(123L);
            playlistChangeDto.setModified(new Date());

            String lastSync = SharedPreferencesService.get("last_sync" + "_" + SharedPreferencesService.get("username"));

            if (lastSync == null || lastSync.equals("")) {
            	lastSync = "01-01-1970 00:00:00";
			}

		    System.out.println("LAST SYNC: " + lastSync);
			Call<List<PlaylistChangeDto>> call = ServiceUtils.syncService.getChanges(lastSync);
			call.enqueue(new Callback<List<PlaylistChangeDto>>() {
				@Override
				public void onResponse(Call<List<PlaylistChangeDto>> call, Response<List<PlaylistChangeDto>> response) {
					if (response.code() == 200){
						SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss", Locale.UK);
						Date syncDate = new Date();
						SharedPreferencesService.add("last_sync" + "_" + SharedPreferencesService.get("username"), sdf.format(syncDate));
						playlistChanges = new LinkedList<>(response.body());
						handlePlaylistChange();
					}
				}

				@Override
				public void onFailure(Call<List<PlaylistChangeDto>> call, Throwable t) {
					Log.d("REZ", t.getMessage() != null?t.getMessage():"error");
				}
			});
		}

		stopSelf();
		
		return START_NOT_STICKY;
	}
	
	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}

	public void handlePlaylistChange() {

		System.out.println("\n\n\n\nHANDLING");
		System.out.println(playlistChanges);

		if (playlistChanges.isEmpty()) {
			return;
		}

		PlaylistChangeDto playlistChange = playlistChanges.poll();

		if (playlistChange == null) {
			return;
		}

		System.out.println(playlistChange);
		System.out.println("\n\n");

		switch (playlistChange.getPlaylistChangeType()) {
			case PLAYLIST_RENAMED:
				playlistRenamedHandler(playlistChange.getPlaylistId());
				break;
			case SONG_POSITION:
				playlistSongPositionChangedHandler(playlistChange.getPlaylistId());
				break;
			case SONG_ADDED:
				playlistSongAddedHandler(playlistChange.getPlaylistId(), playlistChange.getSongId());
				break;
			case SONG_DELETED:
				playlistSongDeletedHandler(playlistChange.getPlaylistId(), playlistChange.getSongId());
				break;
		}
	}


	public void playlistRenamedHandler(Long playlistId) {
		Call<PlaylistDto> call = ServiceUtils.mediaService.getPlaylistById(playlistId);
		call.enqueue(new Callback<PlaylistDto> () {
			@SneakyThrows
			@Override
			public void onResponse(Call<PlaylistDto> call, Response<PlaylistDto> response) {
				if (response.code() == 200) {
					PlaylistDto playlistDto = response.body();
					Playlist playlist = new Playlist();
					playlist.setId(playlistDto.getId());
					playlist.setName(playlistDto.getName());
					PlaylistRepository.updateName(playlist, getApplicationContext(), PlaylistRepository.COLUMN_ID + " = ?", new String[] { playlist.getId().toString() });
					handlePlaylistChange();
				} else if (response.code() == 400) {
					if (getApplicationContext() != null) {
						handlePlaylistChange();
					}
				} else {
					if (getApplicationContext() != null) {
						Toast.makeText(getApplicationContext(), "Connection to server could not be " +
										"established. Please try again later.",
								Toast.LENGTH_LONG).show();
					}
				}
			}

			@Override
			public void onFailure(Call<PlaylistDto> call, Throwable t) {
				Toast.makeText(getApplicationContext(), "Connection to server could not be " +
								"established. Please try again later.",
						Toast.LENGTH_LONG).show();
			}
		});

	}

	public void playlistSongAddedHandler(final Long playlistId, Long songId) {

		if (SongRepository.doesPlaylistSongExist(songId, playlistId, getApplicationContext())) {
			handlePlaylistChange();
			return;
		}

		final SyncService that = this;
		Call<SongDto> call = ServiceUtils.mediaService.getPlaylistSongById(playlistId, songId);
		call.enqueue(new Callback<SongDto> () {
			@SneakyThrows
			@Override
			public void onResponse(Call<SongDto> call, Response<SongDto> response) {
				if (response.code() == 200) {
					SongDto songDto = response.body();
					Downloader.downloadSong(getApplicationContext(), that, songDto, playlistId);
				} else if (response.code() == 400) {
					if (getApplicationContext() != null) {
						handlePlaylistChange();
					}
				} else {
					if (getApplicationContext() != null) {
						Toast.makeText(getApplicationContext(), "Connection to server could not be " +
										"established. Please try again later.",
								Toast.LENGTH_LONG).show();
					}
				}
			}

			@Override
			public void onFailure(Call<SongDto> call, Throwable t) {
				Toast.makeText(getApplicationContext(), "Connection to server could not be " +
								"established. Please try again later.",
						Toast.LENGTH_LONG).show();
			}
		});
	}

	public void playlistSongDeletedHandler(Long playlistId, Long songId) {
		PlaylistRepository.removePlaylistSong(playlistId, songId, getApplicationContext());
		handlePlaylistChange();
	}

	public void playlistSongPositionChangedHandler(Long playlistId) {
		Call<PlaylistDto> call = ServiceUtils.mediaService.getPlaylistById(playlistId);
		call.enqueue(new Callback<PlaylistDto> () {
			@SneakyThrows
			@Override
			public void onResponse(Call<PlaylistDto> call, Response<PlaylistDto> response) {
				if (response.code() == 200) {
					PlaylistDto playlistDto = response.body();
					PlaylistRepository.updatePlaylistSongPositions(playlistDto, getApplicationContext());
					handlePlaylistChange();
				} else if (response.code() == 400) {
					if (getApplicationContext() != null) {
						handlePlaylistChange();
					}
				} else {
					if (getApplicationContext() != null) {
						Toast.makeText(getApplicationContext(), "Connection to server could not be " +
										"established. Please try again later.",
								Toast.LENGTH_LONG).show();
					}
				}
			}

			@Override
			public void onFailure(Call<PlaylistDto> call, Throwable t) {
				Toast.makeText(getApplicationContext(), "Connection to server could not be " +
								"established. Please try again later.",
						Toast.LENGTH_LONG).show();
			}
		});
	}
}
