package rs.ac.uns.ftn.medily.model;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Playlist {

    private Long id;
    private String name;
    private boolean sharePlaylist;
    private String createdBy;
    private String downloadedBy;
    private List<Song> songs;

    private Long globalId; // id on server

    public Playlist(Long id, String name, String createdBy, boolean sharePlaylist,
                    String downloadedBy, Long globalId) {
        this.id = id;
        this.name = name;
        this.sharePlaylist = sharePlaylist;
        this.createdBy = createdBy;
        this.downloadedBy = downloadedBy;
        this.songs = new ArrayList<>();
        this.globalId = globalId;
    }

    public Playlist(Long id, String name, String createdBy, boolean sharePlaylist,
                    String downloadedBy, Long globalId, List<Song> songs) {
        this.id = id;
        this.name = name;
        this.sharePlaylist = sharePlaylist;
        this.createdBy = createdBy;
        this.downloadedBy = downloadedBy;
        this.songs = songs;
        this.globalId = globalId;
    }

    public Playlist(String name, boolean sharePlaylist, String createdBy, Long globalId) {
        this.name = name;
        this.sharePlaylist = sharePlaylist;
        this.createdBy = createdBy;
        this.songs = new ArrayList<>();
        this.globalId = globalId;
    }

    public Playlist(String name, boolean sharePlaylist, String createdBy, Long globalId,
                    String downloadedBy) {
        this.name = name;
        this.sharePlaylist = sharePlaylist;
        this.createdBy = createdBy;
        this.songs = new ArrayList<>();
        this.globalId = globalId;
        this.downloadedBy = downloadedBy;
    }
}
