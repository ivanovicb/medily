package rs.ac.uns.ftn.medily.sync;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import rs.ac.uns.ftn.medily.tools.SharedPreferencesService;

public class SyncManager {

    private PendingIntent pendingIntent;
    private AlarmManager alarmManager;
    private SyncReceiver syncReceiver;
    public static String SYNC_DATA = "SYNC_DATA";
    private static int SYNC_INTERVAL = 1000 * 60;   // 1 minute
    private boolean allowSync;
    private AppCompatActivity activity;
    private Context context;

    private SharedPreferences.OnSharedPreferenceChangeListener sharedPreferenceListener = new SharedPreferences.OnSharedPreferenceChangeListener() {
        @Override
        public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {
//            if (key.equals("sync")) {
//                if (Boolean.parseBoolean(SharedPreferencesService.get(key))) {
//                    setupAlarm();
//                }
//                else {
//                    pauseAlarm();
//                }
//            }
        }
    };

    public SyncManager(AppCompatActivity activity) {
        this.activity = activity;
        this.context = activity.getApplicationContext();
        setUpReceiver();

        allowSync = Boolean.parseBoolean(SharedPreferencesService.get("sync"));

        setupAlarm();

        SharedPreferencesService.getPreferences().registerOnSharedPreferenceChangeListener(sharedPreferenceListener);
    }

    private void setUpReceiver(){
        syncReceiver = new SyncReceiver();

        // Retrieve a PendingIntent that will perform a broadcast
        Intent alarmIntent = new Intent(context, SyncService.class);
        pendingIntent = PendingIntent.getService(context, 0, alarmIntent, 0);

        alarmManager = (AlarmManager) activity.getSystemService(Context.ALARM_SERVICE);
    }

    private void pauseAlarm() {
        if (alarmManager != null) {
            alarmManager.cancel(pendingIntent);
//            Toast.makeText(context, "Alarm Canceled", Toast.LENGTH_SHORT).show();
        }

        if (syncReceiver != null) {
            activity.unregisterReceiver(syncReceiver);
        }
    }

    private void setupAlarm() {
//        if (allowSync) {
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), SYNC_INTERVAL, pendingIntent);
//            Toast.makeText(context, "Alarm Set", Toast.LENGTH_SHORT).show();
//        }

        IntentFilter filter = new IntentFilter();
        filter.addAction(SYNC_DATA);

        activity.registerReceiver(syncReceiver, filter);
    }
}
