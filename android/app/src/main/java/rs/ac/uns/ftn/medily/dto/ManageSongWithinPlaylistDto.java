package rs.ac.uns.ftn.medily.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ManageSongWithinPlaylistDto implements Serializable {

    @SerializedName("playlistId")
    @Expose
    private Long playlistId;

    @SerializedName("songId")
    @Expose
    private Long songId;

    @SerializedName("action")
    @Expose
    private String action;

    @SerializedName("position")
    @Expose
    private int position;

    public ManageSongWithinPlaylistDto() {}

    public ManageSongWithinPlaylistDto(Long playlistId, Long songId, String action, int position) {
        this.playlistId = playlistId;
        this.songId = songId;
        this.action = action;
        this.position = position;
    }

    public Long getPlaylistId() { return playlistId; }

    public void setPlaylistId(Long playlistId) { this.playlistId = playlistId; }

    public Long getSongId() { return songId; }

    public void setSongId(Long songId) { this.songId = songId; }

    public String getAction() { return action; }

    public void setAction(String action) { this.action = action; }

    public int getPosition() { return position; }

    public void setPosition(int position) { this.position = position; }
}
