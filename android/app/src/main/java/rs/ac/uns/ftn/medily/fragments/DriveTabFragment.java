package rs.ac.uns.ftn.medily.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.os.ParcelFileDescriptor;
import android.provider.OpenableColumns;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Stack;

import lombok.SneakyThrows;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rs.ac.uns.ftn.medily.R;
import rs.ac.uns.ftn.medily.activities.ImageActivity;
import rs.ac.uns.ftn.medily.activities.MainActivity;
import rs.ac.uns.ftn.medily.activities.VideoActivity;
import rs.ac.uns.ftn.medily.adapters.DriveGridAdapter;
import rs.ac.uns.ftn.medily.dialogs.DeleteDialog;
import rs.ac.uns.ftn.medily.dialogs.DownloadDialog;
import rs.ac.uns.ftn.medily.dialogs.NewFolderDialog;
import rs.ac.uns.ftn.medily.dialogs.RenameDialog;
import rs.ac.uns.ftn.medily.dto.AddFileDto;
import rs.ac.uns.ftn.medily.dto.AddFolderDto;
import rs.ac.uns.ftn.medily.dto.RenameDto;
import rs.ac.uns.ftn.medily.events.LocalDriveChangeEvent;
import rs.ac.uns.ftn.medily.model.Drive;
import rs.ac.uns.ftn.medily.model.DriveType;
import rs.ac.uns.ftn.medily.model.File;
import rs.ac.uns.ftn.medily.model.Folder;
import rs.ac.uns.ftn.medily.model.Song;
import rs.ac.uns.ftn.medily.service.ServiceUtils;
import rs.ac.uns.ftn.medily.tools.Downloader;
import rs.ac.uns.ftn.medily.tools.FileSystemUtil;
import rs.ac.uns.ftn.medily.tools.FragmentTransition;
import rs.ac.uns.ftn.medily.tools.MusicPlayer;
import rs.ac.uns.ftn.medily.tools.Util;
import rs.ac.uns.ftn.medily.tools.SharedPreferencesService;
import rs.ac.uns.ftn.medily.tools.db.FileRepository;
import rs.ac.uns.ftn.medily.tools.db.SongRepository;


public class DriveTabFragment extends Fragment implements NewFolderDialog.NewFolderDialogListener,
        RenameDialog.RenameDialogListener, DeleteDialog.DeleteDialogListener, DownloadDialog.DownloadDialogListener {

    private List<Folder> folders;
    private List<File> files;

    private Stack<Long> stackParentId;
    private Stack<List<Folder>> stackFolders;
    private Stack<List<File>> stackFiles;
    private Stack<String> stackPath;

    private DriveGridAdapter driveGridAdapter;
    private GridView gridView;

    private DriveType driveType;
    private Drive drive;

    private String[] photoFileExtensions;
    private String[] audioFileExtensions;
    private String[] videoFileExtensions;

    private DriveTabFragment thisReference;

    private final static String ARG_DRIVE_TYPE = "driveType";
    private String localDriveDirectory;

    private final static String STORAGE = "storage";

    private Context context;

    public DriveTabFragment() {
    }

    public static DriveTabFragment newInstance(DriveType driveType) {

        DriveTabFragment fragment = new DriveTabFragment();
        Bundle args = new Bundle();
        args.putString(ARG_DRIVE_TYPE, driveType.name());
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        localDriveDirectory = getLocalDriveDirectoryName();

        thisReference = this;

        requireActivity().getOnBackPressedDispatcher().addCallback(this, backPressedCallback);

        photoFileExtensions = context.getResources().getStringArray(R.array.photo_file_extensions);
        audioFileExtensions = context.getResources().getStringArray(R.array.audio_file_extensions);
        videoFileExtensions = context.getResources().getStringArray(R.array.video_file_extensions);

        files = new ArrayList<>();
        folders = new ArrayList<>();

        if (getArguments() != null) {

            String driveType = getArguments().getString(ARG_DRIVE_TYPE);

            if (DriveType.LOCAL == DriveType.valueOf(driveType)) {
                EventBus.getDefault().register(this);

                String internalStoragePath = context.getFilesDir().toString();
                java.io.File internalLocalDriveDir = new java.io.File(internalStoragePath, localDriveDirectory);

                String externalStoragePath = Objects.requireNonNull(context.getExternalFilesDir(null)).getAbsolutePath();
                java.io.File externalLocalDriveDir = new java.io.File(externalStoragePath, localDriveDirectory);

                if (!internalLocalDriveDir.exists()) {
                    internalLocalDriveDir.mkdir();
                }

                if (!externalLocalDriveDir.exists()) {
                    externalLocalDriveDir.mkdir();
                }

                setLocalDrive();

                SharedPreferencesService.getPreferences().registerOnSharedPreferenceChangeListener(sharedPreferenceListener);

                return;
            }

            Call<Drive> call = ServiceUtils.driveService.getDrive();
            call.enqueue(new Callback<Drive>() {

                @Override
                public void onResponse(Call call, Response response) {

                    drive = (Drive) response.body();

                    if (drive == null) {
                        return;
                    }

                    folders = drive.getFolders();
                    files = drive.getFiles();

                    setGridView(folders, files);
                }

                @Override
                public void onFailure(Call<Drive> call, Throwable t) {
                    if (context != null) {
                        if (getActivity() != null) {
                            FragmentTransition.to(ErrorFragment.newInstance(
                                    "Connection to server could not be established. Please try again later."),
                                    getActivity(), false,
                                    R.id.main_fragment_container);
                        }
                    }
                }

            });
        }

    }

    private String getLocalDriveDirectoryName(){
        return "local_drive_user_" + SharedPreferencesService.get("user_id");
    }

    @Override
    public void onAttach(@androidx.annotation.NonNull Context context) {
        super.onAttach(context);

        this.context = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        stackParentId = new Stack<>();
        stackFolders = new Stack<>();
        stackFiles = new Stack<>();
        stackPath = new Stack<>();

        backPressedCallback.setEnabled(false);

        View view = inflater.inflate(R.layout.fragment_drive_tab, container, false);
        gridView = view.findViewById(R.id.drive_grid_view);

        FloatingActionButton fab = view.findViewById(R.id.fab_drive);
        fab.setOnClickListener(fabOnClickListener);

        if (getArguments() == null) {
            return view;
        }

        String driveType = getArguments().getString(ARG_DRIVE_TYPE);

        if (driveType == null) {
            return view;
        }

        this.driveType = DriveType.valueOf(driveType);

        if (driveType.equals(DriveType.LOCAL.name())) {
            fab.setVisibility(View.INVISIBLE);
        }

        setGridView(folders, files);

        return view;
    }

    private SharedPreferences.OnSharedPreferenceChangeListener sharedPreferenceListener = new SharedPreferences.OnSharedPreferenceChangeListener() {
        @Override
        public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {
            if (key.equals(STORAGE) && driveType == DriveType.LOCAL) {
                setLocalDrive();
                setGridView(folders, files);
            }
        }
    };

    private OnBackPressedCallback backPressedCallback = new OnBackPressedCallback(true) {
        @Override
        public void handleOnBackPressed() {
            if (stackFiles.size() == 1) {
                backPressedCallback.setEnabled(false);
            }
            setGridView(stackFolders.pop(), stackFiles.pop());
            stackPath.pop();
            stackParentId.pop();
        }
    };

    private AdapterView.OnItemClickListener itemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView parent, View view, int position, long id) {

            if (position < folders.size()) {
                Folder folder = folders.get(position);

                List<Folder> subFolders = folder.getSubFolders();
                List<File> subFiles = folder.getFiles();

                stackFolders.add(folders);
                stackFiles.add(files);
                stackPath.add(folder.getName());
                stackParentId.add(folder.getId());

                if (stackFiles.size() == 1 && !backPressedCallback.isEnabled()) {
                    backPressedCallback.setEnabled(true);
                }

                setGridView(subFolders, subFiles);
                driveGridAdapter.notifyDataSetChanged();
            } else {
                final File file = files.get(position - folders.size());

                if (driveType == DriveType.LOCAL) {

                    String extension = FilenameUtils.getExtension(file.getName());

                    if (isPhoto(extension)) {

                        Intent intent = new Intent(context, ImageActivity.class);
                        intent.putExtra("image", file.getPath());
                        context.startActivity(intent);

                    } else if (isAudio(extension)) {
                        try {
                            MainActivity main = ((MainActivity) context);
                            main.getNavigationListener().onNavigationItemSelected(main.getNavigationView().getMenu().getItem(1));
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    Fragment t = ((MainActivity) context).getSupportFragmentManager().findFragmentById(R.id.main_fragment_container);
                                    if (t != null) {
                                        List<File> fs = FileRepository.findFiles(FileRepository.COLUMN_PATH + " = ? ",
                                                new String[] {file.getPath()}, context);
                                        if (fs.size() == 1) {
                                            List<Song> songs = SongRepository.findSongs(SongRepository.COLUMN_FILE  + " = ?",
                                                    new String[] {fs.get(0).getId().toString()}, context);
                                            if (songs.size() == 1) {
                                                MusicFragment f = (MusicFragment) t;
                                                f.getViewPager().setCurrentItem(1);
                                                f.play(songs.get(0));
                                            } else {
                                                List<File> files = FileRepository.findFiles(FileRepository.COLUMN_GLOBAL_ID + " = ? ",
                                                        new String[] {fs.get(0).getGlobalId().toString()}, context);
                                                boolean found = false;
                                                List<Song> songsInDb = null;
                                                for (File f: files) {
                                                    songsInDb = SongRepository.findSongs(SongRepository.COLUMN_FILE  + " = ?",
                                                            new String[] {f.getId().toString()}, context);
                                                    if (songsInDb.size() == 1) {
                                                        found = true;
                                                        break;
                                                    }
                                                }
                                                if (found) {
                                                    MusicFragment f = (MusicFragment) t;
                                                    f.getViewPager().setCurrentItem(1);
                                                    f.play(songsInDb.get(0));
                                                } else {
                                                    if (getActivity() != null) {
                                                        Toast.makeText(getActivity(), "Song could not be played",
                                                                Toast.LENGTH_SHORT).show();
                                                    }
                                                }
                                            }
                                        } else {
                                            if (getActivity() != null) {
                                                Toast.makeText(getActivity(), "Song could not be played",
                                                        Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    }
                                }
                            }, 300);
                        } catch (Exception e) {
                            // ignore
                        }
                    } else if (isVideo(extension)) {
                        Intent intent = new Intent(context, VideoActivity.class);
                        intent.putExtra("video", file.getPath());
                        context.startActivity(intent);
                    } else {
                        Toast.makeText(context, file.getName(), Toast.LENGTH_SHORT).show();
                    }
                } else {

                    DownloadDialog downloadDialog = new DownloadDialog(context, position, thisReference);
                    downloadDialog.show();
                }
            }
        }
    };

    private boolean isPhoto(String extension) {
        for (String photoExtension : photoFileExtensions) {
            if (photoExtension.toLowerCase().equals(extension.toLowerCase())) {
                return true;
            }
        }
        return false;
    }

    private boolean isAudio(String extension) {
        for (String audioExtension : audioFileExtensions) {
            if (audioExtension.toLowerCase().equals(extension.toLowerCase())) {
                return true;
            }
        }
        return false;
    }

    private boolean isVideo(String extension) {
        for (String videoExtension : videoFileExtensions) {
            if (videoExtension.toLowerCase().equals(extension.toLowerCase())) {
                return true;
            }
        }
        return false;
    }

    private void setGridView(List<Folder> folders, List<File> files) {
        driveGridAdapter = new DriveGridAdapter(context, folders, files);
        gridView.setAdapter(driveGridAdapter);

        gridView.setOnItemClickListener(itemClickListener);
        gridView.setOnItemLongClickListener(itemLongClickListener);

        this.folders = folders;
        this.files = files;
    }

    private AdapterView.OnItemLongClickListener itemLongClickListener = new AdapterView.OnItemLongClickListener() {
        @Override
        public boolean onItemLongClick(AdapterView<?> parent, final View view, final int position, long id) {

            List<String> driveItems;
            if (driveType.equals(DriveType.LOCAL)) {
                driveItems = Arrays.asList(getResources().getStringArray(R.array.local_drive_item_options));
            } else {
                driveItems = Arrays.asList(getResources().getStringArray(R.array.remote_drive_item_options));
            }

            final CharSequence[] tags = driveItems.toArray(new String[0]);
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setItems(tags, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) {
                    switch (item) {
                        case 0:
                            onRename(position, view);
                            break;
                        case 1:
                            onDelete(position);
                            break;
                        case 2:
                            onDetails(position);
                            break;
                        case 3:
                            onDownload(position);
                            break;
                    }
                }
            });

            AlertDialog alertDialogObject = builder.create();
            ListView listView = alertDialogObject.getListView();
            listView.setDivider(new ColorDrawable(Color.GRAY));
            listView.setDividerHeight(1);
            alertDialogObject.show();
            return true;
        }
    };

    private void onRename(int position, View view) {
        RenameDialog renameDialog = new RenameDialog(context, position, this, view);
        renameDialog.show();
    }

    private void onDelete(int position) {
        DeleteDialog deleteDialog = new DeleteDialog(context, position, this);
        deleteDialog.show();
    }

    private void onDetails(int position) {
        List<String> details = new ArrayList<>();
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss", Locale.UK);

        if (position < folders.size()) {
            Folder folder = folders.get(position);

            details.add("Name: " + folder.getName());
            details.add("Created: " + sdf.format(folder.getCreated()));
            details.add("Path: " + this.getCurrentPath() + folder.getName());
        } else {
            File file = files.get(position - folders.size());

            details.add("Name: " + file.getName());
            details.add("Created: " + sdf.format(file.getCreated()));

            DecimalFormat dec = new DecimalFormat("#0.0");

            if (file.getSize() > 1000000000) {
                details.add("Size: " + dec.format(file.getSize() / 1000000000) + "GB");
            } else if (file.getSize() > 1000000) {
                details.add("Size: " + dec.format(file.getSize() / 1000000) + "MB");
            } else if (file.getSize() > 1000) {
                details.add("Size: " + dec.format(file.getSize() / 1000) + "KB");
            } else {
                details.add("Size: " + dec.format(file.getSize()) + "B");
            }

            details.add("Path: " + this.getCurrentPath() + file.getName());
        }

        final CharSequence[] tags = details.toArray(new String[0]);
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setItems(tags, null);
        AlertDialog alertDialogObject = builder.create();
        ListView listView = alertDialogObject.getListView();
        listView.setDivider(new ColorDrawable(Color.GRAY));
        listView.setDividerHeight(1);
        alertDialogObject.show();
    }

    private void onDownload(final int position) {
        if (Util.checkInternetAccess(context)) {
            if (position < folders.size()) {
                Folder driveFolder = folders.get(position);
                Downloader.downloadFolder(context, driveFolder, localDriveDirectory);
            } else {
                File driveFile = files.get(position - folders.size());
                Downloader.downloadFile(context, driveFile, localDriveDirectory);
            }
        }
    }

    private void updateSongDB(File file, String oldPath) {
        oldPath = oldPath.replace("'", "''"); // escape ' character
        FileRepository.update(file, context, "path='" + oldPath + "'", null);
    }

    private void deleteSongDB(String path) {
        String filePath = path.replace("'", "''"); // escape ' character
        List<File> dbFiles =  FileRepository.findFiles("path='" + filePath + "'", null, context);

        if(dbFiles.size() > 0){
            File dbFile = dbFiles.get(0);
            List<Song> songs = SongRepository.findSongs(SongRepository.COLUMN_FILE  + " = ?",
                    new String[] {dbFile.getId().toString()}, context);
            if (songs.size() == 1) {
                SongRepository.delete(context, SongRepository.COLUMN_ID + " = ?", new String[] {songs.get(0).getId().toString()}, songs.get(0));
            }
            FileRepository.delete(context, "_id="+dbFile.getId(), null);
        }

        if (MusicPlayer.allPlaying) {
            MusicPlayer.setSongsToPlay(SongRepository
                    .findUsersDownloadedSongs(SharedPreferencesService.get("username"), context));
        }
    }


    private View.OnClickListener fabOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            List<String> fabItems = Arrays.asList(getResources().getStringArray(R.array.fab_options));
            final CharSequence[] tags = fabItems.toArray(new String[0]);
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setItems(tags, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) {
                    switch (item) {
                        case 0:
                            onNewFolder();
                            break;
                        case 1:
                            onUploadFile();
                            break;
                    }
                }
            });
            AlertDialog alertDialogObject = builder.create();
            ListView listView = alertDialogObject.getListView();
            listView.setDivider(new ColorDrawable(Color.GRAY));
            listView.setDividerHeight(1);
            alertDialogObject.show();
        }
    };

    private void onNewFolder() {
        NewFolderDialog dialog = new NewFolderDialog(context, this, getCurrentPath());
        dialog.show();
    }

    @Override
    public void addNewFolder(final Folder folder) {

        if (doesNameExist(folder.getName())) {
            Toast.makeText(context, R.string.name_already_exists, Toast.LENGTH_LONG).show();
            return;
        }

        AddFolderDto addFolderDto = new AddFolderDto();
        addFolderDto.setNewFolder(folder);

        if (stackParentId.empty()) {
            addFolderDto.setRoot(true);
            addFolderDto.setParentId(drive.getId());
        } else {
            addFolderDto.setRoot(false);
            addFolderDto.setParentId(stackParentId.peek());
        }

        Call<Folder> call = ServiceUtils.driveService.addFolder(addFolderDto);
        call.enqueue(new Callback<Folder>() {
            @SneakyThrows
            @Override
            public void onResponse(Call<Folder> call, Response<Folder> response) {
                if (response.code() == 200) {
                    folders.add(response.body());
                    driveGridAdapter.notifyDataSetChanged();
                } else if (response.code() == 400) {
                    if (getContext() != null) {
                        Toast.makeText(getContext(), response.errorBody().string(),
                                Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(context, R.string.error_message, Toast.LENGTH_SHORT).show();
                    Log.e("Add folder response", response.errorBody().string());
                }
            }

            @Override
            public void onFailure(Call<Folder> call, Throwable t) {
                Toast.makeText(context, R.string.error_message, Toast.LENGTH_SHORT).show();
                Log.e("Add folder failure", Objects.requireNonNull(t.getMessage()));
            }
        });

    }

    public String getCurrentPath() {
        StringBuilder path = new StringBuilder();
        path.append("/");
        for (String element : stackPath) {
            path.append(element);
            path.append("/");
        }

        return path.toString();
    }

    private void onUploadFile() {

        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        startActivityForResult(intent, 10);
    }

    @SneakyThrows
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data == null) {
            return;
        }

        if (requestCode == 10) {

            ///// copy to cache (Android 10) /////////
            ParcelFileDescriptor parcelFileDescriptor = context.getContentResolver().openFileDescriptor(Objects.requireNonNull(data.getData()), "r", null);
            if (parcelFileDescriptor == null) {
                return;
            }
            InputStream inputStream = new FileInputStream(parcelFileDescriptor.getFileDescriptor());
            final java.io.File file = new java.io.File(context.getCacheDir(), getFileName(data.getData()));
            OutputStream outputStream = new FileOutputStream(file);
            IOUtils.copy(inputStream, outputStream);
            //////////////////////////////////////////

            final File dtoFile = new File();
            String fileName = file.getName();
            String tempFileName = fileName;
            // if name exists
            int cnt = 1;
            while (doesNameExist(tempFileName)) {
                tempFileName = "(" + cnt + ")_" + fileName;
                cnt += 1;
            }
            dtoFile.setName(tempFileName);
            dtoFile.setPath(getCurrentPath());
            dtoFile.setCreated(new Date());
            dtoFile.setSize(file.length());

            AddFileDto addFileDto = new AddFileDto();
            addFileDto.setNewFile(dtoFile);

            if (stackParentId.empty()) {
                addFileDto.setIsRoot(true);
                addFileDto.setParentId(drive.getId());
            } else {
                addFileDto.setIsRoot(false);
                addFileDto.setParentId(stackParentId.peek());
            }

            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            MultipartBody.Part body = null;
            try {
                body = MultipartBody.Part.createFormData("file", file.getName(), requestFile);
            } catch (IllegalArgumentException e) {
                Toast.makeText(context, R.string.error_message, Toast.LENGTH_SHORT).show();
                Log.e("Invalid data format.", e.getMessage());
                return;
            }

            if (body != null) {
                Call<File> call = ServiceUtils.driveService.addFile(body, addFileDto);
                call.enqueue(new Callback<File>() {
                    @SneakyThrows
                    @Override
                    public void onResponse(Call<File> call, Response<File> response) {
                        if (response.code() == 200) {
                            Toast.makeText(context, "File successfully uploaded", Toast.LENGTH_SHORT).show();
                            File addedFile = response.body();
                            addedFile.setGlobalId(addedFile.getId());
                            files.add(addedFile);
                            driveGridAdapter.notifyDataSetChanged();
                        } else if (response.code() == 400) {
                            if (context != null) {
                                Toast.makeText(context, response.errorBody().string(),
                                        Toast.LENGTH_LONG).show();
                            }
                        } else {
                            Toast.makeText(context, R.string.error_message, Toast.LENGTH_SHORT).show();
                            Log.e("Add file response", response.errorBody().string());
                        }
                    }

                    @Override
                    public void onFailure(Call<File> call, Throwable t) {
                        Toast.makeText(context, R.string.error_message, Toast.LENGTH_SHORT).show();
                        Log.e("Add file failure", Objects.requireNonNull(t.getMessage()));
                    }
                });
            }
        }
    }

    private String getFileName(Uri uri) {
        String name = "";
        Cursor returnCursor = context.getContentResolver().query(uri, null, null, null, null);
        if (returnCursor != null) {
            int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
            returnCursor.moveToFirst();
            name = returnCursor.getString(nameIndex);
            returnCursor.close();
        }
        return name;
    }

    @Override
    public void rename(final String name, int position) {

        if (doesNameExist(name)) {
            Toast.makeText(context, R.string.name_already_exists, Toast.LENGTH_LONG).show();
            return;
        }

        if (position < folders.size()) {
            final Folder folder = folders.get(position);

            if (driveType == DriveType.REMOTE) {
                RenameDto renameDto = new RenameDto();
                renameDto.setId(folder.getId());
                renameDto.setNewName(name);

                Call<ResponseBody> call = ServiceUtils.driveService.renameFolder(renameDto);
                call.enqueue(new Callback<ResponseBody>() {
                    @SneakyThrows
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.code() == 200) {
                            folder.setName(name);
                            driveGridAdapter.notifyDataSetChanged();
                        } else if (response.code() == 400) {
                            if (context != null) {
                                Toast.makeText(context, response.errorBody().string(),
                                        Toast.LENGTH_LONG).show();
                            }
                        } else {
                            Toast.makeText(context, R.string.error_message, Toast.LENGTH_SHORT).show();
                            Log.e("Rename folder response", response.errorBody().string());
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Toast.makeText(context, R.string.error_message, Toast.LENGTH_LONG).show();
                        Log.e("Rename folder failure", Objects.requireNonNull(t.getMessage()));
                    }
                });
            } else {
                String storagePath = FileSystemUtil.getStoragePath(context);
                String path = getCurrentPath();

                String localDriveFolderPath = storagePath + "/" + localDriveDirectory + path;

                java.io.File dir = new java.io.File(localDriveFolderPath, folder.getName());
                java.io.File renameToDir = new java.io.File(localDriveFolderPath, name);
                if (dir.renameTo(renameToDir)) {
                    renameFilesPathInFolder(folder, localDriveFolderPath + folder.getName(), localDriveFolderPath + name);
                    folder.setName(name);
                    driveGridAdapter.notifyDataSetChanged();
                } else {
                    Toast.makeText(context, R.string.error_message, Toast.LENGTH_LONG).show();
                }
            }
        } else {
            final File file = files.get(position - folders.size());
            String extension = FilenameUtils.getExtension(file.getName());
            String newName = name;
            if (extension != null) {
                if (extension.length() > 0) {
                    if (!name.endsWith(extension)) {
                        newName += "." + extension;
                    }
                }
            }
            final String finalName = newName;
            if (driveType == DriveType.REMOTE) {
                RenameDto renameDto = new RenameDto();
                renameDto.setId(file.getId());
                renameDto.setNewName(finalName);

                Call<ResponseBody> call = ServiceUtils.driveService.renameFile(renameDto);
                call.enqueue(new Callback<ResponseBody>() {
                    @SneakyThrows
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.code() == 200) {
                            file.setName(finalName);
                            driveGridAdapter.notifyDataSetChanged();
                        } else if (response.code() == 400) {
                            if (context != null) {
                                Toast.makeText(context, response.errorBody().string(),
                                        Toast.LENGTH_LONG).show();
                            }
                        } else {
                            Toast.makeText(context, R.string.error_message, Toast.LENGTH_SHORT).show();
                            Log.e("Rename file response", response.errorBody().string());
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Toast.makeText(context, R.string.error_message, Toast.LENGTH_SHORT).show();
                        Log.e("Rename file failure", Objects.requireNonNull(t.getMessage()));
                    }
                });
            } else {
                String storagePath = FileSystemUtil.getStoragePath(context);
                String path = getCurrentPath();
                java.io.File fileLocalDrive = new java.io.File(storagePath + "/" + localDriveDirectory + path, file.getName());
                java.io.File renameToFile = new java.io.File(storagePath + "/" + localDriveDirectory + path, finalName);
                if (fileLocalDrive.renameTo(renameToFile)) {
                    file.setName(finalName);
                    file.setPath(renameToFile.getAbsolutePath());

                    updateSongDB(file, fileLocalDrive.getAbsolutePath());
                    driveGridAdapter.notifyDataSetChanged();
                } else {
                    Toast.makeText(context, R.string.error_message, Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    private void renameFilesPathInFolder(Folder folder, String oldPathPart, String newPathPart){

        for(File file: folder.getFiles()){
            String oldPath = file.getPath();
            file.setPath(file.getPath().replace(oldPathPart, newPathPart));

            if(isAudio(FilenameUtils.getExtension(file.getName()))){
                File dbFile = new File();
                dbFile.setName(file.getName());
                dbFile.setPath(file.getPath());
                dbFile.setSize(file.getSize());
                dbFile.setCreated(new Date());
                updateSongDB(dbFile, oldPath);
            }
        }

        for(Folder subfolder: folder.getSubFolders()){
            renameFilesPathInFolder(subfolder, oldPathPart, newPathPart);
        }
    }

    private boolean doesNameExist(String name) {
        for (Folder folder : folders) {
            if (folder.getName().trim().equals(name.trim())) {
                return true;
            }
        }

        for (File file : files) {
            if (file.getName().equals(name)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void delete(final int position) {
        if (position < folders.size()) {
            if (driveType == DriveType.REMOTE) {
                Call<ResponseBody> call = ServiceUtils.driveService.deleteFolder(folders.get(position).getId());
                call.enqueue(new Callback<ResponseBody>() {
                    @SneakyThrows
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.code() == 200) {
                            folders.remove(position);
                            driveGridAdapter.notifyDataSetChanged();
                            Toast.makeText(context, response.body().string(), Toast.LENGTH_SHORT).show();
                        } else if (response.code() == 400) {
                            if (context != null) {
                                Toast.makeText(context, response.errorBody().string(),
                                        Toast.LENGTH_LONG).show();
                            }
                        } else {
                            Toast.makeText(context, R.string.error_message, Toast.LENGTH_SHORT).show();
                            Log.e("Delete folder response", response.errorBody().string());
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Toast.makeText(context, R.string.error_message, Toast.LENGTH_SHORT).show();
                        Log.e("Delete folder failure", Objects.requireNonNull(t.getMessage()));
                    }
                });
            } else {
                Folder folder = folders.get(position);
                String storagePath = FileSystemUtil.getStoragePath(context);
                String path = getCurrentPath();
                java.io.File dirToDelete = new java.io.File(storagePath + "/" + localDriveDirectory + path, folder.getName());

                if (deleteRecursive(dirToDelete)) {
                    folders.remove(position);
                    driveGridAdapter.notifyDataSetChanged();
                } else {
                    Toast.makeText(context, R.string.error_message, Toast.LENGTH_SHORT).show();
                }
            }
        } else {
            if (driveType == DriveType.REMOTE) {
                Call<ResponseBody> call = ServiceUtils.driveService.deleteFile(files.get(position - folders.size()).getId());
                call.enqueue(new Callback<ResponseBody>() {
                    @SneakyThrows
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.code() == 200) {
                            files.remove(position - folders.size());
                            driveGridAdapter.notifyDataSetChanged();
                            Toast.makeText(context, response.body().string(), Toast.LENGTH_SHORT).show();
                        } else if (response.code() == 400) {
                            if (context != null) {
                                Toast.makeText(context, response.errorBody().string(),
                                        Toast.LENGTH_LONG).show();
                            }
                        } else {
                            Toast.makeText(context, R.string.error_message, Toast.LENGTH_SHORT).show();
                            Log.e("Delete file response", response.errorBody().string());
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Toast.makeText(context, R.string.error_message, Toast.LENGTH_SHORT).show();
                        Log.e("Delete file failure", Objects.requireNonNull(t.getMessage()));
                    }
                });
            } else {
                File file = files.get(position - folders.size());
                String storagePath = FileSystemUtil.getStoragePath(context);
                String path = getCurrentPath();
                java.io.File fileToDelete = new java.io.File(storagePath + "/" + localDriveDirectory + path, file.getName());

                if (fileToDelete.delete()) {
                    files.remove(position - folders.size());
                    if(isAudio(FilenameUtils.getExtension(file.getName()))){
                        deleteSongDB(file.getPath());
                    }
                    driveGridAdapter.notifyDataSetChanged();
                } else {
                    Toast.makeText(context, R.string.error_message, Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private boolean deleteRecursive(java.io.File fileOrDirectory) {
        if (fileOrDirectory.isDirectory()) {
            for (java.io.File child : Objects.requireNonNull(fileOrDirectory.listFiles())) {
                deleteRecursive(child);
            }
        }
        else if(isAudio(FilenameUtils.getExtension(fileOrDirectory.getName()))){
            deleteSongDB(fileOrDirectory.getAbsolutePath());
        }
        return fileOrDirectory.delete();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLocalDriveChangeEvent(LocalDriveChangeEvent event) {
        // add to root
        if (event.getFolder() != null) {
            if (stackFolders.isEmpty()) {
                setLocalDrive();
                setGridView(folders, files);
            } else {
                stackFolders.firstElement().add(event.getFolder());
            }
        }
        if (event.getFile() != null) {
            if (stackFiles.isEmpty()) {
                setLocalDrive();
                setGridView(folders, files);
            } else {
                stackFiles.firstElement().add(event.getFile());
            }

        }
    }

    private void setLocalDrive() {
        String storagePath = FileSystemUtil.getStoragePath(context);
        java.io.File localDrive = new java.io.File(storagePath, localDriveDirectory);
        Folder root = new Folder();

        scanDirectory(localDrive, root);

        folders = root.getSubFolders();
        files = root.getFiles();
    }

    private void scanDirectory(java.io.File dir, Folder parentFolder) {
        java.io.File[] localFiles = dir.listFiles();

        if (localFiles == null) {
            return;
        }

        for (java.io.File localFile : localFiles) {
            if (localFile.isDirectory()) {
                Folder folder = new Folder();
                folder.setName(localFile.getName());
                folder.setCreated(new Date());
                folder.setPath(localFile.getAbsolutePath());
                parentFolder.getSubFolders().add(folder);

                scanDirectory(localFile, folder);
            } else {
                File file = new File();
                file.setName(localFile.getName());
                file.setCreated(new Date());
                file.setSize(localFile.length());
                file.setPath(localFile.getAbsolutePath());
                parentFolder.getFiles().add(file);
            }
        }
    }

    @Override
    public void downloadDialogResult(int position) {
        onDownload(position);
    }
}
