package rs.ac.uns.ftn.medily.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import rs.ac.uns.ftn.medily.R;
import rs.ac.uns.ftn.medily.dto.ProfileDto;

public class FamilyMembersAdapter extends BaseAdapter {

    private Context context;
    private LayoutInflater inflater;
    private List<ProfileDto> family;

    public FamilyMembersAdapter(Context con, List<ProfileDto> family) {
        context = con;
        this.family = family;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return family.size();
    }

    @Override
    public Object getItem(int i) {
        return family.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ProfileDto member = (ProfileDto) getItem(i);

        final ViewHolder holder;

        if (view == null) {
            view = inflater.inflate(R.layout.layout_member, viewGroup, false);
            holder = new ViewHolder();
            holder.name = view.findViewById(R.id.member_name);

            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        holder.name.setText(member.getName());
        view.setBackgroundColor(member.getRole().toLowerCase().equals("parent") ?
                view.getResources().getColor(R.color.colorOrange) :
                view.getResources().getColor(R.color.colorChild));
        return view;
    }


    private static class ViewHolder {
        private TextView name;
    }
}
