package rs.ac.uns.ftn.medily.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.android.material.switchmaterial.SwitchMaterial;

import lombok.SneakyThrows;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rs.ac.uns.ftn.medily.R;
import rs.ac.uns.ftn.medily.dto.NewPlaylistDto;
import rs.ac.uns.ftn.medily.dto.PlaylistDto;
import rs.ac.uns.ftn.medily.model.Playlist;
import rs.ac.uns.ftn.medily.model.Song;
import rs.ac.uns.ftn.medily.service.ServiceUtils;
import rs.ac.uns.ftn.medily.tools.SharedPreferencesService;
import rs.ac.uns.ftn.medily.tools.db.PlaylistRepository;

public class NewPlaylistDialog extends Dialog implements
        android.view.View.OnClickListener {

    private Song clickedSong;

    public NewPlaylistDialog(@NonNull Context context, Song clickedSong) {
        super(context);

        this.clickedSong = clickedSong;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.layout_new_playlist);

        Button cancel = (Button) findViewById(R.id.btn_new_playlist_cancel);
        cancel.setOnClickListener(this);
        final Button createPlaylist = (Button) findViewById(R.id.btn_create_playlist);
        createPlaylist.setOnClickListener(this);
        createPlaylist.setTextColor(Color.GRAY);

        final EditText input = (EditText) findViewById(R.id.new_playlist_name);
        input.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (input.getText().toString().length() > 0) {
                    createPlaylist.setEnabled(true);
                    createPlaylist.setTextColor(Color.BLACK);
                } else {
                    createPlaylist.setEnabled(false);
                    createPlaylist.setTextColor(Color.GRAY);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_create_playlist:
                createNewPlaylist();
                break;
            case R.id.btn_new_playlist_cancel:
                dismiss();
                break;
            default:
                break;
        }

        dismiss();
    }

    private void createNewPlaylist() {
        EditText nameField = (EditText) findViewById(R.id.new_playlist_name);
        final String name = nameField.getText().toString();

        SwitchMaterial sw = (SwitchMaterial) findViewById(R.id.share_playlist_toggle);
        final boolean sharePlaylist = sw.isChecked();

        Call<PlaylistDto> call = ServiceUtils.mediaService.createPlaylist(new NewPlaylistDto(name, sharePlaylist));
        call.enqueue(new Callback<PlaylistDto>() {
            @SneakyThrows
            @Override
            public void onResponse(Call<PlaylistDto> call, Response<PlaylistDto> response) {
                if (response.code() == 200) {
                    playlistCreated(name, sharePlaylist, response.body().getId());
                } else if (response.code() == 400) {
                    if (getContext() != null) {
                        Toast.makeText(getContext(), response.errorBody().string(),
                                Toast.LENGTH_LONG).show();
                    }
                } else {
                    if (getContext() != null) {
                        Toast.makeText(getContext(), response.errorBody().string(),
                                Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<PlaylistDto> call, Throwable t) {
                    Toast.makeText(getContext(), R.string.error_message,
                            Toast.LENGTH_LONG).show();
            }
        });
    }

    // playlistId - id on server
    private void playlistCreated(String name, boolean sharePlaylist, Long playlistId) {
        PlaylistRepository.add(new Playlist(name, sharePlaylist,
                SharedPreferencesService.get("username"), playlistId), getContext());

        if (clickedSong != null) {
            ManageSongDialog dialog = new ManageSongDialog(getContext(), clickedSong);
            dialog.show();
        }
    }
}
