package rs.ac.uns.ftn.medily.service;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import rs.ac.uns.ftn.medily.dto.AddFileDto;
import rs.ac.uns.ftn.medily.dto.AddFolderDto;
import rs.ac.uns.ftn.medily.dto.ChangePermissionDto;
import rs.ac.uns.ftn.medily.dto.ItemDto;
import rs.ac.uns.ftn.medily.dto.PermissionDto;
import rs.ac.uns.ftn.medily.dto.RenameDto;
import rs.ac.uns.ftn.medily.dto.SongDto;
import rs.ac.uns.ftn.medily.model.Drive;
import rs.ac.uns.ftn.medily.model.DrivePermissions;
import rs.ac.uns.ftn.medily.model.File;
import rs.ac.uns.ftn.medily.model.Folder;

public interface DriveService {

    @GET("/drive/get")
    Call<Drive> getDrive();

    @POST("/drive/permissions/get")
    Call<DrivePermissions> getDrivePermissions(@Body ItemDto dto);

    @POST("/drive/role")
    Call<ResponseBody> changeRole(@Body PermissionDto dto);

    @POST("/drive/permissions/set")
    Call<ResponseBody> changePermissions(@Body ChangePermissionDto dto);

    @POST("/drive/add/folder")
    Call<Folder> addFolder(@Body AddFolderDto addFolderDto);

    @Multipart
    @POST("/drive/add/file")
    Call<File> addFile(@Part MultipartBody.Part file, @Part("dto") AddFileDto addFileDto);

    @PUT("/drive/rename/file")
    Call<ResponseBody> renameFile(@Body RenameDto renameDto);

    @PUT("/drive/rename/folder")
    Call<ResponseBody> renameFolder(@Body RenameDto renameDto);

    @DELETE("/drive/delete/folder/{id}")
    Call<ResponseBody> deleteFolder(@Path("id") Long id);

    @DELETE("/drive/delete/file/{id}")
    Call<ResponseBody> deleteFile(@Path("id") Long id);

    @GET("/drive/download/file/{id}")
    Call<ResponseBody> downloadFile(@Path("id") Long id);

    @GET("/drive/download/folder/{id}")
    Call<ResponseBody> downloadFolder(@Path("id") Long id);

    @GET("/drive/download/playlist/{id}")
    Call<ResponseBody> downloadPlaylist(@Path("id") Long id);

    @GET("/drive/get/song/{fileId}")
    Call<SongDto> getSongByFileId(@Path("fileId") Long fileId);
}
