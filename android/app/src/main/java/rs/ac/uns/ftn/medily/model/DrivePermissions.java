package rs.ac.uns.ftn.medily.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DrivePermissions implements Serializable {
    @SerializedName("id")
    private Long id;

    @SerializedName("files")
    private List<FilePermission> files;

    @SerializedName("folders")
    private List<FolderPermission> folders;
}
