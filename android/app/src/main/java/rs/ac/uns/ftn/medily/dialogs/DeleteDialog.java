package rs.ac.uns.ftn.medily.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.NonNull;

import rs.ac.uns.ftn.medily.R;

public class DeleteDialog extends Dialog implements View.OnClickListener {

    private int position;
    private DeleteDialog.DeleteDialogListener deleteDialogListener;

    public DeleteDialog(@NonNull Context context, int position, DeleteDialogListener deleteDialogListener) {
        super(context);
        this.position = position;
        this.deleteDialogListener = deleteDialogListener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.layout_delete);

        Button buttonNo = findViewById(R.id.btn_drive_delete_no);
        buttonNo.setOnClickListener(this);
        Button buttonYes = findViewById(R.id.btn_drive_delete_yes);
        buttonYes.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btn_drive_delete_yes){
            delete();
        }
        dismiss();
    }

    private void delete() {
        deleteDialogListener.delete(position);
    }

    public interface DeleteDialogListener {
        void delete(int position);
    }
}
