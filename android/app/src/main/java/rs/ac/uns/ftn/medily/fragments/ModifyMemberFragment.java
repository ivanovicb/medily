package rs.ac.uns.ftn.medily.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Stack;

import lombok.SneakyThrows;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rs.ac.uns.ftn.medily.R;
import rs.ac.uns.ftn.medily.adapters.DrivePermissionsGridAdapter;
import rs.ac.uns.ftn.medily.dto.ChangePermissionDto;
import rs.ac.uns.ftn.medily.dto.ItemDto;
import rs.ac.uns.ftn.medily.dto.PermissionDto;
import rs.ac.uns.ftn.medily.dto.ProfileDto;
import rs.ac.uns.ftn.medily.model.DrivePermissions;
import rs.ac.uns.ftn.medily.model.FilePermission;
import rs.ac.uns.ftn.medily.model.FolderPermission;
import rs.ac.uns.ftn.medily.service.ServiceUtils;
import rs.ac.uns.ftn.medily.tools.FragmentTransition;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ModifyMemberFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ModifyMemberFragment extends Fragment {

    private String name;
    private String role;
    private String username;

    private List<FolderPermission> folders;
    private List<FilePermission> files;

    private Stack<Long> stackParentId;
    private Stack<List<FolderPermission>> stackFolders;
    private Stack<List<FilePermission>> stackFiles;
    private Stack<String> stackPath;

    private DrivePermissionsGridAdapter driveGridAdapter;
    private GridView gridView;
    private DrivePermissions drive;
    private View view;

    public ModifyMemberFragment() {
    }

    public static ModifyMemberFragment newInstance(ProfileDto dto) {
        ModifyMemberFragment fragment = new ModifyMemberFragment();
        Bundle args = new Bundle();
        args.putString("name", dto.getName());
        args.putString("role", dto.getRole());
        args.putString("username", dto.getUsername());
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Bundle args = getArguments();
        role = (String) args.get("role");
        name = (String) args.get("name");
        username = (String) args.get("username");

        super.onCreate(savedInstanceState);

        requireActivity().getOnBackPressedDispatcher().addCallback(this, backPressedCallback);

        files = new ArrayList<>();
        folders = new ArrayList<>();

        if (role.toLowerCase().equals("child")) {
            loadDrive();
        }
    }

    private void loadDrive() {
        Call<DrivePermissions> call = ServiceUtils.driveService.getDrivePermissions(new ItemDto(username));
        call.enqueue(new Callback<DrivePermissions>() {

            @Override
            public void onResponse(Call call, Response response) {
                if (response.code() == 200) {
                    drive = (DrivePermissions) response.body();

                    if (drive == null) {
                        return;
                    }

                    folders = drive.getFolders();
                    files = drive.getFiles();

                    setGridView(folders, files);
                } else if (response.code() == 400) {
                    if (getActivity() != null) {
                        try {
                            FragmentTransition.to(ErrorFragment.newInstance(response.errorBody().string()),
                                    getActivity(), false, R.id.main_fragment_container);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                } else {
                    if (getActivity() != null) {
                        FragmentTransition.to(ErrorFragment.newInstance(
                                "Connection to server could not be established. Please try again later."),
                                getActivity(), false,
                                R.id.main_fragment_container);
                    }
                }
            }

            @Override
            public void onFailure(Call<DrivePermissions> call, Throwable t) {
                if (getActivity() != null) {
                    FragmentTransition.to(ErrorFragment.newInstance(
                            "Connection to server could not be established. Please try again later."),
                            getActivity(), false,
                            R.id.main_fragment_container);
                }
            }
        });
    }

    private void changeRole(final String role) {
        Call<ResponseBody> call = ServiceUtils.driveService.changeRole(new PermissionDto(username, role.toUpperCase()));
        call.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call call, Response response) {
                if (response.code() == 200) {
                    adjustPermissionsView(role);
                    Toast.makeText(getContext(), "Role changed", Toast.LENGTH_SHORT).show();
                } else if (response.code() == 400) {
                    if (getActivity() != null) {
                        try {
                            FragmentTransition.to(ErrorFragment.newInstance(response.errorBody().string()),
                                    getActivity(), false, R.id.main_fragment_container);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                } else {
                    if (getActivity() != null) {
                        FragmentTransition.to(ErrorFragment.newInstance(
                                "Connection to server could not be established. Please try again later."),
                                getActivity(), false,
                                R.id.main_fragment_container);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (getActivity() != null) {
                    FragmentTransition.to(ErrorFragment.newInstance(
                            "Connection to server could not be established. Please try again later."),
                            getActivity(), false,
                            R.id.main_fragment_container);
                }
            }
        });
    }

    private void initListeners(View view) {
        RadioGroup radio = view.findViewById(R.id.modify_member_radio_role);
        if (role.toLowerCase().equals("child")) {
            ((RadioButton) view.findViewById(R.id.modify_member_radio_child)).setChecked(true);
            ((RadioButton) view.findViewById(R.id.modify_member_radio_parent)).setChecked(false);
        } else {
            ((RadioButton) view.findViewById(R.id.modify_member_radio_parent)).setChecked(true);
            ((RadioButton) view.findViewById(R.id.modify_member_radio_child)).setChecked(false);
        }
        radio.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton roleRadioButton = group.findViewById(checkedId);
                if (roleRadioButton.isChecked()) {
                    changeRole(roleRadioButton.getText().toString().toLowerCase());
                }
            }
        });
    }

    private void adjustPermissionsView(String chosenRole) {
        if (chosenRole.equals("child")) {
            this.view.findViewById(R.id.modify_member_grid_view).setVisibility(View.VISIBLE);
            loadDrive();
        } else {
            this.view.findViewById(R.id.modify_member_grid_view).setVisibility(View.GONE);
        }
    }

    @Override
    @SuppressWarnings("ConstantConditions")
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Bundle args = getArguments();
        name = (String) args.get("name");
        role = (String) args.get("role");
        username = (String) args.get("username");
        View view = inflater.inflate(R.layout.fragment_modify_member, container, false);
        if (getActivity() != null) {
            getActivity().setTitle("Manage " + name + "'s access");
            this.view = view;

            stackParentId = new Stack<>();
            stackFolders = new Stack<>();
            stackFiles = new Stack<>();
            stackPath = new Stack<>();

            backPressedCallback.setEnabled(false);
            gridView = view.findViewById(R.id.modify_member_grid_view);
            setGridView(folders, files);
            initListeners(view);
        }
        return view;
    }

    private OnBackPressedCallback backPressedCallback = new OnBackPressedCallback(true) {
        @Override
        public void handleOnBackPressed() {
            if(stackFiles.size() == 1){
                backPressedCallback.setEnabled(false);
            }
            setGridView(stackFolders.pop(), stackFiles.pop());
            stackPath.pop();
            stackParentId.pop();
        }
    };

    private AdapterView.OnItemClickListener itemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView parent, View view, int position, long id) {

            if(position < folders.size()){
                FolderPermission folder = folders.get(position);

                List<FolderPermission> subFolders = folder.getSubFolders();
                List<FilePermission> subFiles = folder.getFiles();

                stackFolders.add(folders);
                stackFiles.add(files);
                stackPath.add(folder.getName());
                stackParentId.add(folder.getId());

                if(stackFiles.size() == 1 && !backPressedCallback.isEnabled()){
                    backPressedCallback.setEnabled(true);
                }

                setGridView(subFolders, subFiles);
            }
            driveGridAdapter.notifyDataSetChanged();
        }
    };

    private static List<String> getDialogOptions(View view, Resources resources) {
        if (view.getTag().equals(R.drawable.border_allowed)) {
            return Arrays.asList(resources.getStringArray(R.array.modify_member_options_forbid));
        } else {
            return Arrays.asList(resources.getStringArray(R.array.modify_member_options_allow));
        }
    }

    private void changePermissions(final View view, String item, int position) {

        ChangePermissionDto dto = new ChangePermissionDto();
        dto.setUsername(username);
        final boolean allow = item.equals("Allow access");
        dto.setAllow(allow);

        if (position < folders.size()) {
            FolderPermission folder = folders.get(position);
            dto.setId(folder.getId());
            dto.setType("folder");
        }
        else {
            FilePermission file = files.get(position - folders.size());
            dto.setId(file.getId());
            dto.setType("file");
        }

        Call<ResponseBody> call = ServiceUtils.driveService.changePermissions(dto);
        call.enqueue(new Callback<ResponseBody>() {
            @SneakyThrows
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200) {
                    Toast.makeText(getActivity(), "Permission changed", Toast.LENGTH_SHORT).show();
                    if (getContext() != null) {
                        if (allow) {
                            view.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.border_allowed));
                            view.setTag(R.drawable.border_allowed);
                        } else {
                            view.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.border_forbidden));
                            view.setTag(R.drawable.border_forbidden);
                        }
                    }
                } else if (response.code() == 400) {
                    if (getActivity() != null) {
                        Toast.makeText(getActivity(), response.errorBody().string(),
                                Toast.LENGTH_LONG).show();
                    }
                } else {
                    if (getActivity() != null) {
                        Toast.makeText(getActivity(), "Connection to server could not be " +
                                        "established. Please try again later.",
                                Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(getActivity(), "Connection to server could not be " +
                                "established. Please try again later.",
                        Toast.LENGTH_LONG).show();
            }
        });
    }

    private AdapterView.OnItemLongClickListener itemLongClickListener = new AdapterView.OnItemLongClickListener() {
        @Override
        public boolean onItemLongClick(AdapterView<?> parent, final View view, final int position, long id) {

            final List<String> items = getDialogOptions(view, getResources());

            final CharSequence[] tags = items.toArray(new String[0]);
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setItems(tags, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) {
                    switch (item) {
                        case 0:
                            changePermissions(view, items.get(item), position);
                            break;
                        case 1:
                            onDetails(position);
                            break;
                    }
                }
            });

            AlertDialog alertDialogObject = builder.create();
            ListView listView=alertDialogObject.getListView();
            listView.setDivider(new ColorDrawable(Color.GRAY));
            listView.setDividerHeight(1);
            alertDialogObject.show();
            return true;
        }
    };

    private String getCurrentPath(){
        StringBuilder path = new StringBuilder();
        path.append("/");
        for(String element: stackPath){
            path.append(element);
            path.append("/");
        }

        return path.toString();
    }

    private void onDetails (int position) {
        List<String> details = new ArrayList<>();
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss", Locale.UK);

        if(position < folders.size()){
            FolderPermission folder = folders.get(position);

            details.add("Name: " + folder.getName());
            details.add("Created: " + sdf.format(folder.getCreated()));
            details.add("Path: " + this.getCurrentPath() + folder.getName());
        }
        else{
            FilePermission file = files.get(position - folders.size());

            details.add("Name: " + file.getName());
            details.add("Created: " + sdf.format(file.getCreated()));

            if (file.getSize() > 1000000000){
                details.add("Size: " + file.getSize() / 1000000000 + "GB");
            } else if (file.getSize() > 1000000) {
                details.add("Size: " + file.getSize() / 1000000 + "MB");
            } else if (file.getSize() > 1000) {
                details.add("Size: " + file.getSize() / 1000 + "KB");
            } else {
                details.add("Size: " + file.getSize() + "B");
            }

            details.add("Path: " + this.getCurrentPath() + file.getName());
        }

        final CharSequence[] tags = details.toArray(new String[0]);
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setItems(tags, null);
        AlertDialog alertDialogObject = builder.create();
        ListView listView=alertDialogObject.getListView();
        listView.setDivider(new ColorDrawable(Color.GRAY));
        listView.setDividerHeight(1);
        alertDialogObject.show();
    }

    private void setGridView(List<FolderPermission> folders, List<FilePermission> files){
        driveGridAdapter = new DrivePermissionsGridAdapter(getContext(), folders, files);
        gridView.setAdapter(driveGridAdapter);

        gridView.setOnItemClickListener(itemClickListener);
        gridView.setOnItemLongClickListener(itemLongClickListener);

        this.folders = folders;
        this.files = files;
    }

    public String getName() {
        return name;
    }

    public String getRole() {
        return role;
    }

    public String getUsername() {
        return username;
    }
}
