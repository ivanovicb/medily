package rs.ac.uns.ftn.medily.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import rs.ac.uns.ftn.medily.R;
import rs.ac.uns.ftn.medily.adapters.FamilyMembersAdapter;
import rs.ac.uns.ftn.medily.dto.ProfileDto;
import rs.ac.uns.ftn.medily.model.FamilyMember;
import rs.ac.uns.ftn.medily.tools.FragmentTransition;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ManageFamilyFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ManageFamilyFragment extends Fragment {

    private FloatingActionButton add;
    private List<ProfileDto> family;

    public ManageFamilyFragment() { }

    public static ManageFamilyFragment newInstance(List<ProfileDto> family) {
        ManageFamilyFragment fragment = new ManageFamilyFragment();
        Bundle args = new Bundle();
        args.putSerializable("family", (Serializable) family);
        fragment.setArguments(args);
        return fragment;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            family = (List<ProfileDto>) getArguments().getSerializable("family");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_manage_family, container, false);

        init(view);
        Objects.requireNonNull(getActivity()).setTitle("Manage family");
        return view;
    }

    private void init(View view) {
        add = view.findViewById(R.id.add_member);

        ListView listView = view.findViewById(R.id.family_members_list);
        FamilyMembersAdapter adapter = new FamilyMembersAdapter(getContext(), family);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                FragmentTransition.to(LoadFamilyMemberFragment.newInstance(family.get(position).getUsername()),
                        Objects.requireNonNull(getActivity()), true,
                        R.id.main_fragment_container);
            }
        });

        add.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                FragmentTransition.to(AddMemberFragment.newInstance(),
                        Objects.requireNonNull(getActivity()), true,
                        R.id.main_fragment_container);
            }
        });
    }
}
