package rs.ac.uns.ftn.medily.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.Nullable;

import rs.ac.uns.ftn.medily.R;
import rs.ac.uns.ftn.medily.dialogs.ManageSongDialog;
import rs.ac.uns.ftn.medily.model.Song;
import rs.ac.uns.ftn.medily.tools.MusicPlayer;
import rs.ac.uns.ftn.medily.tools.SharedPreferencesService;
import rs.ac.uns.ftn.medily.tools.Util;
import rs.ac.uns.ftn.medily.tools.db.SongRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class SongsAdapter extends BaseAdapter implements Filterable {

    private List<Song> data;
    private LayoutInflater layoutInflater;
    private Context context;

    public SongsAdapter(List<Song> data, Context context) {
        super();
        this.data = new ArrayList<>(data);
        this.context = context;
        this.layoutInflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        if (data != null) {
            return data.size();
        }

        return 0;
    }

    @Override
    public @Nullable Song getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return Objects.requireNonNull(getItem(position)).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        final Song song = data.get(position);

        if(view == null) {
            view = layoutInflater.inflate(R.layout.layout_song, null);
        }

        TextView songName = view.findViewById(R.id.song_name);
        TextView artist = view.findViewById(R.id.artist);
        TextView duration = view.findViewById(R.id.duration);

        if (MusicPlayer.currentlyPlaying != null) {
            if (MusicPlayer.currentlyPlaying.getId().equals(song.getId())) {
                view.setBackgroundColor(view.getResources().getColor(R.color.colorAccent));
                songName.setTextColor(view.getResources().getColor(R.color.colorWhite));
                artist.setTextColor(view.getResources().getColor(R.color.colorWhite));
                duration.setTextColor(view.getResources().getColor(R.color.colorWhite));
            } else {
                view.setBackgroundColor(view.getResources().getColor(R.color.colorWhite));
                songName.setTextColor(view.getResources().getColor(R.color.colorPrimary));
                artist.setTextColor(view.getResources().getColor(R.color.colorPrimary));
                duration.setTextColor(view.getResources().getColor(R.color.colorPrimary));
            }
        }

        songName.setText(Util.trimStringForView(song.getFile().getName()));
        artist.setText(song.getArtist());
        duration.setText(Util.convertToMinutesString(song.getDuration()));

        ImageButton btnPlus = view.findViewById(R.id.btn_plus);
        btnPlus.setFocusable(false);
        btnPlus.setFocusableInTouchMode(false);
        btnPlus.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ManageSongDialog dialog = new ManageSongDialog(v.getContext(), song);
                dialog.show();
            }
        });

        return  view;
    }

    private Filter filter = new Filter () {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            List<Song> filtered = performSearch(constraint);
            results.count = filtered.size();
            results.values = filtered;
            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            data = (List<Song>) results.values;
            notifyDataSetChanged();
        }
    };

    @Override
    public Filter getFilter() { return filter; }

    private List<Song> performSearch(CharSequence query) {
        query = query.toString().toLowerCase();
         return SongRepository.searchSongQuery(query.toString(), context,
                 SharedPreferencesService.get("username"));
    }

    public void refresh() {
        data = SongRepository.findSongs(SongRepository.COLUMN_DOWNLOADED_BY + " =?", new String[] {SharedPreferencesService.get("username") }, context);
        notifyDataSetChanged();
    }
}
