package rs.ac.uns.ftn.medily.adapters;

import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.HashMap;

import rs.ac.uns.ftn.medily.R;
import rs.ac.uns.ftn.medily.fragments.MusicFragment;
import rs.ac.uns.ftn.medily.fragments.PlaylistsFragment;
import rs.ac.uns.ftn.medily.fragments.PublicPlaylistsFragment;
import rs.ac.uns.ftn.medily.fragments.SongsFragment;

public class MusicTabsPagerAdapter extends FragmentPagerAdapter {

    private MusicFragment musicFragment;
    private HashMap<Integer, Fragment> fragmentsMap;

    @StringRes
    private static final int[] TAB_TITLES = new int[] { R.string.playlists, R.string.songs, R.string.public_playlists};

    public MusicTabsPagerAdapter(MusicFragment musicFragment, FragmentManager fragmentManager) {
        super(fragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        this.musicFragment = musicFragment;
        this.fragmentsMap = new HashMap<>();
    }

    @Override
    public void destroyItem (ViewGroup container, int position, Object object) {
        super.destroyItem(container, position, object);
        fragmentsMap.remove(position);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        Fragment fragment;
        switch (position) {
            case 0:
                fragment = PlaylistsFragment.newInstance();
                break;
            case 1:
                fragment = SongsFragment.newInstance();
                break;
            case 2:
                fragment = PublicPlaylistsFragment.newInstance();
                break;
            default:
                return null;
        }
        fragmentsMap.put(position, fragment);
        return fragment;
    }

    @Override
    public int getCount() { return TAB_TITLES.length; }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return musicFragment.getResources().getString(TAB_TITLES[position]);
    }

    public HashMap<Integer, Fragment> getFragmentsMap() {
        return fragmentsMap;
    }
}
