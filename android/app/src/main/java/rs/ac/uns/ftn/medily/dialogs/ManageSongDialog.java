package rs.ac.uns.ftn.medily.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import rs.ac.uns.ftn.medily.R;
import rs.ac.uns.ftn.medily.adapters.ManageSongAdapter;
import rs.ac.uns.ftn.medily.model.Song;
import rs.ac.uns.ftn.medily.tools.SharedPreferencesService;
import rs.ac.uns.ftn.medily.tools.db.PlaylistRepository;

public class ManageSongDialog extends Dialog implements
        android.view.View.OnClickListener {

    private Song clickedSong;

    public ManageSongDialog(@NonNull Context context, Song song) {
        super(context);
        this.clickedSong = song;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.layout_manage_song);

        Button cancel = (Button) findViewById(R.id.btn_cancel);
        cancel.setOnClickListener(this);
        Button addPlaylist = (Button) findViewById(R.id.btn_add_playlist);
        addPlaylist.setOnClickListener(this);

        TextView songName = (TextView) findViewById(R.id.song_name_dialog);
        String artist = "";
        if (clickedSong.getArtist() != null) {
            if (clickedSong.getArtist().length() > 0) {
                artist = clickedSong.getArtist() + " - ";
            }
        }
        String songDetails = artist + clickedSong.getFile().getName();
        songName.setText(songDetails);

        ManageSongAdapter adapter = new ManageSongAdapter(clickedSong, getContext(), PlaylistRepository
                .findPlaylistsMineAndShared(getContext(), SharedPreferencesService.get("username")));
        ListView listView = this.findViewById(R.id.playlists_manage);
        listView.setAdapter(adapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_add_playlist:
                NewPlaylistDialog dialog = new NewPlaylistDialog(v.getContext(), clickedSong);
                dialog.show();
                break;
            case R.id.btn_cancel:
                dismiss();
                break;
            default:
                break;
        }

        dismiss();
    }
}
