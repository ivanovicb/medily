package rs.ac.uns.ftn.medily.tools;

import android.content.Context;

import java.util.Objects;
import java.util.Stack;

import rs.ac.uns.ftn.medily.R;
import rs.ac.uns.ftn.medily.model.File;
import rs.ac.uns.ftn.medily.model.Folder;

public class FileSystemUtil {

    public final static String EXTERNAL_STORAGE = "External storage";

    private FileSystemUtil(){}

    public static String localDriveCreateFileName(java.io.File dir, String name) {
        java.io.File file = new java.io.File(dir, name);
        String fileName = name;
        int cnt = 1;
        while (file.exists()) {
            fileName = "(" + cnt + ")_" + name;
            file = new java.io.File(dir, fileName);
            cnt += 1;
        }
        return fileName;
    }

    public static boolean findLocalFilePath(Folder folder, Long id, Stack<String> path) {
        for (File file : folder.getFiles()) {
            if (file.getId().equals(id)) {
                path.add(file.getName());
                path.add(folder.getName());
                return true;
            }
        }

        for (Folder subFolder : folder.getSubFolders()) {
            boolean res = findLocalFilePath(subFolder, id, path);
            if (res) {
                path.add(folder.getName());
                return true;
            }
        }
        return false;
    }

    public static void createFoldersWithoutFiles(java.io.File parentDir, Folder folder) {
        if (folder.getFiles().isEmpty()) {
            String name = localDriveCreateFileName(parentDir, folder.getName());
            java.io.File dir = new java.io.File(parentDir, name);
            dir.mkdir();
        }
        if (!folder.getSubFolders().isEmpty()) {
            for (Folder subfolder : folder.getSubFolders()) {
                createFoldersWithoutFiles(new java.io.File(parentDir, folder.getName()), subfolder);
            }
        }
    }

    public static String getStoragePath(Context context) {
        String storage = SharedPreferencesService.get("storage");

        if (storage.equals(EXTERNAL_STORAGE)) {
            return Objects.requireNonNull(context.getExternalFilesDir(null)).getAbsolutePath();
        } else {
            return context.getFilesDir().toString();      // internal
        }
    }

    public static boolean isPhoto(Context context, String extension) {
        String[] photoFileExtensions = context.getResources().getStringArray(R.array.photo_file_extensions);

        for(String photoExtension: photoFileExtensions){
            if(photoExtension.toLowerCase().equals(extension.toLowerCase())){
                return true;
            }
        }
        return false;
    }

    public static boolean isAudio(Context context, String extension) {
        String[] audioFileExtensions = context.getResources().getStringArray(R.array.audio_file_extensions);

        for(String audioExtension: audioFileExtensions){
            if(audioExtension.toLowerCase().equals(extension.toLowerCase())){
                return true;
            }
        }
        return false;
    }

    public static boolean isVideo(Context context, String extension) {
        String[] videoFileExtensions = context.getResources().getStringArray(R.array.video_file_extensions);

        for(String videoExtension: videoFileExtensions){
            if(videoExtension.toLowerCase().equals(extension.toLowerCase())){
                return true;
            }
        }
        return false;
    }

    public static String getLocalDriveDirectoryName() {
        return "local_drive_user_" + SharedPreferencesService.get("user_id");
    }
}
