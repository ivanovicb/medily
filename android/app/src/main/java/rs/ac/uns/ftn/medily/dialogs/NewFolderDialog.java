package rs.ac.uns.ftn.medily.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;

import java.util.Date;

import rs.ac.uns.ftn.medily.R;
import rs.ac.uns.ftn.medily.model.Folder;

public class NewFolderDialog extends Dialog implements android.view.View.OnClickListener {

    private NewFolderDialogListener newFolderDialogListener;
    private String path;

    public NewFolderDialog(@NonNull Context context, NewFolderDialogListener newFolderDialogListener, String path) {
        super(context);
        this.newFolderDialogListener = newFolderDialogListener;
        this.path = path;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.layout_new_folder);

        Button buttonCancel = findViewById(R.id.btn_drive_new_folder_cancel);
        buttonCancel.setOnClickListener(this);
        final Button buttonOk = findViewById(R.id.btn_drive_new_folder_ok);
        buttonOk.setOnClickListener(this);
        buttonOk.setTextColor(Color.GRAY);

        final EditText input = (EditText) findViewById(R.id.drive_new_folder_text);
        input.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (input.getText().toString().length() > 0) {
                    buttonOk.setEnabled(true);
                    buttonOk.setTextColor(Color.BLACK);
                } else {
                    buttonOk.setEnabled(false);
                    buttonOk.setTextColor(Color.GRAY);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btn_drive_new_folder_ok){
            createNewFolder();
        }
        dismiss();
    }

    private void createNewFolder() {
        EditText editTextNewFolder = findViewById(R.id.drive_new_folder_text);
        String newFolderName = editTextNewFolder.getText().toString();

        Folder folder = new Folder();
        folder.setName(newFolderName);

        folder.setCreated(new Date());
        folder.setPath(path);

        newFolderDialogListener.addNewFolder(folder);
    }

    public interface NewFolderDialogListener {
        void addNewFolder(Folder folder);
    }
}
