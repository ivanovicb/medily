package rs.ac.uns.ftn.medily.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.fragment.app.Fragment;

import java.util.List;
import java.util.Objects;

import rs.ac.uns.ftn.medily.R;
import rs.ac.uns.ftn.medily.adapters.SongsAdapter;
import rs.ac.uns.ftn.medily.model.Song;
import rs.ac.uns.ftn.medily.tools.SharedPreferencesService;
import rs.ac.uns.ftn.medily.tools.db.SongRepository;

public class SongsFragment extends Fragment {

    private SongsAdapter adapter;

    public SongsFragment() {}

    public static SongsFragment newInstance() { return new SongsFragment(); }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_songs, container, false);
        ListView listView = view.findViewById(R.id.song_list);
        adapter = new SongsAdapter(SongRepository.findSongs(SongRepository.COLUMN_DOWNLOADED_BY + " = ?", new String[] {SharedPreferencesService.get("username") }, Objects.requireNonNull(getContext())), Objects.requireNonNull(getContext()));
        listView.setAdapter(adapter);
        listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

        listView.setOnItemClickListener(new ListView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Song clicked = null;
                List<Song> songs = SongRepository.findSongs(SongRepository.COLUMN_ID + " = ?",
                        new String[] { String.valueOf(id) }, getContext());
                if (songs.size() == 1) {
                    clicked = songs.get(0);
                }

                if (getParentFragment() != null && clicked != null) {
                    ((MusicFragment) getParentFragment()).play(clicked);
                }
            }
        });
        return view;
    }

    @Override
    public void onResume() {
        refresh();
        super.onResume();
    }

    public void searchSong(String query) {
        adapter.getFilter().filter(query);
    }

    public void refresh() {
        adapter.refresh();
    }
}
