package rs.ac.uns.ftn.medily.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Drive implements Serializable {
    @SerializedName("id")
    private Long id;

    @SerializedName("files")
    private List<File> files;

    @SerializedName("folders")
    private List<Folder> folders;

    public Drive() {
        files = new ArrayList<>();
        folders = new ArrayList<>();
    }

    public Drive(Long id, List<File> files, List<Folder> folders) {
        this.id = id;
        this.files = files;
        this.folders = folders;
    }
}
