package rs.ac.uns.ftn.medily.fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.util.Objects;

import lombok.SneakyThrows;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rs.ac.uns.ftn.medily.R;
import rs.ac.uns.ftn.medily.activities.LoginActivity;
import rs.ac.uns.ftn.medily.activities.RegistrationActivity;
import rs.ac.uns.ftn.medily.dto.NewMemberDto;
import rs.ac.uns.ftn.medily.dto.RegistrationDto;
import rs.ac.uns.ftn.medily.service.ServiceUtils;
import rs.ac.uns.ftn.medily.tools.FragmentTransition;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AddMemberFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AddMemberFragment extends Fragment {

    private EditText name;
    private EditText username;
    private EditText password;
    private EditText retyped;
    private RadioGroup role;
    private Button add;

    public AddMemberFragment() { }

    public static AddMemberFragment newInstance() {
        return new AddMemberFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_member, container, false);
        init(view);
        return view;
    }

    private void init(View view) {
        name = view.findViewById(R.id.add_name);
        username = view.findViewById(R.id.add_username);
        password = view.findViewById(R.id.add_password);
        retyped = view.findViewById(R.id.add_retype_password);
        role = view.findViewById(R.id.radio_role);
        add = view.findViewById(R.id.add_member_button);

        add.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int selectedId = role.getCheckedRadioButtonId();
                RadioButton roleRadioButton = role.findViewById(selectedId);

                if (validInput()) {
                    Call<ResponseBody> call = ServiceUtils.userService.createMember(
                            new NewMemberDto(name.getText().toString(),
                                    username.getText().toString(),
                                    password.getText().toString(),
                                    roleRadioButton.getText().toString()));
                    call.enqueue(new Callback<ResponseBody>() {
                        @SneakyThrows
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            if (response.code() == 200) {
                                Toast.makeText(getActivity(), "Family member " + name.getText().toString() +
                                        " successfully added!", Toast.LENGTH_SHORT).show();
                                FragmentTransition.to(LoadFamilyManagementFragment.newInstance(),
                                        Objects.requireNonNull(getActivity()), true,
                                        R.id.main_fragment_container);
                            } else if (response.code() == 400) {
                                Toast.makeText(getActivity(), response.errorBody().string(),
                                        Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(getActivity(), "Invalid operation, connection " +
                                                "to server could not be established.", Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            Toast.makeText(getActivity(), t.getMessage(),
                                    Toast.LENGTH_LONG).show();
                        }
                    });

                }
            }
        });
    }

    private boolean validInput() {
        if (name.getText().toString().length() < 1) {
            Toast.makeText(getActivity(), "Name not defined", Toast.LENGTH_SHORT).show();
            return false;
        } else if (username.getText().toString().length() < 1) {
            Toast.makeText(getActivity(), "Username not defined", Toast.LENGTH_SHORT).show();
            return false;
        } else if (password.getText().toString().length() < 1) {
            Toast.makeText(getActivity(), "Password not defined", Toast.LENGTH_SHORT).show();
            return false;
        } else if (retyped.getText().toString().length() < 1) {
            Toast.makeText(getActivity(), "Retyped password not defined",
                    Toast.LENGTH_SHORT).show();
            return false;
        } else if (!password.getText().toString().equals(retyped.getText().toString())) {
            Toast.makeText(getActivity(), "Password not same retyped password",
                    Toast.LENGTH_LONG).show();
            return false;
        } else if (role.getCheckedRadioButtonId() == -1) {
            Toast.makeText(getActivity(), "Role is not chosen", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }
}
