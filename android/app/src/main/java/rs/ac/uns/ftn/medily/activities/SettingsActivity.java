package rs.ac.uns.ftn.medily.activities;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.ListPreference;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.PreferenceManager;
import androidx.preference.SwitchPreferenceCompat;

import java.util.Objects;

import rs.ac.uns.ftn.medily.R;
import rs.ac.uns.ftn.medily.sync.SyncService;
import rs.ac.uns.ftn.medily.tools.SharedPreferencesService;
import rs.ac.uns.ftn.medily.tools.Util;

public class SettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_activity);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.settings, new SettingsFragment())
                .commit();
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    public static class SettingsFragment extends PreferenceFragmentCompat {
        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
            try {
                setPreferencesFromResource(R.xml.root_preferences, rootKey);

                Preference storage = findPreference(getString(R.string.key_storage));
                assert storage != null;
                if (Util.checkIfExternalMemoryExists()) {
                    ((ListPreference) storage).setEntries(getResources().getStringArray(R.array.storage_entries));
                    ((ListPreference) storage).setEntryValues(getResources().getStringArray(R.array.storage_values));
                } else {
                    ((ListPreference) storage).setEntries(getResources().getStringArray(R.array.storage_entries_internal));
                    ((ListPreference) storage).setEntryValues(getResources().getStringArray(R.array.storage_values_internal));
                }
                storage.setDefaultValue(SharedPreferencesService.get("storage"));
                storage.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                    @Override
                    public boolean onPreferenceChange(Preference preference, Object newValue) {
                        ListPreference listPreference = (ListPreference) preference;
                        int index = listPreference.findIndexOfValue(newValue.toString());
                        listPreference.setValueIndex(index);
                        SharedPreferencesService.add("storage", listPreference.getEntries()[index].toString());
                        return true;
                    }
                });
                Preference wifi = findPreference(getString(R.string.key_wifi));
                assert wifi != null;
                wifi.setDefaultValue(Boolean.parseBoolean(SharedPreferencesService.get("wifi")));
                wifi.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                    @Override
                    public boolean onPreferenceChange(Preference preference, Object newValue) {
                        boolean value = Boolean.parseBoolean(newValue.toString());
                        ((SwitchPreferenceCompat) preference).setChecked(value);
                        SharedPreferencesService.add("wifi", newValue.toString());
                        return true;
                    }
                });
                Preference sensors = findPreference(getString(R.string.key_sensors));
                assert sensors != null;
                sensors.setDefaultValue(Boolean.parseBoolean(SharedPreferencesService.get("sensors")));
                sensors.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                    @Override
                    public boolean onPreferenceChange(Preference preference, Object newValue) {
                        boolean value = Boolean.parseBoolean(newValue.toString());
                        ((SwitchPreferenceCompat) preference).setChecked(value);
                        SharedPreferencesService.add("sensors", newValue.toString());
                        return true;
                    }
                });
//                Preference sync = findPreference(getString(R.string.key_sync));
//                assert sync != null;
//                sync.setDefaultValue(Boolean.parseBoolean(SharedPreferencesService.get("sync")));
//                sync.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
//                    @Override
//                    public boolean onPreferenceChange(Preference preference, Object newValue) {
//                        boolean value = Boolean.parseBoolean(newValue.toString());
//                        ((SwitchPreferenceCompat) preference).setChecked(value);
//                        SharedPreferencesService.add("sync", newValue.toString());
//                        if (value) {
//                            Objects.requireNonNull(getActivity()).startService(new Intent(getContext(), SyncService.class));
//                        }
//                        return true;
//                    }
//                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            if (item.getItemId() == android.R.id.home) {
                if (getActivity() != null) {
                    getActivity().onBackPressed();
                }
            }
            return super.onOptionsItemSelected(item);
        }
    }
}