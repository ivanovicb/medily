package rs.ac.uns.ftn.medily.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import lombok.SneakyThrows;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rs.ac.uns.ftn.medily.R;
import rs.ac.uns.ftn.medily.dto.FeedbackDto;
import rs.ac.uns.ftn.medily.service.ServiceUtils;
import rs.ac.uns.ftn.medily.tools.SharedPreferencesService;

public class FeedbackActivity extends AppCompatActivity {

    private Menu menu;
    private EditText feedback;
    private MenuInflater menuInflater;

    public FeedbackActivity() {}

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        this.setTitle("Tell us what you think");
        init();
    }

    private void init() {
        feedback = findViewById(R.id.feedback_text);
        Button feedbackButton = findViewById(R.id.feedback_button);
        feedbackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendFeedback();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.basic_menu, menu);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            super.onBackPressed();
        }

        return true;
    }

    private boolean validInput() {
        if (feedback.getText().toString().length() < 1) {
            Toast.makeText(FeedbackActivity.this, "Feedback not defined",
                    Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void sendFeedback() {
        if (validInput()) {
            Call<ResponseBody> call = ServiceUtils.userService.sendFeedback(new FeedbackDto(SharedPreferencesService.get("username"),
                    feedback.getText().toString()));
            call.enqueue(new Callback<ResponseBody>() {
                @SneakyThrows
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.code() == 200) {
                        Toast.makeText(FeedbackActivity.this, "Feedback is successfully "
                                        + "submitted. Thank you for your effort to improve Medily!",
                                Toast.LENGTH_LONG).show();
                        FeedbackActivity.this.onBackPressed();
                    } else if (response.code() == 400) {
                        Toast.makeText(FeedbackActivity.this, response.errorBody().string(),
                                Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(FeedbackActivity.this, "Connection to server could not be " +
                                        "established. Please try again later.",
                                Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Toast.makeText(FeedbackActivity.this, "Connection to server could not be " +
                                    "established. Please try again later.",
                            Toast.LENGTH_LONG).show();
                }
            });
        }
    }
}
