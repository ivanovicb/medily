package rs.ac.uns.ftn.medily.sync;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import java.util.Date;
import java.util.Objects;


public class SyncReceiver extends BroadcastReceiver {
	
	@Override
	public void onReceive(Context context, Intent intent) {

		if(Objects.equals(intent.getAction(), SyncManager.SYNC_DATA)){
//			int resultCode = intent.getExtras().getInt(SyncService.RESULT_CODE);
//			System.out.println(resultCode);
			System.out.println("RECEIVED: " + new Date());
		}
	}
}
