package rs.ac.uns.ftn.medily.events;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import rs.ac.uns.ftn.medily.model.File;
import rs.ac.uns.ftn.medily.model.Folder;

@Getter
@Setter
@NoArgsConstructor
public class LocalDriveChangeEvent {

    private Folder folder;
    private File file;

    public LocalDriveChangeEvent(Folder folder, File file){
        this.folder = folder;
        this.file = file;
    }
}
