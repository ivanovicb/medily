package rs.ac.uns.ftn.medily.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PlaylistStatusDto {

    @SerializedName("id")
    @Expose
    private Long id;

    @SerializedName("status")
    @Expose
    private Boolean status;

    public PlaylistStatusDto() {
    }

    public PlaylistStatusDto(Long id, Boolean status) {
        this.id = id;
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }
}
