package rs.ac.uns.ftn.medily.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import lombok.SneakyThrows;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rs.ac.uns.ftn.medily.R;
import rs.ac.uns.ftn.medily.adapters.PublicPlaylistsAdapter;
import rs.ac.uns.ftn.medily.dto.PlaylistDto;
import rs.ac.uns.ftn.medily.dto.SongDto;
import rs.ac.uns.ftn.medily.model.File;
import rs.ac.uns.ftn.medily.model.Playlist;
import rs.ac.uns.ftn.medily.model.Song;
import rs.ac.uns.ftn.medily.service.ServiceUtils;
import rs.ac.uns.ftn.medily.tools.LoadingRotation;
import rs.ac.uns.ftn.medily.tools.SharedPreferencesService;
import rs.ac.uns.ftn.medily.tools.db.PlaylistRepository;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PublicPlaylistsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PublicPlaylistsFragment extends Fragment {

    private PublicPlaylistsAdapter publicPlaylistsAdapter;
    private View fragmentView;

    public PublicPlaylistsFragment() { }

    public static PublicPlaylistsFragment newInstance() {
        return new PublicPlaylistsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view =  inflater.inflate(R.layout.fragment_public_playlists, container, false);
        Handler handler = new Handler();
        fragmentView = view;
        init(view);
        handler.postDelayed(task, 100);
        return view;
    }

    private void init(View view) {
        ImageView image = view.findViewById(R.id.loading_image);
        image.startAnimation(LoadingRotation.rotation());
    }

    private Runnable task = new Runnable() {
        public void run() {

            // TESTING ONLY START
            // setLists();
            // TESTING ONLY END

            Call<List<PlaylistDto>> call = ServiceUtils.mediaService.getPublicPlaylists();
            call.enqueue(new Callback<List<PlaylistDto>>() {
                @SneakyThrows
                @Override
                public void onResponse(Call<List<PlaylistDto>> call, Response<List<PlaylistDto>> response) {
                    if (response.code() == 200) {
                        setLists(response.body());
                    } else if (response.code() == 400) {
                        if (getActivity() != null) {
                            Toast.makeText(getActivity(), response.errorBody().string(),
                                    Toast.LENGTH_LONG).show();
                        }
                    } else {
                        if (getActivity() != null) {
                            Toast.makeText(getActivity(), "Connection to server could not be " +
                                            "established. Please try again later.",
                                    Toast.LENGTH_LONG).show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<List<PlaylistDto>> call, Throwable t) {
                    Toast.makeText(getActivity(), "Connection to server could not be " +
                                    "established. Please try again later.",
                            Toast.LENGTH_LONG).show();
                }
            });

        }
    };

    private void setLists(List<PlaylistDto> data) {
        ExpandableListView listView = fragmentView.findViewById(R.id.public_playlists);

        if (getContext() != null) {
            List<Playlist> local = PlaylistRepository
                    .findPlaylistsMineAndShared(getContext(), SharedPreferencesService.get("username"));
            List<Playlist> playlists = new ArrayList<>();
            for (PlaylistDto dto: data) {
                boolean duplicate = false;
                for (Playlist playlist: local) {
                    if (dto.getId().equals(playlist.getGlobalId())) {
                        duplicate = true;
                        break;
                    }
                }
                if (!duplicate) {
                    List<Song> songs = new ArrayList<>();
                    for (SongDto song: dto.getSongs()) {
                        songs.add(new Song(song.getArtist(), song.getGenre(), song.getDuration(), "", new File(song.getFileId(), song.getName(), song.getFilePath(), 0, new Date(), song.getFileId()), dto.getId(),
                                dto.getId()));
                    }
                    playlists.add(new Playlist(dto.getId(), dto.getName(), dto.getCreatedBy(), true, "", dto.getId(), songs));
                }
            }
            publicPlaylistsAdapter = new PublicPlaylistsAdapter(playlists, getContext(), getActivity());
            listView.setAdapter(publicPlaylistsAdapter);
            fragmentView.findViewById(R.id.loading_public_playlists).setVisibility(View.GONE);
            listView.setVisibility(View.VISIBLE);
        }
    }

    public void searchPlaylist(String query) {
        publicPlaylistsAdapter.getFilter().filter(query);
    }

    public void refresh() {
        if (publicPlaylistsAdapter != null) {
            publicPlaylistsAdapter.refresh();
        }
    }

    @Override
    public void onResume() {
        refresh();
        super.onResume();
    }
}
