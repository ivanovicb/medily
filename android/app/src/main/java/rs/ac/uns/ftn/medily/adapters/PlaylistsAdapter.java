package rs.ac.uns.ftn.medily.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import rs.ac.uns.ftn.medily.R;
import rs.ac.uns.ftn.medily.model.Playlist;

public class PlaylistsAdapter extends BaseAdapter {

    private List<Playlist> data;
    private LayoutInflater layoutInflater;

    public PlaylistsAdapter(List<Playlist> data, Context context) {
        super();
        this.data = new ArrayList<>(data);
        this.layoutInflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public @Nullable Playlist getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        final Playlist playlist = data.get(position);

        if(view == null) {
            view = layoutInflater.inflate(R.layout.layout_playlist, null);
        }

        TextView playlistName = view.findViewById(R.id.playlist_name);
        playlistName.setText(playlist.getName());

        ImageButton btnPlus = view.findViewById(R.id.btn_manage_song);
        btnPlus.setFocusable(false);
        btnPlus.setFocusableInTouchMode(false);

        return  view;
    }
}
