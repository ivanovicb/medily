package rs.ac.uns.ftn.medily.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import rs.ac.uns.ftn.medily.R;
import rs.ac.uns.ftn.medily.fragments.MusicFragment;
import rs.ac.uns.ftn.medily.model.Playlist;
import rs.ac.uns.ftn.medily.tools.SharedPreferencesService;
import rs.ac.uns.ftn.medily.tools.Util;
import rs.ac.uns.ftn.medily.tools.db.PlaylistRepository;

public class PlaylistsSegmentAdapter extends BaseAdapter implements Filterable {

    private Context context;
    private List<Playlist> playlists;
    private MusicFragment playlistView;

    public PlaylistsSegmentAdapter(List<Playlist> playlists, Context context, MusicFragment playlistView) {
        this.context = context;
        this.playlists = playlists;
        this.playlistView = playlistView;
    }

    @Override
    public int getCount() {
        return playlists.size();
    }

    @Override
    public @Nullable
    Playlist getItem(int position) {
        return playlists.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public List<Playlist> getPlaylists() { return playlists; }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Playlist playlist = playlists.get(position);

        if (convertView == null) {
            LayoutInflater layoutInflater = LayoutInflater.from(context);
            convertView = layoutInflater.inflate(R.layout.layout_playlists_item, null);
        }

        TextView text = convertView.findViewById(R.id.text_playlist_item);
        text.setText(playlist.getName());

        ImageView image = (ImageView) convertView.findViewById(R.id.image_playlist_item);
        image.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                /*FragmentTransition.to(SongsInPlaylistFragment.newInstance(playlist),
                        (FragmentActivity) v.getContext(), true,
                        R.id.main_fragment_container);*/
                if (playlistView != null) {
                    playlistView.showPlaylist(playlist);
                }
            }
        });

        return convertView;
    }

    private Filter filter = new Filter () {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            List<Playlist> filtered = performSearch(constraint);
            results.count = filtered.size();
            results.values = filtered;
            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            playlists = (List<Playlist>) results.values;
            notifyDataSetChanged();
        }
    };

    @Override
    public Filter getFilter() {
        return filter;
    }

    private List<Playlist> performSearch(CharSequence query) {
        List<Playlist> resultPlaylists = new ArrayList<>();
        query = query.toString().toLowerCase();

        List<Playlist> playlists = PlaylistRepository
                .findPlaylistsMineAndShared(context, SharedPreferencesService.get("username"));

        for (Playlist playlist: playlists) {
            if (playlist.getName().toLowerCase().contains(query)) {
                resultPlaylists.add(playlist);
            }
        }
        return resultPlaylists;
    }

    public void refresh() {
        playlists = Util.filterPlaylists(PlaylistRepository.findPlaylistsMineAndShared(context,
                SharedPreferencesService.get("username")), SharedPreferencesService.get("username"));
        notifyDataSetChanged();
    }

    public void updatePlaylistUI(Long id, String newName) {
        for (int i = 0; i < playlists.size(); i++) {
            if (playlists.get(i).getId().equals(id)) {
                playlists.get(i).setName(newName);
                notifyDataSetChanged();
                break;
            }
        }
    }

    public void deletePlaylistUI(Playlist playlist) {
        for (int i = 0; i < playlists.size(); i++) {
            if (playlists.get(i).getId().equals(playlist.getId())) {
                playlists.remove(i);
                notifyDataSetChanged();
                break;
            }
        }
    }
}
