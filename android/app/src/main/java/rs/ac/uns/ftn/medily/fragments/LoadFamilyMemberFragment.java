package rs.ac.uns.ftn.medily.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.Objects;

import lombok.SneakyThrows;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rs.ac.uns.ftn.medily.R;
import rs.ac.uns.ftn.medily.dto.ItemDto;
import rs.ac.uns.ftn.medily.dto.ProfileDto;
import rs.ac.uns.ftn.medily.service.ServiceUtils;
import rs.ac.uns.ftn.medily.tools.FragmentTransition;
import rs.ac.uns.ftn.medily.tools.LoadingRotation;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link LoadFamilyMemberFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LoadFamilyMemberFragment extends Fragment {

    private ImageView image;
    private String username;

    public LoadFamilyMemberFragment() { }

    public static LoadFamilyMemberFragment newInstance(String username) {
        LoadFamilyMemberFragment fragment = new LoadFamilyMemberFragment();
        Bundle args = new Bundle();
        args.putString("username", username);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            this.username = getArguments().getString("username");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_load_family_member, container, false);
        init(view);
        return view;
    }

    private void init(View view) {
        image = view.findViewById(R.id.loading_image);
        image.startAnimation(LoadingRotation.rotation());
        loadFamily();
    }

    private void loadFamily() {
        Call<ProfileDto> call = ServiceUtils.userService.getMember(new ItemDto(username));
        call.enqueue(new Callback<ProfileDto>() {
            @SneakyThrows
            @Override
            public void onResponse(Call<ProfileDto> call, Response<ProfileDto> response) {
                if (getActivity() != null) {
                    if (response.code() == 200) {
                        FragmentTransition.to(ModifyMemberFragment.newInstance(response.body()),
                                Objects.requireNonNull(getActivity()), false,
                                R.id.main_fragment_container);
                    } else if (response.code() == 400) {
                        FragmentTransition.to(ErrorFragment.newInstance(response.errorBody().string()),
                                Objects.requireNonNull(getActivity()), false,
                                R.id.main_fragment_container);
                    } else {
                        FragmentTransition.to(ErrorFragment.newInstance(
                                "Connection to server could not be established. Please try again later."),
                                Objects.requireNonNull(getActivity()), false,
                                R.id.main_fragment_container);
                    }
                }
            }

            @Override
            public void onFailure(Call<ProfileDto> call, Throwable t) {
                FragmentTransition.to(ErrorFragment.newInstance(
                        "Connection to server could not be established. Please try again later."),
                        Objects.requireNonNull(getActivity()), false,
                        R.id.main_fragment_container);
            }
        });

    }
}