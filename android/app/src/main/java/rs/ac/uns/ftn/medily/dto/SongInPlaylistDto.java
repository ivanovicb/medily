package rs.ac.uns.ftn.medily.dto;

import rs.ac.uns.ftn.medily.model.Playlist;

public class SongInPlaylistDto {
    private Playlist playlist;
    private int position;

    public SongInPlaylistDto() {
    }

    public SongInPlaylistDto(Playlist playlist, int position) {
        this.playlist = playlist;
        this.position = position;
    }

    public Playlist getPlaylist() {
        return playlist;
    }

    public void setPlaylist(Playlist playlist) {
        this.playlist = playlist;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
