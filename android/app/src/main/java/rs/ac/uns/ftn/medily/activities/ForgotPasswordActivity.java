package rs.ac.uns.ftn.medily.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import lombok.SneakyThrows;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rs.ac.uns.ftn.medily.R;
import rs.ac.uns.ftn.medily.dto.ItemDto;
import rs.ac.uns.ftn.medily.service.ServiceUtils;

public class ForgotPasswordActivity extends AppCompatActivity {

    private EditText username;
    private Button forgot;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        this.setTitle("Forgot your password?");
        init();
    }

    private void init() {
        username = findViewById(R.id.forgot_username);
        forgot = findViewById(R.id.button_forgot);
        addListeners();
    }

    private void addListeners() {
        forgot.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (validInput()) {
                    Call<ResponseBody> call = ServiceUtils.userService.requestPasswordRenewal(new ItemDto(username.getText().toString()));
                    call.enqueue(new Callback<ResponseBody>() {
                        @SneakyThrows
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            if (response.code() == 200) {
                                Toast.makeText(ForgotPasswordActivity.this, "Email for password renewal"
                                        +  " is sent to parent's email address!", Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(ForgotPasswordActivity.this, LoginActivity.class);
                                ForgotPasswordActivity.this.startActivity(intent);
                                finish();
                            } else if (response.code() == 400) {
                                Toast.makeText(ForgotPasswordActivity.this, response.errorBody().string(),
                                        Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(ForgotPasswordActivity.this, "Invalid request!",
                                        Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            Toast.makeText(ForgotPasswordActivity.this, t.getMessage(),
                                    Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        });
    }

    private boolean validInput() {
        if (username.getText().toString().length() < 1) {
            Toast.makeText(ForgotPasswordActivity.this, "Username not defined",
                    Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }
}
