package rs.ac.uns.ftn.medily.tools;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.ImageButton;

import androidx.fragment.app.Fragment;

import java.util.List;

import rs.ac.uns.ftn.medily.R;
import rs.ac.uns.ftn.medily.activities.MainActivity;
import rs.ac.uns.ftn.medily.fragments.MusicFragment;
import rs.ac.uns.ftn.medily.fragments.PlaylistsFragment;
import rs.ac.uns.ftn.medily.fragments.PublicPlaylistsFragment;
import rs.ac.uns.ftn.medily.fragments.SongsFragment;

public class MusicFragmentNotificationListener extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getStringExtra("action");
        if (action != null) {
            Activity activity;
            try {
                activity = (Activity) context;
            } catch (Exception e) {
                return;
            }
            boolean performed = false;
            if (activity instanceof MainActivity) {
                Fragment fragment = getVisibleFragment((MainActivity) activity);
                if (fragment != null) {
                    if (fragment.getActivity() != null) {
                        switch (action) {
                            case "play": {
                                ImageButton button = fragment.getActivity().findViewById(R.id.btn_play_pause_player);
                                if (button != null) {
                                    button.performClick();
                                    performed = true;
                                }
                                break;
                            }
                            case "next": {
                                ImageButton button = fragment.getActivity().findViewById(R.id.btn_next_player);
                                if (button != null) {
                                    button.performClick();
                                    performed = true;
                                }
                                break;
                            }
                            case "prev": {
                                ImageButton button = fragment.getActivity().findViewById(R.id.btn_prev_player);
                                if (button != null) {
                                    button.performClick();
                                    performed = true;
                                }
                                break;
                            }
                        }
                    }
                }
            }

            if (!performed) {
                switch (action) {
                    case "play": {
                        if (MusicPlayer.mediaPlayer.isPlaying()) {
                            MusicPlayer.pause();
                        } else {
                            MusicPlayer.playCurrent();
                        }
                        break;
                    }
                    case "next": {
                        MusicPlayer.nextSong((Activity) MainActivity.context);
                        break;
                    }
                    case "prev": {
                        MusicPlayer.previousSong((Activity) MainActivity.context);
                        break;
                    }
                }
            }
        }
    }

    private Fragment getVisibleFragment(MainActivity activity) {
        if (activity != null) {
            Fragment f = activity.getSupportFragmentManager().findFragmentById(R.id.main_fragment_container);
            if (f != null) {
                if (f.isVisible() && f instanceof MusicFragment) {
                    return f;
                }
            }
        }
        return null;
    }
}
