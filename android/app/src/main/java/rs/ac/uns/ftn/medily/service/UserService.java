package rs.ac.uns.ftn.medily.service;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import rs.ac.uns.ftn.medily.dto.FeedbackDto;
import rs.ac.uns.ftn.medily.dto.ItemDto;
import rs.ac.uns.ftn.medily.dto.LoginDto;
import rs.ac.uns.ftn.medily.dto.LoginResultDto;
import rs.ac.uns.ftn.medily.dto.NewMemberDto;
import rs.ac.uns.ftn.medily.dto.ProfileDto;
import rs.ac.uns.ftn.medily.dto.RegistrationDto;

public interface UserService {

    @POST("/users/login")
    Call<LoginResultDto> login(@Body LoginDto dto);

    @POST("/users/registration")
    Call<ResponseBody> registration(@Body RegistrationDto dto);

    @GET("/users/profile")
    Call<ProfileDto> profile();

    @POST("/users/renewal/request")
    Call<ResponseBody> requestPasswordRenewal(@Body ItemDto item);

    @POST("/users/password/change")
    Call<ResponseBody> changePassword(@Body ItemDto item);

    @GET("/users/family")
    Call<List<ProfileDto>> getFamily();

    @POST("/users/member")
    Call<ProfileDto> getMember(@Body ItemDto item);

    @POST("/users/member/new")
    Call<ResponseBody> createMember(@Body NewMemberDto member);

    @GET("/tests/test/{type}")
    Call<ResponseBody> getTestFile(@Path("type") String type);

    @POST("/users/feedback")
    Call<ResponseBody> sendFeedback(@Body FeedbackDto username);
}
