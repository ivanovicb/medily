package rs.ac.uns.ftn.medily.tools.db;

import android.content.Context;

public class DbContext {

    private static DatabaseHelper database;

    public static DatabaseHelper getDB(Context context) {
        if (database == null) {
            database = new DatabaseHelper(context);
        }
        return database;
    }
}
