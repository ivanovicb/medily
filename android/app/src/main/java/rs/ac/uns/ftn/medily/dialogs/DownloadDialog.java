package rs.ac.uns.ftn.medily.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.NonNull;

import rs.ac.uns.ftn.medily.R;

public class DownloadDialog extends Dialog implements View.OnClickListener {
    private int position;
    private DownloadDialog.DownloadDialogListener downloadDialogListener;

    public DownloadDialog(@NonNull Context context, int position, DownloadDialog.DownloadDialogListener downloadDialogListener) {
        super(context);
        this.position = position;
        this.downloadDialogListener = downloadDialogListener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.layout_download);

        Button buttonNo = findViewById(R.id.btn_drive_download_no);
        buttonNo.setOnClickListener(this);
        Button buttonYes = findViewById(R.id.btn_drive_download_yes);
        buttonYes.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btn_drive_download_yes){
            download();
        }
        dismiss();
    }

    private void download() {
        downloadDialogListener.downloadDialogResult(position);
    }

    public interface DownloadDialogListener {
        void downloadDialogResult(int position);
    }

}
