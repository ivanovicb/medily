package rs.ac.uns.ftn.medily.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import lombok.SneakyThrows;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rs.ac.uns.ftn.medily.R;
import rs.ac.uns.ftn.medily.adapters.PlaylistsSegmentAdapter;
import rs.ac.uns.ftn.medily.dialogs.DeleteDialog;
import rs.ac.uns.ftn.medily.dialogs.NewPlaylistDialog;
import rs.ac.uns.ftn.medily.dialogs.RenameDialog;
import rs.ac.uns.ftn.medily.dto.ItemDto;
import rs.ac.uns.ftn.medily.dto.RenameDto;
import rs.ac.uns.ftn.medily.model.Playlist;
import rs.ac.uns.ftn.medily.service.ServiceUtils;
import rs.ac.uns.ftn.medily.tools.Downloader;
import rs.ac.uns.ftn.medily.tools.SharedPreferencesService;
import rs.ac.uns.ftn.medily.tools.Util;
import rs.ac.uns.ftn.medily.tools.db.PlaylistRepository;

public class PlaylistsFragment extends Fragment implements RenameDialog.RenameDialogListener,
        DeleteDialog.DeleteDialogListener {

    private PlaylistsSegmentAdapter playlistsAdapter;

    public PlaylistsFragment() {}

    public PlaylistsFragment(int contentLayoutId) {
        super(contentLayoutId);
    }

    public static PlaylistsFragment newInstance() { return new PlaylistsFragment(); }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Objects.requireNonNull(getActivity()).setTitle("Your music box");
        View view = inflater.inflate(R.layout.fragment_playlists, container, false);
        GridView gridView = view.findViewById(R.id.playlists_grid_view);

        List<Playlist> filtered = Util.filterPlaylists(
                PlaylistRepository.findPlaylistsMineAndShared(getContext(),
                        SharedPreferencesService.get("username")),
                SharedPreferencesService.get("username"));

        playlistsAdapter = new PlaylistsSegmentAdapter(filtered,
                Objects.requireNonNull(getContext()), (MusicFragment) getParentFragment());
        gridView.setAdapter(playlistsAdapter);
        gridView.setOnItemLongClickListener(itemLongClickListener);

        FloatingActionButton addPlaylistButton = view.findViewById(R.id.add_playlist_button);
        addPlaylistButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NewPlaylistDialog dialog = new NewPlaylistDialog(v.getContext(), null);
                dialog.show();
                dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        if (playlistsAdapter != null) {
                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                public void run() {
                                    refreshAdapter();
                                }
                            }, 1000);
                        }
                    }
                });
            }
        });
        return view;
    }

    private void refreshAdapter() {
        int size = playlistsAdapter.getCount();
        playlistsAdapter.refresh();
        int newSize = playlistsAdapter.getCount();
        if (size == newSize) {
            for (int i = 0; i < 3; i++) {
                size = playlistsAdapter.getCount();
                playlistsAdapter.refresh();
                newSize = playlistsAdapter.getCount();
                if (size != newSize) {
                    break;
                } else {
                    try {
                        Thread.sleep(200);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    private AdapterView.OnItemLongClickListener itemLongClickListener = new AdapterView.OnItemLongClickListener() {
        @Override
        public boolean onItemLongClick(AdapterView<?> parent, final View view, final int position, long id) {
            List<String> options = Arrays.asList(getResources().getStringArray(R.array.playlist_options));

            final CharSequence[] tags = options.toArray(new String[0]);
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setItems(tags, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) {
                    switch (item) {
                        case 0:
                            onRename(position, view);
                            break;
                        case 1:
                            onDelete(position);
                            break;
                    }
                }
            });

            AlertDialog alertDialogObject = builder.create();
            ListView listView = alertDialogObject.getListView();
            listView.setDivider(new ColorDrawable(Color.GRAY));
            listView.setDividerHeight(1);
            alertDialogObject.show();
            return true;
        }
    };

    public void searchPlaylist(String query) {
        playlistsAdapter.getFilter().filter(query);
    }

    public void refresh() {
        playlistsAdapter.refresh();
    }

    private void onRename(int position, View view){
        RenameDialog renameDialog = new RenameDialog(Objects.requireNonNull(getContext()), position, this, view);
        renameDialog.show();
    }

    private void onDelete(int position){
        DeleteDialog deleteDialog = new DeleteDialog(Objects.requireNonNull(getContext()), position, this);
        deleteDialog.show();
    }

    @Override
    public void onResume() {
        if (playlistsAdapter != null) {
            playlistsAdapter.refresh();
        }
        super.onResume();
    }

    @Override
    public void delete(int position) {
        final Playlist playlist = playlistsAdapter.getItem(position);
        if (playlist != null) {
            Call<ResponseBody> call = ServiceUtils.mediaService
                    .deletePlaylist(new ItemDto(playlist.getGlobalId().toString()));

            call.enqueue(new Callback<ResponseBody>() {
                @SneakyThrows
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.code() == 200) {
                        if (getContext() != null) {
                            PlaylistRepository.deletePlaylist(playlist, getContext());
                            Toast.makeText(getContext(), "Playlist deleted", Toast.LENGTH_LONG).show();
                            Downloader.downloadingPlaylists.remove(playlist.getGlobalId());
                            playlistsAdapter.deletePlaylistUI(playlist);
                        }
                    } else if (response.code() == 400) {
                        if (getContext() != null) {
                            Toast.makeText(getContext(), response.errorBody().string(),
                                    Toast.LENGTH_LONG).show();
                        }
                    } else {
                        if (getActivity() != null) {
                            Toast.makeText(getActivity(), R.string.error_message,
                                    Toast.LENGTH_LONG).show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Toast.makeText(getActivity(), R.string.error_message,
                            Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    @Override
    public void rename(final String name, int position) {
        final Playlist playlist = playlistsAdapter.getItem(position);
        if (playlist != null) {
            Call<ResponseBody> call = ServiceUtils.mediaService.renamePlaylist(new RenameDto(playlist.getGlobalId(), name));

            call.enqueue(new Callback<ResponseBody>() {
                @SneakyThrows
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.code() == 200) {
                        playlist.setName(name);
                        PlaylistRepository.update(playlist, getContext(), PlaylistRepository.COLUMN_ID + " = ?", new String[] { playlist.getId().toString() });
                        Toast.makeText(getContext(), "Playlist name changed.", Toast.LENGTH_LONG).show();
                        playlistsAdapter.updatePlaylistUI(playlist.getId(), name);
                    } else if (response.code() == 400) {
                        if (getContext() != null) {
                            Toast.makeText(getContext(), response.errorBody().string(),
                                    Toast.LENGTH_LONG).show();
                        }
                    } else {
                        if (getActivity() != null) {
                            Toast.makeText(getActivity(), R.string.error_message,
                                    Toast.LENGTH_LONG).show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Toast.makeText(getActivity(), R.string.error_message,
                            Toast.LENGTH_LONG).show();
                }
            });
        }
    }
}
