package rs.ac.uns.ftn.medily.service;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import rs.ac.uns.ftn.medily.dto.ItemDto;
import rs.ac.uns.ftn.medily.dto.ManageSongWithinPlaylistDto;
import rs.ac.uns.ftn.medily.dto.NewPlaylistDto;
import rs.ac.uns.ftn.medily.dto.PlaylistDto;
import rs.ac.uns.ftn.medily.dto.PlaylistStatusDto;
import rs.ac.uns.ftn.medily.dto.RenameDto;
import rs.ac.uns.ftn.medily.dto.SongDto;
import rs.ac.uns.ftn.medily.dto.SongFileDto;
import rs.ac.uns.ftn.medily.dto.UpdateSongPositionDto;

public interface MediaService {

    @POST("/media/playlists/new")
    Call<PlaylistDto> createPlaylist(@Body NewPlaylistDto dto);

    @GET("/media/playlists/public")
    Call<List<PlaylistDto>> getPublicPlaylists();

    @POST("/media/playlists/status")
    Call<ResponseBody> changePlaylistStatus(@Body PlaylistStatusDto dto);

    @POST("/media/playlists/rename")
    Call<ResponseBody> renamePlaylist(@Body RenameDto dto);

    @POST("/media/playlists/delete")
    Call<ResponseBody> deletePlaylist(@Body ItemDto dto);

    @POST("/media/playlists/manage")
    Call<ResponseBody> manageSongWithinPlaylist(@Body ManageSongWithinPlaylistDto dto);

    @POST("/media/playlists/songs")
    Call<List<SongFileDto>> getSongsWithinPlaylist(@Body ItemDto dto);

    @POST("/media/playlists/update/position")
    Call<ResponseBody> updatePositionOfSongInPlaylist(@Body UpdateSongPositionDto dto);

    @GET("/media/playlist/{id}")
    Call<PlaylistDto> getPlaylistById(@Path("id") Long id);

    @GET("/media/get/song/{playlistId}/{songId}")
    Call<SongDto> getPlaylistSongById(@Path("playlistId") Long playlistId, @Path("songId") Long sondId);
}
