package rs.ac.uns.ftn.medily.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.IntentFilter;
import android.os.Bundle;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.tabs.TabLayout;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import java.util.List;
import java.util.Objects;

import lombok.SneakyThrows;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rs.ac.uns.ftn.medily.R;
import rs.ac.uns.ftn.medily.adapters.MusicTabsPagerAdapter;
import rs.ac.uns.ftn.medily.dto.PlaylistStatusDto;
import rs.ac.uns.ftn.medily.model.Playlist;
import rs.ac.uns.ftn.medily.model.Song;
import rs.ac.uns.ftn.medily.service.ServiceUtils;
import rs.ac.uns.ftn.medily.tools.MusicFragmentNotificationListener;
import rs.ac.uns.ftn.medily.tools.MusicPlayer;
import rs.ac.uns.ftn.medily.tools.SharedPreferencesService;
import rs.ac.uns.ftn.medily.tools.Util;
import rs.ac.uns.ftn.medily.tools.db.PlaylistRepository;
import rs.ac.uns.ftn.medily.tools.db.SongRepository;

public class MusicFragment extends Fragment {

    private TabLayout tabs;
    private Menu menu;
    private MenuInflater menuInflater;

    private SlidingUpPanelLayout slidingLayout;
    private RelativeLayout collapsedView;
    private FrameLayout musicContainer;
    private RelativeLayout songsInPlaylistView;
    private boolean defaultView;
    private SongsInPlaylistFragment songsInPlaylistFragment;
    private static ActionBarDrawerToggle actionBarDrawerToggle;
    private static int selectedTab;

    public static MusicFragmentNotificationListener myReceiver;

    public MusicFragment() {}

    public static MusicFragment newInstance(ActionBarDrawerToggle toggle, int selected) {
        actionBarDrawerToggle = toggle;
        selectedTab = selected;
        return new MusicFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Objects.requireNonNull(getActivity()).setTitle("Your music box");
        setHasOptionsMenu(true);
        if (myReceiver == null) {
            myReceiver = new MusicFragmentNotificationListener();
            IntentFilter intentFilter = new IntentFilter("medily.receiver");
            getActivity().registerReceiver(myReceiver, intentFilter);
        }

        if (!MusicPlayer.started) {
            MusicPlayer.init(null, getContext());
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        this.menu = menu;
        this.menuInflater = inflater;
        loadDefaultMenu();
    }

    public void loadDefaultMenu() {
        menu.clear();
        menuInflater.inflate(R.menu.music_action_bar, menu);
        super.onCreateOptionsMenu(menu, menuInflater);
    }

    public void loadSongsInPlaylistMenu() {
        menu.clear();
        menuInflater.inflate(R.menu.playlist_action_bar, menu);
        super.onCreateOptionsMenu(menu, menuInflater);
        MenuItem menuItem = menu.findItem(R.id.share_playlist);
        songsInPlaylistFragment.setInitTitleSharePlaylist(menuItem);
    }

    public void play(Song clicked) {
        if (getActivity() != null) {
            TextView name = (TextView) getActivity().findViewById(R.id.song_name_player_collapsed);
            name.setText(clicked.getFile().getName());

            TextView artist = (TextView) getActivity().findViewById(R.id.song_artist_player_collapsed);
            artist.setText(clicked.getArtist());

            ImageButton button = (ImageButton) getActivity().findViewById(R.id.play_collapsed);
            button.setImageDrawable(ContextCompat.getDrawable(Objects.requireNonNull(getContext()), R.drawable.ic_pause_collapsed));
            button.setTag(R.drawable.ic_pause_collapsed);
            button.setVisibility(View.VISIBLE);
            Util.syncViewsButtons("collapsed", button, getActivity(), getView());
            Util.syncCurrentlyPlayingSong(clicked, getActivity());

            MusicPlayer.setSongsToPlay(SongRepository
                    .findUsersDownloadedSongs(SharedPreferencesService.get("username"), getContext()));
            MusicPlayer.play(clicked, getActivity());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        char resource = Util.getFlag(item);

        if (defaultView) {
            if (getActivity() != null) {
                ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
                LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                if (actionBar != null) {
                    if (inflater != null) {
                        View v = inflater.inflate(R.layout.layout_search_music, null);
                        actionBar.setCustomView(v);
                    }
                }

                if (item.getItemId() == android.R.id.home) {
                    RelativeLayout theLayout = (RelativeLayout) getActivity().findViewById(R.id.layout_search_songs);
                    theLayout.setVisibility(RelativeLayout.GONE);
                    if (actionBar != null) {
                        actionBar.setDisplayShowTitleEnabled(true);
                        menu.clear();
                        menuInflater.inflate(R.menu.music_action_bar, menu);
                        actionBar.setDisplayHomeAsUpEnabled(true);
                        actionBarDrawerToggle.setDrawerIndicatorEnabled(true);
                        searchSongs("");
                        actionBar.setDisplayShowTitleEnabled(true);
                    }
                } else {
                    if (resource == 's') {
                        if (actionBar != null) {
                            item.setIcon(R.mipmap.ic_clear);
                            Util.setFlag(item, 'c');

                            actionBar.setDisplayHomeAsUpEnabled(true);
                            actionBar.setDisplayShowCustomEnabled(true);
                            actionBar.setDisplayShowTitleEnabled(false);
                            actionBarDrawerToggle.setDrawerIndicatorEnabled(false);

                            if (inflater != null) {
                                View v = inflater.inflate(R.layout.layout_search_music, null);
                                actionBar.setCustomView(v);

                                final EditText searchField = (EditText) getActivity().findViewById(R.id.search_query);
                                setHintLabel(searchField);
                                searchField.addTextChangedListener(new TextWatcher() {

                                    @Override
                                    public void beforeTextChanged(CharSequence s, int start,
                                                                  int count, int after) {
                                    }

                                    @Override
                                    public void onTextChanged(CharSequence s, int start,
                                                              int before, int count) {
                                        searchSongs(searchField.getText().toString());
                                    }

                                    @Override
                                    public void afterTextChanged(Editable s) {
                                    }
                                });
                            }
                        }
                    } else if (resource == 'c') {
                        EditText searchField = (EditText) getActivity().findViewById(R.id.search_query);
                        searchField.setText(null);
                        searchSongs("");
                        searchField.requestFocus();
                        setHintLabel(searchField);
                    }

                }
            }
        } else {
            if (getActivity() != null) {
                ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
                LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                if (actionBar != null) {
                    if (inflater != null) {
                        View v = inflater.inflate(R.layout.layout_search_music, null);
                        actionBar.setCustomView(v);
                    }
                }

                if (item.getItemId() == android.R.id.home) {
                    RelativeLayout theLayout = (RelativeLayout) getActivity().findViewById(R.id.layout_search_songs);
                    theLayout.setVisibility(LinearLayout.GONE);
                    if (actionBar != null) {
                        actionBar.setDisplayShowTitleEnabled(true);
                        menu.clear();
                        menuInflater.inflate(R.menu.playlist_action_bar, menu);
                        actionBar.setDisplayHomeAsUpEnabled(true);
                        actionBarDrawerToggle.setDrawerIndicatorEnabled(true);
                        songsInPlaylistFragment.searchSongInPlaylist("");
                    }
                    songsInPlaylistFragment.setStop(false);
                }  else if (item.getItemId() == R.id.share_playlist && item.getTitle().equals("Share with family")) {
                    if (songsInPlaylistFragment != null) {
                        changeShareStatus(true, songsInPlaylistFragment.getPlaylist(), item);
                    }
                } else if (item.getItemId() == R.id.share_playlist && item.getTitle().equals("Make private again")) {
                    if (songsInPlaylistFragment != null) {
                         changeShareStatus(false, songsInPlaylistFragment.getPlaylist(), item);
                    }
                } else if (item.getItemId() == R.id.shuffle && item.getTitle().equals("Shuffle")) {
                    songsInPlaylistFragment.shufflePlaylist();
                    item.setTitle(R.string.stop_shuffle_text);
                } else if (item.getItemId() == R.id.shuffle && item.getTitle().equals("Stop shuffling")) {
                    songsInPlaylistFragment.stopShuffle();
                    item.setTitle(R.string.shuffle_text);
                } else {
                    if (resource == 's') {
                        if (actionBar != null) {
                            item.setIcon(R.mipmap.ic_clear);
                            Util.setFlag(item, 'c');

                            actionBar.setDisplayHomeAsUpEnabled(true);
                            actionBar.setDisplayShowCustomEnabled(true);
                            actionBar.setDisplayShowTitleEnabled(false);
                            actionBarDrawerToggle.setDrawerIndicatorEnabled(false);

                            if (inflater != null) {
                                View v = inflater.inflate(R.layout.layout_search_music, null);
                                actionBar.setCustomView(v);

                                final EditText searchField = (EditText) getActivity().findViewById(R.id.search_query);
                                searchField.addTextChangedListener(new TextWatcher() {

                                    @Override
                                    public void beforeTextChanged(CharSequence s, int start,
                                                                  int count, int after) {
                                    }

                                    @Override
                                    public void onTextChanged(CharSequence s, int start,
                                                              int before, int count) {
                                        songsInPlaylistFragment.setStop(true);
                                        songsInPlaylistFragment.searchSongInPlaylist(searchField.getText().toString());
                                    }

                                    @Override
                                    public void afterTextChanged(Editable s) {
                                    }
                                });
                            }
                        }

                    } else if (resource == 'c') {
                        EditText searchField = (EditText) getActivity().findViewById(R.id.search_query);
                        searchField.setText(null);
                        songsInPlaylistFragment.searchSongInPlaylist("");
                        searchField.requestFocus();

                        item.setIcon(R.mipmap.ic_search);
                        Util.setFlag(item, 's');
                    }   if (resource == 'p') {
                        songsInPlaylistFragment.playAll(getView());
                        if (actionBar != null) {
                            actionBar.setDisplayHomeAsUpEnabled(false);
                            actionBar.setDisplayShowCustomEnabled(false);
                            actionBar.setDisplayShowTitleEnabled(true);
                        }
                    }
                }

            }
        }
        return true;
    }

    private void changeShareStatus(final boolean share, final Playlist playlist, final MenuItem item) {
        if (playlist != null) {
            Call<ResponseBody> call = ServiceUtils.mediaService.changePlaylistStatus(new PlaylistStatusDto(playlist.getGlobalId(), share));
            call.enqueue(new Callback<ResponseBody>() {
                @SneakyThrows
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.code() == 200) {
                        if (share) {
                            item.setTitle(R.string.stop_share_text);
                        } else {
                            item.setTitle(R.string.switch_text);
                        }
                        playlist.setSharePlaylist(share);
                        PlaylistRepository.update(playlist, getContext(), PlaylistRepository.COLUMN_ID + " = ?", new String[] { playlist.getId().toString() });
                        Toast.makeText(getActivity(), "Playlist status changed to " +
                                        (share ? "public" : "private" ) + ".",
                                Toast.LENGTH_SHORT).show();
                    } else if (response.code() == 400) {
                        if (getActivity() != null) {
                            Toast.makeText(getActivity(), response.errorBody().string(),
                                    Toast.LENGTH_LONG).show();
                        }
                    } else {
                        if (getActivity() != null) {
                            Toast.makeText(getActivity(), "Connection to server could not be " +
                                            "established. Please try again later.",
                                    Toast.LENGTH_LONG).show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    if (getActivity() != null) {
                        Toast.makeText(getActivity(), "Connection to server could not be " +
                                        "established. Please try again later.",
                                Toast.LENGTH_LONG).show();
                    }
                }
            });
        }
    }

    private void setHintLabel(EditText editText) {
        if (editText != null) {
            if (tabs.getSelectedTabPosition() == 0) {
                editText.setHint(R.string.search_playlists_label);
            } else if (tabs.getSelectedTabPosition() == 1) {
                editText.setHint(R.string.search_songs_label);
            } else if (tabs.getSelectedTabPosition() == 2) {
                editText.setHint(R.string.search_playlists_label);
            }
        }
    }

    private void setUpOnClickPlayPauseButton(final ImageButton button) {
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                    if (button.getTag() == null) {
                        button.setTag(R.mipmap.ic_play);
                    }
                    Util.playPauseCurrentSong(button, v, "expanded");

                    changeIconPlayPause(button, v, "expanded");
                    Util.syncViewsButtons("expanded", button, getActivity(), v);
            }
        });
    }

    private void setUpSeekBarChange(SeekBar seekBar, final TextView endDuration, final TextView currentDuration) {
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    MusicPlayer.seek(progress); //if user drags the seekbar, it gets the position and updates in textView.
                }

                currentDuration.setText(Util.convertAfterProgress(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });
    }

    private void setUpPreviousSongButton(ImageButton button) {
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (getActivity() != null) {
                    ImageButton button = getActivity().findViewById(R.id.play_collapsed);
                    if (button != null) {
                        if (button.getTag().equals(R.drawable.ic_play_collapsed)) {
                            button.setImageDrawable(ContextCompat.getDrawable(Objects.requireNonNull(getContext()), R.drawable.ic_pause_collapsed));
                            button.setTag(R.drawable.ic_pause_collapsed);
                            Util.syncViewsButtons("collapsed", button, getActivity(), v);
                        }
                    }
                    MusicPlayer.previousSong(getActivity());
                }
            }
        });
    }

    private void setUpNextSongButton(ImageButton button) {
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (getActivity() != null) {
                    ImageButton button = getActivity().findViewById(R.id.play_collapsed);
                    if (button != null) {
                        if (button.getTag().equals(R.drawable.ic_play_collapsed)) {
                            button.setImageDrawable(ContextCompat.getDrawable(Objects.requireNonNull(getContext()), R.drawable.ic_pause_collapsed));
                            button.setTag(R.drawable.ic_pause_collapsed);
                            Util.syncViewsButtons("collapsed", button, getActivity(), v);
                        }
                    }
                    MusicPlayer.nextSong(getActivity());
                }
            }
        });
    }

    private void setUpShuffleButton(final ImageButton button) {
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                boolean shuffleActive = false;
                Integer shuffle = R.mipmap.ic_shuffle;
                Integer shuffleClicked = R.mipmap.ic_shuffle_clicked;
                Integer resource = Util.getImageTag(button);
                if (resource != null) {
                    if (resource.equals(shuffle)) {
                        button.setImageDrawable(ContextCompat.getDrawable(v.getContext(), R.mipmap.ic_shuffle_clicked));
                        button.setTag(R.mipmap.ic_shuffle_clicked);
                        Toast.makeText(getActivity(), "Shuffle on!", Toast.LENGTH_SHORT).show();
                        shuffleActive = true;
                    } else if (resource.equals(shuffleClicked)) {
                        button.setImageDrawable(ContextCompat.getDrawable(v.getContext(), R.mipmap.ic_shuffle));
                        button.setTag(R.mipmap.ic_shuffle);
                        Toast.makeText(getActivity(), "Shuffle off!", Toast.LENGTH_SHORT).show();
                        shuffleActive = false;
                    }

                    if (shuffleActive) {
                        MusicPlayer.shuffle();
                    } else {
                        MusicPlayer.stopShuffle();
                    }
                }
            }
        });
    }

    private void setUpLoopButton(final ImageButton button) {
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                boolean loopActive = false;
                Integer loop = R.mipmap.ic_loop_round;
                Integer loopClicked = R.mipmap.ic_loop_clicked_round;
                Integer resource = Util.getImageTag(button);
                if (resource != null) {
                    if (resource.equals(loop)) {
                        button.setImageDrawable(ContextCompat.getDrawable(v.getContext(), R.mipmap.ic_loop_clicked_round));
                        button.setTag(R.mipmap.ic_loop_clicked_round);
                        Toast.makeText(getActivity(), "Repeat song on!", Toast.LENGTH_SHORT).show();
                        loopActive = true;
                    } else if (resource.equals(loopClicked)) {
                        button.setImageDrawable(ContextCompat.getDrawable(v.getContext(), R.mipmap.ic_loop_round));
                        button.setTag(R.mipmap.ic_loop_round);
                        Toast.makeText(getActivity(), "Repeat song off!", Toast.LENGTH_SHORT).show();
                    }

                    MusicPlayer.loop(loopActive);
                }
            }
        });
    }

    private Activity getActivity(View view) {
        Context context = view.getContext();
        while (context instanceof ContextWrapper) {
            if (context instanceof Activity) {
                return (Activity)context;
            }
            context = ((ContextWrapper)context).getBaseContext();
        }
        return null;
    }

    private void setUpOptionsPlayer(View view) {
        if (view != null) {

            if (MusicPlayer.mediaPlayer == null) {
                MusicPlayer.init(getContext());
            }

            ImageButton playPauseButton = (ImageButton) view.findViewById(R.id.btn_play_pause_player);
            if (playPauseButton != null) {
                if (MusicPlayer.currentlyPlaying != null) {
                    TextView name = (TextView) view.findViewById(R.id.song_name_player_collapsed);
                    name.setText(MusicPlayer.currentlyPlaying.getFile().getName());

                    TextView artist = (TextView) view.findViewById(R.id.song_artist_player_collapsed);
                    artist.setText(MusicPlayer.currentlyPlaying.getArtist());

                    if (MusicPlayer.mediaPlayer.isPlaying()) {
                        ImageButton button = (ImageButton) view.findViewById(R.id.play_collapsed);
                        button.setImageDrawable(ContextCompat.getDrawable(Objects.requireNonNull(getContext()), R.drawable.ic_pause_collapsed));
                        button.setTag(R.drawable.ic_pause_collapsed);
                        button.setVisibility(View.VISIBLE);
                        Util.syncViewsAtStartButtons("collapsed", button, view);
                        Util.syncCurrentlyPlayingSongAtStart(MusicPlayer.currentlyPlaying, view);
                    } else {
                        ImageButton button = (ImageButton) view.findViewById(R.id.play_collapsed);
                        button.setImageDrawable(ContextCompat.getDrawable(Objects.requireNonNull(getContext()), R.drawable.ic_play_collapsed));
                        button.setTag(R.drawable.ic_play_collapsed);
                        button.setVisibility(View.VISIBLE);
                        Util.syncViewsAtStartButtons("collapsed", button, view);
                        Util.syncCurrentlyPlayingSongAtStart(MusicPlayer.currentlyPlaying, view);
                    }
                } else if (SharedPreferencesService.get("song") != null) {
                    Song song = null;
                    List<Song> songs = SongRepository.findSongs(SongRepository.COLUMN_ID + " = ? AND " + SongRepository.COLUMN_DOWNLOADED_BY + " = ?",
                            new String[] {SharedPreferencesService.get("song"), SharedPreferencesService.get("username")}, view.getContext());
                    if (songs.size() == 1) {
                        song = songs.get(0);
                    }

                    if (song != null) {
                        TextView name = (TextView) view.findViewById(R.id.song_name_player_collapsed);
                        name.setText(song.getFile().getName());

                        TextView artist = (TextView) view.findViewById(R.id.song_artist_player_collapsed);
                        artist.setText(song.getArtist());

                        ImageButton button = (ImageButton) view.findViewById(R.id.play_collapsed);
                        button.setImageDrawable(ContextCompat.getDrawable(Objects.requireNonNull(view.getContext()), R.drawable.ic_play_collapsed));
                        button.setTag(R.drawable.ic_play_collapsed);
                        button.setVisibility(View.VISIBLE);
                        Util.syncViewsAtStartButtons("collapsed", button, view);
                        Util.syncCurrentlyPlayingSongAtStart(song, view);
                        MusicPlayer.currentlyPlaying = song;
                        MusicPlayer.initSong(song);

                        MusicPlayer.setSongsToPlay(SongRepository
                                .findUsersDownloadedSongs(SharedPreferencesService.get("username"), view.getContext()));
                    }
                }
                setUpOnClickPlayPauseButton(playPauseButton);
            }

            SeekBar seekBar = (SeekBar) view.findViewById(R.id.seek_bar);
            TextView endDuration = (TextView) view.findViewById(R.id.duration_end);
            TextView currentDuration = (TextView) view.findViewById(R.id.duration_current);
            if (seekBar != null && endDuration != null && currentDuration != null) {

                if (MusicPlayer.currentlyPlaying != null) {
                    if (MusicPlayer.seekBar != null) {
                        int progress = MusicPlayer.seekBar.getProgress();
                        seekBar.setProgress(progress);
                        MusicPlayer.seekBar = seekBar;
                        MusicPlayer.setSeekBarStatus();
                    } else {
                        seekBar.setProgress(0);
                        MusicPlayer.seekBar = seekBar;
                        MusicPlayer.setSeekBarStatus();
                    }
                    Util.syncCurrentlyPlayingSong(MusicPlayer.currentlyPlaying, getActivity());
                } else {
                    seekBar.setProgress(0);
                    MusicPlayer.seekBar = seekBar;
                    MusicPlayer.setSeekBarStatus();
                }
                setUpSeekBarChange(seekBar, endDuration, currentDuration);
            }

            ImageButton btnPrev = (ImageButton) view.findViewById(R.id.btn_prev_player);
            if (btnPrev != null) {
                setUpPreviousSongButton(btnPrev);
            }

            ImageButton nextBtn = (ImageButton) view.findViewById(R.id.btn_next_player);
            if (nextBtn != null) {
                setUpNextSongButton(nextBtn);
            }

            ImageButton shufflePlayer = (ImageButton) view.findViewById(R.id.btn_shuffle_player);
            if (shufflePlayer != null) {
                setUpShuffleButton(shufflePlayer);
            }

            ImageButton loopPlayer = view.findViewById(R.id.btn_loop_player);
            if (loopPlayer != null) {
                setUpLoopButton(loopPlayer);
            }
        }
    }

    private ViewPager viewPager;

    public ViewPager getViewPager() {
        return viewPager;
    }

    public MusicTabsPagerAdapter getAdapter() {
        return adapter;
    }

    private MusicTabsPagerAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.defaultView = true;
        View view = inflater.inflate(R.layout.fragment_music, container, false);
        adapter = new MusicTabsPagerAdapter(this, getChildFragmentManager());

        viewPager = view.findViewById(R.id.music_view_pager);
        viewPager.setAdapter(adapter);
        musicContainer = view.findViewById(R.id.music_fragment_container);
        songsInPlaylistView = view.findViewById(R.id.song_in_playlist_container);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageScrollStateChanged(int state) {}
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

            public void onPageSelected(int position) {
                if (getActivity() != null) {
                    Objects.requireNonNull(getActivity()).setTitle("Your music box");
                    View viewSearch = getActivity().findViewById(R.id.search_songs);
                    if (viewSearch != null) {
                        if (position == 0 || position == 1 || position == 2) {
                            viewSearch.setVisibility(View.VISIBLE);
                            EditText editText = (EditText) getActivity().findViewById(R.id.search_query);
                            setHintLabel(editText);
                        } else {
                            viewSearch.setVisibility(View.INVISIBLE);
                        }
                    }
                }
            }
        });

        tabs = view.findViewById(R.id.music_tab_layout);
        tabs.setupWithViewPager(viewPager);
        tabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                refresh();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        viewPager.setCurrentItem(selectedTab);

        initSwipeListener(view);
        setUpOptionsPlayer(view);
        return view;
    }

    private void changeIconPlayPause(ImageButton button, View v, String viewType) {

        Integer state = Util.getImageTag(button);
        Integer pauseIcon = null;
        Integer playIcon = null;

        if (viewType.equals("collapsed")) {
            pauseIcon = R.drawable.ic_pause_collapsed;
            playIcon = R.drawable.ic_play_collapsed;

        } else if (viewType.equals("expanded")) {
            pauseIcon = R.mipmap.ic_pause;
            playIcon = R.mipmap.ic_play;
        }

        if (state != null) {
            if (state.equals(pauseIcon) && viewType.equals("collapsed")) {
                button.setImageDrawable(ContextCompat.getDrawable(v.getContext(), R.drawable.ic_play_collapsed));
                button.setTag(R.drawable.ic_play_collapsed);
            } else if (state.equals(playIcon) && viewType.equals("collapsed")) {
                button.setImageDrawable(ContextCompat.getDrawable(v.getContext(), R.drawable.ic_pause_collapsed));
                button.setTag(R.drawable.ic_pause_collapsed);
            } else if (state.equals(pauseIcon) && viewType.equals("expanded")) {
                button.setImageDrawable(ContextCompat.getDrawable(v.getContext(), R.mipmap.ic_play));
                button.setTag(R.mipmap.ic_play);
            } else if (state.equals(playIcon) && viewType.equals("expanded")) {
                button.setImageDrawable(ContextCompat.getDrawable(v.getContext(), R.mipmap.ic_pause));
                button.setTag(R.mipmap.ic_pause);
            }
        }
    }

    private void initSwipeListener(View view) {
        slidingLayout = view.findViewById(R.id.sliding_layout);
        collapsedView = slidingLayout.findViewById(R.id.collapsed_view);

        final ImageButton button = (ImageButton) collapsedView.findViewById(R.id.play_collapsed);
        if (button != null) {
            button.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (button.getTag() == null) {
                        button.setTag(R.drawable.ic_play_collapsed);
                    }
                    Util.playPauseCurrentSong(button, v, "collapsed");

                    changeIconPlayPause(button, v, "collapsed");
                    Util.syncViewsButtons("collapsed", button, getActivity(), v);
                }
            });
        }

        TextView songName = (TextView) collapsedView.findViewById(R.id.song_name_player_collapsed);
        if (songName != null) {
            if (MusicPlayer.mediaPlayer != null) {
                if (MusicPlayer.mediaPlayer.isPlaying()) {
                    songName.startAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.text_translation));
                }
            }
        }

        slidingLayout.addPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {
            }

            @Override
            public void onPanelStateChanged(View panel, SlidingUpPanelLayout.PanelState previousState, SlidingUpPanelLayout.PanelState newState) {
                if (newState.equals(SlidingUpPanelLayout.PanelState.COLLAPSED)) {
                    collapsedView();
                } else if (newState.equals(SlidingUpPanelLayout.PanelState.EXPANDED)) {
                    expandedView();
                }
            }
        });
        slidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
    }

    private void collapsedView() {
        collapsedView.setVisibility(View.VISIBLE);
        if (getActivity() != null) {
            ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
            if (actionBar != null) {
                actionBar.setDisplayHomeAsUpEnabled(true);
                actionBar.setDisplayShowCustomEnabled(true);
                actionBar.setDisplayShowTitleEnabled(false);
            }

            if (getActivity().findViewById(R.id.search_songs) != null) {
                getActivity().findViewById(R.id.search_songs).setVisibility(View.VISIBLE);
                MenuItem menuItem = menu.findItem(R.id.search_songs);
                if (menuItem != null) {
                    char resource = Util.getFlag(menuItem);
                    if (resource == 's') {
                        if (actionBar != null) {
                            actionBar.setDisplayHomeAsUpEnabled(true);
                            actionBar.setDisplayShowTitleEnabled(true);
                            actionBar.setDisplayShowCustomEnabled(true);
                            actionBarDrawerToggle.setDrawerIndicatorEnabled(true);
                        }
                    } else if (resource == 'c') {
                        if (actionBar != null) {
                            actionBar.setDisplayHomeAsUpEnabled(true);
                            actionBar.setDisplayShowTitleEnabled(false);
                            actionBar.setDisplayShowCustomEnabled(true);
                            actionBarDrawerToggle.setDrawerIndicatorEnabled(false);
                        }
                    }
                }
            }
            if (getActivity().findViewById(R.id.search_query) != null) {
                getActivity().findViewById(R.id.search_query).setVisibility(View.VISIBLE);
            }
        }
    }

    private void expandedView() {
        collapsedView.setVisibility(View.GONE);
        if (getActivity() != null) {
            if (getActivity().findViewById(R.id.search_songs) != null) {
                getActivity().findViewById(R.id.search_songs).setVisibility(View.INVISIBLE);
            }
            if (getActivity().findViewById(R.id.search_query) != null) {
                getActivity().findViewById(R.id.search_query).setVisibility(View.INVISIBLE);
            }

            ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
            if (actionBar != null) {
                actionBar.setDisplayHomeAsUpEnabled(false);
                actionBar.setDisplayShowCustomEnabled(false);
                actionBar.setDisplayShowTitleEnabled(true);
            }
        }
    }

    private void searchSongs(String query) {
        Fragment chosenFragment = getVisibleFragment();
        if (chosenFragment instanceof SongsFragment) {
            ((SongsFragment) chosenFragment).searchSong(query);
        } else if (chosenFragment instanceof PlaylistsFragment) {
            ((PlaylistsFragment) chosenFragment).searchPlaylist(query);
        } else if (chosenFragment instanceof PublicPlaylistsFragment) {
            ((PublicPlaylistsFragment) chosenFragment).searchPlaylist(query);
        }
    }

    private Fragment getVisibleFragment() {
        if (getActivity() != null) {
            Fragment f = getActivity().getSupportFragmentManager().findFragmentById(R.id.main_fragment_container);
            if (f != null) {
                List<Fragment> fragments = f.getChildFragmentManager().getFragments();
                for (Fragment fragment : fragments) {
                    if (fragment != null && fragment.isVisible()) {
                        if (tabs.getSelectedTabPosition() == 0 && fragment instanceof PlaylistsFragment) {
                            return fragment;
                        } else if (tabs.getSelectedTabPosition() == 1 && fragment instanceof SongsFragment) {
                            return fragment;
                        } else if (tabs.getSelectedTabPosition() == 2 && fragment instanceof PublicPlaylistsFragment) {
                            return fragment;
                        }
                    }
                }
            }
        }
        return null;
    }

    private void refresh() {
        try {
            if (getActivity() != null) {
                Fragment chosenFragment = getVisibleFragment();
                if (chosenFragment instanceof SongsFragment) {
                    ((SongsFragment) chosenFragment).refresh();
                } else if (chosenFragment instanceof PlaylistsFragment) {
                    ((PlaylistsFragment) chosenFragment).refresh();
                } else if (chosenFragment instanceof PublicPlaylistsFragment) {
                    ((PublicPlaylistsFragment) chosenFragment).refresh();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showPlaylist(Playlist playlist) {
        musicContainer.setVisibility(View.GONE);
        songsInPlaylistView.setVisibility(View.VISIBLE);
        songsInPlaylistFragment = new SongsInPlaylistFragment(playlist, getContext());
        loadSongsInPlaylistMenu();
        defaultView = false;
        songsInPlaylistFragment.onCreateView(songsInPlaylistView);
    }

    public void hidePlaylist() {
        musicContainer.setVisibility(View.VISIBLE);
        songsInPlaylistView.setVisibility(View.GONE);
        tabs.getTabAt(0);
        loadDefaultMenu();
        defaultView = true;
        songsInPlaylistFragment = null;
    }

    public SongsInPlaylistFragment getSongsInPlaylistFragment() {
        return songsInPlaylistFragment;
    }

    public TabLayout getTabs() {
        return tabs;
    }
}
