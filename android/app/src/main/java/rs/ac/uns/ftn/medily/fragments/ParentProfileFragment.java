package rs.ac.uns.ftn.medily.fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Objects;

import lombok.SneakyThrows;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rs.ac.uns.ftn.medily.R;
import rs.ac.uns.ftn.medily.activities.LoginActivity;
import rs.ac.uns.ftn.medily.dto.ItemDto;
import rs.ac.uns.ftn.medily.service.ServiceUtils;
import rs.ac.uns.ftn.medily.tools.Downloader;
import rs.ac.uns.ftn.medily.tools.FragmentTransition;
import rs.ac.uns.ftn.medily.tools.MusicPlayer;
import rs.ac.uns.ftn.medily.tools.NotificationUtil;
import rs.ac.uns.ftn.medily.tools.SharedPreferencesService;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ParentProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ParentProfileFragment extends Fragment {

    private String role;
    private TextView nameText;
    private EditText password;
    private EditText retyped;
    private Button change;
    private Button manage;
    private Button logout;

    public ParentProfileFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public static ParentProfileFragment newInstance(String role) {
        ParentProfileFragment parentProfileFragment = new ParentProfileFragment();
        // Supply index input as an argument.
        Bundle args = new Bundle();
        args.putString("role", role);
        parentProfileFragment.setArguments(args);
        return parentProfileFragment;
    }

    @Override
    @SuppressWarnings("ConstantConditions")
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Bundle args = getArguments();
        role = (String) args.get("role");
        View view = inflater.inflate(R.layout.fragment_parent_profile, container, false);
        init(view);
        if (getActivity() != null) {
            getActivity().setTitle("Profile");
        }
        return view;
    }

    private void init(View view) {
        nameText = view.findViewById(R.id.parent_name_text_view);
        password = view.findViewById(R.id.parent_password_new);
        retyped = view.findViewById(R.id.parent_password_new_retype);
        change = view.findViewById(R.id.change_parent_password_button);
        manage = view.findViewById(R.id.manage_family_button);
        logout = view.findViewById(R.id.parent_logout);
        if (change != null && manage != null && logout != null) {
            nameText.setText(SharedPreferencesService.get("name"));
            if (role.toLowerCase().equals("child")) {
                manage.setVisibility(View.GONE);
            }
            addListeners();
        }
    }

    private void addListeners() {
        change.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (validInput()) {
                    Call<ResponseBody> call = ServiceUtils.userService.changePassword(new ItemDto(password.getText().toString()));
                    call.enqueue(new Callback<ResponseBody>() {
                        @SneakyThrows
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            if (response.code() == 200) {
                                Toast.makeText(getActivity(), "Password successfully changed!",
                                        Toast.LENGTH_SHORT).show();
                                password.setText("");
                                retyped.setText("");
                            } else if (response.code() == 400) {
                                Toast.makeText(getActivity(), response.errorBody().string(),
                                        Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(getActivity(), "Invalid password change!",
                                        Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            Toast.makeText(getActivity(), t.getMessage(),
                                    Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        });
        if (role.toLowerCase().equals("parent")) {
            manage.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    FragmentTransition.to(LoadFamilyManagementFragment.newInstance(),
                            Objects.requireNonNull(getActivity()), true,
                            R.id.main_fragment_container);
                }
            });
        }
        logout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SharedPreferencesService.remove("username");
                SharedPreferencesService.remove("token");
                SharedPreferencesService.remove("song");
                Downloader.downloadingPlaylists.clear();
                shutDownMusic();
                NotificationUtil.clearPlayerNotificationView(Objects.requireNonNull(getContext()));
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                Objects.requireNonNull(getActivity()).finish();
            }
        });
    }

    private void shutDownMusic() {
        MusicPlayer.clearMediaPlayer();
    }

    private boolean validInput() {
        if (password.getText().toString().length() < 1) {
            Toast.makeText(getActivity(), "Password not defined",
                    Toast.LENGTH_SHORT).show();
            return false;
        } else if (retyped.getText().toString().length() < 1) {
            Toast.makeText(getActivity(), "Retyped password not defined",
                    Toast.LENGTH_SHORT).show();
            return false;
        } else if (!password.getText().toString().equals(retyped.getText().toString())) {
            Toast.makeText(getActivity(), "Password not same retyped password",
                    Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }
}
