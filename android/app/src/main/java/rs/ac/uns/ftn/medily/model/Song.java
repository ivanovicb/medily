package rs.ac.uns.ftn.medily.model;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Song {

    private Long id;
    private String artist;
    private String genre;
    private int duration; // seconds
    private String downloadedBy; // seconds
    private File file;
    private List<Playlist> playlists; // song can belong to 0..n playlists

    private Long globalId; // id on server

    public Song(){
        playlists = new ArrayList<>();
    }

    public Song(String artist, String genre, int duration, String downloadedBy, File file, Long id,
                Long globalId) {
        this.artist = artist;
        this.genre = genre;
        this.duration = duration;
        this.downloadedBy = downloadedBy;
        this.file = file;
        this.id = id;
        this.globalId = globalId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Song song = (Song) o;
        return id.equals(song.id);
    }

    @Override
    public String toString() { return file.getName(); }
}
