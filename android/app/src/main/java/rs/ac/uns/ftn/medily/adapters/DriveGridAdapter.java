package rs.ac.uns.ftn.medily.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.apache.commons.io.FilenameUtils;

import java.util.List;

import rs.ac.uns.ftn.medily.R;
import rs.ac.uns.ftn.medily.model.File;
import rs.ac.uns.ftn.medily.model.Folder;
import rs.ac.uns.ftn.medily.tools.FileSystemUtil;

public class DriveGridAdapter extends BaseAdapter {

    private final Context mContext;
    private List<Folder> folders;
    private List<File> files;

    public DriveGridAdapter(Context context, List<Folder> folders, List<File> files) {
        this.mContext = context;
        this.files = files;
        this.folders = folders;
    }

    @Override
    public int getCount() {
        return folders.size() + files.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater layoutInflater = LayoutInflater.from(mContext);
            convertView = layoutInflater.inflate(R.layout.drive_item, null);
        }

        TextView nameTextView = convertView.findViewById(R.id.text_view_drive_item);
        ImageView imageView = convertView.findViewById(R.id.image_view_drive_item);

        if(position < folders.size()){
            Folder folder = folders.get(position);

            imageView.setImageResource(R.drawable.ic_folder);
            nameTextView.setText(folder.getName());
        }
        else {
            File file = files.get(position-folders.size());
            String extension = FilenameUtils.getExtension(file.getName());

            if(FileSystemUtil.isPhoto(mContext, extension)){
                imageView.setImageResource(R.drawable.ic_photo);
            } else if (FileSystemUtil.isAudio(mContext, extension)) {
                imageView.setImageResource(R.drawable.ic_audio);
            } else if (FileSystemUtil.isVideo(mContext, extension)) {
                imageView.setImageResource(R.drawable.ic_video);
            } else {
                imageView.setImageResource(R.drawable.ic_file);
            }

            nameTextView.setText(file.getName());
        }

        return convertView;
    }

}
