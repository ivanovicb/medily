package rs.ac.uns.ftn.medily.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import rs.ac.uns.ftn.medily.model.File;

public class SongFileDto implements Serializable {

    @SerializedName("id")
    @Expose
    private Long id;

    @SerializedName("artist")
    @Expose
    private String artist;

    @SerializedName("genre")
    @Expose
    private String genre;

    @SerializedName("duration")
    @Expose
    private int duration;

    @SerializedName("file")
    @Expose
    private File file;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("position")
    @Expose
    private int position;

    public SongFileDto() {
    }

    public SongFileDto(Long id, String artist, String genre, int duration, File file, String name) {
        this.id = id;
        this.artist = artist;
        this.genre = genre;
        this.duration = duration;
        this.file = file;
        this.name = name;
        this.position = 0;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
