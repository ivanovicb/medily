package rs.ac.uns.ftn.medily.tools.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import rs.ac.uns.ftn.medily.model.File;
import rs.ac.uns.ftn.medily.model.Folder;
import rs.ac.uns.ftn.medily.tools.Util;

public class FolderRepository {

    public static final String TABLE_FOLDERS = "folders";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_PATH = "path";
    public static final String COLUMN_CREATED = "created";

    public static final String DB_CREATE = "create table "
            + TABLE_FOLDERS + "("
            + COLUMN_ID  + " integer primary key autoincrement, "
            + COLUMN_NAME + " text, "
            + COLUMN_PATH + " text, "
            + COLUMN_CREATED + " text"
            + ")";

    public static final String TABLE_SUBFOLDERS = "subfolders";
    public static final String COLUMN_PARENT_FOLDER = "parent";
    public static final String COLUMN_CHILD_FOLDER = "child";

    public static final String DB_CREATE_SUBFOLDERS = "create table "
            + TABLE_SUBFOLDERS + "("
            + COLUMN_ID  + " integer primary key autoincrement, "
            + COLUMN_PARENT_FOLDER + " integer not null, "
            + COLUMN_CHILD_FOLDER + " integer not null, "
            + "FOREIGN KEY (" + COLUMN_PARENT_FOLDER + ") REFERENCES " + TABLE_FOLDERS + " (_id), "
            + "FOREIGN KEY (" + COLUMN_CHILD_FOLDER + ") REFERENCES " + TABLE_FOLDERS + " (_id)"
            + ")";

    public static final String TABLE_FOLDERS_FILES = "folders_files";
    public static final String COLUMN_FOLDER = "folder";
    public static final String COLUMN_FILE = "file";

    public static final String DB_CREATE_FOLDERS_FILES = "create table "
            + TABLE_FOLDERS_FILES + "("
            + COLUMN_ID  + " integer primary key autoincrement, "
            + COLUMN_FOLDER + " integer not null, "
            + COLUMN_FILE + " integer not null, "
            + "FOREIGN KEY (" + COLUMN_FOLDER + ") REFERENCES " + TABLE_FOLDERS + " (_id), "
            + "FOREIGN KEY (" + COLUMN_FILE + ") REFERENCES " + FileRepository.TABLE_FILES + " (_id)"
            + ")";

    public static Cursor query(String selection, String[] selectionArgs, Context context) {
        String[] projection = {COLUMN_ID, COLUMN_NAME, COLUMN_PATH, COLUMN_CREATED};
        SQLiteDatabase db = DbContext.getDB(context).getWritableDatabase();
        return db.query(TABLE_FOLDERS, projection, selection,
                selectionArgs, null, null, null);
    }

    public static List<Folder> findFolders(String selection, String[] selectionArgs, Context context) {
        List<Folder> result = new ArrayList<>();
        Cursor cursor = query(selection, selectionArgs, context);
        while(cursor.moveToNext()) {
            Long id = cursor.getLong(cursor.getColumnIndexOrThrow(COLUMN_ID));
            Folder folder = new Folder(id,
                    cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_NAME)),
                    cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_PATH)),
                    Util.stringToDate(cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_CREATED))),
                    findSubFolders(COLUMN_ID + " = ?", new String[] {id.toString()}, context),
                    findFilesInFolder(COLUMN_ID + " = ?", new String[] {id.toString()}, context));
            result.add(folder);
        }
        cursor.close();
        return result;
    }

    public static Folder add(Folder folder, Context context) {
        SQLiteDatabase db = DbContext.getDB(context).getWritableDatabase();
        ContentValues entry = new ContentValues();
        entry.put(COLUMN_NAME, folder.getName());
        entry.put(COLUMN_PATH, folder.getPath());
        entry.put(COLUMN_CREATED, Util.dateToString(folder.getCreated()));
        long result = db.insert(TABLE_FOLDERS, null, entry);
        folder.setId(result);
        return folder;
    }

    public static Folder update(Folder folder, Context context, String selection, String[] selectionArgs) {
        SQLiteDatabase db = DbContext.getDB(context).getWritableDatabase();
        ContentValues entry = new ContentValues();
        entry.put(COLUMN_NAME, folder.getName());
        entry.put(COLUMN_PATH, folder.getPath());
        entry.put(COLUMN_CREATED, Util.dateToString(folder.getCreated()));
        db.update(TABLE_FOLDERS, entry, selection, selectionArgs);
        return folder;
    }

    public static int delete(Context context, String selection, String[] selectionArgs) {
        SQLiteDatabase db = DbContext.getDB(context).getWritableDatabase();
        return db.delete(TABLE_FOLDERS, selection, selectionArgs);
    }

    public static Folder addFilesToFolder(Folder folder, List<File> files, Context context) {
        SQLiteDatabase db = DbContext.getDB(context).getWritableDatabase();
        for (File file: files) {
            ContentValues entry = new ContentValues();
            entry.put(COLUMN_FOLDER, folder.getId().toString());
            entry.put(COLUMN_FILE, file.getId().toString());
            db.insert(TABLE_FOLDERS_FILES, null, entry);
        }
        return folder;
    }

    public static int deleteFilesFromFolder(Context context, String selection, String[] selectionArgs) {
        SQLiteDatabase db = DbContext.getDB(context).getWritableDatabase();
        return db.delete(TABLE_FOLDERS_FILES, selection, selectionArgs);
    }

    public static Folder addSubFolders(Folder folder, List<Folder> subFolders, Context context) {
        SQLiteDatabase db = DbContext.getDB(context).getWritableDatabase();
        for (Folder subFolder: subFolders) {
            ContentValues entry = new ContentValues();
            entry.put(COLUMN_PARENT_FOLDER, folder.getId().toString());
            entry.put(COLUMN_CHILD_FOLDER, subFolder.getId().toString());
            db.insert(TABLE_SUBFOLDERS, null, entry);
        }
        return folder;
    }

    public static int deleteSubFolders(Context context, String selection, String[] selectionArgs) {
        SQLiteDatabase db = DbContext.getDB(context).getWritableDatabase();
        return db.delete(TABLE_SUBFOLDERS, selection, selectionArgs);
    }

    public static Cursor queryFilesInFolder(String selection, String[] selectionArgs, Context context) {
        String[] projection = {COLUMN_ID, COLUMN_FOLDER, COLUMN_FILE};
        SQLiteDatabase db = DbContext.getDB(context).getWritableDatabase();
        return db.query(TABLE_FOLDERS_FILES, projection, selection,
                selectionArgs, null, null, null);
    }

    public static List<File> findFilesInFolder(String selection, String[] selectionArgs, Context context) {
        List<File> result = new ArrayList<>();
        Cursor cursor = queryFilesInFolder(selection, selectionArgs, context);
        while(cursor.moveToNext()) {
            Long id = cursor.getLong(cursor.getColumnIndexOrThrow(COLUMN_FILE));
            List<File> files = FileRepository.findFiles(FileRepository.COLUMN_ID + " = ?",
                    new String[] {id.toString()}, context);
            if (files.size() == 1) {
                result.add(files.get(0));
            }
        }
        cursor.close();
        return result;
    }

    public static Cursor querySubFolders(String selection, String[] selectionArgs, Context context) {
        String[] projection = {COLUMN_ID, COLUMN_PARENT_FOLDER, COLUMN_CHILD_FOLDER};
        SQLiteDatabase db = DbContext.getDB(context).getWritableDatabase();
        return db.query(TABLE_SUBFOLDERS, projection, selection,
                selectionArgs, null, null, null);
    }

    public static List<Folder> findSubFolders(String selection, String[] selectionArgs, Context context) {
        List<Folder> result = new ArrayList<>();
        Cursor cursor = querySubFolders(selection, selectionArgs, context);
        while (cursor.moveToNext()) {
            Long id = cursor.getLong(cursor.getColumnIndexOrThrow(COLUMN_FILE));
            List<Folder> folders = findFolders(COLUMN_ID + " = ?",
                    new String[]{id.toString()}, context);
            if (folders.size() == 1) {
                result.add(folders.get(0));
            }
        }
        cursor.close();
        return result;
    }
}

