package rs.ac.uns.ftn.medily.tools;

import android.app.Activity;
import android.content.Context;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import lombok.SneakyThrows;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rs.ac.uns.ftn.medily.R;
import rs.ac.uns.ftn.medily.activities.MainActivity;
import rs.ac.uns.ftn.medily.fragments.MusicFragment;
import rs.ac.uns.ftn.medily.fragments.SongsFragment;
import rs.ac.uns.ftn.medily.model.Playlist;
import rs.ac.uns.ftn.medily.dto.ManageSongWithinPlaylistDto;
import rs.ac.uns.ftn.medily.dto.UpdateSongPositionDto;
import rs.ac.uns.ftn.medily.model.Song;
import rs.ac.uns.ftn.medily.service.ServiceUtils;
import rs.ac.uns.ftn.medily.tools.db.DatabaseHelper;
import rs.ac.uns.ftn.medily.tools.db.PlaylistRepository;
import rs.ac.uns.ftn.medily.tools.db.SongRepository;

public class Util {

    public static String convertToMinutesString(int duration) {
        long minutes = TimeUnit.SECONDS.toMinutes(duration) - (TimeUnit.SECONDS.toHours(duration) * 60);
        long seconds = TimeUnit.SECONDS.toSeconds(duration) - (TimeUnit.SECONDS.toMinutes(duration) * 60);

        String sec = String.valueOf(seconds);
        if (sec.length() == 1) {
            sec = "0" + sec;
        }
        return minutes + ":" + sec;
    }

    public static String convertAfterProgress(int progress) {
        final long minutes = (progress / 1000) / 60; //converting into minutes
        final int seconds = ((progress / 1000) % 60); //converting into seconds

        String sec = String.valueOf(seconds);
        if (sec.length() == 1) {
            sec = "0" + sec;
        }

        return minutes + ":" + sec;
    }

    /**
     *   Sync collapsed and expanded view for player.
     *  Ex. when song is being played, pause button
     *  needs to be visible on both expanded and
     *  collapsed view.
     */
    public static void syncViewsButtons(String viewType, ImageButton button, Activity activity, View v) {
        if (button != null) {
            if (button.getDrawable() != null) {
                if (button.getDrawable().getConstantState() != null) {
                    if (viewType.equals("collapsed")) {
                        Integer pauseIcon = R.drawable.ic_pause_collapsed;
                        Integer playIcon = R.drawable.ic_play_collapsed;

                        ImageButton syncButton = (ImageButton) activity.findViewById(R.id.btn_play_pause_player);
                        if (syncButton != null) {
                            Integer resource = getImageTag(button);
                            if (resource != null) {
                                if (resource.equals(pauseIcon)) {
                                    syncButton.setImageDrawable(ContextCompat.getDrawable(v.getContext(), R.mipmap.ic_pause));
                                    syncButton.setTag(R.mipmap.ic_pause);
                                } else if (resource.equals(playIcon)) {
                                    syncButton.setImageDrawable(ContextCompat.getDrawable(v.getContext(), R.mipmap.ic_play));
                                    syncButton.setTag(R.mipmap.ic_play);
                                }
                            }
                        }
                    } else if (viewType.equals("expanded")) {
                        Integer pauseIcon = R.mipmap.ic_pause;
                        Integer playIcon = R.mipmap.ic_play;

                        ImageButton syncButton = (ImageButton) activity.findViewById(R.id.play_collapsed);
                        if (syncButton != null) {
                            Integer resource = getImageTag(button);
                            if (resource != null) {
                                if (resource.equals(pauseIcon)) {
                                    syncButton.setImageDrawable(ContextCompat.getDrawable(v.getContext(), R.drawable.ic_pause_collapsed));
                                    syncButton.setTag(R.drawable.ic_pause_collapsed);
                                } else if (resource.equals(playIcon)) {
                                    syncButton.setImageDrawable(ContextCompat.getDrawable(v.getContext(), R.drawable.ic_play_collapsed));
                                    syncButton.setTag(R.drawable.ic_play_collapsed);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     *   Sync collapsed and expanded view for player.
     *  Ex. when song is being played, pause button
     *  needs to be visible on both expanded and
     *  collapsed view.
     */
    public static void syncViewsAtStartButtons(String viewType, ImageButton button, View v) {
        if (button != null) {
            if (button.getDrawable() != null) {
                if (button.getDrawable().getConstantState() != null) {
                    if (viewType.equals("collapsed")) {
                        Integer pauseIcon = R.drawable.ic_pause_collapsed;
                        Integer playIcon = R.drawable.ic_play_collapsed;

                        ImageButton syncButton = (ImageButton) v.findViewById(R.id.btn_play_pause_player);
                        if (syncButton != null) {
                            Integer resource = getImageTag(button);
                            if (resource != null) {
                                if (resource.equals(pauseIcon)) {
                                    syncButton.setImageDrawable(ContextCompat.getDrawable(v.getContext(), R.mipmap.ic_pause));
                                    syncButton.setTag(R.mipmap.ic_pause);
                                } else if (resource.equals(playIcon)) {
                                    syncButton.setImageDrawable(ContextCompat.getDrawable(v.getContext(), R.mipmap.ic_play));
                                    syncButton.setTag(R.mipmap.ic_play);
                                }
                            }
                        }
                    } else if (viewType.equals("expanded")) {
                        Integer pauseIcon = R.mipmap.ic_pause;
                        Integer playIcon = R.mipmap.ic_play;

                        ImageButton syncButton = (ImageButton) v.findViewById(R.id.play_collapsed);
                        if (syncButton != null) {
                            Integer resource = getImageTag(button);
                            if (resource != null) {
                                if (resource.equals(pauseIcon)) {
                                    syncButton.setImageDrawable(ContextCompat.getDrawable(v.getContext(), R.drawable.ic_pause_collapsed));
                                    syncButton.setTag(R.drawable.ic_pause_collapsed);
                                } else if (resource.equals(playIcon)) {
                                    syncButton.setImageDrawable(ContextCompat.getDrawable(v.getContext(), R.drawable.ic_play_collapsed));
                                    syncButton.setTag(R.drawable.ic_play_collapsed);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public static Integer getImageTag(ImageButton button) {
        try {
            return Integer.valueOf(button.getTag().toString());
        } catch (Exception e) {
            return null;
        }
    }

    public static char getFlag(MenuItem item) {
        return item.getAlphabeticShortcut();
    }

    public static void setFlag(MenuItem item, char c) {
        item.setAlphabeticShortcut(c);
    }

    /**
     * Does sync with player - expanded view, sets
     * song details.
     */
    public static void syncCurrentlyPlayingSong(Song clicked, Activity activity) {
        if (activity != null) {
            TextView currentlyPlaying = (TextView) activity.findViewById(R.id.currently_playing);
            if (currentlyPlaying != null) {
                String artist = "";
                if (clicked.getArtist() != null) {
                    if (clicked.getArtist().length() > 0) {
                        artist = clicked.getArtist() + " - ";
                    }
                }
                String details = artist + clicked.getFile().getName();
                currentlyPlaying.setText(details);
            }

            TextView durationEnd = (TextView) activity.findViewById(R.id.duration_end);
            if (durationEnd != null) {
                durationEnd.setText(convertToMinutesString(clicked.getDuration()));
            }

            TextView name = (TextView) activity.findViewById(R.id.song_name_player_collapsed);
            if (name != null) {
                if (!name.getText().toString().equals(clicked.getFile().getName())) {
                    name.setText(clicked.getFile().getName());
                }
            }

            TextView artist = (TextView) activity.findViewById(R.id.song_artist_player_collapsed);
            if (artist != null) {
                if (!artist.getText().toString().equals(clicked.getArtist())) {
                    artist.setText(clicked.getArtist());
                }
            }

            ImageButton button = (ImageButton) activity.findViewById(R.id.btn_play_pause_player);
            ImageButton buttonCollapsed = (ImageButton) activity.findViewById(R.id.play_collapsed);
            if (button != null && buttonCollapsed != null) {
                button.setImageDrawable(ContextCompat.getDrawable(activity, R.mipmap.ic_pause));
                button.setTag(R.mipmap.ic_pause);
                buttonCollapsed.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.ic_pause_collapsed));
                buttonCollapsed.setTag(R.drawable.ic_pause_collapsed);
            }

            if (activity instanceof MainActivity) {
                List<Fragment> fragments = ((MainActivity) activity).getFragments();
                for (Fragment fragment: fragments) {
                    if (fragment instanceof MusicFragment) {
                        if (((MusicFragment) fragment).getAdapter().getFragmentsMap().containsKey(1)) {
                            Fragment activeFragment = ((MusicFragment) fragment).getAdapter().getFragmentsMap().get(1);
                            if (activeFragment instanceof SongsFragment) {
                                ((SongsFragment) activeFragment).refresh();
                            }
                        }
                        break;
                    }
                }
            }
        }
    }

    public static List<Playlist> filterPlaylists(List<Playlist> playlists, String username) {
        List<Playlist> filtered = new ArrayList<>();
        for (Playlist playlist: playlists) {
            if (playlist.getDownloadedBy() == null) {
                if (playlist.getCreatedBy().equals(username)) {
                    filtered.add(playlist);
                }
            } else {
                if (playlist.getDownloadedBy().equals(username)) {
                    filtered.add(playlist);
                }
            }
        }

        return filtered;
    }

    public static void syncCurrentlyPlayingSongAtStart(Song clicked, View view) {
        if (view != null) {
            TextView currentlyPlaying = (TextView) view.findViewById(R.id.currently_playing);
            if (currentlyPlaying != null) {
                String artist = "";
                if (clicked.getArtist() != null) {
                    if (clicked.getArtist().length() > 0) {
                        artist = clicked.getArtist() + " - ";
                    }
                }
                String details = artist + clicked.getFile().getName();
                currentlyPlaying.setText(details);
            }

            TextView durationEnd = (TextView) view.findViewById(R.id.duration_end);
            if (durationEnd != null) {
                durationEnd.setText(convertToMinutesString(clicked.getDuration()));
            }

            TextView name = (TextView) view.findViewById(R.id.song_name_player_collapsed);
            if (name != null) {
                if (!name.getText().toString().equals(clicked.getFile().getName())) {
                    name.setText(clicked.getFile().getName());
                }
            }

            TextView artist = (TextView) view.findViewById(R.id.song_artist_player_collapsed);
            if (artist != null) {
                if (!artist.getText().toString().equals(clicked.getArtist())) {
                    artist.setText(clicked.getArtist());
                }
            }
        }
    }

    public static void syncCurrentlyPlayingSong(String currentSong, String duration, Activity activity) {
        if (activity != null) {
            TextView currentlyPlaying = (TextView) activity.findViewById(R.id.currently_playing);
            if (currentlyPlaying != null) {
                currentlyPlaying.setText(currentSong);
            }

            TextView durationEnd = (TextView) activity.findViewById(R.id.duration_end);
            if (durationEnd != null) {
                durationEnd.setText(duration);
            }
        }
    }

    private static void playCurrent() {
        MusicPlayer.playCurrent();
    }

    private static void pauseCurrent() {
        MusicPlayer.pause();
    }

    public static void playPauseCurrentSong(ImageButton button, View v, String viewType) {
        Integer pauseIcon = null;
        Integer playIcon = null;
        if (viewType.equals("collapsed")) {
            pauseIcon = R.drawable.ic_pause_collapsed;
            playIcon = R.drawable.ic_play_collapsed;
        } else if (viewType.equals("expanded")) {
            pauseIcon = R.mipmap.ic_pause;
            playIcon = R.mipmap.ic_play;
        }
        Integer resource = getImageTag(button);
        if (resource != null) {
            if (resource.equals(pauseIcon)) {
                NotificationUtil.setPlayerNotificationView(button.getContext());
                pauseCurrent();
            } else if (resource.equals(playIcon)) {
                NotificationUtil.setPlayerNotificationView(button.getContext());
                playCurrent();
            }
        }
    }

    /*
    *  Sets 0:00 as current duration label in media player.
    * */
    public static void setInitCurrentDurationPlayer(Activity activity) {
        TextView currentDuration = (TextView) activity.findViewById(R.id.duration_current);
        if (currentDuration != null) {
            currentDuration.setText("0:00");
        }

    }

    public static void setPauseIcon(Activity activity) {
        ImageButton button = (ImageButton) activity.findViewById(R.id.btn_play_pause_player);
        ImageButton buttonCollapsed = (ImageButton) activity.findViewById(R.id.play_collapsed);
        if (button != null && buttonCollapsed != null) {
            button.setImageDrawable(ContextCompat.getDrawable(activity, R.mipmap.ic_play));
            button.setTag(R.mipmap.ic_play);
            buttonCollapsed.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.ic_play_collapsed));
            buttonCollapsed.setTag(R.drawable.ic_play_collapsed);
        }
    }

    public static boolean checkIfExternalMemoryExists() {
        String state = Environment.getExternalStorageState();

        if (Environment.MEDIA_MOUNTED.equals(state)) {
            // We can read and write the media
            return true;
        } else {
            return false;
        }
    }

    public static int TYPE_WIFI = 1;
    public static int TYPE_MOBILE = 2;
    private static int TYPE_NOT_CONNECTED = 0;

    public static int getConnectivityStatus(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm == null) {
            return TYPE_NOT_CONNECTED;
        }
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (null != activeNetwork) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                return TYPE_WIFI;

            if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                return TYPE_MOBILE;
        }
        return TYPE_NOT_CONNECTED;
    }

    public static boolean checkInternetAccess(Context context) {
        int connectivityStatus = getConnectivityStatus(context);
        if (connectivityStatus == TYPE_WIFI) {
            return true;
        } else if (connectivityStatus == TYPE_NOT_CONNECTED) {
            return false;
        } else {
            return !Boolean.parseBoolean(SharedPreferencesService.get("wifi"));
        }
    }

    private static long getProfilesCount(Activity activity) {
        DatabaseHelper dbHelper = new DatabaseHelper(activity);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        long count = DatabaseUtils.queryNumEntries(db, SongRepository.TABLE_SONGS);
        db.close();
        return count;
    }

    public static Date stringToDate(String date) {
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss", Locale.UK);
        try {
            return sdf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static void animateSongOnCollapsedView(Activity activity) {
        SlidingUpPanelLayout slidingLayout = activity.findViewById(R.id.sliding_layout);
        if (slidingLayout != null) {
            RelativeLayout collapsedView = slidingLayout.findViewById(R.id.collapsed_view);
            if (collapsedView != null) {
                TextView songName = (TextView) collapsedView.findViewById(R.id.song_name_player_collapsed);
                startTextAnimation(songName, activity);
            }
        }
    }

    private static void animateSongOnExpandedView(Activity activity) {
        RelativeLayout expandedView = activity.findViewById(R.id.player_frame);
        if (expandedView != null) {
            TextView songName = (TextView) expandedView.findViewById(R.id.currently_playing);
            startTextAnimation(songName, activity);
        }
    }

    private static void stopAnimationOnCollapsedView(Activity activity) {
        SlidingUpPanelLayout slidingLayout = activity.findViewById(R.id.sliding_layout);
        if (slidingLayout != null) {
            RelativeLayout collapsedView = slidingLayout.findViewById(R.id.collapsed_view);
            if (collapsedView != null) {
                TextView songName = (TextView) collapsedView.findViewById(R.id.song_name_player_collapsed);
                stopTextAnimation(songName);
            }
        }
    }

    private static void stopAnimationOnExpandedView(Activity activity) {
        RelativeLayout expandedView = activity.findViewById(R.id.player_frame);
        if (expandedView != null) {
            TextView songName = (TextView) expandedView.findViewById(R.id.currently_playing);
            stopTextAnimation(songName);
        }
    }

    public static void startAllTextAnimationsOnPlay(Activity activity) {
        animateSongOnCollapsedView(activity);
        animateSongOnExpandedView(activity);
    }

    public static void stopAllTextAnimationsOnPause(Activity activity) {
        stopAnimationOnCollapsedView(activity);
        stopAnimationOnExpandedView(activity);
    }

    public static String trimStringForView(String text){
        if (text.length() <= 25) {
            return text;
        }
        StringBuilder stringBuilder = new StringBuilder();
        while (text.length() > 25) {
            String buffer = text.substring(0, 25);
            stringBuilder.append(buffer).append("\n");
            text = text.substring(25);
        }


        return stringBuilder.toString().substring(0, 25) + "...";
    }

    public static String splitStringIntoMultiLine(String text){
        if (text.length() <= 25) {
            return text;
        }
        StringBuilder stringBuilder = new StringBuilder();
        while (text.length() > 25) {
            String buffer = text.substring(0, 25);
            stringBuilder.append(buffer).append("\n");
            text = text.substring(25);
        }

        stringBuilder = stringBuilder.append(text.substring(0));
        return stringBuilder.toString();
    }

    private static void stopTextAnimation(TextView textView) {
        if (textView != null) {
            textView.clearAnimation();
        }
    }

    private static void startTextAnimation(TextView textView, Activity activity) {
        if (textView != null) {
            textView.startAnimation(AnimationUtils.loadAnimation(activity, R.anim.text_translation));
        }
    }

    public static String dateToString(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss", Locale.UK);
        try {
            return sdf.format(date);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void removeSongToPlaylistInGlobalDb(Playlist playlist, Song clickedSong, Context context, int position) {
        manageSongWithinPlaylistInGlobalDb(new ManageSongWithinPlaylistDto(playlist.getGlobalId(),
                clickedSong.getGlobalId(), "remove", position), context);
    }

    public static void updateSongPositionInPlaylist(UpdateSongPositionDto dto, final Context context) {
        Call<ResponseBody> call = ServiceUtils.mediaService.updatePositionOfSongInPlaylist(dto);
        call.enqueue(new Callback<ResponseBody>() {
            @SneakyThrows
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200) {
                    // leave like this for now
                    //Toast.makeText(context, "Successful operation!",
                    //            Toast.LENGTH_LONG).show();

                } else if (response.code() == 400) {
                    if (context != null) {
                        Toast.makeText(context, response.errorBody().string(),
                                Toast.LENGTH_LONG).show();
                    }
                } else {
                    if (context != null) {
                        Toast.makeText(context, R.string.error_message,
                                Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(context, R.string.error_message,
                        Toast.LENGTH_LONG).show();
            }
        });
    }

    public static void manageSongWithinPlaylistInGlobalDb(ManageSongWithinPlaylistDto dto, final Context context) {
        Call<ResponseBody> call = ServiceUtils.mediaService.manageSongWithinPlaylist(dto);
        call.enqueue(new Callback<ResponseBody>() {
            @SneakyThrows
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200) {
                    Toast.makeText(context, "Successful operation!",
                            Toast.LENGTH_LONG).show();
                } else if (response.code() == 400) {
                    if (context != null) {
                        Toast.makeText(context, response.errorBody().string(),
                                Toast.LENGTH_LONG).show();
                    }
                } else {
                    if (context != null) {
                        Toast.makeText(context, R.string.error_message,
                                Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(context, R.string.error_message,
                        Toast.LENGTH_LONG).show();
            }
        });
    }

    public static void updatePositionOfSongsAfterDelete(Long playlistId, int position, Context context) {
        PlaylistRepository.updatePositionOfSongsInPlaylistAfterDeletingSong(playlistId, position, context);
    }

    /*public static boolean isSongOnExternalStorage(Song song) {
        boolean exists = false;
        if (checkIfExternalMemoryExists()) {
            String externalPath = Environment.getExternalStorageDirectory().getPath();
            if (song.getFile().getPath().contains(externalPath)) {
                // song is on external storage
                exists = true;
            }
        }

        return exists;
    }

    public static boolean isFileOnInternalStorage(String path) {
       // String path = activity.getApplicationContext().getFilesDir().getAbsolutePath() + "/" +  fileName;
        java.io.File file = new java.io.File(path);
        return file.exists();
    }*/

}
