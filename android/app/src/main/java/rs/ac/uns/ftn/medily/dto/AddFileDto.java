package rs.ac.uns.ftn.medily.dto;

import lombok.Getter;
import lombok.Setter;
import rs.ac.uns.ftn.medily.model.File;

@Getter
@Setter
public class AddFileDto {
    private Boolean isRoot;
    private Long parentId;
    private File newFile;
}
