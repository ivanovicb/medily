package rs.ac.uns.ftn.medily.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import lombok.SneakyThrows;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rs.ac.uns.ftn.medily.R;
import rs.ac.uns.ftn.medily.dto.ItemDto;
import rs.ac.uns.ftn.medily.dto.SongFileDto;
import rs.ac.uns.ftn.medily.model.Playlist;
import rs.ac.uns.ftn.medily.model.Song;
import rs.ac.uns.ftn.medily.service.ServiceUtils;
import rs.ac.uns.ftn.medily.tools.Downloader;
import rs.ac.uns.ftn.medily.tools.NotificationUtil;
import rs.ac.uns.ftn.medily.tools.SharedPreferencesService;
import rs.ac.uns.ftn.medily.tools.Util;
import rs.ac.uns.ftn.medily.tools.db.PlaylistRepository;

public class PublicPlaylistsAdapter extends BaseExpandableListAdapter implements Filterable {

    private List<Playlist> data;
    private LayoutInflater layoutInflater;
    private List<Playlist> original;
    private Context context;
    private Activity activity;

    public PublicPlaylistsAdapter(List<Playlist> playlists, Context context, Activity activity) {
        super();
        this.data = new ArrayList<>(playlists);
        this.original = new ArrayList<>(playlists);
        this.layoutInflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.context = context;
        this.activity = activity;
    }

    @Override
    public int getGroupCount() {
        return data.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return data.get(groupPosition).getSongs().size();
    }

    @Override
    public Playlist getGroup(int groupPosition) {
        return data.get(groupPosition);
    }

    @Override
    public Song getChild(int groupPosition, int childPosition) {
        return data.get(groupPosition).getSongs().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        View view = convertView;
        final Playlist playlist = data.get(groupPosition);

        if(view == null) {
            view = layoutInflater.inflate(R.layout.layout_public_playlist, null);
        }

        final TextView playlistName = view.findViewById(R.id.public_playlist_name);

        playlistName.setText(playlist.getName());

        ImageButton btnDownload = view.findViewById(R.id.btn_download_playlist);
        btnDownload.setFocusable(false);
        btnDownload.setFocusableInTouchMode(false);

        List<Playlist> playlists = PlaylistRepository
                .findPlaylistsMineAndShared(context, SharedPreferencesService.get("username"));

        boolean downloaded = false;

        for (Playlist downloadedPlaylist: playlists) {
            if (downloadedPlaylist.getGlobalId().equals(playlist.getId())) {
                downloaded = true;
                break;
            }
        }

        if (Downloader.downloadingPlaylists.contains(playlist.getId()) || downloaded) {
            btnDownload.setVisibility(View.INVISIBLE);
        } else {
            btnDownload.setVisibility(View.VISIBLE);
        }

        btnDownload.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (Util.checkInternetAccess(context)) {
                    NotificationUtil.notifyDownloadCompleted(context, playlist.getName(),
                            context.getSystemService(Context.NOTIFICATION_SERVICE));

                    Downloader.downloadingPlaylists.add(playlist.getId());
                    refresh();
                    downloadPlaylist(playlist);
                }
            }
        });

        return  view;
    }

    private void downloadPlaylist(Playlist playlist) {
        Playlist newPlaylist = new Playlist(playlist.getName(), playlist.isSharePlaylist(),
                playlist.getCreatedBy(), playlist.getGlobalId(), SharedPreferencesService.get("username"));
        PlaylistRepository.add(newPlaylist, context);

        getSongsFromPlaylist(newPlaylist, playlist.getId());
    }

    private void getSongsFromPlaylist(final Playlist newPlaylist, final Long downloadingPlaylistWithId) {
        Call<List<SongFileDto>> call = ServiceUtils.mediaService.getSongsWithinPlaylist(new ItemDto(newPlaylist.getGlobalId().toString()));
        call.enqueue(new Callback<List<SongFileDto>>() {
            @SneakyThrows
            @Override
            public void onResponse(Call<List<SongFileDto>> call, Response<List<SongFileDto>> response) {
                if (response.code() == 200) {
                    String user = "local_drive_user_" + SharedPreferencesService.get("user_id");
                    Downloader.downloadPlaylist(activity, user, newPlaylist, response.body(), downloadingPlaylistWithId);
                } else if (response.code() == 400) {
                    if (context != null) {
                        Toast.makeText(context, response.errorBody().string(),
                                Toast.LENGTH_LONG).show();
                        Downloader.downloadingPlaylists.remove(newPlaylist.getGlobalId());
                    }
                } else {
                    if (context != null) {
                        Toast.makeText(context, "Connection to server could not be " +
                                        "established. Please try again later.",
                                Toast.LENGTH_LONG).show();
                        Downloader.downloadingPlaylists.remove(newPlaylist.getGlobalId());
                    }
                }
            }

            @Override
            public void onFailure(Call<List<SongFileDto>> call, Throwable t) {
                Toast.makeText(context, "Connection to server could not be " +
                                "established. Please try again later.",
                        Toast.LENGTH_LONG).show();
                Downloader.downloadingPlaylists.remove(newPlaylist.getGlobalId());
            }
        });
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        View view = convertView;
        final Song song = data.get(groupPosition).getSongs().get(childPosition);

        if(view == null) {
            view = layoutInflater.inflate(R.layout.layout_song, null);
        }

        TextView songName = view.findViewById(R.id.song_name);
        TextView artist = view.findViewById(R.id.artist);
        TextView duration = view.findViewById(R.id.duration);

        songName.setText(song.getFile().getName());
        artist.setText(song.getArtist());
        duration.setText(Util.convertToMinutesString(song.getDuration()));

        view.findViewById(R.id.btn_plus).setVisibility(View.GONE);
        return  view;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    private Filter filter = new Filter () {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            List<Playlist> filtered = performSearch(constraint);
            results.count = filtered.size();
            results.values = filtered;
            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            data = (List<Playlist>) results.values;
            notifyDataSetChanged();
        }
    };

    @Override
    public Filter getFilter() {
        return filter;
    }

    private List<Playlist> performSearch(CharSequence query) {
        List<Playlist> resultPlaylists = new ArrayList<>();
        query = query.toString().toLowerCase();
        for (Playlist playlist: original) {
            if (playlist.getName().toLowerCase().contains(query)) {
                resultPlaylists.add(playlist);
            }
        }
        return resultPlaylists;
    }

    public void refresh() {
        data = new ArrayList<>(original);
        notifyDataSetChanged();
    }
}
