package rs.ac.uns.ftn.medily.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UpdateSongPositionDto {

    private Long songId;
    private Long playlistId;
    private int oldPosition;
    private int newPosition;
}
