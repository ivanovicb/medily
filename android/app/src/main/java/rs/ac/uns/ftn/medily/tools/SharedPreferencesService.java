package rs.ac.uns.ftn.medily.tools;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;

public class SharedPreferencesService {

    private static SharedPreferences preferences;
    private static SharedPreferences.Editor editor;

    public SharedPreferencesService() {
    }

    @SuppressLint("CommitPrefEdits")
    public static void init(Context context) {
        preferences = context.getSharedPreferences("medilyPrefs", MODE_PRIVATE);
        editor = preferences.edit();
    }

    public static void add(String key, String value) {
        editor.putString(key, value).apply();
    }

    public static void remove(String key) {
        editor.remove(key).apply();
    }

    public static String get(String key) {
        if (key.contains("last_sync")) {
            return preferences.getString(key, "");
        }

        if (preferences != null) {
            switch (key) {
                case "user_id":
                case "token":
                case "name":
                case "username":
                case "family":
                case "sync":
                    return preferences.getString(key, "");
                case "storage":
                    return preferences.getString(key, Util.checkIfExternalMemoryExists() ? "external" : "internal");
                case "wifi":
                    return preferences.getString(key, "false");
                case "sensors":
                    return preferences.getString(key, "true");
                default:
                    return preferences.getString(key, null);
            }
        }

        return null;
    }

    public static SharedPreferences getPreferences(){
        return preferences;
    }
}
