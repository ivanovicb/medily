package rs.ac.uns.ftn.medily.dto;

import lombok.Getter;
import lombok.Setter;
import rs.ac.uns.ftn.medily.model.Folder;

@Getter
@Setter
public class AddFolderDto {
    private boolean isRoot;
    private Long parentId;
    private Folder newFolder;
}
