package rs.ac.uns.ftn.medily.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SongDto {

    @SerializedName("id")
    @Expose
    private Long id;

    @SerializedName("artist")
    @Expose
    private String artist;

    @SerializedName("genre")
    @Expose
    private String genre;

    @SerializedName("duration")
    @Expose
    private int duration;

    @SerializedName("fileId")
    @Expose
    private Long fileId;        // global id

    @SerializedName("filePath")
    @Expose
    private String filePath;

    @SerializedName("name")
    @Expose
    private String name;

    private int position; // if in playlist

    public SongDto() {
    }

    public SongDto(Long id, String artist, String genre, int duration, Long fileId, String filePath, String name) {
        this.id = id;
        this.artist = artist;
        this.genre = genre;
        this.duration = duration;
        this.fileId = fileId;
        this.filePath = filePath;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPosition() { return position; }

    public void setPosition(int position) { this.position = position; }

    @Override
    public String toString() {
        return "SongDto{" +
                "id=" + id +
                ", artist='" + artist + '\'' +
                ", genre='" + genre + '\'' +
                ", duration=" + duration +
                ", fileId=" + fileId +
                ", filePath='" + filePath + '\'' +
                ", name='" + name + '\'' +
                ", position=" + position +
                '}';
    }
}
