package rs.ac.uns.ftn.medily.tools.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "medily.db";
    private static final int DATABASE_VERSION = 1;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DriveRepository.DB_CREATE);
        db.execSQL(FileRepository.DB_CREATE);
        db.execSQL(FolderRepository.DB_CREATE);
        db.execSQL(FolderRepository.DB_CREATE_SUBFOLDERS);
        db.execSQL(FolderRepository.DB_CREATE_FOLDERS_FILES);
        db.execSQL(DriveRepository.DB_CREATE_DRIVE_FILES);
        db.execSQL(DriveRepository.DB_CREATE_DRIVE_FOLDERS);
        db.execSQL(SongRepository.DB_CREATE);
        db.execSQL(PlaylistRepository.DB_CREATE);
        db.execSQL(PlaylistRepository.DB_CREATE_PLAYLISTS_SONGS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + FolderRepository.TABLE_SUBFOLDERS);
        db.execSQL("DROP TABLE IF EXISTS " + FolderRepository.TABLE_FOLDERS_FILES);
        db.execSQL("DROP TABLE IF EXISTS " + PlaylistRepository.TABLE_PLAYLISTS_SONGS);
        db.execSQL("DROP TABLE IF EXISTS " + DriveRepository.TABLE_DRIVE_FILES);
        db.execSQL("DROP TABLE IF EXISTS " + DriveRepository.TABLE_DRIVE_FOLDERS);
        db.execSQL("DROP TABLE IF EXISTS " + PlaylistRepository.TABLE_PLAYLISTS);
        db.execSQL("DROP TABLE IF EXISTS " + SongRepository.TABLE_SONGS);
        db.execSQL("DROP TABLE IF EXISTS " + FileRepository.TABLE_FILES);
        db.execSQL("DROP TABLE IF EXISTS " + FolderRepository.TABLE_FOLDERS);
        db.execSQL("DROP TABLE IF EXISTS " + DriveRepository.TABLE_DRIVES);
        onCreate(db);
    }
}
