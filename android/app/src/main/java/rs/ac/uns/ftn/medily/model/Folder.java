package rs.ac.uns.ftn.medily.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public
class Folder {
    @SerializedName("id")
    private Long id;

    @SerializedName("name")
    private String name;

    @SerializedName("path")
    private String path;

    @SerializedName("created")
    private Date created;

    @SerializedName("subFolders")
    private List<Folder> subFolders;

    @SerializedName("files")
    private List<File> files;

    public Folder(){
        this.subFolders = new ArrayList<>();
        this.files = new ArrayList<>();
    }

    public Folder(Long id, String name, String path, Date created, List<Folder> subFolders, List<File> files) {
        this.id = id;
        this.name = name;
        this.path = path;
        this.created = created;
        this.subFolders = subFolders;
        this.files = files;
    }
}
