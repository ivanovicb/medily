package rs.ac.uns.ftn.medily.tools.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import java.util.ArrayList;
import java.util.List;

import rs.ac.uns.ftn.medily.model.File;
import rs.ac.uns.ftn.medily.tools.Util;

public class FileRepository {

    public static final String TABLE_FILES = "files";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_GLOBAL_ID = "global_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_PATH = "path";
    public static final String COLUMN_SIZE = "size";
    public static final String COLUMN_CREATED = "created";

    public static final String DB_CREATE = "create table "
            + TABLE_FILES + "("
            + COLUMN_ID  + " integer primary key autoincrement, "
            + COLUMN_GLOBAL_ID + " integer, "
            + COLUMN_NAME + " text, "
            + COLUMN_PATH + " text, "
            + COLUMN_SIZE + " real, "
            + COLUMN_CREATED + " text"
            + ")";

    public static Cursor query(String selection, String[] selectionArgs, Context context) {
        String[] projection = {COLUMN_ID, COLUMN_GLOBAL_ID, COLUMN_NAME, COLUMN_PATH, COLUMN_SIZE, COLUMN_CREATED};
        SQLiteDatabase db = DbContext.getDB(context).getWritableDatabase();
        return db.query(TABLE_FILES, projection, selection,
                selectionArgs, null, null, null);
    }

    public static List<File> findFiles(String selection, String[] selectionArgs, Context context) {
        List<File> result = new ArrayList<>();
        Cursor cursor = query(selection, selectionArgs, context);
        while(cursor.moveToNext()) {
            File file = new File(cursor.getLong(cursor.getColumnIndexOrThrow(COLUMN_ID)),
                    cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_NAME)),
                    cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_PATH)),
                    cursor.getDouble(cursor.getColumnIndexOrThrow(COLUMN_SIZE)),
                    Util.stringToDate(cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_CREATED))),
                    cursor.getLong(cursor.getColumnIndexOrThrow(COLUMN_GLOBAL_ID)));
            result.add(file);
        }
        cursor.close();
        return result;
    }

    public static List<File> findByGlobalId(Long id, Context context) {
        String selection = COLUMN_GLOBAL_ID + " = ?";
       return findFiles(selection, new String[] { id.toString() }, context);
    }

    public static File add(File file, Context context) {
        SQLiteDatabase db = DbContext.getDB(context).getWritableDatabase();
        ContentValues entry = new ContentValues();
        entry.put(COLUMN_NAME, file.getName());
        entry.put(COLUMN_GLOBAL_ID, file.getGlobalId());
        entry.put(COLUMN_PATH, file.getPath());
        entry.put(COLUMN_SIZE, file.getSize());
        entry.put(COLUMN_CREATED, Util.dateToString(file.getCreated()));
        long result = db.insert(TABLE_FILES, null, entry);
        file.setId(result);
        return file;
    }

    public static File update(File file, Context context, String selection, String[] selectionArgs) {
        SQLiteDatabase db = DbContext.getDB(context).getWritableDatabase();
        ContentValues entry = new ContentValues();
        entry.put(COLUMN_NAME, file.getName());
        entry.put(COLUMN_GLOBAL_ID, file.getGlobalId());
        entry.put(COLUMN_PATH, file.getPath());
        entry.put(COLUMN_SIZE, file.getSize());
        entry.put(COLUMN_CREATED, Util.dateToString(file.getCreated()));
        db.update(TABLE_FILES, entry, selection, selectionArgs);
        return file;
    }

    public static int delete(Context context, String selection, String[] selectionArgs) {
        SQLiteDatabase db = DbContext.getDB(context).getWritableDatabase();
        return db.delete(TABLE_FILES, selection, selectionArgs);
    }
}
