package rs.ac.uns.ftn.medily.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public
class FolderPermission {
    @SerializedName("id")
    private Long id;

    @SerializedName("name")
    private String name;

    @SerializedName("path")
    private String path;

    @SerializedName("created")
    private Date created;

    @SerializedName("subFolders")
    private List<FolderPermission> subFolders;

    @SerializedName("files")
    private List<FilePermission> files;

    @SerializedName("allow")
    private boolean allow;

    public FolderPermission(){
        this.subFolders = new ArrayList<>();
        this.files = new ArrayList<>();
    }
}
