package rs.ac.uns.ftn.medily.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.view.Window;
import android.widget.ImageView;

import androidx.annotation.NonNull;

import rs.ac.uns.ftn.medily.R;

public class ShowImageDialog extends Dialog {

    private Uri uri;

    public ShowImageDialog(@NonNull Context context, Uri uri) {
        super(context);
        this.uri = uri;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.layout_show_image);
        ImageView imageView = findViewById(R.id.image_view_drive_show_image);
        imageView.setImageURI(uri);
    }
}
