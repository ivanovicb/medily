package rs.ac.uns.ftn.medily.tools;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.greenrobot.eventbus.EventBus;

import java.io.ByteArrayInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.Stack;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import lombok.SneakyThrows;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rs.ac.uns.ftn.medily.R;
import rs.ac.uns.ftn.medily.dto.SongDto;
import rs.ac.uns.ftn.medily.dto.SongFileDto;
import rs.ac.uns.ftn.medily.events.LocalDriveChangeEvent;
import rs.ac.uns.ftn.medily.fragments.MusicFragment;
import rs.ac.uns.ftn.medily.fragments.PublicPlaylistsFragment;
import rs.ac.uns.ftn.medily.model.File;
import rs.ac.uns.ftn.medily.model.Folder;
import rs.ac.uns.ftn.medily.model.Playlist;
import rs.ac.uns.ftn.medily.model.Song;
import rs.ac.uns.ftn.medily.service.ServiceUtils;
import rs.ac.uns.ftn.medily.sync.SyncService;
import rs.ac.uns.ftn.medily.tools.db.FileRepository;
import rs.ac.uns.ftn.medily.tools.db.PlaylistRepository;
import rs.ac.uns.ftn.medily.tools.db.SongRepository;

public class Downloader {

    public static Set<Long> downloadingPlaylists = new HashSet<>();

    private Downloader() {}

    public static void downloadFolder(final Context context, final Folder driveFolder, final String localDriveDirectory){
        ServiceUtils.driveService.downloadFolder(driveFolder.getId()).enqueue(new Callback<ResponseBody>() {
            @SneakyThrows
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200) {
                    try {
                        if (response.body() == null) {
                            Toast.makeText(context, "Folder could not be found!", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        String storagePath = FileSystemUtil.getStoragePath(context);

                        byte[] buffer = new byte[1024];
                        ByteArrayInputStream bin = new ByteArrayInputStream(response.body().bytes());
                        ZipInputStream zis = new ZipInputStream(bin);
                        ZipEntry zipEntry = zis.getNextEntry();

                        String rootName = FileSystemUtil.localDriveCreateFileName(new java.io.File(storagePath, localDriveDirectory), driveFolder.getName());

                        while (zipEntry != null) {
                            Long fileId = Long.parseLong(zipEntry.getName());
                            Stack<String> path = new Stack<>();
                            FileSystemUtil.findLocalFilePath(driveFolder, fileId, path);
                            java.io.File dir = new java.io.File(storagePath, localDriveDirectory);

                            path.pop();
                            path.add(rootName);

                            while (path.size() > 1) {
                                dir = new java.io.File(dir, path.pop());
                                if (!dir.exists()) {
                                    dir.mkdir();
                                }
                            }
                            String name = path.pop();
                            java.io.File fileToSave = new java.io.File(dir, name);
                            FileOutputStream fos = new FileOutputStream(fileToSave);

                            int len;
                            while ((len = zis.read(buffer)) > 0) {
                                fos.write(buffer, 0, len);
                            }
                            fos.close();

                            // if file is audio file, save to db
                            if (FileSystemUtil.isAudio(context, FilenameUtils.getExtension(name))) {
                                File dbFile = new File();
                                dbFile.setId(fileId);
                                dbFile.setName(name);
                                dbFile.setPath(fileToSave.getAbsolutePath());
                                dbFile.setSize(fileToSave.length());
                                dbFile.setCreated(new Date());
                                saveSongToDB(context, dbFile);
                            }

                            zipEntry = zis.getNextEntry();
                        }
                        zis.closeEntry();
                        zis.close();

                        FileSystemUtil.createFoldersWithoutFiles(new java.io.File(storagePath, localDriveDirectory), driveFolder);

                        Toast.makeText(context, R.string.download_completed, Toast.LENGTH_LONG).show();

                        NotificationUtil.notifyDownloadCompleted(context, driveFolder.getName(),
                                context.getSystemService(Context.NOTIFICATION_SERVICE));
                        EventBus.getDefault().post(new LocalDriveChangeEvent(driveFolder, null));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else if (response.code() == 400) {
                    Toast.makeText(context, response.errorBody().string(),
                            Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(context, "Invalid action!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(context, R.string.error_message, Toast.LENGTH_SHORT).show();
                Log.e("Download file failure", Objects.requireNonNull(t.getMessage()));
            }
        });

    }

    public static void downloadFile(final Context context, final File driveFile, final String localDriveDirectory){
        ServiceUtils.driveService.downloadFile(driveFile.getId()).enqueue(new Callback<ResponseBody>() {
            @SneakyThrows
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200) {
                    try {
                        if (response.body() == null) {
                            Toast.makeText(context, "File could not be found!", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        String storagePath = FileSystemUtil.getStoragePath(context);

                        java.io.File dirDrive = new java.io.File(storagePath, localDriveDirectory);
                        if (!dirDrive.exists()) {
                            dirDrive.mkdir();
                        }

                        java.io.File dir = new java.io.File(storagePath, localDriveDirectory);
                        String name = FileSystemUtil.localDriveCreateFileName(dir, driveFile.getName());
                        java.io.File fileToSave = new java.io.File(dir, name);
                        FileUtils.writeByteArrayToFile(fileToSave, response.body().bytes());

                        Toast.makeText(context, R.string.download_completed, Toast.LENGTH_LONG).show();
                        NotificationUtil.notifyDownloadCompleted(context, driveFile.getName(),
                                context.getSystemService(Context.NOTIFICATION_SERVICE));
                        EventBus.getDefault().post(new LocalDriveChangeEvent(null, driveFile));

                        if (FileSystemUtil.isAudio(context, FilenameUtils.getExtension(name))) {
                            File dbFile = new File();
                            if (driveFile.getGlobalId() != null) {
                                dbFile.setId(driveFile.getGlobalId());
                            } else {
                                dbFile.setId(driveFile.getId());
                            }

                            dbFile.setName(name);
                            dbFile.setPath(fileToSave.getAbsolutePath());
                            dbFile.setSize(fileToSave.length());
                            dbFile.setCreated(new Date());
                            saveSongToDB(context, dbFile);
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                } else if (response.code() == 400) {
                    Toast.makeText(context, response.errorBody().string(),
                            Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(context, "Invalid action!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(context, R.string.error_message, Toast.LENGTH_SHORT).show();
                Log.e("Download file failure", Objects.requireNonNull(t.getMessage()));
            }
        });
    }

    public static void downloadPlaylist(final Activity activity, final String localDriveDirectory, final Playlist newPlaylist, final List<SongFileDto> songs,
                                        final Long downloadingPlaylistWithId){
        final Context context = activity.getApplicationContext();
        ServiceUtils.driveService.downloadPlaylist(newPlaylist.getGlobalId()).enqueue(new Callback<ResponseBody>() {
            @SneakyThrows
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200) {
                    try {
                        String storagePath = FileSystemUtil.getStoragePath(context);

                        java.io.File dirDrive = new java.io.File(storagePath, localDriveDirectory);
                        if (!dirDrive.exists()) {
                            dirDrive.mkdir();
                        }

                        byte[] buffer = new byte[1024];
                        ByteArrayInputStream bin = new ByteArrayInputStream(response.body().bytes());
                        ZipInputStream zis = new ZipInputStream(bin);
                        ZipEntry zipEntry = zis.getNextEntry();
                        int counter = 0;

                        while (zipEntry != null) {
                            SongFileDto songDto = songs.get(counter);
                            List<Song> songsDb = SongRepository.findSongs(SongRepository.COLUMN_GLOBAL_ID + " = ? AND " + SongRepository.COLUMN_DOWNLOADED_BY + " = ?",
                                    new String[]{songDto.getId().toString(), SharedPreferencesService.get("username")}, context);
                            if (songsDb.size() < 1) {
                                java.io.File dir = new java.io.File(storagePath, localDriveDirectory);
                                String name = FileSystemUtil.localDriveCreateFileName(dir, songDto.getFile().getName());
                                java.io.File fileToSave = new java.io.File(dir, name);
                                FileOutputStream fos = new FileOutputStream(fileToSave);

                                int len;
                                while ((len = zis.read(buffer)) > 0) {
                                    fos.write(buffer, 0, len);
                                }
                                fos.close();

                                File dbFile = new File();
                                dbFile.setName(name);
                                dbFile.setPath(fileToSave.getAbsolutePath());
                                dbFile.setSize(fileToSave.length());
                                dbFile.setCreated(new Date());
                                dbFile.setGlobalId(songDto.getFile().getId());
                                FileRepository.add(dbFile, context);

                                Song song = new Song();
                                song.setFile(dbFile);
                                String artist = songDto.getArtist() == null ? "" : songDto.getArtist();
                                String genre = songDto.getGenre() == null ? "" : songDto.getGenre();
                                song.setArtist(artist);
                                song.setDuration(songDto.getDuration());
                                song.setGenre(genre);
                                song.setGlobalId(songDto.getId());
                                song.setDownloadedBy(SharedPreferencesService.get("username"));
                                SongRepository.add(song, context);
                                songs.get(counter).setId(song.getId());
                            } else {
                                songs.get(counter).setId(songsDb.get(0).getId());
                            }
                            zipEntry = zis.getNextEntry();
                            counter++;
                        }
                        zis.closeEntry();
                        zis.close();
                        PlaylistRepository.deleteSongsFromPlaylist(context, PlaylistRepository.COLUMN_PLAYLIST + " = ?", new String[]{String.valueOf(newPlaylist.getId())});
                        PlaylistRepository.addSongsToDownloadedPlaylist(newPlaylist, songs, context);
                        // FileSystemUtil.createFoldersWithoutFiles(new java.io.File(storagePath, localDriveDirectory), driveFolder);

                        Toast.makeText(context, R.string.download_completed, Toast.LENGTH_LONG).show();
                        Downloader.downloadingPlaylists.remove(newPlaylist.getGlobalId());
                        refreshAfterDownload((FragmentActivity) activity, downloadingPlaylistWithId);
                        NotificationUtil.notifyDownloadCompleted(context, newPlaylist.getName(),
                                context.getSystemService(Context.NOTIFICATION_SERVICE));
                    } catch (IOException e) {
                        e.printStackTrace();
                        Downloader.downloadingPlaylists.remove(newPlaylist.getGlobalId());
                    }
                } else if (response.code() == 400) {
                    Toast.makeText(context, response.errorBody().string(),
                            Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(context, "Invalid action!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(context, R.string.error_message, Toast.LENGTH_SHORT).show();
                Log.e("Download file failure", Objects.requireNonNull(t.getMessage()));
                Downloader.downloadingPlaylists.remove(newPlaylist.getGlobalId());
            }
        });
    }

    private static void refreshAfterDownload(FragmentActivity activity, Long playlistId) {
        Fragment foundFragment = null;
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        List<Fragment> fragments = fragmentManager.getFragments();
        for(Fragment fragment : fragments){
            if(fragment != null && fragment.isVisible())
                foundFragment = fragment;
        }

        if (foundFragment != null) {
            if (foundFragment instanceof MusicFragment) {
                if (((MusicFragment) foundFragment).getAdapter().getFragmentsMap().containsKey(2)) {
                    Fragment activeFragment = ((MusicFragment) foundFragment).getAdapter().getFragmentsMap().get(2);
                    if (activeFragment instanceof PublicPlaylistsFragment) {
                        ((PublicPlaylistsFragment) activeFragment).refresh();
                    }
                }
            }
        }
    }

    public static void downloadSong(final Context context, final SyncService syncService, final SongDto songDto, final Long playlistId){
        final String localDriveDirectory = FileSystemUtil.getLocalDriveDirectoryName();
        ServiceUtils.driveService.downloadFile(songDto.getFileId()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200) {
                    try {
                        List<Song> songsDb = SongRepository.findSongs(SongRepository.COLUMN_GLOBAL_ID + " = ? AND " + SongRepository.COLUMN_DOWNLOADED_BY + " = ?",
                                new String[] { songDto.getId().toString(), SharedPreferencesService.get("username") }, context);
                        String username = SharedPreferencesService.get("username");
                        List<Playlist> playlistsDb = PlaylistRepository.findPlaylists(PlaylistRepository.COLUMN_GLOBAL_ID + " = ? AND (" + PlaylistRepository.COLUMN_DOWNLOADED_BY + " = ? OR " + PlaylistRepository.COLUMN_CREATED_BY +  " = ?)", new String[] { playlistId.toString(), username, username }, context);
                        long playlistDbId = 0;
                        if (playlistsDb.size() < 1) {
                            return;
                        } else if (playlistsDb.size() == 1) {   // user can download playlist only once
                            playlistDbId = playlistsDb.get(0).getId();
                        } else {
                            for (Playlist playlist: playlistsDb) {  // owner of playlist and multiple users use same device so need to find owner
                                if (playlist.getCreatedBy().equals(username) && playlist.getDownloadedBy() == null) {
                                    playlistDbId = playlist.getId();
                                    break;
                                }
                            }
                        }
                        if (songsDb.size() < 1) {
                            String storagePath = FileSystemUtil.getStoragePath(context);

                            java.io.File dirDrive = new java.io.File(storagePath, localDriveDirectory);
                            if (!dirDrive.exists()) {
                                dirDrive.mkdir();
                            }

                            java.io.File dir = new java.io.File(storagePath, localDriveDirectory);
                            String name = FileSystemUtil.localDriveCreateFileName(dir, songDto.getName());
                            java.io.File fileToSave = new java.io.File(dir, name);
                            FileUtils.writeByteArrayToFile(fileToSave, response.body().bytes());

        //                    Toast.makeText(context, R.string.download_completed, Toast.LENGTH_LONG).show();
                            NotificationUtil.notifyDownloadCompleted(context, songDto.getName(),
                                    context.getSystemService(Context.NOTIFICATION_SERVICE));

        //                    EventBus.getDefault().post(new LocalDriveChangeEvent(null, driveFile));

                            if(FileSystemUtil.isAudio(context, FilenameUtils.getExtension(name))) {
                                File dbFile = new File();

                                dbFile.setName(name);
                                dbFile.setPath(fileToSave.getAbsolutePath());
                                dbFile.setSize(fileToSave.length());
                                dbFile.setCreated(new Date());
                                dbFile.setGlobalId(songDto.getFileId());
                                FileRepository.add(dbFile, context);

                                // save song to db
                                Song song = new Song();
                                song.setFile(dbFile);
                                String artist = songDto.getArtist() == null ? "" : songDto.getArtist();
                                String genre = songDto.getGenre() == null ? "" : songDto.getGenre();
                                song.setArtist(artist);
                                song.setDuration(songDto.getDuration());
                                song.setGenre(genre);
                                song.setGlobalId(songDto.getId());
                                song.setDownloadedBy(SharedPreferencesService.get("username"));
                                Song savedSong = SongRepository.add(song, context);
                                PlaylistRepository.addSongToDownloadedPlaylist(playlistDbId, savedSong.getId(), songDto.getPosition(), context);
                                syncService.handlePlaylistChange();
                            }
                        } else {
                            PlaylistRepository.addSongToDownloadedPlaylist(playlistDbId, songsDb.get(0).getId(), songDto.getPosition(), context);
                            syncService.handlePlaylistChange();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else if (response.code() == 400) {
                    syncService.handlePlaylistChange();
                } else {
                    Toast.makeText(context, "Invalid action!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(context, R.string.error_message, Toast.LENGTH_SHORT).show();
                Log.e("Download file failure", Objects.requireNonNull(t.getMessage()));
            }
        });
    }


    private static void saveSongToDB(final Context context, final File file) {
        ServiceUtils.driveService.getSongByFileId(file.getId()).enqueue(new Callback<SongDto>() {
            @SneakyThrows
            @Override
            public void onResponse(Call<SongDto> call, Response<SongDto> response) {
                if (response.code() == 200) {
                    SongDto songDto = response.body();
                    if (songDto != null) {
                        List<Song> songs = SongRepository.findSongs(SongRepository.COLUMN_GLOBAL_ID + " = ? AND " + SongRepository.COLUMN_DOWNLOADED_BY + " = ?",
                                new String[]{songDto.getId().toString(), SharedPreferencesService.get("username")}, context);

                        file.setGlobalId(songDto.getFileId());
                        FileRepository.add(file, context);

                        if (songs.size() < 1) {
                            Song song = new Song();
                            song.setFile(file);
                            String artist = songDto.getArtist() == null ? "" : songDto.getArtist();
                            String genre = songDto.getGenre() == null ? "" : songDto.getGenre();
                            song.setArtist(artist);
                            song.setDuration(songDto.getDuration());
                            song.setGenre(genre);
                            song.setGlobalId(songDto.getId());
                            song.setDownloadedBy(SharedPreferencesService.get("username"));
                            SongRepository.add(song, context);

                            if (MusicPlayer.allPlaying) {
                                MusicPlayer.setSongsToPlay(SongRepository
                                        .findUsersDownloadedSongs(SharedPreferencesService.get("username"), context));
                            }
                        }
                    }
                } else if (response.code() == 400) {
                    Toast.makeText(context, response.errorBody().string(),
                            Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(context, "Invalid action!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<SongDto> call, Throwable t) {
                Toast.makeText(context, R.string.error_message, Toast.LENGTH_SHORT).show();
                Log.e("Download song failure", Objects.requireNonNull(t.getMessage()));
            }
        });
    }
}
