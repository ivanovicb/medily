package rs.ac.uns.ftn.medily.tools.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import rs.ac.uns.ftn.medily.dto.PlaylistDto;
import rs.ac.uns.ftn.medily.dto.SongDto;
import rs.ac.uns.ftn.medily.dto.SongFileDto;
import rs.ac.uns.ftn.medily.dto.SongInPlaylistDto;
import rs.ac.uns.ftn.medily.dto.UpdateSongPositionDto;
import rs.ac.uns.ftn.medily.model.Playlist;
import rs.ac.uns.ftn.medily.model.Song;
import rs.ac.uns.ftn.medily.tools.Util;

public class PlaylistRepository {

    public static final String TABLE_PLAYLISTS = "playlists";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_GLOBAL_ID = "global_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_SHARE_PLAYLIST = "share_playlist";
    public static final String COLUMN_DOWNLOADED_BY = "downloaded_by";
    public static final String COLUMN_CREATED_BY = "created_by";

    public static final String DB_CREATE = "create table "
            + TABLE_PLAYLISTS + "("
            + COLUMN_ID  + " integer primary key autoincrement, "
            + COLUMN_GLOBAL_ID + " integer, "
            + COLUMN_NAME + " text, "
            + COLUMN_SHARE_PLAYLIST + " integer, "
            + COLUMN_DOWNLOADED_BY + " text, "
            + COLUMN_CREATED_BY + " text"
            + ")";

    public static final String TABLE_PLAYLISTS_SONGS = "playlists_songs";
    public static final String COLUMN_SONG = "song";
    public static final String COLUMN_POSITION = "position";
    public static final String COLUMN_PLAYLIST = "playlist";

    public static final String DB_CREATE_PLAYLISTS_SONGS = "create table "
            + TABLE_PLAYLISTS_SONGS + "("
            + COLUMN_ID  + " integer primary key autoincrement, "
            + COLUMN_SONG + " integer not null, "
            + COLUMN_PLAYLIST + " integer not null, "
            + COLUMN_POSITION + " integer not null, "
            + "FOREIGN KEY (" + COLUMN_SONG + ") REFERENCES " + SongRepository.TABLE_SONGS + " (_id), "
            + "FOREIGN KEY (" + COLUMN_PLAYLIST + ") REFERENCES " + TABLE_PLAYLISTS + " (_id)"
            + ")";

    public static Cursor query(String selection, String[] selectionArgs, Context context) {
        String[] projection = {COLUMN_ID, COLUMN_GLOBAL_ID, COLUMN_NAME, COLUMN_SHARE_PLAYLIST, COLUMN_DOWNLOADED_BY, COLUMN_CREATED_BY};
        SQLiteDatabase db = DbContext.getDB(context).getWritableDatabase();
        return db.query(TABLE_PLAYLISTS, projection, selection,
                selectionArgs, null, null, null);
    }

    public static List<Playlist> findPlaylists(String selection, String[] selectionArgs, Context context) {
        List<Playlist> result = new ArrayList<>();
        Cursor cursor = query(selection, selectionArgs, context);
        while(cursor.moveToNext()) {
            Long id = cursor.getLong(cursor.getColumnIndexOrThrow(COLUMN_ID));
            Long globalId = cursor.getLong(cursor.getColumnIndexOrThrow(COLUMN_GLOBAL_ID));
            List<Song> songs = findSongsInPlaylist(COLUMN_PLAYLIST + " = ?", new String[] {id.toString()}, context);
            Playlist playlist = new Playlist(id,
                    cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_NAME)),
                    cursor.getInt(cursor.getColumnIndexOrThrow(COLUMN_SHARE_PLAYLIST)) == 1,
                    cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_CREATED_BY)),
                    cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_DOWNLOADED_BY)),
                    songs,
                    globalId);
            result.add(playlist);
        }
        cursor.close();
        return result;
    }

    public static Playlist findById(Long id, Context context) {
        String selection = COLUMN_ID + " = ?";
        List<Playlist> playlists = findPlaylists(selection, new String[] { id.toString() }, context);
        return playlists.size() == 1 ? playlists.get(0) : null;
    }

    public static Playlist add(Playlist playlist, Context context) {
        SQLiteDatabase db = DbContext.getDB(context).getWritableDatabase();
        ContentValues entry = new ContentValues();
        entry.put(COLUMN_NAME, playlist.getName());
        entry.put(COLUMN_SHARE_PLAYLIST, playlist.isSharePlaylist() ? 1 : 0);
        entry.put(COLUMN_GLOBAL_ID, playlist.getGlobalId());
        entry.put(COLUMN_DOWNLOADED_BY, playlist.getDownloadedBy());
        entry.put(COLUMN_CREATED_BY, playlist.getCreatedBy());
        long result = db.insert(TABLE_PLAYLISTS, null, entry);
        playlist.setId(result);
        return playlist;
    }

    public static Playlist update(Playlist playlist, Context context, String selection, String[] selectionArgs) {
        SQLiteDatabase db = DbContext.getDB(context).getWritableDatabase();
        ContentValues entry = new ContentValues();
        entry.put(COLUMN_NAME, playlist.getName());
        entry.put(COLUMN_SHARE_PLAYLIST, playlist.isSharePlaylist() ? 1 : 0);
        entry.put(COLUMN_DOWNLOADED_BY, playlist.getDownloadedBy());
        entry.put(COLUMN_GLOBAL_ID, playlist.getGlobalId());
        entry.put(COLUMN_CREATED_BY, playlist.getCreatedBy());
        db.update(TABLE_PLAYLISTS, entry, selection, selectionArgs);
        return playlist;
    }

    public static Playlist updateName(Playlist playlist, Context context, String selection, String[] selectionArgs) {
        SQLiteDatabase db = DbContext.getDB(context).getWritableDatabase();
        ContentValues entry = new ContentValues();
        entry.put(COLUMN_NAME, playlist.getName());
        db.update(TABLE_PLAYLISTS, entry, selection, selectionArgs);
        return playlist;
    }

    public static List<Playlist> findPlaylistsMineAndShared(Context context, String currentUser) {
        String selection = PlaylistRepository.COLUMN_CREATED_BY + " = ? OR "
                + PlaylistRepository.COLUMN_DOWNLOADED_BY + " = ?";
        return findPlaylists(selection, new String[] {currentUser, currentUser}, context);
        //return findPlaylists(null, null, context); // all
    }

    public static boolean deletePlaylist(Playlist playlist, Context context) {
        deleteSongsFromPlaylist(context, COLUMN_PLAYLIST + " = ?", new String[] {String.valueOf(playlist.getId())});
        SQLiteDatabase db = DbContext.getDB(context).getWritableDatabase();
        String selection = COLUMN_ID + " = ?";
        return db.delete(TABLE_PLAYLISTS, selection, new String[] {String.valueOf(playlist.getId())}) == 1;
    }

    public static Playlist addSongsToPlaylist(Playlist playlist, List<Song> songs, Context context) {
        SQLiteDatabase db = DbContext.getDB(context).getWritableDatabase();
        int position = playlist.getSongs().size() + 1;
        for (Song song: songs) {
            ContentValues entry = new ContentValues();
            entry.put(COLUMN_PLAYLIST, playlist.getId().toString());
            entry.put(COLUMN_SONG, song.getId().toString());
            entry.put(COLUMN_POSITION, position);
            db.insert(TABLE_PLAYLISTS_SONGS, null, entry);
            position++;
        }
        return playlist;
    }

    public static Playlist addSongsToDownloadedPlaylist(Playlist playlist, List<SongFileDto> songs, Context context) {
        SQLiteDatabase db = DbContext.getDB(context).getWritableDatabase();

        for (SongFileDto song: songs) {
            ContentValues entry = new ContentValues();
            entry.put(COLUMN_PLAYLIST, playlist.getId().toString());
            entry.put(COLUMN_SONG, song.getId().toString());
            entry.put(COLUMN_POSITION, song.getPosition());
            db.insert(TABLE_PLAYLISTS_SONGS, null, entry);
        }

        return playlist;
    }

    public static void addSongToDownloadedPlaylist(Long playlistId, Long songId, int position, Context context) {
        SQLiteDatabase db = DbContext.getDB(context).getWritableDatabase();

        ContentValues entry = new ContentValues();
        entry.put(COLUMN_PLAYLIST, playlistId.toString());
        entry.put(COLUMN_SONG, songId.toString());
        entry.put(COLUMN_POSITION, position);
        db.insert(TABLE_PLAYLISTS_SONGS, null, entry);
    }

    public static boolean updateSongsInPlaylist(Long playlistId, Long songId, int position,
                                                Context context, String selection, String[] selectionArgs) {
        SQLiteDatabase db = DbContext.getDB(context).getWritableDatabase();
        ContentValues entry = new ContentValues();
        entry.put(COLUMN_SONG, playlistId);
        entry.put(COLUMN_PLAYLIST, songId);
        entry.put(COLUMN_POSITION, position);
        db.update(TABLE_PLAYLISTS_SONGS, entry, selection, selectionArgs);
        return true;
    }

    public static int deleteSongsFromPlaylist(Context context, String selection, String[] selectionArgs) {
        SQLiteDatabase db = DbContext.getDB(context).getWritableDatabase();
        return db.delete(TABLE_PLAYLISTS_SONGS, selection, selectionArgs);
    }

    public static Cursor querySongsInPlaylist(String selection, String[] selectionArgs, Context context) {
        String[] projection = {COLUMN_ID, COLUMN_PLAYLIST, COLUMN_SONG, COLUMN_POSITION};
        SQLiteDatabase db = DbContext.getDB(context).getWritableDatabase();

        return db.query(TABLE_PLAYLISTS_SONGS, projection, selection,
                selectionArgs, null, null, COLUMN_POSITION);
    }

    public static List<Song> findSongsInPlaylist(String selection, String[] selectionArgs, Context context) {
        List<Song> result = new ArrayList<>();
        Cursor cursor = querySongsInPlaylist(selection, selectionArgs, context);
        while(cursor.moveToNext()) {
            Long id = cursor.getLong(cursor.getColumnIndexOrThrow(COLUMN_SONG));
            List<Song> songs = SongRepository.findSongs(SongRepository.COLUMN_ID + " = ?",
                    new String[] {id.toString()}, context);
            if (songs.size() == 1) {
                result.add(songs.get(0));
            }
        }
        cursor.close();
        return result;
    }

    public static List<SongInPlaylistDto> findPlaylistsOfSong(String selection, String[] selectionArgs, Context context) {
        List<SongInPlaylistDto> result = new ArrayList<>();
        Cursor cursor = querySongsInPlaylist(selection, selectionArgs, context);
        while(cursor.moveToNext()) {
            Long id = cursor.getLong(cursor.getColumnIndexOrThrow(COLUMN_PLAYLIST));
            List<Playlist> playlists = findPlaylists(COLUMN_ID + " = ?",
                    new String[] {id.toString()}, context);
            if (playlists.size() == 1) {
                result.add(new SongInPlaylistDto(playlists.get(0), cursor.getInt(cursor.getColumnIndexOrThrow(COLUMN_POSITION))));
            }
        }
        cursor.close();
        return result;
    }

    public static void updateSongPositionInPlaylist(UpdateSongPositionDto dto, Context context) {
        if (dto.getOldPosition() > dto.getNewPosition()) {
            autoUpdatePositionsWhenOldPositionGreater(dto, context);
        } else {
            autoUpdatePositionsWhenOldPositionSmaller(dto, context);
        }

        updatePositionOfDraggedSong(dto, context);
    }

    private static void updatePositionOfDraggedSong(UpdateSongPositionDto dto, Context context) {
        SQLiteDatabase db = DbContext.getDB(context).getWritableDatabase();

        String selection = COLUMN_SONG + " = ? AND " + COLUMN_PLAYLIST + " = ?";
        String[] selectionArgs = new String[] { dto.getSongId().toString(), dto.getPlaylistId().toString() };

        ContentValues entry = new ContentValues();
        entry.put(COLUMN_POSITION, dto.getNewPosition());
        db.update(TABLE_PLAYLISTS_SONGS, entry, selection, selectionArgs);
    }

    private static void autoUpdatePositionsWhenOldPositionGreater(UpdateSongPositionDto dto, Context context) {
        SQLiteDatabase db = DbContext.getDB(context).getWritableDatabase();

        String sql = "UPDATE " + TABLE_PLAYLISTS_SONGS + " SET " + COLUMN_POSITION + " = " +
                COLUMN_POSITION  + " + 1 " + " WHERE " + COLUMN_PLAYLIST + " = " + dto.getPlaylistId()
                + " AND " + COLUMN_POSITION + " < " + dto.getOldPosition() +" AND " + COLUMN_POSITION + " >= " +
                dto.getNewPosition();

        db.execSQL(sql);
    }

    private static void autoUpdatePositionsWhenOldPositionSmaller(UpdateSongPositionDto dto, Context context) {
        SQLiteDatabase db = DbContext.getDB(context).getWritableDatabase();

        String sql = "UPDATE " + TABLE_PLAYLISTS_SONGS + " SET " + COLUMN_POSITION + " = " +
                COLUMN_POSITION  + " - 1 " + " WHERE " + COLUMN_PLAYLIST + " = " +
                dto.getPlaylistId() + " AND " + COLUMN_POSITION + " > " + dto.getOldPosition() +
                " AND " + COLUMN_POSITION + " <= " + dto.getNewPosition();

        db.execSQL(sql);
    }

    public static void updatePositionOfSongsInPlaylistAfterDeletingSong(Long playlistId, int deletedPosition, Context context) {
        SQLiteDatabase db = DbContext.getDB(context).getWritableDatabase();

        String sql = "UPDATE " + TABLE_PLAYLISTS_SONGS + " SET " + COLUMN_POSITION + " = " +
                COLUMN_POSITION  + " - 1 " + " WHERE " + COLUMN_PLAYLIST + " = " +
                playlistId + " AND " + COLUMN_POSITION + " > " + deletedPosition;

        db.execSQL(sql);
    }

    public static void removePlaylistSong(Long globalPlaylistId, Long globalSongId, Context context) {
        String queryPlaylist = String.format("SELECT playlist, position\n" +
                "FROM playlists_songs\n" +
                "INNER JOIN songs, playlists ON playlists_songs.song = songs._id AND playlists_songs.playlist = playlists._id\n" +
                "WHERE songs.global_id = %s AND playlists.global_id = %s", globalSongId.toString(), globalPlaylistId.toString());
        SQLiteDatabase db = DbContext.getDB(context).getWritableDatabase();
        Cursor cursor =  db.rawQuery(queryPlaylist, null);

        if (!cursor.moveToFirst()) {
           return;
        }

        long playlistId = cursor.getLong(cursor.getColumnIndexOrThrow(COLUMN_PLAYLIST));
        int deletedPosition = cursor.getInt(cursor.getColumnIndexOrThrow(COLUMN_POSITION));
        cursor.close();

        String queryDelete = String.format("DELETE FROM playlists_songs WHERE _id = (SELECT playlists_songs._id\n" +
                "FROM playlists_songs\n" +
                "INNER JOIN songs, playlists ON playlists_songs.song = songs._id AND playlists_songs.playlist = playlists._id\n" +
                "WHERE songs.global_id = %s AND playlists.global_id = %s)", globalSongId.toString(), globalPlaylistId.toString());
        db.execSQL(queryDelete);

        updatePositionOfSongsInPlaylistAfterDeletingSong(playlistId, deletedPosition, context);
    }

    public static void updatePlaylistSongPositions(PlaylistDto playlistDto, Context context) {

        String queryPlaylistSong = String.format("SELECT song, playlist, songs.global_id\n" +
                "FROM playlists_songs\n" +
                "INNER JOIN songs, playlists ON playlists_songs.song = songs._id AND playlists_songs.playlist = playlists._id\n" +
                "WHERE playlists.global_id = %s", playlistDto.getId().toString());

        SQLiteDatabase db = DbContext.getDB(context).getWritableDatabase();

        Cursor cursor =  db.rawQuery(queryPlaylistSong, null);

        if (cursor.moveToFirst()) {
            do {
                int songId = cursor.getInt(0);
                int playlistId = cursor.getInt(1);
                int songGlobalId = cursor.getInt(2);

                int position = getPositionBySongId(playlistDto.getSongs(), songGlobalId);

                if (position == -1) {
                    System.out.println("POSITION NOT FOUND!");
                    return;
                }

                String queryUpdatePosition = String.format(Locale.UK, "UPDATE playlists_songs SET position = %d WHERE playlist = %d AND song = %d", position, playlistId, songId);
                db.execSQL(queryUpdatePosition);
            } while (cursor.moveToNext());
        }

        cursor.close();
    }

    private static int getPositionBySongId(List<SongDto> songs, int songId) {
        for (SongDto songDto: songs) {
            if (songDto.getId() == songId) {
                return songDto.getPosition();
            }
        }
        return -1;
    }
}
