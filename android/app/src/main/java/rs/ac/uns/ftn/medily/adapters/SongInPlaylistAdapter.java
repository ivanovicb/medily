package rs.ac.uns.ftn.medily.adapters;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;

import com.woxthebox.draglistview.DragItemAdapter;
import com.woxthebox.draglistview.swipe.ListSwipeItem;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import rs.ac.uns.ftn.medily.R;
import rs.ac.uns.ftn.medily.model.Playlist;
import rs.ac.uns.ftn.medily.model.Song;
import rs.ac.uns.ftn.medily.tools.MusicPlayer;
import rs.ac.uns.ftn.medily.tools.Util;

public class SongInPlaylistAdapter extends DragItemAdapter<Song, SongInPlaylistAdapter.ViewHolder> implements Filterable {

    private int mLayoutId;
    private int mGrabHandleId;
    private boolean mDragOnLongPress;
    private Playlist playlist;
    private List<Song> data;

    public SongInPlaylistAdapter(List<Song> list, int layoutId, int grabHandleId, boolean dragOnLongPress,
                       Playlist playlist) {
        mLayoutId = layoutId;
        mGrabHandleId = grabHandleId;
        mDragOnLongPress = dragOnLongPress;
        this.playlist = playlist;
        this.data = list;
        setItemList(list);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(mLayoutId, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);
        holder.songName.setText(mItemList.get(position).getFile().getName());
        holder.artist.setText(mItemList.get(position).getArtist());
        holder.duration.setText(Util.convertToMinutesString(mItemList.get(position).getDuration()));
        holder.itemView.setTag(mItemList.get(position));
        holder.songId = mItemList.get(position).getFile().getId();
        ((ListSwipeItem) holder.view).setSupportedSwipeDirection(ListSwipeItem.SwipeDirection.RIGHT);
    }

    @Override
    public long getUniqueItemId(int position) {
        return mItemList.get(position).getId();
    }

    private Filter filter = new Filter () {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            List<Song> filtered = performSearch(constraint);
            results.count = filtered.size();
            results.values = filtered;
            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            setItemList((List<Song>) results.values);
            notifyDataSetChanged();
        }
    };

    @Override
    public Filter getFilter() { return filter; }

    private List<Song> performSearch(CharSequence query) {
        List<Song> songs = new ArrayList<>();
        query = query.toString().toLowerCase();
        for (Song song: data) {
            if (song.getFile().getName().toLowerCase().contains(query) || song.getArtist().toLowerCase().contains(query)) {
                songs.add(song);
            }
        }
        return songs;
    }

    public class ViewHolder extends DragItemAdapter.ViewHolder {
        private TextView songName;
        private TextView artist ;
        private TextView duration;
        View view;
        private Long songId;

        ViewHolder(final View itemView) {
            super(itemView, mGrabHandleId, mDragOnLongPress);
            songName = itemView.findViewById(R.id.song_in_playlist_name);
            artist = itemView.findViewById(R.id.artist_of_song);
            duration = itemView.findViewById(R.id.duration_of_song);
            this.view = itemView;
        }

        @Override
        public void onItemClicked(View view) {
            if (view != null) {

                TextView name = (TextView) ((Activity) view.getContext()).findViewById(R.id.song_name_player_collapsed);
                name.setText(songName.getText());

                TextView artistField = (TextView) ((Activity) view.getContext()).findViewById(R.id.song_artist_player_collapsed);
                artistField.setText(artist.getText());

                ImageButton button = (ImageButton) ((Activity) view.getContext()).findViewById(R.id.play_collapsed);
                button.setImageDrawable(ContextCompat.getDrawable(Objects.requireNonNull(((Activity) view.getContext())), R.drawable.ic_pause_collapsed));
                button.setTag(R.drawable.ic_pause_collapsed);
                button.setVisibility(View.VISIBLE);

                Util.syncViewsButtons("collapsed", button, (Activity) view.getContext(), view);
                String artistName = "";
                if (artist != null) {
                    if (artist.getText().length() > 0) {
                        artistName = artist.getText() + " - ";
                    }
                }
                String songDetails = artistName + songName.getText();
                Util.syncCurrentlyPlayingSong(songDetails, duration.getText().toString(), (Activity) view.getContext());

                SeekBar seekBar = (SeekBar) ((Activity) view.getContext()).findViewById(R.id.seek_bar);
                if (seekBar != null) {
                    MusicPlayer.playInList(playlist, songId, (Activity) view.getContext());
                    // Util.selectCurrentlyPlayingSongOnClick(view);
                }
            }
        }

        @Override
        public boolean onItemLongClicked(View view) {
           // Toast.makeText(view.getContext(), "Item long clicked", Toast.LENGTH_SHORT).show();
            return true;
        }
    }
}
