package rs.ac.uns.ftn.medily.tools;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.widget.RemoteViews;

import androidx.core.app.NotificationCompat;

import rs.ac.uns.ftn.medily.R;
import rs.ac.uns.ftn.medily.model.Song;

public class NotificationUtil {

    public static final String channelId = "MEDILY_CHANNEL_ID";
    private static NotificationChannel mChannel;

    private static void initChannel(NotificationManager mNotificationManager) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                mChannel = new NotificationChannel(channelId, channelId, NotificationManager.IMPORTANCE_DEFAULT);
                mChannel.setSound(null, null);
                mNotificationManager.createNotificationChannel(mChannel);
            }
        }
    }

    public static void notifyDownloadCompleted(Context context, String file, Object service) {
        NotificationManager mNotificationManager = (NotificationManager) service;
        if (mChannel == null) {
            initChannel(mNotificationManager);
        }
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, channelId)
                .setSmallIcon(R.drawable.ic_download_file)
                .setContentTitle("Medily: Download completed")
                .setContentText(file + " is successfully downloaded.")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);

        if (MusicPlayer.mediaPlayer.isPlaying()) {
            mNotificationManager.notify(2, builder.build());
        } else {
            mNotificationManager.notify(1, builder.build());
        }
    }

    public static void setPlayerNotificationView(Context context) {
        NotificationManager mNotificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (mChannel == null) {
            initChannel(mNotificationManager);
        }
        publishNotification(context);
    }

    public static void clearPlayerNotificationView(Context context) {
        NotificationManager mNotificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);

        if (mNotificationManager != null) {
            mNotificationManager.cancel(1);
        }
    }

    public static void publishNotification(Context context) {
        String mode = SharedPreferencesService.get("phone_theme");
        NotificationManager mNotificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (mNotificationManager != null) {
            RemoteViews notificationLayout = new RemoteViews(context.getPackageName(), R.layout.notification_player);

            int icon = 0;
            int color = 0;
            if (mode.equals("dark")) {
                icon = R.drawable.ic_music_for_dark;
                color = Color.WHITE;
                notificationLayout.setImageViewResource(R.id.prev_notification, R.drawable.ic_prev_notification_view_for_dark);
                notificationLayout.setImageViewResource(R.id.next_notification, R.drawable.ic_next_notification_view_for_dark);
            } else if (mode.equals("light")) {
                icon = R.drawable.ic_music;
                color = Color.BLACK;
            }

            NotificationCompat.Builder builder = new NotificationCompat.Builder(context, NotificationUtil.channelId)
                    .setSmallIcon(icon)
                    .setStyle(new NotificationCompat.DecoratedCustomViewStyle())
                    .setCustomContentView(notificationLayout);

            builder.setOngoing(true);
            try {
                NotificationUtil.setUpMusicNotificationButtons(context, notificationLayout);
                NotificationUtil.setMusicData(MusicPlayer.currentlyPlaying, notificationLayout, color);

                if (MusicPlayer.mediaPlayer.isPlaying()) {
                    builder.setOngoing(false);
                    notificationLayout.setImageViewResource(R.id.play_notification, R.drawable.ic_pause_notification_view);
                } else {
                    builder.setOngoing(true);
                    notificationLayout.setImageViewResource(R.id.play_notification, R.drawable.ic_play_notification_view);
                }

                builder.setPriority(NotificationCompat.PRIORITY_MAX);
                mNotificationManager.notify(1, builder.build());
            } catch (Exception e) {
                // ignore
            }
        }
    }

    public static void setMusicData(Song song, RemoteViews remoteViews, int color) {
        String currentSong = song.getFile().getName();
        if (song.getArtist() != null) {
            if (!song.getArtist().equals("")) {
                currentSong = song.getArtist() + " - " + currentSong;
            }
        }
        remoteViews.setTextViewText(R.id.song_name_player_notification,
                Util.splitStringIntoMultiLine(currentSong));

        remoteViews.setTextColor(R.id.song_name_player_notification, color);
    }

    public static void setUpMusicNotificationButtons(Context context, RemoteViews remoteViews) {
        Intent prevIntent = new Intent(context, PrevButtonNotificationListener.class);
        PendingIntent pendingPrevIntent = PendingIntent.getBroadcast(context, 0,
                prevIntent, 0);
        remoteViews.setOnClickPendingIntent(R.id.prev_notification, pendingPrevIntent);

        Intent nextIntent = new Intent(context, NextButtonNotificationListener.class);
        PendingIntent pendingNextIntent = PendingIntent.getBroadcast(context, 0,
                nextIntent, 0);
        remoteViews.setOnClickPendingIntent(R.id.next_notification, pendingNextIntent);

        Intent playPauseIntent = new Intent(context, PlayPauseButtonNotificationListener.class);
        PendingIntent pendingPlayPauseIntent = PendingIntent.getBroadcast(context, 0,
                playPauseIntent, 0);
        remoteViews.setOnClickPendingIntent(R.id.play_notification, pendingPlayPauseIntent);
    }
}
