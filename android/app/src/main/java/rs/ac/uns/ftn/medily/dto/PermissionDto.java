package rs.ac.uns.ftn.medily.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PermissionDto implements Serializable {

    @SerializedName("username")
    @Expose
    private String username;

    @SerializedName("role")
    @Expose
    private String role;

    public PermissionDto() {
    }

    public PermissionDto(String username, String role) {
        this.username = username;
        this.role = role;
    }
}
