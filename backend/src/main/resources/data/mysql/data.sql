insert into families (id, active, activation_token, email, name) values (1, true, '', 'family1@mailinator.com', 'Family 1');
insert into families (id, active, activation_token, email, name) values (2, true, '', 'family2@mailinator.com', 'Family 2');

insert into users (id, name, role, username, password, family_id)
values (1, 'Marko', 'PARENT', 'marko123', '$2a$10$dZN.tZrgeI14KaPKv0oH/OoFcRy0hlOZcckDZxSJsa.7rxIAsRcaK', 1); -- password: 123
insert into users (id, name, role, username, password, family_id)
values (2, 'Pera', 'CHILD', 'pera123', '$2a$10$dZN.tZrgeI14KaPKv0oH/OoFcRy0hlOZcckDZxSJsa.7rxIAsRcaK', 1); -- password: 123
insert into users (id, name, role, username, password, family_id)
values (3, 'Ana', 'CHILD', 'ana', '$2a$10$dZN.tZrgeI14KaPKv0oH/OoFcRy0hlOZcckDZxSJsa.7rxIAsRcaK', 1); -- password: 123
insert into users (id, name, role, username, password, family_id)
values (4, 'Jelena', 'PARENT', 'jelena', '$2a$10$dZN.tZrgeI14KaPKv0oH/OoFcRy0hlOZcckDZxSJsa.7rxIAsRcaK', 2); -- password: 123

insert into families_family_members (family_id, family_members_id) values (1, 1);
insert into families_family_members (family_id, family_members_id) values (1, 2);
insert into families_family_members (family_id, family_members_id) values (1, 3);
insert into families_family_members (family_id, family_members_id) values (2, 4);

insert into drives (id, family_id) values (1, 1);
insert into drives (id, family_id) values (2, 2);

update families set drive_id = 1 where id = 1;
update families set drive_id = 2 where id = 2;

insert into files (id, name, size, created, server_path, active) values (1, 'CrazyInLove.mp3', 3720000, '2020-04-10', './files/test_CrazyInLove.mp3', 1);
insert into files (id, name, size, created, server_path, active) values (2, 'Diamonds.mp3', 4300000, '2020-04-11', './files/test_Diamonds.mp3', 1);
insert into files (id, name, size, created, server_path, active) values (3, 'File 1.txt', 32000, '2020-04-12', './files/test_file_1.txt', 1);
insert into files (id, name, size, created, server_path, active) values (4, 'DontStopMeNow.mp3', 5200000, '2020-04-13', './files/test_DontStopMeNow.mp3', 1);
insert into files (id, name, size, created, server_path, active) values (5, 'Image 1.jpeg', 1330000, '2020-04-14', './files/test_image_1.jpeg', 1);
insert into files (id, name, size, created, server_path, active) values (6, 'Video_1.mp4', 3200000, '2020-04-14', './files/test_video_1.mp4', 1);
insert into files (id, name, size, created, server_path, active) values (7, 'File 2.txt', 32, '2020-04-14', './files/test_file_2.txt', 1);
insert into files (id, name, size, created, server_path, active) values (8, 'Image 2.jpg', 1560000, '2020-04-14', './files/test_image_2.jpg', 1);

insert into folders (id, name, created, active) values (1, 'Folder 1', '2020-04-10', 1);
insert into folders (id, name, created, active) values (2, 'Folder 2', '2020-04-11', 1);
insert into folders (id, name, created, active) values (3, 'Folder 3', '2020-04-12', 1);
insert into folders (id, name, created, active) values (4, 'Folder 4',  '2020-04-13', 1);
insert into folders (id, name, created, active) values (5, 'Folder 5', '2020-04-14', 1);

insert into drives_files (drive_id, files_id) values (1, 1);
insert into drives_files (drive_id, files_id) values (1, 2);
insert into drives_files (drive_id, files_id) values (1, 6);
insert into drives_files (drive_id, files_id) values (1, 7);
insert into drives_files (drive_id, files_id) values (1, 8);

insert into drives_folders (drive_id, folders_id) values (1, 1);
insert into drives_folders (drive_id, folders_id) values (1, 2);

insert into folders_files (folder_id, files_id) values (1, 3);
insert into folders_files (folder_id, files_id) values (1, 4);

insert into folders_files (folder_id, files_id) values (4, 5);

insert into folders_sub_folders (folder_id, sub_folders_id) values (1, 3);
insert into folders_sub_folders (folder_id, sub_folders_id) values (1, 4);

insert into folders_sub_folders (folder_id, sub_folders_id) values (3, 5);

insert into songs (id, artist, duration, genre, file_id) values (1, 'Beyonce', 235, 'pop', 1);
insert into songs (id, artist, duration, genre, file_id) values (2, 'Rihanna', 224, 'pop', 2);
insert into songs (id, artist, duration, genre, file_id) values (3, 'Queen', 217, 'rock', 4);

/*insert into playlists (id, name, share_playlist, created_by_user) values (1, 'Playlist_1', true, 1);

insert into playlists_songs (playlist_id, songs_id) values (1, 1);
insert into playlists_songs (playlist_id, songs_id) values (1, 2);

insert into users_playlists (user_id, playlists_id) values (1, 1);

insert into playlist_changes (id, playlist_change_type, playlist_id, modified) values (1, 'PLAYLIST_METADATA', 1, '2020-05-29 17:30:00.00')
*/
