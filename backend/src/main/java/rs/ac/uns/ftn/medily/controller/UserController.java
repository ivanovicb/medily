package rs.ac.uns.ftn.medily.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.*;
import rs.ac.uns.ftn.medily.dto.*;
import rs.ac.uns.ftn.medily.dto_validators.*;
import rs.ac.uns.ftn.medily.exceptions.BadRequestException;
import rs.ac.uns.ftn.medily.exceptions.InvalidRequestException;
import rs.ac.uns.ftn.medily.model.User;
import rs.ac.uns.ftn.medily.security.TokenUtils;
import rs.ac.uns.ftn.medily.service.UserService;

import javax.servlet.http.HttpServletRequest;

import static rs.ac.uns.ftn.medily.constants.AppConstants.MAIN_URL;

@RestController
@RequestMapping("users")
public class UserController {

    private UtilValidator utilValidator;
    private final TokenUtils tokenUtils;
    private final UserService userService;
    private ItemDtoValidator itemDtoValidator;
    private LoginDtoValidator loginDtoValidator;
    private FeedbackDtoValidator feedbackDtoValidator;
    private final UserDetailsService userDetailsService;
    private NewMemberDtoValidator newMemberDtoValidator;
    private final AuthenticationManager authenticationManager;
    private RegistrationDtoValidation registrationDtoValidation;

    @Autowired
    public UserController(TokenUtils tokenUtils, UserService userService, UserDetailsService userDetailsService,
                          AuthenticationManager authenticationManager, LoginDtoValidator loginDtoValidator,
                          RegistrationDtoValidation registrationDtoValidation, UtilValidator utilValidator,
                          ItemDtoValidator itemDtoValidator, NewMemberDtoValidator newMemberDtoValidator,
                          FeedbackDtoValidator feedbackDtoValidator) {
        this.tokenUtils = tokenUtils;
        this.userService = userService;
        this.utilValidator = utilValidator;
        this.itemDtoValidator = itemDtoValidator;
        this.loginDtoValidator = loginDtoValidator;
        this.userDetailsService = userDetailsService;
        this.feedbackDtoValidator = feedbackDtoValidator;
        this.authenticationManager = authenticationManager;
        this.newMemberDtoValidator = newMemberDtoValidator;
        this.registrationDtoValidation = registrationDtoValidation;
    }

    /*
     * Method checks if user with these credentials exists on system.
     * @param loginDto Contains users information from login form.
     * @return Method returns generated token if user with these credentials exists and 200 status,
     * else returns invalid login and 400 status.
     */
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseEntity<?> login(@RequestBody LoginDto loginDto, HttpServletRequest request) {
        try {
            loginDtoValidator.validateLogin(loginDto);
            // Perform the authentication
            UsernamePasswordAuthenticationToken token =
                    new UsernamePasswordAuthenticationToken(
                            loginDto.getUsername(), loginDto.getPassword());
            Authentication authentication = authenticationManager.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);

            // Reload user details so we can generate token
            UserDetails details = userDetailsService. loadUserByUsername(loginDto.getUsername());
            User user = userService.findByUsername(loginDto.getUsername());
            if (user == null) {
                throw new InvalidRequestException("User not recognized, bad credentials.");
            }
            return new ResponseEntity<>(new LoginResultDto(user.getId(), loginDto.getUsername(), tokenUtils.generateToken(details),
                    user.getName(), user.getFamily().getName()), HttpStatus.OK);
        } catch (Exception ex) {
            return ResponseEntity.badRequest().body(ex.getMessage());
        }
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public ResponseEntity<?> registration(@RequestBody RegistrationDto dto) {
        try {
            registrationDtoValidation.validateRegistration(dto);
            userService.registration(dto);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception ex) {
            if (ex instanceof BadRequestException) {
                return ResponseEntity.badRequest().body(ex.getMessage());
            } else {
                return ResponseEntity.status(503).body("Service currently unavailable, please try again later!");
            }
        }
    }

    @RequestMapping(value = "/activation/{token}", method = RequestMethod.GET)
    public ResponseEntity<?> activation(@PathVariable String token) {
        try {
            utilValidator.validateToken(token);
            userService.activation(token);
            return ResponseEntity.status(HttpStatus.MOVED_PERMANENTLY).header(HttpHeaders.LOCATION, MAIN_URL + "/success.html").build();
        } catch (Exception ex) {
            ex.printStackTrace();
            return ResponseEntity.status(HttpStatus.MOVED_PERMANENTLY).header(HttpHeaders.LOCATION, MAIN_URL + "/error.html").build();
        }
    }

    @RequestMapping(value = "/renewal/request", method = RequestMethod.POST)
    public ResponseEntity<?> requestPasswordRenewal(@RequestBody ItemDto dto) {
        try {
            itemDtoValidator.validateStringValueOfItem(dto);
            userService.requestPasswordRenewal(dto.getItem());
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception ex) {
            if (ex instanceof BadRequestException) {
                return ResponseEntity.badRequest().body(ex.getMessage());
            } else {
                return ResponseEntity.status(503).body("Service currently unavailable, please try again later!");
            }
        }
    }

    @RequestMapping(value = "/renewal/{username}/*", method = RequestMethod.GET)
    public ResponseEntity<?> renewalRequest(@PathVariable String username) {
        try {
            utilValidator.validateString(username);
            userService.renewPassword(username);
            return ResponseEntity.status(HttpStatus.MOVED_PERMANENTLY).header(HttpHeaders.LOCATION, MAIN_URL + "/password.html").build();
        } catch (Exception ex) {
            ex.printStackTrace();
            return ResponseEntity.status(HttpStatus.MOVED_PERMANENTLY).header(HttpHeaders.LOCATION, MAIN_URL + "/error.html").build();
        }
    }

    @RequestMapping(value = "/profile", method = RequestMethod.GET)
    public ResponseEntity<?> profile(HttpServletRequest request) {
        try {
            return new ResponseEntity<>(userService.getProfile(request), HttpStatus.OK);
        } catch (Exception ex) {
            if (ex instanceof BadRequestException) {
                return ResponseEntity.badRequest().body(ex.getMessage());
            } else {
                return ResponseEntity.status(503).body("Service currently unavailable, please try again later!");
            }
        }
    }

    @RequestMapping(value = "/password/change", method = RequestMethod.POST)
    public ResponseEntity<?> changePassword(@RequestBody ItemDto item, HttpServletRequest request) {
        try {
            utilValidator.validatePassword(item.getItem());
            userService.changePassword(item.getItem(), request);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception ex) {
            if (ex instanceof BadRequestException) {
                return ResponseEntity.badRequest().body(ex.getMessage());
            } else {
                return ResponseEntity.status(503).body("Service currently unavailable, please try again later!");
            }
        }
    }

    @RequestMapping(value = "/family", method = RequestMethod.GET)
    public ResponseEntity<?> getFamily(HttpServletRequest request) {
        try {
            return new ResponseEntity<>(userService.getFamily(request), HttpStatus.OK);
        } catch (Exception ex) {
            if (ex instanceof BadRequestException) {
                return ResponseEntity.badRequest().body(ex.getMessage());
            } else {
                return ResponseEntity.status(503).body("Service currently unavailable, please try again later!");
            }
        }
    }

    @RequestMapping(value = "/member", method = RequestMethod.POST)
    public ResponseEntity<?> getMember(@RequestBody ItemDto dto, HttpServletRequest request) {
        try {
            itemDtoValidator.validateStringValueOfItem(dto);
            return new ResponseEntity<>(userService.getProfileForUser(request, dto.getItem()), HttpStatus.OK);
        } catch (Exception ex) {
            if (ex instanceof BadRequestException) {
                return ResponseEntity.badRequest().body(ex.getMessage());
            } else {
                return ResponseEntity.status(503).body("Service currently unavailable, please try again later!");
            }
        }
    }

    @RequestMapping(value = "/member/new", method = RequestMethod.POST)
    public ResponseEntity<?> createMember(@RequestBody NewMemberDto dto, HttpServletRequest request) {
        try {
            newMemberDtoValidator.validateCreateMember(dto);
            userService.createMember(request, dto);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception ex) {
            if (ex instanceof BadRequestException) {
                return ResponseEntity.badRequest().body(ex.getMessage());
            } else {
                return ResponseEntity.status(503).body("Service currently unavailable, please try again later!");
            }
        }
    }

    @RequestMapping(value = "/feedback", method = RequestMethod.POST)
    public ResponseEntity<?> sendFeedback(@RequestBody FeedbackDto dto) {
        try {
            feedbackDtoValidator.validateSendFeedback(dto);
            userService.saveFeedback(dto);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception ex) {
            if (ex instanceof BadRequestException) {
                return ResponseEntity.badRequest().body(ex.getMessage());
            } else {
                return ResponseEntity.status(503).body("Service currently unavailable, please try again later!");
            }
        }
    }
}
