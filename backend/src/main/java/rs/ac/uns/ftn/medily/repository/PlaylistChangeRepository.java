package rs.ac.uns.ftn.medily.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import rs.ac.uns.ftn.medily.model.PlaylistChange;

import java.util.Date;
import java.util.List;

public interface PlaylistChangeRepository extends JpaRepository<PlaylistChange, Long> {

  @Query("SELECT playlist_change FROM PlaylistChange playlist_change "
      + " JOIN playlist_change.playlist.downloadedBy userDownloadedBy"
      + " JOIN playlist_change.playlist.user userCreatedBy"
      + " WHERE playlist_change.playlist.sharePlaylist = True "
      + " AND playlist_change.modified > ?1 "
      + " AND playlist_change.changedBy.id != ?2 "
      + " AND (userDownloadedBy.id = ?2 OR userCreatedBy.id = ?2)")
  List<PlaylistChange> getUserPlaylistChanges(Date lastSync, Long userId);
}
