package rs.ac.uns.ftn.medily.service.impl;

import org.apache.commons.io.FilenameUtils;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.Parser;
import org.apache.tika.parser.mp3.Mp3Parser;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.tritonus.share.sampled.file.TAudioFileFormat;
import rs.ac.uns.ftn.medily.dto.*;
import rs.ac.uns.ftn.medily.exceptions.BadRequestException;
import rs.ac.uns.ftn.medily.exceptions.InvalidRequestException;
import rs.ac.uns.ftn.medily.mapper.DriveMapper;
import rs.ac.uns.ftn.medily.mapper.FileMapper;
import rs.ac.uns.ftn.medily.mapper.FolderMapper;
import rs.ac.uns.ftn.medily.mapper.SongMapper;
import rs.ac.uns.ftn.medily.model.*;
import rs.ac.uns.ftn.medily.repository.*;
import rs.ac.uns.ftn.medily.service.*;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioSystem;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.tika.parser.ParseContext;
import org.xml.sax.ContentHandler;
import org.xml.sax.helpers.DefaultHandler;

@Service
public class DriveServiceImpl implements DriveService {

    private final DriveRepository driveRepository;
    private final PermissionRepository permissionRepository;
    private final FolderRepository folderRepository;
    private final FileRepository fileRepository;
    private final UserService userService;
    private final SongService songService;
    private final PlaylistService playlistService;
    private final SongWithinPlaylistService songWithinPlaylistService;

    private final String[] audioFileExtensions = new String[]{"mp3", "pcm", "wav", "aiff", "acc"};

    @Value("${server-files-path}")
    private String serverFilesPath;

    public DriveServiceImpl(DriveRepository driveRepository, FolderRepository folderRepository,
                            FileRepository fileRepository, UserService userService,
                            PermissionRepository permissionRepository, SongService songService,
                            PlaylistService playlistService, SongWithinPlaylistService songWithinPlaylistService) {
        this.driveRepository = driveRepository;
        this.permissionRepository = permissionRepository;
        this.folderRepository = folderRepository;
        this.fileRepository = fileRepository;
        this.userService = userService;
        this.songService = songService;
        this.playlistService = playlistService;
        this.songWithinPlaylistService = songWithinPlaylistService;
    }

    @Override
    public DriveDto getDrive() {
        User user = userService.getLoggedInUser();
        Drive drive = driveRepository.findByFamilyId(user.getFamily().getId()).orElseThrow();
        return DriveMapper.driveToDriveDto(drive, user.getRole().equals(Role.PARENT) ? new ArrayList<>() : permissionRepository.findAllByUser(user));
    }

    @Override
    public Folder addFolder(AddFolderDto addFolderDto) {
        Folder folder = FolderMapper.folderDtoToFolder(addFolderDto.getNewFolder());
        folder.setActive(true);
        if(addFolderDto.getIsRoot()){
            Drive drive = driveRepository.findById(addFolderDto.getParentId()).orElseThrow();
            folder = folderRepository.save(folder);
            drive.getFolders().add(folder);
            driveRepository.save(drive);
            return folder;
        }

        Folder parentFolder = folderRepository.findById(addFolderDto.getParentId()).orElseThrow();
        folder = folderRepository.save(folder);
        parentFolder.getSubFolders().add(folder);
        folderRepository.save(parentFolder);
        return folder;
    }

    @Override
    public File addFile(MultipartFile multipartFile, AddFileDto addFileDto) {

        String fileName = UUID.randomUUID().toString() + "_" + (new Date()).getTime();

        File file = FileMapper.fileDtoToFile(addFileDto.getNewFile());
        file.setServerPath(serverFilesPath + fileName);
        file.setActive(true);

        if (addFileDto.getIsRoot()){
            Drive drive = driveRepository.findById(addFileDto.getParentId()).orElseThrow();
            file = fileRepository.save(file);
            drive.getFiles().add(file);
            driveRepository.save(drive);
        } else{
            Folder parentFolder = folderRepository.findById(addFileDto.getParentId()).orElseThrow();
            file = fileRepository.save(file);
            parentFolder.getFiles().add(file);
            folderRepository.save(parentFolder);
        }

        saveFile(multipartFile, fileName);

        if(isAudio(FilenameUtils.getExtension(addFileDto.getNewFile().getName()))){
            Song song = new Song();
            song.setFile(file);

            try {
                InputStream input = new FileInputStream(new java.io.File(file.getServerPath()));
                ContentHandler handler = new DefaultHandler();
                Metadata metadata = new Metadata();
                Parser parser = new Mp3Parser();
                ParseContext parseCtx = new ParseContext();
                parser.parse(input, handler, metadata, parseCtx);
                input.close();

                // List all metadata
                String[] metadataNames = metadata.names();
                System.out.println("FILE METADATA: ");
                for (String name : metadataNames) {
                    if (name.toLowerCase().contains("author") || name.toLowerCase().contains("artist")) {
                        song.setArtist(metadata.get(name));
                    }
                    if (name.toLowerCase().contains("genre")) {
                        song.setGenre(metadata.get(name));
                    }
                    System.out.println(name + ": " + metadata.get(name));
                }

                AudioFileFormat fileFormat = AudioSystem.getAudioFileFormat(new java.io.File(file.getServerPath()));
                if (fileFormat instanceof TAudioFileFormat) {
                    Map<?, ?> properties = ((TAudioFileFormat) fileFormat).properties();
                    Long microseconds = (Long) properties.get("duration");
                    int sec = (int) (microseconds / 1000000);
                    song.setDuration(sec);
                }

            } catch (Exception exception) {
                // Ignore ...
            }

            songService.save(song);
        }
      return file;
    }

    private boolean isAudio(String extension) {
      for (String audioExtension : audioFileExtensions) {
        if (audioExtension.toLowerCase().equals(extension.toLowerCase())) {
          return true;
        }
      }
      return false;
    }

    @Override
    public void saveFile(MultipartFile file, String fileName) {
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(serverFilesPath + fileName);
            fileOutputStream.write(file.getBytes());
            fileOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void renameFile(RenameDto renameDto) {
        File file = fileRepository.findById(renameDto.getId()).orElseThrow();
        file.setName(renameDto.getNewName());
        fileRepository.save(file);
    }

    @Override
    public void renameFolder(RenameDto renameDto) {
        Folder folder = folderRepository.findById(renameDto.getId()).orElseThrow();
        folder.setName(renameDto.getNewName());
        folderRepository.save(folder);
    }

    @Override
    public void deleteFolder(Long id) {
        Folder folder = folderRepository.findById(id).orElseThrow();
        folder.setActive(false);
        folderRepository.save(folder);
    }

    @Override
    public void deleteFile(Long id) {
        File file = fileRepository.findById(id).orElseThrow();
        file.setActive(false);
        fileRepository.save(file);
    }

    @Override
    public SongDto getSongByFileId(Long id) {
        Song song = songService.findByFileId(id);
        if (song == null){
            throw new EntityNotFoundException();
        }
        return SongMapper.songToSongDto(song);
    }

    @Override
    public DrivePermissionsDto getPermissions(String username, User user) {
        if (user == null) {
            throw new InvalidRequestException("User not recognized, bad credentials.");
        }
        Drive drive = driveRepository.findByFamilyId(user.getFamily().getId()).orElseThrow();
        drive.setFolders(drive.getFolders().stream().filter(Folder::isActive).collect(Collectors.toList()));
        drive.setFiles(drive.getFiles().stream().filter(File::isActive).collect(Collectors.toList()));
        User member = userService.findByUsername(username);
        if (member == null) {
            throw new InvalidRequestException("Member not recognized.");
        }
        if (member.getRole().equals(Role.PARENT)) {
            throw new InvalidRequestException("Retrieving permission of parent in family not allowed.");
        }
        if (user.getFamily().getId().longValue() != member.getFamily().getId().longValue()) {
            throw new InvalidRequestException("Chosen member not part of family.");
        }
        if (user.getRole().equals(Role.CHILD)) {
            throw new InvalidRequestException("User not parent, access denied.");
        }
        return DriveMapper.drivePermissionsToDriveDto(drive, permissionRepository.findAllByUser(member));
    }

    @Override
    public void changeRole(PermissionDto dto, User user) {
        if (user == null) {
            throw new InvalidRequestException("User not recognized, bad credentials.");
        }
        User member = userService.findByUsername(dto.getUsername());
        if (member == null) {
            throw new InvalidRequestException("Member not recognized.");
        }
        if (user.getRole().equals(Role.CHILD)) {
            throw new InvalidRequestException("User not parent, access denied.");
        }
        if (user.getFamily().getId().longValue() != member.getFamily().getId().longValue()) {
            throw new InvalidRequestException("Chosen member not part of family.");
        }
        member.setRole(Role.valueOf(dto.getRole().toUpperCase()));
        userService.save(member);
    }

    @Override
    public void changePermissions(ChangePermissionDto dto, User user) {
        if (user == null) {
            throw new InvalidRequestException("User not recognized, bad credentials.");
        }
        User member = userService.findByUsername(dto.getUsername());
        if (member == null) {
            throw new InvalidRequestException("Member not recognized.");
        }
        if (user.getRole().equals(Role.CHILD)) {
            throw new InvalidRequestException("User not parent, access denied.");
        }
        if (user.getFamily().getId().longValue() != member.getFamily().getId().longValue()) {
            throw new InvalidRequestException("Chosen member not part of family.");
        }
        if (dto.getType().equals("file")) {
            Optional<File> file = fileRepository.findById(dto.getId());
            if (file.isPresent()) {
                Optional<Permission> permission = permissionRepository.findByUserAndFile(member, file.get());
                if (dto.isAllow()) {
                    permission.ifPresent(permissionRepository::delete);
                } else {
                    if (permission.isEmpty()) {
                        permissionRepository.save(new Permission(member, file.get()));
                    }
                }
            } else {
                throw new InvalidRequestException("File not found.");
            }
        } else {
            Optional<Folder> folder = folderRepository.findById(dto.getId());
            if (folder.isPresent()) {
                Optional<Permission> permission = permissionRepository.findByUserAndFolder(member, folder.get());
                if (dto.isAllow()) {
                    permission.ifPresent(permissionRepository::delete);
                } else {
                    if (permission.isEmpty()) {
                        permissionRepository.save(new Permission(member, folder.get()));
                    }
                }
            } else {
                throw new InvalidRequestException("Folder not found.");
            }
        }
    }

    @Override
    public Resource downloadFile(Long id) throws Exception {
        User user = userService.getLoggedInUser();
        if (user == null) {
            throw new InvalidRequestException("User not recognized!");
        }
        List<Permission> permissions = permissionRepository.findAllByUser(user);
        File dbFile = fileRepository.findById(id).orElseThrow();
        if (!dbFile.isActive()) {
            throw new InvalidRequestException("File does not exist!");
        }
        if (!FileMapper.checkFilePermission(dbFile, permissions)) {
            throw new InvalidRequestException("Access to file denied!");
        }
        java.io.File file = new java.io.File(dbFile.getServerPath());
        return new ByteArrayResource(Files.readAllBytes(Paths.get(file.getAbsolutePath())));
    }

    public byte[] downloadFolder(Long id) throws Exception {
        User user = userService.getLoggedInUser();
        if (user == null) {
            throw new InvalidRequestException("User not recognized!");
        }
        List<Permission> permissions = permissionRepository.findAllByUser(user);
        Folder folder = folderRepository.findById(id).orElseThrow();
        if (!folder.isActive()) {
            throw new InvalidRequestException("Folder does not exist!");
        }
        if (!FolderMapper.checkFolderPermission(folder, permissions)) {
            throw new InvalidRequestException("Access to folder denied!");
        }
        List<rs.ac.uns.ftn.medily.model.File> files = new ArrayList<>();
        getFilesFromTree(folder, files, permissions);

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ZipOutputStream zipOut = new ZipOutputStream(bos);
        for (rs.ac.uns.ftn.medily.model.File dbFile: files) {
            if (FileMapper.checkFilePermission(dbFile, permissions) && dbFile.isActive()) {
                java.io.File fileToZip = new java.io.File(dbFile.getServerPath());
                FileInputStream fis = new FileInputStream(fileToZip);
                ZipEntry zipEntry = new ZipEntry(dbFile.getId().toString());
                zipOut.putNextEntry(zipEntry);

                byte[] bytes = new byte[1024];
                int length;
                while ((length = fis.read(bytes)) >= 0) {
                    zipOut.write(bytes, 0, length);
                }
                fis.close();
            }
        }
        zipOut.close();
        bos.close();
        return bos.toByteArray();
    }

    @Override
    public byte[] downloadPlaylist(Long id, HttpServletRequest request) throws Exception {
        Playlist playlist = playlistService.findById(id);
        if (playlist == null) {
            throw new InvalidRequestException("Playlist not found.");
        }
        User user = userService.getLoggedInUser(request);
        if (user == null) {
            throw new InvalidRequestException("User not found.");
        }

        if (!playlist.getUser().getFamily().getId().equals(user.getFamily().getId())){
            throw new InvalidRequestException("Access denied.");
        }

        List<rs.ac.uns.ftn.medily.model.File> files = new ArrayList<>();
        for (PlaylistSong playlistSong: songWithinPlaylistService.findSongsWithinPlaylist(playlist)) {
            files.add(playlistSong.getSong().getFile());
        }
        Map<String, String> entries = new HashMap<>();
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ZipOutputStream zipOut = new ZipOutputStream(bos);
        for (rs.ac.uns.ftn.medily.model.File dbFile: files) {
            java.io.File fileToZip = new java.io.File(dbFile.getServerPath());
            FileInputStream fis = new FileInputStream(fileToZip);
            String key = dbFile.getId().toString();
            if (entries.containsKey(key)) {
                key = key + "_" + UUID.randomUUID().toString() + "_" + (new Date()).getTime();
            }
            ZipEntry zipEntry = new ZipEntry(key);
            zipOut.putNextEntry(zipEntry);
            entries.put(dbFile.getId().toString(), dbFile.getId().toString());

            byte[] bytes = new byte[1024];
            int length;
            while((length = fis.read(bytes)) >= 0) {
                zipOut.write(bytes, 0, length);
            }
            fis.close();
        }
        zipOut.close();
        bos.close();

        addPlaylistToUser(user, playlist);

        return bos.toByteArray();
    }

    private void getFilesFromTree(Folder folder, List<rs.ac.uns.ftn.medily.model.File> files, List<Permission> permissions){
        files.addAll(folder.getFiles());
        for (Folder subfolder: folder.getSubFolders()){
            if (FolderMapper.checkFolderPermission(subfolder, permissions) && subfolder.isActive()) {
                getFilesFromTree(subfolder, files, permissions);
            }
        }
    }

    private void addPlaylistToUser(User user, Playlist playlist) {
        playlist.getDownloadedBy().add(user);
        playlistService.save(playlist);
    }
}
