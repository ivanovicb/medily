package rs.ac.uns.ftn.medily.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.medily.model.Song;
import rs.ac.uns.ftn.medily.repository.SongRepository;
import rs.ac.uns.ftn.medily.service.SongService;

@Service
public class SongServiceImpl implements SongService {

    private SongRepository songRepository;

    @Autowired
    public SongServiceImpl(SongRepository songRepository) {
        this.songRepository = songRepository;
    }

    @Override
    public Song save(Song song) { return songRepository.save(song); }

    @Override
    public Song findById(Long id) { return songRepository.findById(id).orElse(null); }

    @Override
    public Song findByFileId(Long id) { return songRepository.findByFileId(id); }
}
