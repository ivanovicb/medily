package rs.ac.uns.ftn.medily.dto_validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.medily.constants.ValidationConstants;
import rs.ac.uns.ftn.medily.dto.LoginDto;
import rs.ac.uns.ftn.medily.utils.BadRequestUtil;

@Service
public class LoginDtoValidator {

    private UtilValidator utilValidator;

    @Autowired
    public LoginDtoValidator(UtilValidator utilValidator) {
        this.utilValidator = utilValidator;
    }

    public void validateLogin(LoginDto dto) {
        BadRequestUtil.throwIfInvalidRequest(dto == null, ValidationConstants.dtoShouldNotBeNull());

        BadRequestUtil.throwIfInvalidRequest(dto.getUsername() == null,
                ValidationConstants.fieldShouldNotBeNull(ValidationConstants.USERNAME));
        BadRequestUtil.throwIfInvalidRequest(dto.getUsername().equals(""),
                ValidationConstants.fieldShouldNotBeEmptyString(ValidationConstants.USERNAME));

        utilValidator.validatePassword(dto.getPassword());
    }
}
