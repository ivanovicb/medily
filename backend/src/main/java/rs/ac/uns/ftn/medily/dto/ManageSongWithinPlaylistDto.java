package rs.ac.uns.ftn.medily.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Getter
public class ManageSongWithinPlaylistDto {

    private Long playlistId;
    private Long songId;
    private String action;
    private int position;
}
