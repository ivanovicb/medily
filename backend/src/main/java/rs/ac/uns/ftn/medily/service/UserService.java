package rs.ac.uns.ftn.medily.service;

import rs.ac.uns.ftn.medily.dto.FeedbackDto;
import rs.ac.uns.ftn.medily.dto.NewMemberDto;
import rs.ac.uns.ftn.medily.dto.ProfileDto;
import rs.ac.uns.ftn.medily.dto.RegistrationDto;
import rs.ac.uns.ftn.medily.model.User;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface UserService {
    void registration(RegistrationDto dto);

    void activation(String token);

    void requestPasswordRenewal(String username);

    void renewPassword(String username);

    ProfileDto getProfile(HttpServletRequest request);

    User getLoggedInUser(HttpServletRequest request);

    User getLoggedInUser();

    User findByUsername(String username);

    void changePassword(String password, HttpServletRequest request);

    List<ProfileDto> getFamily(HttpServletRequest request);

    ProfileDto getProfileForUser(HttpServletRequest request, String username);

    void createMember(HttpServletRequest request, NewMemberDto dto);

    User save(User user);

    void saveFeedback(FeedbackDto dto);
}
