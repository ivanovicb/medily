package rs.ac.uns.ftn.medily.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import rs.ac.uns.ftn.medily.model.Playlist;
import rs.ac.uns.ftn.medily.model.PlaylistSong;
import rs.ac.uns.ftn.medily.model.Song;

import java.util.List;

@Repository
public interface SongWithinPlaylistRepository extends JpaRepository<PlaylistSong, Long> {

    PlaylistSong findByPlaylistAndSongAndPosition(Playlist playlist, Song song, int position);

    @Modifying
    @Query(value = "UPDATE songs_in_playlists SET songs_in_playlists.position = songs_in_playlists.position - 1 " +
            "WHERE songs_in_playlists.position > ?1 AND songs_in_playlists.playlist_id = ?2",
            nativeQuery = true)
    int updatePositionsAfterRemovingSong(int position, Long playlistId);

    List<PlaylistSong> findByPlaylistOrderByPositionAsc(Playlist playlist);

    int countAllByPlaylist(Playlist playlist);

    @Modifying
    @Query(value = "UPDATE songs_in_playlists SET songs_in_playlists.position = songs_in_playlists.position + 1 " +
            "WHERE songs_in_playlists.position < ?1 AND songs_in_playlists.position >= ?2 AND " +
            "songs_in_playlists.playlist_id = ?3",
            nativeQuery = true)
    int updatePositionsAfterDragWhenFromGreater(int from, int to, Long playlistId);

    @Modifying
    @Query(value = "UPDATE songs_in_playlists SET songs_in_playlists.position = songs_in_playlists.position - 1 " +
            "WHERE songs_in_playlists.position > ?1 AND songs_in_playlists.position <= ?2 AND " +
            "songs_in_playlists.playlist_id = ?3",
            nativeQuery = true)
    int updatePositionsAfterDragWhenFromSmaller(int from, int to, Long playlistId);

    PlaylistSong findByPlaylistIdAndSongId(Long playlistId, Long songId);
}
