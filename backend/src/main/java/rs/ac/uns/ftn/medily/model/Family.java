package rs.ac.uns.ftn.medily.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="families")
public class Family {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "activation_token", nullable = false)
    private String activationToken;

    @Column(name = "email", nullable = false, unique = true)
    private String email;

    @Column(name = "active", nullable = false)
    private boolean active;

    @JsonBackReference
    @OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY)
    private List<User> familyMembers;

    @OneToOne (cascade=CascadeType.ALL)
    @JoinColumn(name="drive_id")
    private Drive drive;

    public Family() {}

    public Family(String name, String activationToken, String email) {
        this.name = name;
        this.activationToken = activationToken;
        this.email = email;
        this.active = false;
        this.familyMembers = new ArrayList<>();
    }

    public Long getId() { return id; }

    public void setId(Long id) { this.id = id; }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public String getActivationToken() { return activationToken; }

    public void setActivationToken(String activationToken) { this.activationToken = activationToken; }

    public String getEmail() { return email; }

    public void setEmail(String email) { this.email = email; }

    public boolean isActive() { return active; }

    public void setActive(boolean active) { this.active = active; }

    public List<User> getFamilyMembers() { return familyMembers; }

    public void setFamilyMembers(List<User> familyMembers) { this.familyMembers = familyMembers; }

    public Drive getDrive() { return drive; }

    public void setDrive(Drive drive) { this.drive = drive; }
}
