package rs.ac.uns.ftn.medily.dto_validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.medily.constants.ValidationConstants;
import rs.ac.uns.ftn.medily.dto.FileDto;
import rs.ac.uns.ftn.medily.utils.BadRequestUtil;

@Service
public class FileDtoValidator {

    private UtilValidator utilValidator;

    @Autowired
    public FileDtoValidator(UtilValidator utilValidator) {
        this.utilValidator = utilValidator;
    }

    public void validateFileForAddNewFile(FileDto dto) {
        BadRequestUtil.throwIfInvalidRequest(dto == null, ValidationConstants.dtoShouldNotBeNull());

        utilValidator.validateString(dto.getName());

        BadRequestUtil.throwIfInvalidRequest(dto.getCreated() == null,
                ValidationConstants.fieldShouldNotBeNull(ValidationConstants.DATE));
        utilValidator.validatePositiveDouble(dto.getSize());
    }
}
