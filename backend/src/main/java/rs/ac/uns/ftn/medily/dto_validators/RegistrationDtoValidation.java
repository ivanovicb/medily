package rs.ac.uns.ftn.medily.dto_validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.medily.constants.ValidationConstants;
import rs.ac.uns.ftn.medily.dto.RegistrationDto;
import rs.ac.uns.ftn.medily.utils.BadRequestUtil;

@Service
public class RegistrationDtoValidation {

    private UtilValidator utilValidator;

    @Autowired
    public RegistrationDtoValidation(UtilValidator utilValidator) {
        this.utilValidator = utilValidator;
    }

    public void validateRegistration(RegistrationDto dto) {
        BadRequestUtil.throwIfInvalidRequest(dto == null, ValidationConstants.dtoShouldNotBeNull());

        utilValidator.validateString(dto.getFamily());
        utilValidator.validateString(dto.getUsername());
        utilValidator.validateString(dto.getName());
        utilValidator.validateEmail(dto.getEmail());
        utilValidator.validatePassword(dto.getPassword());
    }
}
