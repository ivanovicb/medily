package rs.ac.uns.ftn.medily.exceptions;

import rs.ac.uns.ftn.medily.constants.ValidationConstants;
import rs.ac.uns.ftn.medily.utils.ExceptionReturnValue;

public class InvalidRequestException extends BadRequestException {

    private static final long serialVersionUID = 1L;

    public InvalidRequestException(String reason) {
        super(new ExceptionReturnValue(ValidationConstants.BAD_REQUEST_DATA, "Invalid data exception."), reason);
    }
}
