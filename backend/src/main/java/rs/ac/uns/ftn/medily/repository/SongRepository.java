package rs.ac.uns.ftn.medily.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import rs.ac.uns.ftn.medily.model.File;
import rs.ac.uns.ftn.medily.model.Song;

import java.util.List;

@Repository
public interface SongRepository extends JpaRepository<Song, Long> {

    List<Song> findAllByFileIn(List<File> files);
    Song findByFileId(Long fileId);
}
