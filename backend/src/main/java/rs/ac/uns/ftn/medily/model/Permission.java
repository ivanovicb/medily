package rs.ac.uns.ftn.medily.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;

@Entity
@Table(name="permissions")
public class Permission {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @JsonManagedReference
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    // permission = user and folder/file

    @OneToOne (cascade=CascadeType.PERSIST)
    @JoinColumn(name="folder_id")
    private Folder folder;

    @OneToOne (cascade=CascadeType.PERSIST)
    @JoinColumn(name="file_id")
    private File file;

    public Permission() {}

    public Permission(User user, Folder folder) {
        this.user = user;
        this.folder = folder;
    }

    public Permission(User user, File file) {
        this.user = user;
        this.file = file;
    }

    public Long getId() { return id; }

    public void setId(Long id) { this.id = id; }

    public User getUser() { return user; }

    public void setUser(User user) { this.user = user; }

    public Folder getFolder() { return folder; }

    public void setFolder(Folder folder) { this.folder = folder; }

    public File getFile() { return file; }

    public void setFile(File file) { this.file = file; }
}
