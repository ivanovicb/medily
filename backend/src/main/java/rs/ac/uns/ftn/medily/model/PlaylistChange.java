package rs.ac.uns.ftn.medily.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name="playlist_changes")
public class PlaylistChange {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id")
  private Long id;

  @ManyToOne
  private Playlist playlist;

  @Column(name = "playlist_change_type", nullable = false)
  @Enumerated(EnumType.STRING)
  private PlaylistChangeType playlistChangeType;

  @Column(name = "modified", nullable = false)
  private Date modified;

  @ManyToOne
  private User changedBy;

  @ManyToOne
  private Song song;
}
