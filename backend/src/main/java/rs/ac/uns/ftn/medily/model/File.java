package rs.ac.uns.ftn.medily.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name="files")
public class File {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "server_path", nullable = false)
    private String serverPath;

    @Column(name = "size", nullable = false)
    private double size;

    @Column(name = "created", nullable = false)
    private Date created;

    @Column(name = "active", nullable = false)
    private boolean active;
}
