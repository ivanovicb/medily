package rs.ac.uns.ftn.medily.dto;

import rs.ac.uns.ftn.medily.model.PlaylistSong;

public class SongDto {
    private Long id;
    private String artist;
    private String genre;
    private int duration;
    private Long fileId;
    private String filePath;
    private String name;

    private int position;

    public SongDto() {
    }

    public SongDto(Long id, String artist, String genre, int duration, Long fileId, String filePath, String name, int position) {
        this.id = id;
        this.artist = artist;
        this.genre = genre;
        this.duration = duration;
        this.fileId = fileId;
        this.filePath = filePath;
        this.name = name;
        this.position = position;
    }

    public SongDto(PlaylistSong playlistSong) {
        this.id = playlistSong.getSong().getId();
        this.artist = playlistSong.getSong().getArtist();
        this.genre = playlistSong.getSong().getGenre();
        this.duration = playlistSong.getSong().getDuration();
        this.fileId = playlistSong.getSong().getFile().getId();
        this.filePath = playlistSong.getSong().getFile().getServerPath();
        this.name = playlistSong.getSong().getFile().getName();
        this.position = playlistSong.getPosition();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPosition() { return position; }

    public void setPosition(int position) { this.position = position; }
}
