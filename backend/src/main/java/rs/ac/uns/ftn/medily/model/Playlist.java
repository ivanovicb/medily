package rs.ac.uns.ftn.medily.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name="playlists")
public class Playlist {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "share_playlist", nullable = false)
    private boolean sharePlaylist;

    @JsonManagedReference
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "created_by_user", nullable = false)
    private User user;

    @JsonBackReference
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "downloaded_by", nullable = false)
    private List<User> downloadedBy;

    public Playlist() {}

    public Playlist(String name, boolean sharePlaylist, User user) {
        this.name = name;
        this.user = user;
        this.sharePlaylist = sharePlaylist;
    }
}
