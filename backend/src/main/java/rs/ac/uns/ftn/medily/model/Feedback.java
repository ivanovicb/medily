package rs.ac.uns.ftn.medily.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;

@Entity
@Table(name="feedbacks")
public class Feedback {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "feedback", nullable = false)
    private String feedback;

    @JsonManagedReference
    @ManyToOne(cascade=CascadeType.PERSIST, fetch=FetchType.EAGER)
    private User user;

    public Feedback() {
    }

    public Feedback(String feedback, User user) {
        this.feedback = feedback;
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
