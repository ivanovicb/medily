package rs.ac.uns.ftn.medily.dto_validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.medily.constants.ValidationConstants;
import rs.ac.uns.ftn.medily.dto.NewMemberDto;
import rs.ac.uns.ftn.medily.utils.BadRequestUtil;

@Service
public class NewMemberDtoValidator {

    private UtilValidator utilValidator;

    @Autowired
    public NewMemberDtoValidator(UtilValidator utilValidator) {
        this.utilValidator = utilValidator;
    }

    public void validateCreateMember(NewMemberDto dto) {
        BadRequestUtil.throwIfInvalidRequest(dto == null, ValidationConstants.dtoShouldNotBeNull());

        utilValidator.validateString(dto.getName());
        utilValidator.validateString(dto.getUsername());
        utilValidator.validatePassword(dto.getPassword());
        utilValidator.validateFamilyRoles(dto.getRole());
    }
}
