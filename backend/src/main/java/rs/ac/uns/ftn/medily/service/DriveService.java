package rs.ac.uns.ftn.medily.service;

import org.apache.tika.exception.TikaException;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;
import org.xml.sax.SAXException;
import rs.ac.uns.ftn.medily.dto.*;
import rs.ac.uns.ftn.medily.model.File;
import rs.ac.uns.ftn.medily.model.Folder;
import rs.ac.uns.ftn.medily.model.User;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public interface DriveService {
    DriveDto getDrive();
    Folder addFolder(AddFolderDto addFolderDto);
    File addFile(MultipartFile file, AddFileDto addFileDto) throws IOException, TikaException, SAXException;
    void saveFile(MultipartFile file, String fileName);
    void renameFile(RenameDto renameDto);
    void renameFolder(RenameDto renameDto);
    void deleteFolder(Long id);
    void deleteFile(Long id);
    SongDto getSongByFileId(Long id);
    Resource downloadFile(Long id) throws Exception;
    byte[] downloadFolder(Long id) throws Exception;
    byte[] downloadPlaylist(Long id, HttpServletRequest request) throws Exception;
    DrivePermissionsDto getPermissions(String username, User user);
    void changeRole(PermissionDto dto, User user);
    void changePermissions(ChangePermissionDto dto, User user);
}
