package rs.ac.uns.ftn.medily;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MedilyApplication {

    public static void main(String[] args) {
        SpringApplication.run(MedilyApplication.class, args);
    }
}
