package rs.ac.uns.ftn.medily.dto;

public class RegistrationDto {

    private String family;
    private String email;
    private String name;
    private String username;
    private String password;

    public RegistrationDto() {}

    public RegistrationDto(String family, String email, String name, String username, String password) {
        this.family = family;
        this.email = email;
        this.name = name;
        this.username = username;
        this.password = password;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
