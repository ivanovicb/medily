package rs.ac.uns.ftn.medily.dto;

public class ProfileDto {
    private String username;
    private String name;
    private String role;
    private String family;

    public ProfileDto() {
    }

    public ProfileDto(String username, String name, String role, String family) {
        this.username = username;
        this.name = name;
        this.role = role;
        this.family = family;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }
}
