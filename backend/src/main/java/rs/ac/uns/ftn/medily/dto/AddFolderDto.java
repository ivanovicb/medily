package rs.ac.uns.ftn.medily.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AddFolderDto {
    private Boolean isRoot;
    private Long parentId;
    private FolderDto newFolder;
}
