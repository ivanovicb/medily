package rs.ac.uns.ftn.medily.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FeedbackDto {
    private String username;
    private String feedback;

    public FeedbackDto() {}

    public FeedbackDto(String username, String feedback) {
        this.username = username;
        this.feedback = feedback;
    }
}
