package rs.ac.uns.ftn.medily.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PermissionDto {
    private String username;
    private String role;

    public PermissionDto() {}

    public PermissionDto(String username, String role) {
        this.username = username;
        this.role = role;
    }
}
