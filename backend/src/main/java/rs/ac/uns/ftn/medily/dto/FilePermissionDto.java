package rs.ac.uns.ftn.medily.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class FilePermissionDto {
    private Long id;
    private String name;
    private double size;
    @JsonFormat(pattern="MMM dd, yyyy HH:mm:ss a")
    private Date created;
    private boolean allow;
}
