package rs.ac.uns.ftn.medily.dto;


public class NewPlaylistDto {

    private String name;
    private boolean share;

    public NewPlaylistDto() {}

    public NewPlaylistDto(String name, boolean share) {
        this.name = name;
        this.share = share;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isShare() {
        return share;
    }

    public void setShare(boolean share) {
        this.share = share;
    }
}
