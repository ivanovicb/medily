package rs.ac.uns.ftn.medily.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name="folders")
public class Folder {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "created", nullable = false)
    private Date created;

    @Column(name = "active", nullable = false)
    private boolean active;

    @JsonBackReference
    @OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY)
    private List<Folder> subFolders;

    @JsonBackReference
    @OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY)
    private List<File> files;
}
