package rs.ac.uns.ftn.medily.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AddFileDto {
    private Boolean isRoot;
    private Long parentId;
    private FileDto newFile;
}
