package rs.ac.uns.ftn.medily.dto_validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.medily.constants.ValidationConstants;
import rs.ac.uns.ftn.medily.dto.AddFolderDto;
import rs.ac.uns.ftn.medily.utils.BadRequestUtil;

@Service
public class AddFolderDtoValidator {

    private UtilValidator utilValidator;
    private FolderDtoValidator folderDtoValidator;

    @Autowired
    public AddFolderDtoValidator(UtilValidator utilValidator, FolderDtoValidator folderDtoValidator) {
        this.utilValidator = utilValidator;
        this.folderDtoValidator = folderDtoValidator;
    }

    public void validateAddFolder(AddFolderDto dto) {
        BadRequestUtil.throwIfInvalidRequest(dto == null, ValidationConstants.dtoShouldNotBeNull());

        utilValidator.validateLongId(dto.getParentId());
        folderDtoValidator.validateFolderDtoForAddFolder(dto.getNewFolder());
    }
}
