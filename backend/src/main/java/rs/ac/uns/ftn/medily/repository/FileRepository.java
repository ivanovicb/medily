package rs.ac.uns.ftn.medily.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import rs.ac.uns.ftn.medily.model.File;

import java.util.List;

@Repository
public interface FileRepository extends JpaRepository<File, Long> {

    File findByServerPath(String path);
}
