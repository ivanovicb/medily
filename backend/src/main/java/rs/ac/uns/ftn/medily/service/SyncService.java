package rs.ac.uns.ftn.medily.service;

import rs.ac.uns.ftn.medily.model.Playlist;
import rs.ac.uns.ftn.medily.model.PlaylistChange;
import rs.ac.uns.ftn.medily.model.PlaylistChangeType;
import rs.ac.uns.ftn.medily.model.Song;

import java.util.Date;
import java.util.List;

public interface SyncService {

  void addPlaylistChange(Playlist playlist, PlaylistChangeType playlistChangeType);

  void addPlaylistChange(Playlist playlist, PlaylistChangeType playlistChangeType, Song song);

  List<PlaylistChange> getPlaylistChanges(Date lastSync);
}
