package rs.ac.uns.ftn.medily.exceptions;


import rs.ac.uns.ftn.medily.utils.ExceptionReturnValue;

public class BadRequestException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    private final ExceptionReturnValue code;

    private final String message;

    public BadRequestException(ExceptionReturnValue code, String message) {
        this.code = code;
        this.message = message;
    }

    public ExceptionReturnValue getCode() { return code; }

    public String getMessage() { return message; }
}
