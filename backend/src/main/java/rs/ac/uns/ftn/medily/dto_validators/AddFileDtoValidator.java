package rs.ac.uns.ftn.medily.dto_validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.medily.constants.ValidationConstants;
import rs.ac.uns.ftn.medily.dto.AddFileDto;
import rs.ac.uns.ftn.medily.utils.BadRequestUtil;

@Service
public class AddFileDtoValidator {

    private UtilValidator utilValidator;
    private FileDtoValidator fileDtoValidator;

    @Autowired
    public AddFileDtoValidator(UtilValidator utilValidator, FileDtoValidator fileDtoValidator) {
        this.utilValidator = utilValidator;
        this.fileDtoValidator = fileDtoValidator;
    }

    public void validateAddFile(AddFileDto dto) {
        BadRequestUtil.throwIfInvalidRequest(dto == null, ValidationConstants.dtoShouldNotBeNull());

        utilValidator.validateLongId(dto.getParentId());
        fileDtoValidator.validateFileForAddNewFile(dto.getNewFile());
    }
}
