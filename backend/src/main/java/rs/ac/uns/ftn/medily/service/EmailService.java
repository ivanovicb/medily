package rs.ac.uns.ftn.medily.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.Objects;

import static rs.ac.uns.ftn.medily.constants.AppConstants.MAIN_URL;


/*
 * Class represents Service for user notification
 */
@Service
public class EmailService {

    private final JavaMailSender javaMailSender;
    private final Environment env;

    @Autowired
    public EmailService(JavaMailSender javaMailSender, Environment env) {
        this.javaMailSender = javaMailSender;
        this.env = env;
    }

    @Async
    public void sendRegistrationEmail(String email, String token) {
        try {
            System.out.println("Sending email...");

            String path = MAIN_URL + "/users/activation/" + token;

            SimpleMailMessage mail = new SimpleMailMessage();
            mail.setTo(email);
            mail.setFrom(Objects.requireNonNull(env.getProperty("spring.mail.username")));
            mail.setSubject("Account confirmation for joining Medily");
            mail.setText("Dear new member, \n" +
                    "Welcome to Medily - Media for Family application that allows you to connect with your " +
                    "family and enjoy your favourite media together. Please confirm your account by clicking " +
                    "on link below. \n "
                    + path + "\n\n" + "Best regards,\nMedily");

            javaMailSender.send(mail);

            System.out.println("Email sent!");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Async
    public void sendPasswordConfirmationLink(String email, String family, String token, String username, String name) {
        try {
            System.out.println("Sending email...");

            String path = MAIN_URL + "/users/renewal/" + username + "/" + token;

            SimpleMailMessage mail = new SimpleMailMessage();
            mail.setTo(email);
            mail.setFrom(Objects.requireNonNull(env.getProperty("spring.mail.username")));
            mail.setSubject("Account confirmation for joining Medily");
            mail.setText("Dear family "+ family + ", \n" +
                    "We have recently received a request for password change for your family member " + name + " (" +
                    username + "). If this request is valid and you really want to change password for this user " +
                    "please confirm it by clicking on link below. \n"
                    + path + "\n\nIf this user did not request password renewal then please ignore or delete this " +
                    "email and we are truly sorry for the inconvenience.\n\n" + "Best regards,\nMedily");

            javaMailSender.send(mail);

            System.out.println("Email sent!");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Async
    public void sendNewPassword(String email, String family, String password, String username, String name) {
        try {
            System.out.println("Sending email...");

            SimpleMailMessage mail = new SimpleMailMessage();
            mail.setTo(email);
            mail.setFrom(Objects.requireNonNull(env.getProperty("spring.mail.username")));
            mail.setSubject("Account confirmation for joining Medily");
            mail.setText("Dear family "+ family + ", \n" +
                    "You have recently received a request for a new password for your family member " + name + " (" +
                    username + "). New password for login for this user is " + password + ". \nAfter successful login" +
                    " we would advise user to change password and remember it for further use.\n\n" +
                    "Best regards,\nMedily");

            javaMailSender.send(mail);

            System.out.println("Email sent!");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

