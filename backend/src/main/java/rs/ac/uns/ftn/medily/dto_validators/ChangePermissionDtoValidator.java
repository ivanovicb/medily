package rs.ac.uns.ftn.medily.dto_validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.medily.constants.ValidationConstants;
import rs.ac.uns.ftn.medily.dto.ChangePermissionDto;
import rs.ac.uns.ftn.medily.utils.BadRequestUtil;

import java.util.Arrays;
import java.util.List;

@Service
public class ChangePermissionDtoValidator {

    private UtilValidator utilValidator;

    @Autowired
    public ChangePermissionDtoValidator(UtilValidator utilValidator) {
        this.utilValidator = utilValidator;
    }

    public void validateChangePermission(ChangePermissionDto dto) {
        BadRequestUtil.throwIfInvalidRequest(dto == null, ValidationConstants.dtoShouldNotBeNull());

        utilValidator.validateString(dto.getUsername());
        utilValidator.validateLongId(dto.getId());

        List<String> types = Arrays.asList("file", "folder");

        BadRequestUtil.throwIfInvalidRequest(!types.contains(dto.getType()),
                ValidationConstants.invalidTypeOfPermissionObject(dto.getType()));
    }
}
