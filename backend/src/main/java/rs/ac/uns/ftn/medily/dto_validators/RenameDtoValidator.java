package rs.ac.uns.ftn.medily.dto_validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.medily.constants.ValidationConstants;
import rs.ac.uns.ftn.medily.dto.RenameDto;
import rs.ac.uns.ftn.medily.utils.BadRequestUtil;

@Service
public class RenameDtoValidator {

    private UtilValidator utilValidator;

    @Autowired
    public RenameDtoValidator(UtilValidator utilValidator) {
        this.utilValidator = utilValidator;
    }

    public void validateRename(RenameDto dto) {
        BadRequestUtil.throwIfInvalidRequest(dto == null, ValidationConstants.dtoShouldNotBeNull());

        utilValidator.validateString(dto.getNewName());
        utilValidator.validateLongId(dto.getId());
    }
}
