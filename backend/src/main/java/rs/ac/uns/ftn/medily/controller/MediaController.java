package rs.ac.uns.ftn.medily.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import rs.ac.uns.ftn.medily.dto.*;
import rs.ac.uns.ftn.medily.dto_validators.*;
import rs.ac.uns.ftn.medily.exceptions.BadRequestException;
import rs.ac.uns.ftn.medily.model.Playlist;
import rs.ac.uns.ftn.medily.model.PlaylistSong;
import rs.ac.uns.ftn.medily.model.User;
import rs.ac.uns.ftn.medily.service.PlaylistService;
import rs.ac.uns.ftn.medily.service.SongWithinPlaylistService;
import rs.ac.uns.ftn.medily.service.UserService;
import rs.ac.uns.ftn.medily.service.UtilService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("media")
public class MediaController {

    private final UserService userService;
    private final UtilService utilService;
    private final ItemDtoValidator itemDtoValidator;
    private final PlaylistService playlistService;
    private final RenameDtoValidator renameDtoValidator;
    private final NewPlaylistDtoValidator newPlaylistDtoValidator;
    private final SongWithinPlaylistService songWithinPlaylistService;
    private final PlaylistStatusDtoValidator playlistStatusDtoValidator;
    private final UpdateSongPositionDtoValidator updateSongPositionDtoValidator;
    private final ManageSongWithinPlaylistDtoValidator manageSongWithinPlaylistDtoValidator;
    private final UtilValidator utilValidator;

    @Autowired
    public MediaController(PlaylistService playlistService, SongWithinPlaylistService songWithinPlaylistService,
                           UtilService utilService, UserService userService, ItemDtoValidator itemDtoValidator,
                           NewPlaylistDtoValidator newPlaylistDtoValidator,
                           PlaylistStatusDtoValidator playlistStatusDtoValidator,
                           RenameDtoValidator renameDtoValidator,
                           UpdateSongPositionDtoValidator updateSongPositionDtoValidator,
                           ManageSongWithinPlaylistDtoValidator manageSongWithinPlaylistDtoValidator,
                            UtilValidator utilValidator) {
        this.utilService = utilService;
        this.userService = userService;
        this.playlistService = playlistService;
        this.itemDtoValidator = itemDtoValidator;
        this.renameDtoValidator = renameDtoValidator;
        this.newPlaylistDtoValidator = newPlaylistDtoValidator;
        this.songWithinPlaylistService = songWithinPlaylistService;
        this.playlistStatusDtoValidator = playlistStatusDtoValidator;
        this.updateSongPositionDtoValidator = updateSongPositionDtoValidator;
        this.manageSongWithinPlaylistDtoValidator = manageSongWithinPlaylistDtoValidator;
        this.utilValidator = utilValidator;
    }

    @RequestMapping(value = "/playlists/new", method = RequestMethod.POST)
    public ResponseEntity<?> createPlaylist(HttpServletRequest request, @RequestBody NewPlaylistDto dto) {
        try {
            User user = userService.getLoggedInUser(request);
            newPlaylistDtoValidator.validateCreatePlaylist(dto, user);
            return new ResponseEntity<>(playlistService.createPlaylist(user, dto), HttpStatus.OK);
        } catch (Exception ex) {
            ex.printStackTrace();
            if (ex instanceof BadRequestException) {
                return ResponseEntity.badRequest().body(ex.getMessage());
            } else {
                return ResponseEntity.status(503).body("Service currently unavailable, please try again later!");
            }
        }
    }

    @RequestMapping(value = "/playlists/public", method = RequestMethod.GET)
    public ResponseEntity<?> getPublicPlaylists(HttpServletRequest request) {
        try {
            return new ResponseEntity<>(playlistService.getPublicPlaylists(request), HttpStatus.OK);
        } catch (Exception ex) {
            if (ex instanceof BadRequestException) {
                return ResponseEntity.badRequest().body(ex.getMessage());
            } else {
                return ResponseEntity.status(503).body("Service currently unavailable, please try again later!");
            }
        }
    }

    @RequestMapping(value = "/playlists/status", method = RequestMethod.POST)
    public ResponseEntity<?> changePlaylistStatus(HttpServletRequest request, @RequestBody PlaylistStatusDto dto) {
        try {
            playlistStatusDtoValidator.validateChangePlaylistStatus(dto);
            playlistService.changePlaylistStatus(request, dto);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception ex) {
            if (ex instanceof BadRequestException) {
                return ResponseEntity.badRequest().body(ex.getMessage());
            } else {
                return ResponseEntity.status(503).body("Service currently unavailable, please try again later!");
            }
        }
    }

    @RequestMapping(value = "/playlists/rename", method = RequestMethod.POST)
    public ResponseEntity<?> renamePlaylist(@RequestBody RenameDto renameDto) {
        try {
            renameDtoValidator.validateRename(renameDto);
            playlistService.renamePlaylist(renameDto.getId(), renameDto.getNewName());
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception ex) {
            if (ex instanceof BadRequestException) {
                return ResponseEntity.badRequest().body(ex.getMessage());
            } else {
                return ResponseEntity.status(503).body("Service currently unavailable, please try again later!");
            }
        }
    }

    @RequestMapping(value = "/playlists/delete", method = RequestMethod.POST)
    public ResponseEntity<?> deletePlaylist(HttpServletRequest request, @RequestBody ItemDto itemDto) {
        try {
            itemDtoValidator.validateStringValueOfLongInItem(itemDto);
            playlistService.deletePlaylist(request, Long.valueOf(itemDto.getItem()));
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception ex) {
            if (ex instanceof BadRequestException) {
                return ResponseEntity.badRequest().body(ex.getMessage());
            } else {
                return ResponseEntity.status(503).body("Service currently unavailable, please try again later!");
            }
        }
    }

    @RequestMapping(value = "/playlists/manage", method = RequestMethod.POST)
    public ResponseEntity<?> manageSongWithinPlaylist(@RequestBody ManageSongWithinPlaylistDto dto) {
        try {
            manageSongWithinPlaylistDtoValidator.validateManageSongWithinPlaylist(dto);
            boolean actionSuccess = playlistService.manageSongWithinPlaylist(dto);
            if (actionSuccess) {
                return new ResponseEntity<>(HttpStatus.OK);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            if (ex instanceof BadRequestException) {
                return ResponseEntity.badRequest().body(ex.getMessage());
            } else {
                return ResponseEntity.status(503).body("Service currently unavailable, please try again later!");
            }
        }

        return ResponseEntity.badRequest().body("Operation not successful.");
    }

    @RequestMapping(value = "/playlists/songs", method = RequestMethod.POST)
    public ResponseEntity<?> getSongsWithinPlaylist(@RequestBody ItemDto dto) {
        try {
            itemDtoValidator.validateStringValueOfLongInItem(dto);
            Playlist playlist = playlistService.findById(Long.valueOf(dto.getItem()));
            if (playlist != null) {
                List<PlaylistSong> data = songWithinPlaylistService.findSongsWithinPlaylist(playlist);
                return ResponseEntity.ok(utilService.convertToSongsWithFiles(data));
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            if (ex instanceof BadRequestException) {
                return ResponseEntity.badRequest().body(ex.getMessage());
            } else {
                return ResponseEntity.status(503).body("Service currently unavailable, please try again later!");
            }
        }

        return ResponseEntity.badRequest().body("Operation not successful.");
    }

    @RequestMapping(value = "/playlists/update/position", method = RequestMethod.POST)
    public ResponseEntity<?> updateSongPositionInPlaylist(@RequestBody UpdateSongPositionDto dto) {
        try {
            updateSongPositionDtoValidator.validateUpdateSongPositionInPlaylist(dto);
            boolean actionSuccess = songWithinPlaylistService.updateSongPositionInPlaylist(dto);
            if (actionSuccess) {
                return new ResponseEntity<>(HttpStatus.OK);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            if (ex instanceof BadRequestException) {
                return ResponseEntity.badRequest().body(ex.getMessage());
            } else {
                return ResponseEntity.status(503).body("Service currently unavailable, please try again later!");
            }
        }

        return ResponseEntity.badRequest().body("Operation not successful.");
    }

    @RequestMapping(value = "/playlist/{playlistId}", method = RequestMethod.GET)
    public ResponseEntity<PlaylistDto> getPlaylistById(@PathVariable Long playlistId) {
        try {
            PlaylistDto playlistDto = playlistService.getPlaylistById(playlistId);
            return ResponseEntity.ok().body(playlistDto);
        } catch (Exception ex) {
            return ResponseEntity.badRequest().build();
        }
    }

    @RequestMapping(value = "/get/song/{playlistId}/{songId}", method = RequestMethod.GET)
    public ResponseEntity<?> getSongByFileId(@PathVariable Long playlistId, @PathVariable Long songId){
        try {
            utilValidator.validateLongId(songId);
            PlaylistSong playlistSong = songWithinPlaylistService.findSongsWithinPlaylistBySongId(playlistId, songId);
            return ResponseEntity.ok().body(new SongDto(playlistSong));
        } catch (Exception ex){
            return ResponseEntity.badRequest().build();
        }
    }
}
