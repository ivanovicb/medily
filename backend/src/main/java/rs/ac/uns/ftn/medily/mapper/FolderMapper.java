package rs.ac.uns.ftn.medily.mapper;

import rs.ac.uns.ftn.medily.dto.FolderDto;
import rs.ac.uns.ftn.medily.dto.FolderPermissionDto;
import rs.ac.uns.ftn.medily.model.Folder;
import rs.ac.uns.ftn.medily.model.Permission;

import java.util.ArrayList;
import java.util.List;


public class FolderMapper {

    public static FolderDto folderToFolderDto(Folder folder, List<Permission> permissions) {
        FolderDto folderDto = new FolderDto();
        folderDto.setId(folder.getId());
        folderDto.setName(folder.getName());
        folderDto.setCreated(folder.getCreated());
        folderDto.setFiles(FileMapper.fileListToFileDtoList(folder.getFiles(), permissions));
        folderDto.setSubFolders(folderListToFolderDtoList(folder.getSubFolders(), permissions));

        return folderDto;
    }

    public static List<FolderDto> folderListToFolderDtoList(List<Folder> folders, List<Permission> permissions) {
        List<FolderDto> foldersDtos = new ArrayList<>();

        for(Folder folder: folders){
            if (checkFolderPermission(folder, permissions) && folder.isActive()) {
                foldersDtos.add(folderToFolderDto(folder, permissions));
            }
        }

        return foldersDtos;
    }

    public static Folder folderDtoToFolder(FolderDto folderDto){
        Folder folder = new Folder();
        folder.setId(folderDto.getId());
        folder.setName(folderDto.getName());
        folder.setCreated(folderDto.getCreated());
        folder.setFiles(FileMapper.fileDtoListToFileList(folderDto.getFiles()));
        folder.setSubFolders(folderDtoListToFolderList(folderDto.getSubFolders()));

        return folder;
    }

    public static List<Folder> folderDtoListToFolderList(List<FolderDto> folderDtos) {
        List<Folder> folders = new ArrayList<>();

        for(FolderDto folderDto: folderDtos){
            folders.add(folderDtoToFolder(folderDto));
        }

        return folders;
    }

    public static List<FolderPermissionDto> folderPermissionListToFolderPermissionDtoList(List<Folder> folders,
                                                                                          List<Permission> permissions) {
        List<FolderPermissionDto> foldersDtos = new ArrayList<>();

        for(Folder folder: folders){
            foldersDtos.add(folderToFolderPermissionDto(folder, permissions));
        }

        return foldersDtos;
    }

    private static FolderPermissionDto folderToFolderPermissionDto(Folder folder, List<Permission> permissions) {
        FolderPermissionDto folderDto = new FolderPermissionDto();
        folderDto.setId(folder.getId());
        folderDto.setName(folder.getName());
        folderDto.setCreated(folder.getCreated());
        folderDto.setFiles(FileMapper.filePermissionListToFilePermissionDtoList(folder.getFiles(), permissions));
        folderDto.setSubFolders(folderPermissionListToFolderPermissionDtoList(folder.getSubFolders(), permissions));
        folderDto.setAllow(checkFolderPermission(folder, permissions));
        return folderDto;
    }

    public static boolean checkFolderPermission(Folder folder, List<Permission> permissions) {
        for (Permission permission: permissions) {
            if (permission.getFolder() != null) {
                if (folder.getId().equals(permission.getFolder().getId())) {
                    return false;
                }
            }
        }
        return true;
    }
}
