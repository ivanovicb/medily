package rs.ac.uns.ftn.medily.security;

import rs.ac.uns.ftn.medily.constants.AppConstants;
import org.springframework.stereotype.Component;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Random;

@Component
public class CryptoModule {

    private IvParameterSpec ivParameterSpec;
    private SecretKeySpec secretKeySpec;
    private Cipher cipher;


    public CryptoModule() { }

    public String generateRandomString() {
        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 10;
        Random random = new Random();
        StringBuilder buffer = new StringBuilder(targetStringLength);
        for (int i = 0; i < targetStringLength; i++) {
            int randomLimitedInt = leftLimit + (int)
                    (random.nextFloat() * (rightLimit - leftLimit + 1));
            buffer.append((char) randomLimitedInt);
        }
        return buffer.toString();
    }

    public String encrypt(String message) {
        try {
            ivParameterSpec = new IvParameterSpec(AppConstants.SECRET_KEY_1.getBytes(StandardCharsets.UTF_8));
            secretKeySpec = new SecretKeySpec(AppConstants.SECRET_KEY_2.getBytes(StandardCharsets.UTF_8), "AES");
            cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, ivParameterSpec);
            byte[] encrypted = cipher.doFinal(message.trim().getBytes());

            return Base64.getEncoder().encodeToString(encrypted);
        } catch (Exception e) { e.printStackTrace(); }

        return null;
    }

    public String decrypt(String encryptedMessage) {
        try {
            if (encryptedMessage == null) {
                return null;
            }
            ivParameterSpec = new IvParameterSpec(AppConstants.SECRET_KEY_1.getBytes(StandardCharsets.UTF_8));
            secretKeySpec = new SecretKeySpec(AppConstants.SECRET_KEY_2.getBytes(StandardCharsets.UTF_8), "AES");
            cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.DECRYPT_MODE, secretKeySpec, ivParameterSpec);
            byte[] decryptedBytes = cipher.doFinal(Base64.getDecoder().decode(encryptedMessage.trim()));
            return new String(decryptedBytes);
        } catch (Exception e ) {
            e.printStackTrace();
        }

        return null;
    }
}
