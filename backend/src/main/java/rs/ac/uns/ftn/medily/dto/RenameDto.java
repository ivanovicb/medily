package rs.ac.uns.ftn.medily.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RenameDto {
    private Long id;
    private String newName;
}
