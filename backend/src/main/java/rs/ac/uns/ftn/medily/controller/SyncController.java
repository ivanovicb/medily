package rs.ac.uns.ftn.medily.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import rs.ac.uns.ftn.medily.dto.PlaylistChangeDto;
import rs.ac.uns.ftn.medily.dto_validators.UtilValidator;
import rs.ac.uns.ftn.medily.model.PlaylistChange;
import rs.ac.uns.ftn.medily.service.SyncService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@RestController
@RequestMapping("sync")
public class SyncController {

  private final UtilValidator utilValidator;
  private final SyncService syncService;

  public SyncController(UtilValidator utilValidator, SyncService syncService) {
    this. utilValidator = utilValidator;
    this.syncService = syncService;
  }

  @RequestMapping(method = RequestMethod.GET)
  public ResponseEntity<List<PlaylistChangeDto>> getPlaylistChanges(@RequestParam String lastSync)
      throws ParseException {
    utilValidator.validateString(lastSync);
    SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss", Locale.UK);
    List<PlaylistChange> playlistChanges = syncService.getPlaylistChanges(sdf.parse(lastSync));
    return ResponseEntity.ok().body(playlistChanges.stream().map(PlaylistChangeDto::new).collect(Collectors.toList()));
  }

}
