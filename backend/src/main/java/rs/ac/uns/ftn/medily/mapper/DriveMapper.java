package rs.ac.uns.ftn.medily.mapper;

import rs.ac.uns.ftn.medily.dto.DriveDto;
import rs.ac.uns.ftn.medily.dto.DrivePermissionsDto;
import rs.ac.uns.ftn.medily.dto.FilePermissionDto;
import rs.ac.uns.ftn.medily.dto.FolderPermissionDto;
import rs.ac.uns.ftn.medily.model.Drive;
import rs.ac.uns.ftn.medily.model.Permission;

import java.util.List;

public class DriveMapper {

    public static DriveDto driveToDriveDto(Drive drive, List<Permission> permissions) {
        DriveDto driveDto = new DriveDto();
        driveDto.setId(drive.getId());
        driveDto.setFiles(FileMapper.fileListToFileDtoList(drive.getFiles(), permissions));
        driveDto.setFolders(FolderMapper.folderListToFolderDtoList(drive.getFolders(), permissions));

        return driveDto;
    }

    public static DrivePermissionsDto drivePermissionsToDriveDto(Drive drive, List<Permission> permissions){
        DrivePermissionsDto driveDto = new DrivePermissionsDto();
        driveDto.setId(drive.getId());
        driveDto.setFiles(FileMapper.filePermissionListToFilePermissionDtoList(drive.getFiles(), permissions));
        driveDto.setFolders(FolderMapper.folderPermissionListToFolderPermissionDtoList(drive.getFolders(), permissions));
        return driveDto;
    }
}
