package rs.ac.uns.ftn.medily.dto_validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.medily.constants.ValidationConstants;
import rs.ac.uns.ftn.medily.dto.UpdateSongPositionDto;
import rs.ac.uns.ftn.medily.utils.BadRequestUtil;

@Service
public class UpdateSongPositionDtoValidator {

    private UtilValidator utilValidator;

    @Autowired
    public UpdateSongPositionDtoValidator(UtilValidator utilValidator) {
        this.utilValidator = utilValidator;
    }

    public void validateUpdateSongPositionInPlaylist(UpdateSongPositionDto dto) {
        BadRequestUtil.throwIfInvalidRequest(dto == null, ValidationConstants.dtoShouldNotBeNull());

        utilValidator.validateLongId(dto.getSongId());
        utilValidator.validateLongId(dto.getPlaylistId());
        utilValidator.validatePosition(dto.getOldPosition());
        utilValidator.validatePosition(dto.getNewPosition());
    }

}
