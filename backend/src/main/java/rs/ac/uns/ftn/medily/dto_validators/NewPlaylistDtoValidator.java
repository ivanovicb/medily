package rs.ac.uns.ftn.medily.dto_validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.medily.constants.ValidationConstants;
import rs.ac.uns.ftn.medily.dto.NewPlaylistDto;
import rs.ac.uns.ftn.medily.model.User;
import rs.ac.uns.ftn.medily.utils.BadRequestUtil;

@Service
public class NewPlaylistDtoValidator {

    private UtilValidator utilValidator;

    @Autowired
    public NewPlaylistDtoValidator(UtilValidator utilValidator) {
        this.utilValidator = utilValidator;
    }

    public void validateCreatePlaylist(NewPlaylistDto dto, User user) {
        BadRequestUtil.throwIfInvalidRequest(dto == null, ValidationConstants.dtoShouldNotBeNull());
        utilValidator.validateString(dto.getName());

        BadRequestUtil.throwIfInvalidRequest(user == null, ValidationConstants.noLoggedInUser());
    }
}
