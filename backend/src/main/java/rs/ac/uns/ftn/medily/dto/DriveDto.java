package rs.ac.uns.ftn.medily.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class DriveDto {
    private Long id;
    private List<FileDto> files;
    private List<FolderDto> folders;
}
