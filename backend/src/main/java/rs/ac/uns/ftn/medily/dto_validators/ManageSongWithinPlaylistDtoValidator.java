package rs.ac.uns.ftn.medily.dto_validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.medily.constants.ValidationConstants;
import rs.ac.uns.ftn.medily.dto.ManageSongWithinPlaylistDto;
import rs.ac.uns.ftn.medily.utils.BadRequestUtil;

import java.util.Arrays;
import java.util.List;

@Service
public class ManageSongWithinPlaylistDtoValidator {

    private UtilValidator utilValidator;

    @Autowired
    public ManageSongWithinPlaylistDtoValidator(UtilValidator utilValidator) {
        this.utilValidator = utilValidator;
    }

    public void validateManageSongWithinPlaylist(ManageSongWithinPlaylistDto dto) {
        BadRequestUtil.throwIfInvalidRequest(dto == null, ValidationConstants.dtoShouldNotBeNull());

        utilValidator.validateLongId(dto.getPlaylistId());
        utilValidator.validateLongId(dto.getSongId());
        utilValidator.validateString(dto.getAction());

        List<String> actions = Arrays.asList("add", "remove");

        BadRequestUtil.throwIfInvalidRequest(!actions.contains(dto.getAction()),
                ValidationConstants.invalidAction(dto.getAction()));
    }
}
