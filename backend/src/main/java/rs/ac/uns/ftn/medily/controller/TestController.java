package rs.ac.uns.ftn.medily.controller;

import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import rs.ac.uns.ftn.medily.exceptions.BadRequestException;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@RestController
@RequestMapping("tests")
public class TestController {

    @RequestMapping(value = "/test/{type}", method = RequestMethod.GET)
    public ResponseEntity<?> test(@PathVariable String type) {
        try {
            String path = "static/image/ftn.jpg";
            if (type.equals("video")) {
                path = "static/image/video.mp4";
            }
            File file = new ClassPathResource(path).getFile();
            ByteArrayResource resource = new ByteArrayResource(Files.readAllBytes(Paths.get(file.getAbsolutePath())));

            return ResponseEntity.ok()
                    .contentLength(file.length())
                    .contentType(MediaType.parseMediaType("application/octet-stream"))
                    .body(resource);
        } catch (Exception ex) {
            if (ex instanceof BadRequestException) {
                return ResponseEntity.badRequest().body(ex.getMessage());
            } else {
                return ResponseEntity.status(503).body("Service currently unavailable, please try again later!");
            }
        }
    }
}
