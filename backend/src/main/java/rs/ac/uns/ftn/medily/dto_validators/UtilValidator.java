package rs.ac.uns.ftn.medily.dto_validators;

import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.medily.constants.ValidationConstants;
import rs.ac.uns.ftn.medily.model.Role;
import rs.ac.uns.ftn.medily.utils.BadRequestUtil;

@Service
public class UtilValidator {

    public void validateString(String value) {
        BadRequestUtil.throwIfInvalidRequest(value == null,
                ValidationConstants.fieldShouldNotBeNull(ValidationConstants.STRING_VALUE));
        BadRequestUtil.throwIfInvalidRequest(value.equals(""),
                ValidationConstants.fieldShouldNotBeEmptyString(ValidationConstants.STRING_VALUE));
    }

    public void validateBoolean(Boolean value) {
        BadRequestUtil.throwIfInvalidRequest(value == null,
                ValidationConstants.fieldShouldNotBeNull(ValidationConstants.STATUS));
    }

    public void validateLongId(Long id) {
        BadRequestUtil.throwIfInvalidRequest(id == null,
                ValidationConstants.fieldShouldNotBeNull(ValidationConstants.ID_PARAMETER));

        try {
            id = Long.parseLong(id.toString());
            BadRequestUtil.throwIfInvalidRequest(id < 0,
                    ValidationConstants.fieldShouldNotBeNegative(id.toString()));
        } catch (NumberFormatException | NullPointerException nfe) {
            BadRequestUtil.throwException(ValidationConstants.invalidNumber(id.toString()));
        }
    }

    public void validateStringValueOfLong(String dto) {
        BadRequestUtil.throwIfInvalidRequest(dto == null, ValidationConstants.dtoShouldNotBeNull());

        Long id = 0L;
        try {
            id = Long.parseLong(id.toString());
            BadRequestUtil.throwIfInvalidRequest(id < 0,
                    ValidationConstants.fieldShouldNotBeNegative(id.toString()));
        } catch (NumberFormatException | NullPointerException nfe) {
            BadRequestUtil.throwException(ValidationConstants.invalidNumber(dto));
        }
    }

    public void validatePosition(Integer integer) {
        BadRequestUtil.throwIfInvalidRequest(integer == null,
                ValidationConstants.invalidNumber(integer.toString()));

        try {
            integer = Integer.parseInt(integer.toString());
            BadRequestUtil.throwIfInvalidRequest(integer <= 0,
                    ValidationConstants.fieldShouldNotBeNegative(integer.toString()));
        } catch (NumberFormatException | NullPointerException nfe) {
            BadRequestUtil.throwException(ValidationConstants.invalidNumber(integer.toString()));
        }
    }

    public void validatePositiveDouble(double value) {
        try {
            BadRequestUtil.throwIfInvalidRequest(value < 0,
                    ValidationConstants.fieldShouldNotBeNegative(String.valueOf(value)));
        } catch (NumberFormatException | NullPointerException nfe) {
            BadRequestUtil.throwException(ValidationConstants.invalidNumber(String.valueOf(value)));
        }
    }

    public void validateEmail(String email) {
        BadRequestUtil.throwIfInvalidRequest(email == null,
                ValidationConstants.fieldShouldNotBeNull(ValidationConstants.EMAIL));
        BadRequestUtil.throwIfInvalidRequest(email.equals(""),
                ValidationConstants.fieldShouldNotBeEmptyString(ValidationConstants.EMAIL));
        BadRequestUtil.throwIfInvalidRequest(!email.matches(ValidationConstants.EMAIL_REGEX),
                ValidationConstants.emailRegexCheck());
    }

    public void validatePassword(String password) {
        BadRequestUtil.throwIfInvalidRequest(password == null,
                ValidationConstants.fieldShouldNotBeNull(ValidationConstants.PASSWORD));
        BadRequestUtil.throwIfInvalidRequest(password.equals(""),
                ValidationConstants.fieldShouldNotBeEmptyString(ValidationConstants.PASSWORD));

        BadRequestUtil.throwIfInvalidRequest(!password.matches(ValidationConstants.PASSWORD_REGEX),
                ValidationConstants.passwordFieldContainsInvalidChars());
    }

    /* for UserController.activation*/
    public void validateToken(String token) {
        BadRequestUtil.throwIfInvalidRequest(token == null,
                ValidationConstants.fieldShouldNotBeNull(ValidationConstants.TOKEN));
        BadRequestUtil.throwIfInvalidRequest(token.equals(""),
                ValidationConstants.fieldShouldNotBeEmptyString(ValidationConstants.TOKEN));
    }

    public void validateFamilyRoles(String value) {
        boolean equalValue = false;
        for (Role role : Role.values()) {
            if (role.toString().equals(value.toUpperCase())) {
                equalValue = true;
                break;
            }
        }

        BadRequestUtil.throwIfInvalidRequest(!equalValue,
                ValidationConstants.invalidRoleValue(value));
    }
}
