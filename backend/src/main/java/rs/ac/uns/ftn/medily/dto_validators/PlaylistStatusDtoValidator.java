package rs.ac.uns.ftn.medily.dto_validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.medily.constants.ValidationConstants;
import rs.ac.uns.ftn.medily.dto.PlaylistStatusDto;
import rs.ac.uns.ftn.medily.utils.BadRequestUtil;

@Service
public class PlaylistStatusDtoValidator {

    private UtilValidator utilValidator;

    @Autowired
    public PlaylistStatusDtoValidator(UtilValidator utilValidator) {
        this.utilValidator = utilValidator;
    }

    public void validateChangePlaylistStatus(PlaylistStatusDto dto) {
        BadRequestUtil.throwIfInvalidRequest(dto == null, ValidationConstants.dtoShouldNotBeNull());

        utilValidator.validateLongId(dto.getId());
        utilValidator.validateBoolean(dto.getStatus());
    }
}
