package rs.ac.uns.ftn.medily.model;

public enum PlaylistChangeType {
  PLAYLIST_RENAMED,
  SONG_POSITION,
  SONG_ADDED,
  SONG_DELETED
}
