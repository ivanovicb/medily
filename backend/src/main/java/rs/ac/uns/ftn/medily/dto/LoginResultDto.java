package rs.ac.uns.ftn.medily.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoginResultDto {
    private Long id;
    private String username;
    private String token;
    private String name;
    private String family;

    public LoginResultDto() {
    }

    public LoginResultDto(Long id, String username, String token, String name, String family) {
        this.id = id;
        this.username = username;
        this.token = token;
        this.name = name;
        this.family = family;
    }
}
