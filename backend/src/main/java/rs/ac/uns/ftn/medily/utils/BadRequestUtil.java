package rs.ac.uns.ftn.medily.utils;

import rs.ac.uns.ftn.medily.exceptions.InvalidRequestException;

public final class BadRequestUtil {

    private BadRequestUtil() {}

    public static void throwException(String reason) {
        throw new InvalidRequestException(reason);
    }

    public static void throwIfInvalidRequest(boolean invalid, String reason) {
        if (invalid)
            throwException(reason);
    }
}

