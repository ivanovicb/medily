package rs.ac.uns.ftn.medily.service;

import rs.ac.uns.ftn.medily.dto.ManageSongWithinPlaylistDto;
import rs.ac.uns.ftn.medily.dto.NewPlaylistDto;
import rs.ac.uns.ftn.medily.dto.PlaylistDto;
import rs.ac.uns.ftn.medily.dto.PlaylistStatusDto;
import rs.ac.uns.ftn.medily.model.Playlist;
import rs.ac.uns.ftn.medily.model.User;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface PlaylistService {

    PlaylistDto createPlaylist(User user, NewPlaylistDto dto);

    List<PlaylistDto> getPublicPlaylists(HttpServletRequest request);

    void changePlaylistStatus(HttpServletRequest request, PlaylistStatusDto dto);

    Playlist renamePlaylist(Long id, String newName);

    void deletePlaylist(HttpServletRequest request, Long id);

    boolean manageSongWithinPlaylist(ManageSongWithinPlaylistDto dto);

    Playlist findById(Long id);

    Playlist save(Playlist playlist);

    PlaylistDto getPlaylistById(Long playlistId);
}
