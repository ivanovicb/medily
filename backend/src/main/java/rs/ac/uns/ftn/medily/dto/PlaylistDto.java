package rs.ac.uns.ftn.medily.dto;

import rs.ac.uns.ftn.medily.model.Playlist;

import java.util.List;

public class PlaylistDto {
    private Long id;
    private String name;
    private boolean share;
    private String createdBy;
    private List<SongDto> songs;

    public PlaylistDto() {}

    public PlaylistDto(Long id, String name, boolean share, List<SongDto> songs, String createdBy) {
        this.id = id;
        this.name = name;
        this.share = share;
        this.songs = songs;
        this.createdBy = createdBy;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isShare() {
        return share;
    }

    public void setShare(boolean share) {
        this.share = share;
    }

    public List<SongDto> getSongs() {
        return songs;
    }

    public void setSongs(List<SongDto> songs) {
        this.songs = songs;
    }

    public String getCreatedBy() { return createdBy; }

    public void setCreatedBy(String createdBy) { this.createdBy = createdBy; }
}
