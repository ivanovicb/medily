package rs.ac.uns.ftn.medily.model;

public enum Role {
    CHILD, PARENT
}
