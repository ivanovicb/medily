package rs.ac.uns.ftn.medily.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import rs.ac.uns.ftn.medily.model.*;

import java.util.List;
import java.util.Optional;

@Repository
public interface PermissionRepository extends JpaRepository<Permission, Long> {
    List<Permission> findAllByUser(User user);
    Optional<Permission> findByUserAndFile(User user, File File);
    Optional<Permission> findByUserAndFolder(User user, Folder Folder);
}
