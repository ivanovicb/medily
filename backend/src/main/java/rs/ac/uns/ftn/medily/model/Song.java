package rs.ac.uns.ftn.medily.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="songs")
public class Song {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "artist")
    private String artist;

    @Column(name = "genre")
    private String genre;

    @Column(name = "duration")
    private int duration; // seconds

    @OneToOne (cascade=CascadeType.ALL)
    @JoinColumn(name="file_id", unique= true, nullable=false)
    private File file;

    public Song() {}

    public Song(String artist, String genre, int duration) {
        this.artist = artist;
        this.genre = genre;
        this.duration = duration;
    }

    public Long getId() { return id; }

    public void setId(Long id) { this.id = id; }

    public String getArtist() { return artist; }

    public void setArtist(String artist) { this.artist = artist; }

    public String getGenre() { return genre; }

    public void setGenre(String genre) { this.genre = genre; }

    public int getDuration() { return duration; }

    public void setDuration(int duration) { this.duration = duration; }

    public File getFile() { return file; }

    public void setFile(File file) { this.file = file; }
}
