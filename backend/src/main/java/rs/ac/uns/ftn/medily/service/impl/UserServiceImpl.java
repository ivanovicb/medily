package rs.ac.uns.ftn.medily.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.medily.dto.FeedbackDto;
import rs.ac.uns.ftn.medily.dto.NewMemberDto;
import rs.ac.uns.ftn.medily.dto.ProfileDto;
import rs.ac.uns.ftn.medily.dto.RegistrationDto;
import rs.ac.uns.ftn.medily.exceptions.InvalidRequestException;
import rs.ac.uns.ftn.medily.model.*;
import rs.ac.uns.ftn.medily.repository.DriveRepository;
import rs.ac.uns.ftn.medily.repository.FamilyRepository;
import rs.ac.uns.ftn.medily.repository.FeedbackRepository;
import rs.ac.uns.ftn.medily.repository.UserRepository;
import rs.ac.uns.ftn.medily.security.TokenUtils;
import rs.ac.uns.ftn.medily.service.EmailService;
import rs.ac.uns.ftn.medily.service.UserService;
import rs.ac.uns.ftn.medily.service.UtilService;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final FamilyRepository familyRepository;
    private final DriveRepository driveRepository;
    private final FeedbackRepository feedbackRepository;
    private final PasswordEncoder passwordEncoder;
    private final EmailService emailService;
    private final TokenUtils tokenUtils;
    private final UtilService utilService;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, FamilyRepository familyRepository, EmailService emailService,
                           PasswordEncoder passwordEncoder, TokenUtils tokenUtils, DriveRepository driveRepository,
                           UtilService utilService, FeedbackRepository feedbackRepository) {
        this.userRepository = userRepository;
        this.familyRepository = familyRepository;
        this.passwordEncoder = passwordEncoder;
        this.emailService = emailService;
        this.tokenUtils = tokenUtils;
        this.driveRepository = driveRepository;
        this.utilService = utilService;
        this.feedbackRepository = feedbackRepository;
    }

    @Override
    public void registration(RegistrationDto dto) {
        if (userRepository.findByUsername(dto.getUsername()).isPresent()) {
            throw new InvalidRequestException("User with provided username already exists.");
        } else if (familyRepository.findByEmail(dto.getEmail()).isPresent()) {
            throw new InvalidRequestException("User with provided email already exists.");
        } else {
            String token = UUID.randomUUID().toString();
            Family family = new Family(dto.getFamily(), token, dto.getEmail());
            family = familyRepository.save(family);
            User user = new User(dto.getUsername(), passwordEncoder.encode(dto.getPassword()), dto.getName(),
                    Role.PARENT, family);
            family.getFamilyMembers().add(user);
            userRepository.save(user);
            emailService.sendRegistrationEmail(dto.getEmail(), token);
        }
    }

    @Override
    public void activation(String token) {
        Optional<Family> family = familyRepository.findByActivationToken(token);
        if (family.isPresent()) {
            Drive drive = new Drive();
            drive = driveRepository.save(drive);
            family.get().setActive(true);
            family.get().setDrive(drive);
            drive.setFamily(family.get());
            familyRepository.save(family.get());
            try {
                utilService.initializeDrive(drive.getId().toString());
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            throw new InvalidRequestException("User with provided token not found.");
        }
    }

    @Override
    public void requestPasswordRenewal(String username) {
        Optional<User> user = userRepository.findByUsername(username);
        if (user.isPresent()) {
            emailService.sendPasswordConfirmationLink(user.get().getFamily().getEmail(), user.get().getFamily().getName(),
                    UUID.randomUUID().toString(), username, user.get().getName());
        } else {
            throw new InvalidRequestException("User with provided username does not exist.");
        }
    }

    @Override
    public void renewPassword(String username) {
        User user = findByUsername(username);
        if (user == null) {
            throw new InvalidRequestException("User with provided username does not exist.");
        } else {
            String password = UUID.randomUUID().toString().substring(0, 6);
            user.setPassword(passwordEncoder.encode(password));
            userRepository.save(user);
            emailService.sendNewPassword(user.getFamily().getEmail(), user.getFamily().getName(), password, username,
                    user.getName());
        }
    }

    @Override
    public ProfileDto getProfile(HttpServletRequest request) {
        User user = getLoggedInUser(request);
        if (user == null) {
            throw new InvalidRequestException("User not recognized, bad credentials.");
        }
        return new ProfileDto(user.getUsername(), user.getName(), user.getRole().toString(), user.getFamily().getName());
    }

    @Override
    public User getLoggedInUser(HttpServletRequest httpRequest) {
        String authToken = httpRequest.getHeader("Authorization");          // X-Auth-Token
        if(authToken != null) {
            if(authToken.length() > 7)
                authToken = authToken.substring(7);
        }
        String username = tokenUtils.getUsernameFromToken(authToken);
        if (username == null) {
            return null;
        }
        return findByUsername(username);
    }

    @Override
    public User getLoggedInUser() {
        String username = ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
        return userRepository.findByUsername(username).orElseThrow();
    }

    @Override
    public User findByUsername(String username) {
        return userRepository.findByUsername(username).orElse(null);
    }

    @Override
    public void changePassword(String password, HttpServletRequest request) {
        User user = getLoggedInUser(request);
        if (user == null) {
            throw new InvalidRequestException("User not recognized, bad credentials.");
        }

        user.setPassword(passwordEncoder.encode(password));
        userRepository.save(user);
    }

    @Override
    public List<ProfileDto> getFamily(HttpServletRequest request) {
        User user = getLoggedInUser(request);
        if (user == null) {
            throw new InvalidRequestException("User not recognized, bad credentials.");
        }
        List<ProfileDto> result = new ArrayList<>();
        for (User member : user.getFamily().getFamilyMembers()) {
            if (!member.getUsername().equals(user.getUsername())) {
                result.add(new ProfileDto(member.getUsername(), member.getName(), member.getRole().toString(),
                        user.getFamily().getName()));
            }
        }
        return result;
    }

    @Override
    public ProfileDto getProfileForUser(HttpServletRequest request, String username) {
        User user = getLoggedInUser(request);
        if (user == null) {
            throw new InvalidRequestException("User not recognized, bad credentials.");
        }
        if (!user.getRole().equals(Role.PARENT)) {
            throw new InvalidRequestException("Permission denied, only parent can perform this action.");
        }
        User member = findByUsername(username);
        if (member == null) {
            throw new InvalidRequestException("Family member not recognized, invalid input.");
        }
        if (!user.getFamily().getId().equals(member.getFamily().getId())) {
            throw new InvalidRequestException("Family member not recognized, invalid input.");
        }
        return new ProfileDto(username, member.getName(), member.getRole().toString(), member.getFamily().getName());
    }

    @Override
    public void createMember(HttpServletRequest request, NewMemberDto dto) {
        User user = getLoggedInUser(request);
        if (user == null) {
            throw new InvalidRequestException("User not recognized, bad credentials.");
        }
        if (!user.getRole().equals(Role.PARENT)) {
            throw new InvalidRequestException("Permission denied, only parent can perform this action.");
        }
        User member = new User(dto.getUsername(), passwordEncoder.encode(dto.getPassword()), dto.getName(),
                Role.valueOf(dto.getRole().toUpperCase()), user.getFamily());
        user.getFamily().getFamilyMembers().add(member);
        userRepository.save(member);
    }

    @Override
    public User save(User user) {
        return userRepository.save(user);
    }

    @Override
    public void saveFeedback(FeedbackDto dto) {
        User user = findByUsername(dto.getUsername());
        if (user == null) {
            throw new InvalidRequestException("User not recognized, bad credentials.");
        }
        feedbackRepository.save(new Feedback(dto.getFeedback(), user));
    }
}
