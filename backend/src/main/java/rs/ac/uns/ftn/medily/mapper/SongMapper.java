package rs.ac.uns.ftn.medily.mapper;

import rs.ac.uns.ftn.medily.dto.SongDto;
import rs.ac.uns.ftn.medily.model.Song;

public class SongMapper {

  public static SongDto songToSongDto(Song song){
    SongDto songDto = new SongDto();
    songDto.setId(song.getId());
    songDto.setArtist(song.getArtist());
    songDto.setDuration(song.getDuration());
    songDto.setFileId(song.getFile().getId());
    songDto.setGenre(song.getGenre());

    return songDto;
  }
}
