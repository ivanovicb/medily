package rs.ac.uns.ftn.medily.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "username", nullable = false, unique = true)
    private String username;

    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "role", nullable = false)
    @Enumerated(EnumType.STRING)
    private Role role;

    @JsonManagedReference
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "family_id", nullable = false)
    private Family family;

    @JsonBackReference
    @ManyToMany (cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "downloadedBy")
    private List<Playlist> playlists;

    @JsonBackReference
    @OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy = "user")
    private List<Playlist> createdPlaylists;

    @JsonBackReference
    @OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY)
    private List<Permission> permissions;

    public User() {}

    public User(String username, String password, String name, Role role, Family family) {
        this.username = username;
        this.password = password;
        this.name = name;
        this.role = role;
        this.family = family;
        this.playlists = new ArrayList<>();
        this.createdPlaylists = new ArrayList<>();
        this.permissions = new ArrayList<>();
    }

    public Long getId() { return id; }

    public void setId(Long id) { this.id = id; }

    public String getUsername() { return username; }

    public void setUsername(String username) { this.username = username; }

    public String getPassword() { return password; }

    public void setPassword(String password) { this.password = password; }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public Role getRole() { return role; }

    public void setRole(Role role) { this.role = role; }

    public Family getFamily() { return family; }

    public void setFamily(Family family) { this.family = family; }

    public List<Playlist> getPlaylists() { return playlists; }

    public void setPlaylists(List<Playlist> playlists) { this.playlists = playlists; }

    public List<Playlist> getCreatedPlaylists() { return createdPlaylists; }

    public void setCreatedPlaylists(List<Playlist> createdPlaylists) { this.createdPlaylists = createdPlaylists; }

    public List<Permission> getPermissions() { return permissions; }

    public void setPermissions(List<Permission> permissions) { this.permissions = permissions; }
}
