package rs.ac.uns.ftn.medily.dto_validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.medily.constants.ValidationConstants;
import rs.ac.uns.ftn.medily.dto.FeedbackDto;
import rs.ac.uns.ftn.medily.utils.BadRequestUtil;

@Service
public class FeedbackDtoValidator {

    private UtilValidator utilValidator;

    @Autowired
    public FeedbackDtoValidator(UtilValidator utilValidator) {
        this.utilValidator = utilValidator;
    }

    public void validateSendFeedback(FeedbackDto dto) {
        BadRequestUtil.throwIfInvalidRequest(dto == null, ValidationConstants.dtoShouldNotBeNull());

        utilValidator.validateString(dto.getUsername());
        utilValidator.validateString(dto.getFeedback());
    }
}
