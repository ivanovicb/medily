package rs.ac.uns.ftn.medily.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.medily.model.User;
import rs.ac.uns.ftn.medily.repository.UserRepository;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Primary
public class UserDetailsServiceImpl implements UserDetailsService {

    private final UserRepository userRepository;

    @Autowired
    public UserDetailsServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username).orElse(null);

        if (user == null) {
            throw new UsernameNotFoundException(String.format("No user found with username '%s'.", username));
        } else {
            if (!user.getFamily().isActive()) {
                throw new UsernameNotFoundException(String.format("User with username '%s' not activated.", username));
            }
            if (user.getRole() != null) {
                List<GrantedAuthority> grantedAuthorities = new ArrayList<>();

                grantedAuthorities.add(new SimpleGrantedAuthority(user.getRole().toString()));

                return new org.springframework.security.core.userdetails.User(
                        user.getUsername().trim(),
                        user.getPassword().trim(),
                        grantedAuthorities);
            } else {
                throw new UsernameNotFoundException(String.format("Account of user with username '%s' is not active.", username));
            }
        }
    }
}

