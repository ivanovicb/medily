package rs.ac.uns.ftn.medily.dto;

import lombok.Value;
import rs.ac.uns.ftn.medily.model.PlaylistChange;
import rs.ac.uns.ftn.medily.model.PlaylistChangeType;

import java.util.Date;

@Value
public class PlaylistChangeDto {
    Long id;
    Long playlistId;
    PlaylistChangeType playlistChangeType;
    Date modified;
    Long songId;

    public PlaylistChangeDto(PlaylistChange playlistChange) {
        this.id = playlistChange.getId();
        this.playlistId = playlistChange.getPlaylist().getId();
        this.playlistChangeType = playlistChange.getPlaylistChangeType();
        this.modified = playlistChange.getModified();
        if (playlistChange.getSong() != null) {
            this.songId = playlistChange.getSong().getId();
        }
        else {
            this.songId = null;
        }
    }
}
