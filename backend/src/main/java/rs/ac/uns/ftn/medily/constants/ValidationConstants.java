package rs.ac.uns.ftn.medily.constants;

public class ValidationConstants {

    public static final Integer BAD_REQUEST_DATA = 400;

    public static final String ID_PARAMETER = "Id parameter";
    public static final String EMAIL = "Email parameter";
    public static final String USERNAME = "Username parameter";
    public static final String PASSWORD = "Password parameter";
    public static final String TOKEN = "Token parameter";
    public static final String STRING_VALUE = "String value";
    public static final String STATUS = "Status";
    public static final String DATE = "Date";
    public static final String PASSWORD_REGEX = "[a-zA-Z0-9]+";
    public static final String EMAIL_REGEX = "[a-zA-Z0-9_.]+@[a-zA-Z0-9]+.[a-zA-Z]{2,3}[.] {0,1}[a-zA-Z]+";
    public static final String TOKEN_ACTIVATION_REGEX = "[a-z]+";
    public static final String URL_REGEX = "\\b(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";

    public static String dtoShouldNotBeNull() { return "Invalid data provided."; }

    public static String fieldShouldNotBeNull(String field) { return field + " field should not be null."; }

    public static String fieldShouldNotBeNegative(String field) {
        return field + " should not be a negative value!";
    }

    public static String invalidNumber(String value) { return "Provided value " + value + " is not valid."; }

    public static String fieldShouldNotBeEmptyString(String field) { return field + " field should not be empty."; }

    public static String passwordFieldContainsInvalidChars() {
        return "Password field contains invalid characters.";
    }

    public static String emailRegexCheck() { return "Email field not in valid format."; }

    public static String notValidFieldLength(String field) { return field + " field has not valid size."; }

    public static String tokenRegexCheck() { return "Token field contains invalid characters."; }

    public static String noLoggedInUser() { return "User not recognized, bad credentials."; }

    public static String invalidAction(String action) { return "Invalid action '" + action + "'."; }

    public static String invalidRoleValue(String value) { return "Invalid role value '" + value + "'."; }

    public static String invalidTypeOfPermissionObject(String value) {
        return "Invalid value of type '" + value + "'. You can set permission only for file or folder.";
    }
}
