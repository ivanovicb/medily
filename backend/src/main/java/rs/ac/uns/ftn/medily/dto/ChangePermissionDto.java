package rs.ac.uns.ftn.medily.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ChangePermissionDto {
    private String username;
    private Long id;
    private String type;
    private boolean allow;

    public ChangePermissionDto() {}

    public ChangePermissionDto(String username, Long id, String type, boolean allow) {
        this.username = username;
        this.id = id;
        this.type = type;
        this.allow = allow;
    }
}
