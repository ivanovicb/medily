package rs.ac.uns.ftn.medily.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.medily.model.Playlist;
import rs.ac.uns.ftn.medily.model.PlaylistChange;
import rs.ac.uns.ftn.medily.model.PlaylistChangeType;
import rs.ac.uns.ftn.medily.model.Song;
import rs.ac.uns.ftn.medily.model.User;
import rs.ac.uns.ftn.medily.repository.PlaylistChangeRepository;
import rs.ac.uns.ftn.medily.service.SyncService;
import rs.ac.uns.ftn.medily.service.UserService;

import java.util.Date;
import java.util.List;

@Service
public class SyncServiceImpl implements SyncService {

  private final PlaylistChangeRepository playlistChangeRepository;
  private final UserService userService;

  @Autowired
  public SyncServiceImpl(PlaylistChangeRepository playlistChangeRepository, UserService userService) {
    this.playlistChangeRepository = playlistChangeRepository;
    this.userService = userService;
  }

  @Override
  public void addPlaylistChange(Playlist playlist, PlaylistChangeType playlistChangeType) {
    PlaylistChange playlistChange = createPlaylistChange(playlist, playlistChangeType);

    playlistChangeRepository.save(playlistChange);
  }

  @Override
  public void addPlaylistChange(Playlist playlist, PlaylistChangeType playlistChangeType, Song song) {
    PlaylistChange playlistChange = createPlaylistChange(playlist, playlistChangeType);
    playlistChange.setSong(song);

    playlistChangeRepository.save(playlistChange);
  }

  @Override
  public List<PlaylistChange> getPlaylistChanges(Date lastSync) {
    User currentUser = userService.getLoggedInUser();
    System.out.println("Sync request -> " + currentUser.getName() + " " + lastSync);
    return playlistChangeRepository.getUserPlaylistChanges(lastSync, currentUser.getId());
  }

  private PlaylistChange createPlaylistChange(Playlist playlist, PlaylistChangeType playlistChangeType) {
    PlaylistChange playlistChange = new PlaylistChange();
    playlistChange.setModified(new Date());
    playlistChange.setPlaylist(playlist);
    playlistChange.setPlaylistChangeType(playlistChangeType);
    User loggedUser = userService.getLoggedInUser();
    playlistChange.setChangedBy(loggedUser);

    return playlistChange;
  }
}
