package rs.ac.uns.ftn.medily.service;

import rs.ac.uns.ftn.medily.model.Song;

public interface SongService {

    Song save(Song song);

    Song findById(Long id);

    Song findByFileId(Long id);
}
