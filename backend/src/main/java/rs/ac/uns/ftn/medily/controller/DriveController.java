package rs.ac.uns.ftn.medily.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import rs.ac.uns.ftn.medily.dto.AddFileDto;
import rs.ac.uns.ftn.medily.dto.AddFolderDto;
import rs.ac.uns.ftn.medily.dto.RenameDto;
import rs.ac.uns.ftn.medily.dto_validators.*;
import rs.ac.uns.ftn.medily.exceptions.BadRequestException;
import rs.ac.uns.ftn.medily.dto.*;
import rs.ac.uns.ftn.medily.service.DriveService;
import rs.ac.uns.ftn.medily.service.UserService;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("drive")
public class DriveController {

    private final UtilValidator utilValidator;
    private final UserService userService;
    private final DriveService driveService;
    private final ItemDtoValidator itemDtoValidator;
    private final RenameDtoValidator renameDtoValidator;
    private final AddFileDtoValidator addFileDtoValidator;
    private final AddFolderDtoValidator addFolderDtoValidator;
    private final PermissionDtoValidator permissionDtoValidator;
    private final ChangePermissionDtoValidator changePermissionDtoValidator;

    @Autowired
    public DriveController(DriveService driveService, UserService userService, RenameDtoValidator renameDtoValidator,
                           AddFolderDtoValidator addFolderDtoValidator, AddFileDtoValidator addFileDtoValidator,
                           UtilValidator utilValidator, ItemDtoValidator itemDtoValidator,
                           PermissionDtoValidator permissionDtoValidator,
                           ChangePermissionDtoValidator changePermissionDtoValidator) {
        this.userService = userService;
        this.driveService = driveService;
        this.utilValidator = utilValidator;
        this.itemDtoValidator = itemDtoValidator;
        this.renameDtoValidator = renameDtoValidator;
        this.addFileDtoValidator = addFileDtoValidator;
        this.addFolderDtoValidator = addFolderDtoValidator;
        this.permissionDtoValidator = permissionDtoValidator;
        this.changePermissionDtoValidator = changePermissionDtoValidator;
    }

    @RequestMapping(value = "/get", method = RequestMethod.GET)
    public ResponseEntity<?> getDrive(){
        try {
            return ResponseEntity.ok(driveService.getDrive());
        } catch (Exception ex){
            return ResponseEntity.badRequest().body(ex.getMessage());
        }
    }

    @RequestMapping(value = "/add/folder", method = RequestMethod.POST)
    public ResponseEntity<?> addFolder(@RequestBody AddFolderDto addFolderDto){
        try {
            addFolderDtoValidator.validateAddFolder(addFolderDto);
            return ResponseEntity.ok().body(driveService.addFolder(addFolderDto));
        } catch (Exception ex){
            return ResponseEntity.badRequest().body(ex.getMessage());
        }
    }

    @RequestMapping(value = "/add/file", method = RequestMethod.POST)
    public ResponseEntity<?> addFile(@RequestPart("file") MultipartFile file, @RequestPart("dto") AddFileDto addFileDto){
        try {
            addFileDtoValidator.validateAddFile(addFileDto);
            return ResponseEntity.ok().body(driveService.addFile(file, addFileDto));
        } catch (Exception ex){
            return ResponseEntity.badRequest().body(ex.getMessage());
        }
    }

    @RequestMapping(value = "/rename/file", method = RequestMethod.PUT)
    public ResponseEntity<?> renameFile(@RequestBody RenameDto renameDto){
        try {
            renameDtoValidator.validateRename(renameDto);
            driveService.renameFile(renameDto);
            return ResponseEntity.ok().body("File successfully renamed.");
        } catch (Exception ex){
            return ResponseEntity.badRequest().body(ex.getMessage());
        }
    }

    @RequestMapping(value = "/rename/folder", method = RequestMethod.PUT)
    public ResponseEntity<?> renameFolder(@RequestBody RenameDto renameDto){
        try {
            renameDtoValidator.validateRename(renameDto);
            driveService.renameFolder(renameDto);
            return ResponseEntity.ok().body("Folder successfully renamed.");
        } catch (Exception ex){
            return ResponseEntity.badRequest().body(ex.getMessage());
        }
    }

    @RequestMapping(value = "/delete/folder/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteFolder(@PathVariable Long id){
        try {
            utilValidator.validateLongId(id);
            driveService.deleteFolder(id);
            return ResponseEntity.ok().body("Folder successfully deleted.");
        } catch (Exception ex){
            return ResponseEntity.badRequest().body(ex.getMessage());
        }
    }

    @RequestMapping(value = "/delete/file/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteFile(@PathVariable Long id){
        try {
            utilValidator.validateLongId(id);
            driveService.deleteFile(id);
            return ResponseEntity.ok().body("File successfully deleted.");
        } catch (Exception ex){
            return ResponseEntity.badRequest().body(ex.getMessage());
        }
    }

    @RequestMapping(value = "/download/file/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> downloadFile(@PathVariable Long id) {
        try {
            utilValidator.validateLongId(id);
            Resource resource = driveService.downloadFile(id);
            return ResponseEntity.ok()
                    .contentType(MediaType.parseMediaType("application/octet-stream"))
                    .body(resource);
        } catch (Exception ex) {
            if (ex instanceof BadRequestException) {
                return ResponseEntity.badRequest().body(ex.getMessage());
            } else {
                ex.printStackTrace();
                return ResponseEntity.status(503).body("Service currently unavailable, please try again later!");
            }
        }
    }

    @RequestMapping(value = "/download/folder/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> downloadFolder(@PathVariable Long id) {
        try {
            utilValidator.validateLongId(id);
            return ResponseEntity.ok()
                    .contentType(MediaType.parseMediaType("application/octet-stream"))
                    .body(driveService.downloadFolder(id));
        } catch (Exception ex) {
            if (ex instanceof BadRequestException) {
                return ResponseEntity.badRequest().body(ex.getMessage());
            } else {
                ex.printStackTrace();
                return ResponseEntity.status(503).body("Service currently unavailable, please try again later!");
            }
        }
    }

    @RequestMapping(value = "/download/playlist/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> downloadPlaylist(@PathVariable Long id, HttpServletRequest request) {
        try {
            utilValidator.validateLongId(id);
            return ResponseEntity.ok()
                    .contentType(MediaType.parseMediaType("application/octet-stream"))
                    .body(driveService.downloadPlaylist(id, request));
        } catch (Exception ex) {
            if (ex instanceof BadRequestException) {
                return ResponseEntity.badRequest().body(ex.getMessage());
            } else {
                ex.printStackTrace();
                return ResponseEntity.status(503).body("Service currently unavailable, please try again later!");
            }
        }
    }

    @RequestMapping(value = "/get/song/{fileId}", method = RequestMethod.GET)
    public ResponseEntity<?> getSongByFileId(@PathVariable Long fileId){
        try {
            utilValidator.validateLongId(fileId);
            return ResponseEntity.ok().body(driveService.getSongByFileId(fileId));
        } catch (Exception ex){
            return ResponseEntity.badRequest().build();
        }
    }

    @RequestMapping(value = "/permissions/get", method = RequestMethod.POST)
    public ResponseEntity<?> getPermissions(@RequestBody ItemDto dto, HttpServletRequest request) {
        try {
            itemDtoValidator.validateStringValueOfItem(dto);
            return ResponseEntity.ok(driveService.getPermissions(dto.getItem(), userService.getLoggedInUser(request)));
        } catch (Exception ex){
            return ResponseEntity.badRequest().body(ex.getMessage());
        }
    }

    @RequestMapping(value = "/role", method = RequestMethod.POST)
    public ResponseEntity<?> changeRole(@RequestBody PermissionDto dto, HttpServletRequest request) {
        try {
            permissionDtoValidator.validateChangeRole(dto);
            driveService.changeRole(dto, userService.getLoggedInUser(request));
            return ResponseEntity.ok().build();
        } catch (Exception ex){
            return ResponseEntity.badRequest().body(ex.getMessage());
        }
    }

    @RequestMapping(value = "/permissions/set", method = RequestMethod.POST)
    public ResponseEntity<?> changePermission(@RequestBody ChangePermissionDto dto, HttpServletRequest request) {
        try {
            changePermissionDtoValidator.validateChangePermission(dto);
            driveService.changePermissions(dto, userService.getLoggedInUser(request));
            return ResponseEntity.ok().build();
        } catch (Exception ex){
            return ResponseEntity.badRequest().body(ex.getMessage());
        }
    }
}
