package rs.ac.uns.ftn.medily.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import rs.ac.uns.ftn.medily.model.Drive;

import java.util.Optional;

@Repository
public interface DriveRepository extends JpaRepository<Drive, Long> {

    Optional<Drive> findByFamilyId(Long familyId);
}
