package rs.ac.uns.ftn.medily.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name="drives")
public class Drive {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @JsonBackReference
    @OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY)
    private List<File> files;

    @JsonBackReference
    @OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY)
    private List<Folder> folders;

    @OneToOne
    private Family family;

    public Drive() {
        files = new ArrayList<>();
        folders = new ArrayList<>();
    }
}
