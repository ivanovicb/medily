package rs.ac.uns.ftn.medily.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.medily.dto.*;
import rs.ac.uns.ftn.medily.exceptions.InvalidRequestException;
import rs.ac.uns.ftn.medily.model.*;
import rs.ac.uns.ftn.medily.repository.PlaylistRepository;
import rs.ac.uns.ftn.medily.service.PlaylistService;
import rs.ac.uns.ftn.medily.service.SongService;
import rs.ac.uns.ftn.medily.service.SongWithinPlaylistService;
import rs.ac.uns.ftn.medily.service.SyncService;
import rs.ac.uns.ftn.medily.service.UserService;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PlaylistServiceImpl implements PlaylistService {

    private final UserService userService;
    private final PlaylistRepository playlistRepository;
    private final SongService songService;
    private final SongWithinPlaylistService songWithinPlaylistService;
    private final SyncService syncService;

    @Autowired
    public PlaylistServiceImpl(UserService userService, PlaylistRepository playlistRepository,
                               SongService songService, SyncService syncService,
                               SongWithinPlaylistService songWithinPlaylistService) {
        this.userService = userService;
        this.playlistRepository = playlistRepository;
        this.songService = songService;
        this.songWithinPlaylistService = songWithinPlaylistService;
        this.syncService = syncService;
    }

    @Override
    public PlaylistDto createPlaylist(User user, NewPlaylistDto dto) {
       /* User user = userService.getLoggedInUser(request);
        if (user == null) {
            throw new InvalidRequestException("User not recognized, bad credentials.");
        }*/
        Playlist playlist = new Playlist(dto.getName(), dto.isShare(), user); //, new ArrayList<>()
        user.getCreatedPlaylists().add(playlist);
        playlist = playlistRepository.save(playlist);
        userService.save(user);
        return new PlaylistDto(playlist.getId(), playlist.getName(), playlist.isSharePlaylist(), new ArrayList<>(),
                playlist.getUser().getUsername());
    }

    @Override
    public List<PlaylistDto> getPublicPlaylists(HttpServletRequest request) {
        User user = userService.getLoggedInUser(request);
        if (user == null) {
            throw new InvalidRequestException("User not recognized, bad credentials.");
        }
        List<PlaylistDto> result = new ArrayList<>();
        for (User member : user.getFamily().getFamilyMembers()) {
            List<Playlist> playlists = playlistRepository.findAllByUserAndSharePlaylist(member, true);
            for (Playlist playlist: playlists) {
                List<SongDto> songs = new ArrayList<>();
                List<PlaylistSong> playlistSongs = songWithinPlaylistService.findSongsWithinPlaylist(playlist);
                playlistSongs.forEach(item -> songs.add(new SongDto(item.getSong().getId(), item.getSong().getArtist(),
                        item.getSong().getGenre(), item.getSong().getDuration(), item.getSong().getFile().getId(),
                        item.getSong().getFile().getServerPath(), item.getSong().getFile().getName(), item.getPosition())));
                result.add(new PlaylistDto(playlist.getId(), playlist.getName(), playlist.isSharePlaylist(), songs,
                        playlist.getUser().getUsername()));
            }
        }
        return result;
    }

    @Override
    public void changePlaylistStatus(HttpServletRequest request, PlaylistStatusDto dto) {
        Optional<Playlist> playlist = playlistRepository.findById(dto.getId());
        if (playlist.isPresent()) {
            User user = userService.getLoggedInUser(request);
            if (user == null) {
                throw new InvalidRequestException("User not recognized, bad credentials.");
            }
            if (user.getCreatedPlaylists().contains(playlist.get())) {
                playlist.get().setSharePlaylist(dto.getStatus());
                playlistRepository.save(playlist.get());
            } else {
                throw new InvalidRequestException("Only author of playlist can modify.");
            }
        } else {
            throw new InvalidRequestException("Provided playlist not found.");
        }
    }

    @Override
    public Playlist renamePlaylist(Long id, String newName) {
        Playlist playlist = playlistRepository.findById(id).orElse(null);
        if (playlist != null) {
            playlist.setName(newName);
            playlist = playlistRepository.save(playlist);
            syncService.addPlaylistChange(playlist, PlaylistChangeType.PLAYLIST_RENAMED);
        }

        return playlist;
    }

    @Override
    public void deletePlaylist(HttpServletRequest request, Long id) {
        User user = userService.getLoggedInUser(request);
        Playlist playlist = findById(id);
        if (playlist != null && user != null) {
            if (playlist.getUser().getUsername().equals(user.getUsername())) {
                playlistRepository.deleteById(id);
            }
        }
     }

    @Override
    public Playlist findById(Long id) { return playlistRepository.findById(id).orElse(null); }

    @Override
    public boolean manageSongWithinPlaylist(ManageSongWithinPlaylistDto dto) {
        boolean actionSuccess = false;

        Playlist playlist = findById(dto.getPlaylistId());
        Song song = songService.findById(dto.getSongId());

        if (dto.getAction().equals("add") && playlist != null && song != null) {
            actionSuccess = addSongToPlaylist(playlist, song) != null;
        } else if (dto.getAction().equals("remove") && playlist != null && song != null) {
            int size = 0;
            List<PlaylistSong> songsList = songWithinPlaylistService.findSongsWithinPlaylist(playlist);
            if (songsList != null) {
                size = songsList.size();
            }
            int positionOfRemovedSong = removeSongFromPlaylist(playlist, song, dto.getPosition());
            boolean lastSong = positionOfRemovedSong == size;
            if (lastSong) {
                actionSuccess = true;
            } else {
                int updatedRows = songWithinPlaylistService.updatePositionAfterRemovedSong(positionOfRemovedSong, dto.getPlaylistId());
                actionSuccess = updatedRows > 0;
            }
        }

        return actionSuccess;
    }


    private PlaylistSong addSongToPlaylist(Playlist playlist, Song song) {
        int numberOfSongsInPlaylist = songWithinPlaylistService.countSongsWithinPlaylist(playlist);
        PlaylistSong playlistSong = new PlaylistSong(song, playlist, numberOfSongsInPlaylist + 1);
        syncService.addPlaylistChange(playlist, PlaylistChangeType.SONG_ADDED, song);
        return songWithinPlaylistService.save(playlistSong);
    }

    // returns position of removed song
    private int removeSongFromPlaylist(Playlist playlist, Song song, int position) {
        PlaylistSong playlistSong = songWithinPlaylistService.findByPlaylistAndSongAndPosition(playlist, song, position);
        if (playlistSong != null) {
            songWithinPlaylistService.deleteSongWithinPlaylist(playlistSong);
        }
        syncService.addPlaylistChange(playlist, PlaylistChangeType.SONG_DELETED, song);
        return playlistSong != null ? playlistSong.getPosition() : -1;
    }

    @Override
    public Playlist save(Playlist playlist) { return playlistRepository.save(playlist); }

    @Override
    public PlaylistDto getPlaylistById(Long playlistId) {
        Playlist playlist = playlistRepository.findById(playlistId).orElseThrow(EntityNotFoundException::new);
        List<SongDto> songs = new ArrayList<>();
        List<PlaylistSong> playlistSongs = songWithinPlaylistService.findSongsWithinPlaylist(playlist);
        playlistSongs.forEach(item -> songs.add(new SongDto(item.getSong().getId(), item.getSong().getArtist(),
            item.getSong().getGenre(), item.getSong().getDuration(), item.getSong().getFile().getId(),
            item.getSong().getFile().getServerPath(), item.getSong().getFile().getName(), item.getPosition())));
        return new PlaylistDto(playlist.getId(), playlist.getName(), playlist.isSharePlaylist(), songs,
            playlist.getUser().getUsername());
    }

}
