package rs.ac.uns.ftn.medily.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class DrivePermissionsDto {
    private Long id;
    private List<FilePermissionDto> files;
    private List<FolderPermissionDto> folders;
}
