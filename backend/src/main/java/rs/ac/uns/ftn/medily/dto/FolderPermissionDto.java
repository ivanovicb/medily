package rs.ac.uns.ftn.medily.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Getter
@Setter
public class FolderPermissionDto {
    private Long id;
    private String name;
    @JsonFormat(pattern="MMM dd, yyyy HH:mm:ss a")
    private Date created;
    private List<FolderPermissionDto> subFolders;
    private List<FilePermissionDto> files;
    private boolean allow;
}
