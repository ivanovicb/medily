package rs.ac.uns.ftn.medily.dto_validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.medily.constants.ValidationConstants;
import rs.ac.uns.ftn.medily.dto.FolderDto;
import rs.ac.uns.ftn.medily.utils.BadRequestUtil;

@Service
public class FolderDtoValidator {

    private UtilValidator utilValidator;

    @Autowired
    public FolderDtoValidator(UtilValidator utilValidator) {
        this.utilValidator = utilValidator;
    }

    public void validateFolderDtoForAddFolder(FolderDto dto) {
        BadRequestUtil.throwIfInvalidRequest(dto == null, ValidationConstants.dtoShouldNotBeNull());

        utilValidator.validateString(dto.getName());
        BadRequestUtil.throwIfInvalidRequest(dto.getCreated() == null,
                ValidationConstants.fieldShouldNotBeNull(ValidationConstants.DATE));
    }
}
