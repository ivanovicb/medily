package rs.ac.uns.ftn.medily.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import rs.ac.uns.ftn.medily.model.Folder;

@Repository
public interface FolderRepository extends JpaRepository<Folder, Long> {

}
