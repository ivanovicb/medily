package rs.ac.uns.ftn.medily.mapper;

import rs.ac.uns.ftn.medily.dto.FileDto;
import rs.ac.uns.ftn.medily.dto.FilePermissionDto;
import rs.ac.uns.ftn.medily.model.File;
import rs.ac.uns.ftn.medily.model.Permission;

import java.util.ArrayList;
import java.util.List;

public class FileMapper {

    public static FileDto fileToFileDto(File file) {
        FileDto fileDto = new FileDto();
        fileDto.setId(file.getId());
        fileDto.setName(file.getName());
        fileDto.setCreated(file.getCreated());
        fileDto.setSize(file.getSize());

        return fileDto;
    }

    public static List<FileDto> fileListToFileDtoList(List<File> files, List<Permission> permissions) {
        List<FileDto> fileDtos = new ArrayList<>();

        for(File file: files){
            if (checkFilePermission(file, permissions) && file.isActive()) {
                fileDtos.add(fileToFileDto(file));
            }
        }

        return fileDtos;
    }

    public static File fileDtoToFile(FileDto fileDto) {
        File file = new File();
        file.setId(fileDto.getId());
        file.setName(fileDto.getName());
        file.setCreated(fileDto.getCreated());
        file.setSize(fileDto.getSize());

        return file;
    }

    public static List<File> fileDtoListToFileList(List<FileDto> fileDtos) {
        List<File> files = new ArrayList<>();

        for(FileDto fileDto: fileDtos){
            files.add(fileDtoToFile(fileDto));
        }

        return files;
    }

    public static List<FilePermissionDto> filePermissionListToFilePermissionDtoList(List<File> files,
                                                                                    List<Permission> permissions) {
        List<FilePermissionDto> fileDtos = new ArrayList<>();
        for(File file: files){
            fileDtos.add(fileToFilePermissionDto(file, permissions));
        }
        return fileDtos;
    }

    private static FilePermissionDto fileToFilePermissionDto(File file, List<Permission> permissions) {
        FilePermissionDto fileDto = new FilePermissionDto();
        fileDto.setId(file.getId());
        fileDto.setName(file.getName());
        fileDto.setCreated(file.getCreated());
        fileDto.setSize(file.getSize());
        fileDto.setAllow(checkFilePermission(file, permissions));

        return fileDto;
    }

    public static boolean checkFilePermission(File file, List<Permission> permissions) {
        for (Permission permission: permissions) {
            if (permission.getFile() != null) {
                if (file.getId().equals(permission.getFile().getId())) {
                    return false;
                }
            }
        }
        return true;
    }
}
