package rs.ac.uns.ftn.medily.dto;

public class UpdateSongPositionDto {

    private Long songId;
    private Long playlistId;
    private int oldPosition;
    private int newPosition;

    public UpdateSongPositionDto() {}

    public UpdateSongPositionDto(Long songId, Long playlistId, int oldPosition, int newPosition) {
        this.songId = songId;
        this.playlistId = playlistId;
        this.newPosition = newPosition;
        this.oldPosition= oldPosition;
    }

    public Long getSongId() { return songId; }

    public void setSongId(Long songId) { this.songId = songId; }

    public Long getPlaylistId() { return playlistId; }

    public void setPlaylistId(Long playlistId) { this.playlistId = playlistId; }

    public int getNewPosition() { return newPosition; }

    public void setNewPosition(int newPosition) { this.newPosition = newPosition; }

    public int getOldPosition() { return oldPosition; }

    public void setOldPosition(int oldPosition) { this.oldPosition = oldPosition; }
}
