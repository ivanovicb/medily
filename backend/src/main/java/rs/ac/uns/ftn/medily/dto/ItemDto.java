package rs.ac.uns.ftn.medily.dto;

public class ItemDto {

    private String item;

    public ItemDto() {}

    public ItemDto(String item) {
        this.item = item;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }
}
