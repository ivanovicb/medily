package rs.ac.uns.ftn.medily.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rs.ac.uns.ftn.medily.dto.UpdateSongPositionDto;
import rs.ac.uns.ftn.medily.model.Playlist;
import rs.ac.uns.ftn.medily.model.PlaylistChangeType;
import rs.ac.uns.ftn.medily.model.PlaylistSong;
import rs.ac.uns.ftn.medily.model.Song;
import rs.ac.uns.ftn.medily.repository.PlaylistRepository;
import rs.ac.uns.ftn.medily.repository.SongWithinPlaylistRepository;
import rs.ac.uns.ftn.medily.service.SongService;
import rs.ac.uns.ftn.medily.service.SongWithinPlaylistService;
import rs.ac.uns.ftn.medily.service.SyncService;

import java.util.List;
import java.util.Optional;

@Service
public class SongWithinPlaylistServiceImpl implements SongWithinPlaylistService {

    private final SongService songService;
    private final PlaylistRepository playlistRepository;
    private final SongWithinPlaylistRepository songWithinPlaylistRepository;
    private final SyncService syncService;

    @Autowired
    public SongWithinPlaylistServiceImpl(SongService songService, PlaylistRepository playlistRepository,
                                         SongWithinPlaylistRepository songWithinPlaylistRepository, SyncService syncService) {
        this.songService = songService;
        this.playlistRepository = playlistRepository;
        this.songWithinPlaylistRepository = songWithinPlaylistRepository;
        this.syncService = syncService;
    }

    @Override
    public PlaylistSong save(PlaylistSong playlistSong) {
        return songWithinPlaylistRepository.save(playlistSong);
    }

    @Override
    public void deleteSongWithinPlaylist(PlaylistSong playlist) { songWithinPlaylistRepository.delete(playlist); }

    @Override
    public PlaylistSong findByPlaylistAndSongAndPosition(Playlist playlist, Song song, int position) {
        return songWithinPlaylistRepository.findByPlaylistAndSongAndPosition(playlist, song, position);
    }

    @Transactional
    @Override
    public int updatePositionAfterRemovedSong(int position, Long playlistId) {
        return songWithinPlaylistRepository.updatePositionsAfterRemovingSong(position, playlistId);
    }

    @Override
    public List<PlaylistSong> findSongsWithinPlaylist(Playlist playlist) {
        return songWithinPlaylistRepository.findByPlaylistOrderByPositionAsc(playlist);
    }

    @Override
    public PlaylistSong findSongsWithinPlaylistBySongId(Long playlistId, Long songId) {
        return songWithinPlaylistRepository.findByPlaylistIdAndSongId(playlistId, songId);
    }

    @Override
    public int countSongsWithinPlaylist(Playlist playlist) {
        return songWithinPlaylistRepository.countAllByPlaylist(playlist);
    }

    @Transactional
    @Override
    public boolean updateSongPositionInPlaylist(UpdateSongPositionDto dto) {
        Optional<Playlist> optionalPlaylist = playlistRepository.findById(dto.getPlaylistId());
        Playlist playlist = optionalPlaylist.orElse(null);
        Song song = songService.findById(dto.getSongId());

        int updated = 0;
        if (playlist != null && song != null) {
            PlaylistSong playlistSong = findByPlaylistAndSongAndPosition(playlist, song, dto.getOldPosition());

            if (dto.getOldPosition() > dto.getNewPosition()) {
                updated = autoUpdatePositionsWhenOldPositionGreater(dto);
            } else {
                updated = autoUpdatePositionsWhenOldPositionSmaller(dto);
            }

            if (updated > 0) {
                updatePositionOfDraggedSong(playlistSong, dto.getNewPosition());
            }
        }
        syncService.addPlaylistChange(playlist, PlaylistChangeType.SONG_POSITION);
        return updated > 0;
    }

    private void updatePositionOfDraggedSong(PlaylistSong playlistSong, int newPosition) {
        playlistSong.setPosition(newPosition);
        save(playlistSong);
    }

    private int autoUpdatePositionsWhenOldPositionGreater(UpdateSongPositionDto dto) {
       return songWithinPlaylistRepository
                .updatePositionsAfterDragWhenFromGreater(dto.getOldPosition(), dto.getNewPosition(), dto.getPlaylistId());
    }

    private int autoUpdatePositionsWhenOldPositionSmaller(UpdateSongPositionDto dto) {
        return songWithinPlaylistRepository
                .updatePositionsAfterDragWhenFromSmaller(dto.getOldPosition(), dto.getNewPosition(), dto.getPlaylistId());
    }
}
