package rs.ac.uns.ftn.medily.dto_validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.medily.constants.ValidationConstants;
import rs.ac.uns.ftn.medily.dto.PermissionDto;
import rs.ac.uns.ftn.medily.model.Role;
import rs.ac.uns.ftn.medily.utils.BadRequestUtil;

@Service
public class PermissionDtoValidator {

    private UtilValidator utilValidator;

    @Autowired
    public PermissionDtoValidator(UtilValidator utilValidator) {
        this.utilValidator = utilValidator;
    }

    public void validateChangeRole(PermissionDto dto) {
        BadRequestUtil.throwIfInvalidRequest(dto == null, ValidationConstants.dtoShouldNotBeNull());

        utilValidator.validateString(dto.getUsername());
        utilValidator.validateFamilyRoles(dto.getRole());
    }
}
