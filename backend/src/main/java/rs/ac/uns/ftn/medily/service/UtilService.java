package rs.ac.uns.ftn.medily.service;

import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.medily.dto.SongDto;
import rs.ac.uns.ftn.medily.dto.SongFileDto;
import rs.ac.uns.ftn.medily.mapper.FileMapper;
import rs.ac.uns.ftn.medily.model.PlaylistSong;

import java.io.*;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

@Service
public class UtilService {

    public String loadFileFromPath(String path) {
        try {
            File file = new ClassPathResource(path).getFile();
            return Base64.getEncoder().withoutPadding().encodeToString(Files.readAllBytes(file.toPath()));
        } catch (Exception e) {
            return null;
        }
    }

    /*
     * Method saves file on disk
     * @param First parameter represents input stream with file
     * @param Second parameter represents path of file
     * @return Path of new file
     */
    @SuppressWarnings("ResultOfMethodCallIgnored")
    public String writeToFile(InputStream inputStream, String path) throws IOException {
        String filePath = new ClassPathResource("").getFile().getAbsolutePath() + File.separator + path;
        File f = new File(filePath);
        if (f.exists()) {
            f.delete();
        }
        OutputStream outputStream = null;
        f = new File(filePath);
        f.getParentFile().mkdirs();
        f.createNewFile();
        try {
            outputStream = new FileOutputStream(new File(filePath));
            int read = 0;
            byte[] bytes = new byte[1024];      //write part of file
            while ((read = inputStream.read(bytes)) != -1) {
                outputStream.write(bytes, 0, read);
            }
            outputStream.flush();
            return path;
        }
        catch (IOException ioe) {
            ioe.printStackTrace();
            return null;
        }
        finally {
            // release resource, if any
            if (outputStream != null) {
                outputStream.close();
            }
        }
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    public void initializeDrive(String id) throws IOException {
        File drive = new File(new ClassPathResource("").getFile().getAbsolutePath() + File.separator + "drives" + File.separator + id);
        drive.mkdir();
    }

    public List<SongDto> convertToSongs(List<PlaylistSong> data) {
        List<SongDto> songDtos = new ArrayList<>();
        for (PlaylistSong item: data) {
            SongDto dto = new SongDto(item.getSong().getId(), item.getSong().getArtist(), item.getSong().getGenre(),
                    item.getSong().getDuration(), item.getSong().getFile().getId(), item.getSong().getFile().getServerPath(),
                    item.getSong().getFile().getName(), item.getPosition());
            dto.setPosition(item.getPosition());
            songDtos.add(dto);
        }
        return songDtos;
    }

    public List<SongFileDto> convertToSongsWithFiles(List<PlaylistSong> data) {
        List<SongFileDto> songDtos = new ArrayList<>();
        for (PlaylistSong item: data) {
            SongFileDto dto = new SongFileDto(item.getSong().getId(), item.getSong().getArtist(), item.getSong().getGenre(),
                    item.getSong().getDuration(), FileMapper.fileToFileDto(item.getSong().getFile()),
                    item.getSong().getFile().getName());
            dto.setPosition(item.getPosition());
            songDtos.add(dto);
        }
        return songDtos;
    }
}
