package rs.ac.uns.ftn.medily.service;

import rs.ac.uns.ftn.medily.dto.UpdateSongPositionDto;
import rs.ac.uns.ftn.medily.model.Playlist;
import rs.ac.uns.ftn.medily.model.PlaylistSong;
import rs.ac.uns.ftn.medily.model.Song;

import java.util.List;

public interface SongWithinPlaylistService {

    PlaylistSong save(PlaylistSong playlistSong);

    void deleteSongWithinPlaylist(PlaylistSong playlist);

    PlaylistSong findByPlaylistAndSongAndPosition(Playlist playlist, Song song, int position);

    int updatePositionAfterRemovedSong(int position, Long playlistId);

    List<PlaylistSong> findSongsWithinPlaylist(Playlist playlist);

    PlaylistSong findSongsWithinPlaylistBySongId(Long playlistId, Long songId);

    int countSongsWithinPlaylist(Playlist playlist);

    boolean updateSongPositionInPlaylist(UpdateSongPositionDto dto);
}
