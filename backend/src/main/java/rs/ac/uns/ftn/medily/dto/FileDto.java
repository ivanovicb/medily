package rs.ac.uns.ftn.medily.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class FileDto {
    private Long id;
    private String name;
    private double size;
    @JsonFormat(pattern="MMM dd, yyyy HH:mm:ss")
    private Date created;

    public FileDto(Long id, String name, double size, Date created) {
        this.id = id;
        this.name = name;
        this.size = size;
        this.created = created;
    }
}
