package rs.ac.uns.ftn.medily.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Getter
@Setter
public class FolderDto {
    private Long id;
    private String name;
    @JsonFormat(pattern="MMM dd, yyyy HH:mm:ss")
    private Date created;
    private List<FolderDto> subFolders;
    private List<FileDto> files;
}
