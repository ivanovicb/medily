package rs.ac.uns.ftn.medily.dto;

public class SongFileDto {

    private Long id;
    private String artist;
    private String genre;
    private int duration;
    private FileDto file;
    private String name;
    private int position;

    public SongFileDto() {
    }

    public SongFileDto(Long id, String artist, String genre, int duration, FileDto file, String name) {
        this.id = id;
        this.artist = artist;
        this.genre = genre;
        this.duration = duration;
        this.file = file;
        this.name = name;
        this.position = 0;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public FileDto getFile() {
        return file;
    }

    public void setFile(FileDto file) {
        this.file = file;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
