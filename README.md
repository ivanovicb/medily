# Medily - Media for Family

![Medily logo](android/app/src/main/res/drawable/medily.png "Medily")

**Medily** - Media for Family is an Android application whose intention is to gather family online in order to enjoy media together. It provides drive for family and allows members to share files. Parents can assign roles to other family members and modify permissions for accessing files and folders for the children.

In addition to drive features, it allows creating playlists consisting of your favourite songs and sharing them with family members. Displaying images and videos downloaded from family drive is also supported by Medily.

This application is done as a project in subject "Mobile applications programming" by team:

1. Vanja Mijatov
2. Bojana Ivanović
3. Ivan Adamov
 

Back-End application is developed using Spring Boot framework.

You can watch application **[demo](https://www.youtube.com/watch?v=64s6oJPQSto&feature=youtu.be)** on Youtube. Project documentation can be found in *other* folder.

